﻿using System;
using System.ComponentModel.DataAnnotations;
using Contracts.Domain.Base;
using DAL.App.DTO.Identity;
using Domain.Base;

namespace DAL.App.DTO
{
    public class BankCard: DomainEntity, IDomainAppUserId, IDomainAppUser<AppUser>
    {
        public string Number { get; set; } = default!;

        public DateTime ExpiryDate { get; set; }

        [StringLength(128,MinimumLength = 1)]
        public string CardOwnerName { get; set; } = default!;

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; } = default!;
    }
}