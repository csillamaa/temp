﻿using Contracts.Domain.Base;
using DAL.App.DTO.Identity;
using Domain.Base;

namespace DAL.App.DTO
{
    public class AppUserOrder: DomainEntity, IDomainAppUserId
    {
        public int OrderId { get; set; }
        // public Order.Order Order { get; set; } = default!;

        public int AppUserId { get; set; }
        // public AppUser AppUser { get; set; } = default!;
    }
}