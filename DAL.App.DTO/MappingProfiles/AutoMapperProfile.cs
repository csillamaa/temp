﻿using AutoMapper;
using DAL.App.DTO.Menu;
using DAL.App.DTO.Order;
using DAL.App.DTO.Product;
using Domain.App.Archive;

namespace DAL.App.DTO.MappingProfiles
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<DAL.App.DTO.Archive.ArchivedOrder, Domain.App.Archive.ArchivedOrder>().ReverseMap();
            CreateMap<DAL.App.DTO.Archive.ArchivedAppUserOrder, Domain.App.Archive.ArchivedAppUserOrder>().ReverseMap();
            CreateMap<DAL.App.DTO.Archive.ArchivedProductInOrder, Domain.App.Archive.ArchivedProductInOrder>().ReverseMap();
            CreateMap<DAL.App.DTO.Archive.ArchivedSubProductFromProduct, Domain.App.Archive.ArchivedSubProductFromProduct>().ReverseMap();
            
            CreateMap<DAL.App.DTO.Identity.AppUser, Domain.App.Identity.AppUser>().ReverseMap();
            CreateMap<DAL.App.DTO.Identity.AppRole, Domain.App.Identity.AppRole>().ReverseMap();
            
            CreateMap<DAL.App.DTO.AppUserFavoriteRestaurant, Domain.App.AppUserFavoriteRestaurant>().ReverseMap();
            CreateMap<DAL.App.DTO.AppUserOrder, Domain.App.AppUserOrder>().ReverseMap();
            CreateMap<DAL.App.DTO.AppUserWorkRestaurant, Domain.App.AppUserWorkRestaurant>().ReverseMap();
            CreateMap<DAL.App.DTO.BankCard, Domain.App.BankCard>().ReverseMap();
            CreateMap<DAL.App.DTO.Company, Domain.App.Company>().ReverseMap();
            CreateMap<DAL.App.DTO.Menu.Menu, Domain.App.Menu>().ReverseMap();
            CreateMap<MenuCategory, Domain.App.MenuCategory>().ReverseMap();
            CreateMap<MenuInRestaurant, Domain.App.MenuInRestaurant>().ReverseMap();
            CreateMap<Order.Order, Domain.App.Order>().ReverseMap();
            CreateMap<Product.Product, Domain.App.Product>().ReverseMap();
            CreateMap<ProductInOrder, Domain.App.ProductInOrder>().ReverseMap();
            CreateMap<ProductsSubCategory, Domain.App.ProductsSubCategory>().ReverseMap();
            CreateMap<Restaurant.Restaurant, Domain.App.Restaurant>().ReverseMap();
            CreateMap<ProductsSubCategoryProduct, Domain.App.ProductsSubCategoryProduct>().ReverseMap();
            CreateMap<SubProductFromProduct, Domain.App.SubProductFromProduct>().ReverseMap();
            CreateMap<DAL.App.DTO.Time, Domain.App.Time>().ReverseMap();
        }
    }
}