﻿using System.Linq;
using AutoMapper;
using DAL.App.DTO.Product;
using DomainApp = Domain.App;
using DALAppDTO = DAL.App.DTO;

namespace DAL.App.DTO.Mappers
{
    public class ProductsSubCategoryMapper: BaseMapper<ProductsSubCategory, DomainApp.ProductsSubCategory>
    {
        public ProductsSubCategoryMapper(IMapper mapper) : base(mapper)
        {
        }

        public new static ProductsSubCategory Map(DomainApp.ProductsSubCategory subCategory)
        {
            return new()
            {
                Id = subCategory.Id,
                Name = subCategory.Name,
                Position = subCategory.Position,
                MandatoryItemCount = subCategory.MandatoryItemCount,
                MaxItemCount = subCategory.MaxItemCount,
                SubCategoryProducts = subCategory.SubCategoryProducts?
                    .Select(ProductsSubCategoryProductMapper.Map)
                    .OrderBy(p => p.Position)
                    .ToList()
            };
        }

        public static ProductsSubCategoryData? MapToData(DomainApp.ProductsSubCategory? subCategory)
        {
            if (subCategory == null)
            {
                return null;
            }

            return new()
            {
                Id = subCategory.Id,
                Name = subCategory.Name,
                ProductId = subCategory.ProductId,
                MandatoryItemCount = subCategory.MandatoryItemCount,
                MaxItemCount = subCategory.MaxItemCount
            };
        }
        
        public static ProductsSubCategoryClient MapToDalClient(DomainApp.ProductsSubCategory subCategory)
        {

            return new()
            {
                Id = subCategory.Id,
                Name = subCategory.Name,
                MandatoryItemCount = subCategory.MandatoryItemCount,
                MaxItemCount = subCategory.MaxItemCount,
                SubCategoryProducts = subCategory.SubCategoryProducts?
                    .OrderBy(p => p.Position)
                    .Select(ProductsSubCategoryProductMapper.MapToDalClient)
                    .ToList()
            };
        }
        
        public static ProductsSubCategoryItemCount MapToDalItemCount(DomainApp.ProductsSubCategoryProduct subProduct)
        {

            return new()
            {
                Id = subProduct.ProductsSubCategory.Id,
                MandatoryItemCount = subProduct.ProductsSubCategory.MandatoryItemCount,
                MaxItemCount = subProduct.ProductsSubCategory.MaxItemCount,
            };
        }
    }
}