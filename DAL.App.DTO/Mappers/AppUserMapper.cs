﻿using System.Linq;
using AutoMapper;
using DALAppDTOIdentity = DAL.App.DTO.Identity;
using DomainAppIdentity = Domain.App.Identity;

namespace DAL.App.DTO.Mappers
{
    public class AppUserMapper:  BaseMapper<DALAppDTOIdentity.AppUser, DomainAppIdentity.AppUser>
    {
        public AppUserMapper(IMapper mapper) : base(mapper)
        {
        }

        public static DALAppDTOIdentity.AppUserIndex MapToDalIndex(DomainAppIdentity.AppUser user)
        {
            return new()
            {
                FullName = user.FirstName + " " + user.LastName,
                Id = user.Id,
                Email = user.Email,
                CompanyName = user.Company?.Name,
                WorkRestaurants = user.WorkRestaurants?.Select(AppUserWorkRestaurantMapper.MapToDalRestaurant)
            };
        }
        
        public static DALAppDTOIdentity.AppUserOnlyName MapToDalOnlyName(DomainAppIdentity.AppUser user)
        {
            return new()
            {
                FullName = user.FirstName + " " + user.LastName,
                Id = user.Id,
            };
        }
        
        public static DALAppDTOIdentity.AppUserEdit MapToDalEdit(DomainAppIdentity.AppUser user)
        {
            return new()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                // Address = user.Address,
                CompanyName = user.Company?.Name,
                CompanyId = user.CompanyId,
                LockoutEnd = user.LockoutEnd,
            };
        }
        
        public static DALAppDTOIdentity.AppUserData MapToDalData(DomainAppIdentity.AppUser user)
        {
            return new()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                // Address = user.Address,
                CompanyId = user.CompanyId,
            };
        }
        public static DomainAppIdentity.AppUser MapToDomain(DALAppDTOIdentity.AppUserData user)
        {
            return new()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                UserName = user.Email,
                PhoneNumber = user.PhoneNumber,
                // Address = user.Address,
                CompanyId = user.CompanyId,
            };
        }
        
    }
}