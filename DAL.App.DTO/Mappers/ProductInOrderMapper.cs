﻿using System.Linq;
using AutoMapper;
using DALAppDTOOrder = DAL.App.DTO.Order;

namespace DAL.App.DTO.Mappers
{
    public class ProductInOrderMapper : BaseMapper<DALAppDTOOrder.ProductInOrder, Domain.App.ProductInOrder>
    {
        public ProductInOrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static DALAppDTOOrder.ProductInOrderClient MapToDalClient(Domain.App.ProductInOrder productInOrder)
        {
            return new()
            {
                Id = productInOrder.Id,
                ProductId = productInOrder.ProductId,
                Amount = productInOrder.Amount,
                OneProductPrice = productInOrder.OneProductPrice,
                Name = productInOrder.Product?.Name ?? "",
                SubProducts = productInOrder.SubProducts?
                    .OrderBy(p => p.ProductsSubCategoryProduct?.ProductsSubCategory.Name)
                    .Select(SubProductFromProductMapper.MapToDalClient)
                    .ToList()
            };
        }
    }
}