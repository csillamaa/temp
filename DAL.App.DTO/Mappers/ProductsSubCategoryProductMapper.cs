﻿using AutoMapper;
using DAL.App.DTO.Product;
using DomainApp = Domain.App;
using DALAppDTO = DAL.App.DTO;

namespace DAL.App.DTO.Mappers
{
    public class ProductsSubCategoryProductMapper: BaseMapper<ProductsSubCategoryProduct, DomainApp.ProductsSubCategoryProduct>
    {
        public ProductsSubCategoryProductMapper(IMapper mapper) : base(mapper)
        {
        }

        public new static ProductsSubCategoryProduct Map(DomainApp.ProductsSubCategoryProduct subProduct)
        {
            return new()
            {
                Id = subProduct.Id,
                Position = subProduct.Position,
                ProductsSubCategoryId = subProduct.ProductsSubCategoryId,
                Price = subProduct.Price,
                Name = subProduct.Name,
                Description = subProduct.Description,
                IsSelectedOnDefault = subProduct.IsSelectedOnDefault
            };
        }

        public static ProductsSubCategoryProductData? MapToData(DomainApp.ProductsSubCategoryProduct? subProduct)
        {
            if (subProduct == null)
            {
                return null;
            }
            return new()
            {
                Id = subProduct.Id,
                Price = subProduct.Price,
                Name = subProduct.Name,
                Description = subProduct.Description,
                IsSelectedOnDefault = subProduct.IsSelectedOnDefault,
                ProductsSubCategoryId = subProduct.ProductsSubCategoryId
            };
        }
        
        public static ProductsSubCategoryProductClient MapToDalClient(DomainApp.ProductsSubCategoryProduct subProduct)
        {
            return new()
            {
                Id = subProduct.Id,
                Price = subProduct.Price,
                Name = subProduct.Name,
                Description = subProduct.Description,
                IsSelectedOnDefault = subProduct.IsSelectedOnDefault,
            };
        }
        
        public static ProductsSubCategoryProductOnlyPrice MapToDalOnlyPrice(DomainApp.ProductsSubCategoryProduct subProduct)
        {
            return new()
            {
                Id = subProduct.Id,
                Price = subProduct.Price,
            };
        }
    }
}