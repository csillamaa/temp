﻿using AutoMapper;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.DTO.Mappers
{
    public class AppUserFavoriteRestaurantMapper: BaseMapper<DAL.App.DTO.AppUserFavoriteRestaurant, Domain.App.AppUserFavoriteRestaurant>
    {
        public AppUserFavoriteRestaurantMapper(IMapper mapper) : base(mapper)
        {
        }
    }
}