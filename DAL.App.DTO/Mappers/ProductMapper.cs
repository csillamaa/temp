﻿using System.Linq;
using AutoMapper;
using DAL.App.DTO.Product;
using DALAppDTO = DAL.App.DTO;
using DomainApp = Domain.App;

namespace DAL.App.DTO.Mappers
{
    public class ProductMapper : BaseMapper<Product.Product, DomainApp.Product>
    {
        public ProductMapper(IMapper mapper) : base(mapper)
        {
        }

        public new static Product.Product Map(DomainApp.Product product)
        {
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Price = product.Price,
                Picture = product.Picture,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                Position = product.Position
            };
        }

        public static ProductEdit? MapToEdit(DomainApp.Product? product)
        {
            if (product == null)
            {
                return null;
            }
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Picture = product.Picture,
                Price = product.Price,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                ProductsSubCategories = product.ProductsSubCategories?
                    .Select(ProductsSubCategoryMapper.Map)
                    .OrderBy(c => c.Position)
                    .ToList()
            };
        }
        
        public static Product.ProductData MapToDalData(DomainApp.Product product)
        {
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Price = product.Price,
                Picture = product.Picture,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
            };
        }
        
        public static ProductClient? MapToDalClient(DomainApp.Product? product)
        {
            if (product == null)
            {
                return null;
            }
            return new()
            {
                Id = product.Id,
                Name = product.Name,
                Price = product.Price,
                Picture = product.Picture,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                ProductsSubCategories = product.ProductsSubCategories?
                    .OrderBy(c => c.Position)
                    .Select(ProductsSubCategoryMapper.MapToDalClient)
                    .ToList()
            };
            
        }

        public static ProductOnlyPrice MapToDalOnlyPrice(DomainApp.Product product)
        {

            return new()
            {
                Id = product.Id,
                Price = product.Price,
            };
        }

    }
}