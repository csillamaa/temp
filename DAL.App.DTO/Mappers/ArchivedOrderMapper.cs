﻿using System.Linq;
using AutoMapper;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;

namespace DAL.App.DTO.Mappers
{
    public class ArchivedOrderMapper : BaseMapper<DALAppDTOOrderArchived.ArchivedOrder, Domain.App.Archive.ArchivedOrder>
    {
        public ArchivedOrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static DALAppDTOOrderArchived.ArchivedOrderClient? MapToDalOrderClient(Domain.App.Archive.ArchivedOrder? order)
        {
            if (order == null)
            {
                return null;
            }

            return new()
            {
                Id = order.Id,
                SubmitTime = order.SubmitTime,
                CompletedBy = order.ActualCompleteTime,
                RestaurantId = order.RestaurantId,
                RestaurantName = order.Restaurant.Name,
                CustomerComment = order.CustomerComment,
                OrderRating = order.OrderRating,
                CustomerOrderReviewComment = order.CustomerOrderReviewComment,
                IsSelfPickup = order.IsSelfPickup,
                AmountPaid = order.AmountPaid,
                FoodCost = order.FoodCost,
                ProductsInOrder = order.ArchivedProductsInOrder.Select(ArchivedProductInOrderMapper.MapToDalClient).ToList()
            };
        }
        
        public static DALAppDTOOrderArchived.ArchivedOrderWorker MapToDalOrderWorker(Domain.App.Archive.ArchivedOrder order)
        {
           return new()
            {
                Id = order.Id,
                SubmitTime = order.SubmitTime,
                ActualCompleteTime = order.ActualCompleteTime,
                EstimatedCompleteTime = order.EstimatedCompleteTime,
                OrderHandedOver = order.OrderHandedOver,
                CustomerComment = order.CustomerComment,
                Comment = order.Comment,
                OrderRating = order.OrderRating,
                CustomerOrderReviewComment = order.CustomerOrderReviewComment,
                AmountPaid = order.AmountPaid,
                FoodCost = order.FoodCost,
                ProductsInOrder = order.ArchivedProductsInOrder.Select(ArchivedProductInOrderMapper.MapToDalClient).ToList(),
                OrderAppUsers = order.AppUsers.Select(ArchivedAppUserOrderMapper.MapToDalOnlyName).ToList()
            };
        }
    }
}