﻿using System.Xml.Serialization;
using AutoMapper;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.DTO.Mappers
{
    public class AppUserOrderMapper: BaseMapper<DAL.App.DTO.AppUserOrder, Domain.App.AppUserOrder>
    {
        public AppUserOrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static DAL.App.DTO.Identity.AppUserOnlyName MapToDalOnlyName(Domain.App.AppUserOrder appUserOrder)
        {
            return new()
            {
                Id = appUserOrder.AppUserId,
                FullName = appUserOrder.AppUser.FirstName + " " + appUserOrder.AppUser.LastName
            };
        }
    }
}