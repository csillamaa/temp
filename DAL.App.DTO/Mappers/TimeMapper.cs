﻿using System;
using AutoMapper;

namespace DAL.App.DTO.Mappers
{
    public class TimeMapper: BaseMapper<DAL.App.DTO.Time, Domain.App.Time>
    {
        public TimeMapper(IMapper mapper) : base(mapper)
        {
        }

        public new static DAL.App.DTO.Time? Map(Domain.App.Time? time)
        {
            if (time == null)
            {
                return null;
            }
            return new()
            {
                DayOfWeek = time.DayOfWeek,
                EndingTime = time.EndingTime,
                StartingTime = time.StartingTime
            };
        }
    }
}