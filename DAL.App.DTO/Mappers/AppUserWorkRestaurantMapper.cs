﻿using AutoMapper;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.DTO.Mappers
{
    public class AppUserWorkRestaurantMapper :  BaseMapper<DAL.App.DTO.AppUserWorkRestaurant, Domain.App.AppUserWorkRestaurant>,
        IBaseMapper<DAL.App.DTO.AppUserWorkRestaurant, Domain.App.AppUserWorkRestaurant>
    {
        public AppUserWorkRestaurantMapper(IMapper mapper) : base(mapper)
        {
        }
        public static DAL.App.DTO.Restaurant.RestaurantOnlyName MapToDalRestaurant(Domain.App.AppUserWorkRestaurant restaurant)
        {
            return new()
            {
                Id = restaurant.RestaurantId,
                Name = restaurant.Restaurant.Name,
                IsPublic = restaurant.Restaurant.IsPublic
            };
        }
        
        public static DAL.App.DTO.Identity.AppUserOnlyName MapToDalAppUser(Domain.App.AppUserWorkRestaurant restaurant)
        {
            return new()
            {
                Id = restaurant.AppUserId,
                FullName = restaurant.AppUser.FirstName + " " + restaurant.AppUser.LastName
            };
        }
    }
}