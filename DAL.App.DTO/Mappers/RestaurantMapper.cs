﻿using System;
using System.Linq;
using AutoMapper;
using Contracts.DAL.Base.Mappers;
using DAL.App.DTO.Identity;
using DomainAppRestaurant = Domain.App.Restaurant;
using DALAppDTORestaurant = DAL.App.DTO.Restaurant;

namespace DAL.App.DTO.Mappers
{
    public class RestaurantMapper: BaseMapper<DALAppDTORestaurant.Restaurant, DomainAppRestaurant>
    {
        public RestaurantMapper(IMapper mapper) : base(mapper)
        {
        }

        public static DALAppDTORestaurant.RestaurantEdit MapToDal(DomainAppRestaurant restaurant)
        {
            return new ()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                CompanyName = restaurant.Company.Name,
                CompanyId = restaurant.CompanyId,
                Address = restaurant.Address,
                DateJoined = restaurant.DateJoined,
                PhoneNumber = restaurant.PhoneNumber,
                Picture = restaurant.Picture,
                Description = restaurant.Description,
                Email = restaurant.Email,
                IsPublic = restaurant.IsPublic,
                Workers = restaurant.Workers?.Select(AppUserWorkRestaurantMapper.MapToDalAppUser).ToList(),
                OpeningTimes = restaurant.OpeningTimes?.Select(t => TimeMapper.Map(t)!).ToList(),
                RestaurantMenus = restaurant.Menus?.Select(m => MenuMapper.MapToOnlyName(m.Menu)).ToList()
            };
        }
        
        public static DomainAppRestaurant MapToDomain(DALAppDTORestaurant.RestaurantEdit restaurant)
        {
            return new ()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                CompanyId = restaurant.CompanyId,
                Address = restaurant.Address,
                DateJoined = restaurant.DateJoined,
                PhoneNumber = restaurant.PhoneNumber,
                Picture = restaurant.Picture,
                Description = restaurant.Description,
                Email = restaurant.Email,
                IsPublic = restaurant.IsPublic,
            };
        }
        
        public static DALAppDTORestaurant.RestaurantIndex MapToDalIndex(DomainAppRestaurant restaurant)
        {
            return new ()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                CompanyName = restaurant.Company.Name,
                Address = restaurant.Address,
                DateJoined = restaurant.DateJoined,
                IsPublic = restaurant.IsPublic,
                WorkerCount = restaurant.Workers?.Count ?? 0
            };
        }
        
        public static DALAppDTORestaurant.RestaurantOnlyName MapToDalJustName(DomainAppRestaurant restaurant)
        {
            return new ()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                IsPublic = restaurant.IsPublic,
            };
        }
        
        // new---
        public static DALAppDTORestaurant.RestaurantClientAll MapToDalClientAll(DomainAppRestaurant restaurant)
        {
            var currentDayOfWeek = (int) DateTime.Now.DayOfWeek;
            var dalRestaurant = new DALAppDTORestaurant.RestaurantClientAll
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                Picture = restaurant.Picture,
                OpeningTimes = restaurant.OpeningTimes?
                    .Select(TimeMapper.Map)
                    .FirstOrDefault(t => t?.DayOfWeek.Equals(currentDayOfWeek) ?? false)
            };
            return dalRestaurant;
        }

        public override DALAppDTORestaurant.Restaurant? Map(DomainAppRestaurant? restaurant)
        {
            if (restaurant == null)
            {
                return null;
            }

            return new()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                CompanyId = restaurant.CompanyId,
                Address = restaurant.Address,
                DateJoined = restaurant.DateJoined,
                PhoneNumber = restaurant.PhoneNumber,
                Picture = restaurant.Picture,
                Description = restaurant.Description,
                Email = restaurant.Email,
                IsPublic = restaurant.IsPublic,
                Workers = restaurant.Workers?.Select(AppUserWorkRestaurantMapper.MapToDalAppUser).ToList(),
                OpeningTimes = restaurant.OpeningTimes?.Select(t => TimeMapper.Map(t)!).ToList(),
            };
        }
        
        public static DALAppDTORestaurant.RestaurantClient? MapToDalClient(DomainAppRestaurant? restaurant)
        {
            if (restaurant == null)
            {
                return null;
            }

            return new()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                Address = restaurant.Address,
                PhoneNumber = restaurant.PhoneNumber,
                Picture = restaurant.Picture,
                Description = restaurant.Description,
                Email = restaurant.Email,
                OpeningTimes = restaurant.OpeningTimes?.Select(t => TimeMapper.Map(t)!).ToList(),
            };
        }
    }
}