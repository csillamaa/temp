﻿using AutoMapper;
using DALAppDTOOrder = DAL.App.DTO.Order;

namespace DAL.App.DTO.Mappers
{
    public class SubProductFromProductMapper : BaseMapper<DALAppDTOOrder.SubProductFromProduct, Domain.App.SubProductFromProduct>
    {
        public SubProductFromProductMapper(IMapper mapper) : base(mapper)
        {
        }

        public static DALAppDTOOrder.SubProductFromProductClient MapToDalClient(
            Domain.App.SubProductFromProduct subProduct)
        {
            return new()
            {
                Name = subProduct.ProductsSubCategoryProduct?.Name ?? "",
                Price = subProduct.ProductsSubCategoryProduct?.Price ?? 0,
                SubCategoryName = subProduct.ProductsSubCategoryProduct?.ProductsSubCategory.Name ?? ""
            };
        }
    }
}