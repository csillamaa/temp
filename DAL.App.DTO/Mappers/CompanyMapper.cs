﻿using System.Linq;
using AutoMapper;
using DomainApp = Domain.App;
using DALAppDTO = DAL.App.DTO;

namespace DAL.App.DTO.Mappers
{
    public class CompanyMapper: BaseMapper<DALAppDTO.Company, DomainApp.Company>
    {
        public CompanyMapper(IMapper mapper) : base(mapper)
        {
        }

        public static DALAppDTO.CompanyOnlyName MapToDalJustName(DomainApp.Company company)
        {
            return new()
            {
                Id = company.Id,
                Name = company.Name
            };
        }
        
        public override DALAppDTO.Company Map(DomainApp.Company? company)
        {
            return new()
            {
                Id = company!.Id,
                Name = company.Name,
                DateJoined = company.DateJoined,
                Website = company.Website,
                PhoneNumber = company.PhoneNumber,
                Email = company.Email,
                Restaurants = company.Restaurants?.Select(RestaurantMapper.MapToDalJustName).ToList(),
                CompanyUsers = company.CompanyUsers?.Select(AppUserMapper.MapToDalOnlyName).ToList()
            };
        }
        
        
    }
}