﻿using System.Linq;
using AutoMapper;
using DALAppDTOMenu = DAL.App.DTO.Menu;
using DomainAppMenuCategory = Domain.App.MenuCategory;

namespace DAL.App.DTO.Mappers
{
    public class MenuCategoryMapper: BaseMapper<DALAppDTOMenu.MenuCategory, DomainAppMenuCategory>
    {
        public MenuCategoryMapper(IMapper mapper) : base(mapper)
        {
        }

        public new static DALAppDTOMenu.MenuCategory Map(DomainAppMenuCategory category)
        {
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                Position = category.Position,
                Products = category.Products?.OrderBy(p => p.Position).Select(ProductMapper.MapToDalData).ToList()
            };
        }
        
        public static DALAppDTOMenu.MenuCategoryOnlyName MapToOnlyName(DomainAppMenuCategory category)
        {
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                Position = category.Position
            };
        }
        
        public static DomainAppMenuCategory Map(DALAppDTOMenu.MenuCategoryData category)
        {
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                MenuId = category.MenuId
            };
        }
        
        public static DALAppDTOMenu.MenuCategoryData?  MapToDalData(DomainAppMenuCategory? category)
        {
            if (category == null)
            {
                return null;
            }
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                MenuId = category.MenuId
            };
        }
        
        public static DALAppDTOMenu.MenuCategory MapToRegDal(DALAppDTOMenu.MenuCategoryData category)
        {
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                MenuId = category.MenuId
            };
        }
    }
}