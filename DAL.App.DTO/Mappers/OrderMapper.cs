﻿using System.Linq;
using AutoMapper;
using DALAppDTOOrder = DAL.App.DTO.Order;

namespace DAL.App.DTO.Mappers
{
    public class OrderMapper : BaseMapper<DALAppDTOOrder.Order, Domain.App.Order>
    {
        public OrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static DALAppDTOOrder.OrderClient? MapToDalOrderClient(Domain.App.Order? order)
        {
            if (order == null)
            {
                return null;
            }

            return new()
            {
                Id = order.Id,
                SubmitTime = order.SubmitTime,
                CompletedBy = order.CompletedBy,
                RestaurantId = order.RestaurantId,
                RestaurantName = order.Restaurant.Name,
                CustomerComment = order.CustomerComment,
                IsSelfPickup = order.IsSelfPickup,
                IsReady = order.IsReady,
                AmountPaid = order.AmountPaid,
                FoodCost = order.FoodCost,
                ProductsInOrder = order.ProductsInOrder.Select(ProductInOrderMapper.MapToDalClient).ToList()
            };
        }
        
        public static DALAppDTOOrder.OrderWorker MapToDalOrderWorker(Domain.App.Order order)
        {
           return new()
            {
                Id = order.Id,
                SubmitTime = order.SubmitTime,
                CompletedBy = order.CompletedBy,
                InitialCompleteTimeEstimate = order.InitialCompleteTimeEstimate,
                CustomerComment = order.CustomerComment,
                AmountPaid = order.AmountPaid,
                FoodCost = order.FoodCost,
                // IsReady = order.IsReady,
                ProductsInOrder = order.ProductsInOrder.Select(ProductInOrderMapper.MapToDalClient).ToList(),
                OrderAppUsers = order.OrderAppUsers.Select(AppUserOrderMapper.MapToDalOnlyName).ToList()
            };
        }
    }
}