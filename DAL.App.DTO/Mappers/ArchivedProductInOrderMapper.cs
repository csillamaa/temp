﻿using System.Linq;
using AutoMapper;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;

namespace DAL.App.DTO.Mappers
{
    public class ArchivedProductInOrderMapper : BaseMapper<DALAppDTOOrderArchived.ArchivedProductInOrder, Domain.App.Archive.ArchivedProductInOrder>
    {
        public ArchivedProductInOrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static DALAppDTOOrderArchived.ArchivedProductInOrderClient MapToDalClient(Domain.App.Archive.ArchivedProductInOrder productInOrder)
        {
            return new()
            {
                Id = productInOrder.Id,
                ProductId = productInOrder.ProductId,
                Amount = productInOrder.Amount,
                OneProductPrice = productInOrder.OneProductPrice,
                Name = productInOrder.Product?.Name ?? "",
                SubProducts = productInOrder.ArchivedSubProducts?
                    .OrderBy(p => p.ProductsSubCategoryProduct?.ProductsSubCategory.Name)
                    .Select(ArchivedSubProductFromProductMapper.MapToDalClient)
                    .ToList()
            };
        }
    }
}