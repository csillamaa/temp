﻿using System.Linq;
using AutoMapper;
using DAL.App.DTO.Restaurant;
using DALAppDTOMenu = DAL.App.DTO.Menu;
using DomainAppMenu = Domain.App.Menu;

namespace DAL.App.DTO.Mappers
{
    public class MenuMapper : BaseMapper<DALAppDTOMenu.Menu, DomainAppMenu>
    {
        public MenuMapper(IMapper mapper) : base(mapper)
        {
        }

        public static DALAppDTOMenu.MenuIndex MapToDalIndex(DomainAppMenu menu)
        {
            return new()
            {
                Id = menu.Id,
                CompanyName = menu.Company.Name,
                Name = menu.Name,
                RestaurantsWithTheMenu = menu.RestaurantsWithTheMenu?.Select(r => new RestaurantOnlyName
                {
                    Id = r.Id,
                    Name = r.Restaurant.Name,
                    IsPublic = r.Restaurant.IsPublic
                }).ToList()
            };
        }
        
        public static DALAppDTOMenu.MenuEdit MapToDalEdit(DomainAppMenu menu)
        {
            return new()
            {
                Id = menu.Id,
                CompanyName = menu.Company.Name,
                Name = menu.Name,
                CompanyId = menu.CompanyId,
                MenuCategories = menu.MenuCategories?.Select(MenuCategoryMapper.MapToOnlyName).ToList()
            };
        }
        
        public static DomainAppMenu Map(DALAppDTOMenu.MenuData menu)
        {
            return new()
            {
                Id = menu.Id,
                Name = menu.Name,
                CompanyId = menu.CompanyId
            };
        }
        
        public static DALAppDTOMenu.MenuOnlyName MapToOnlyName(DomainAppMenu menu)
        {
            return new()
            {
                Id = menu.Id,
                Name = menu.Name,
            };
        }
    }
}