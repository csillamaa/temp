﻿using System.Linq;
using AutoMapper;
using DAL.App.DTO.Restaurant;
using DALAppDTOMenu = DAL.App.DTO.Menu;
using DomainAppMenuInRestaurant = Domain.App.MenuInRestaurant;

namespace DAL.App.DTO.Mappers
{
    public class MenuInRestaurantMapper : BaseMapper<DALAppDTOMenu.MenuInRestaurant, DomainAppMenuInRestaurant>
    {
        public MenuInRestaurantMapper(IMapper mapper) : base(mapper)
        {
        }
        
    }
}