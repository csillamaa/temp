﻿using System.Xml.Serialization;
using AutoMapper;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.DTO.Mappers
{
    public class ArchivedAppUserOrderMapper: BaseMapper<DAL.App.DTO.Archive.ArchivedAppUserOrder, Domain.App.Archive.ArchivedAppUserOrder>
    {
        public ArchivedAppUserOrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static DAL.App.DTO.Identity.AppUserOnlyName MapToDalOnlyName(Domain.App.Archive.ArchivedAppUserOrder appUserOrder)
        {
            return new()
            {
                Id = appUserOrder.AppUserId,
                FullName = appUserOrder.AppUser.FirstName + " " + appUserOrder.AppUser.LastName
            };
        }
    }
}