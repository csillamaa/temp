﻿using AutoMapper;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;

namespace DAL.App.DTO.Mappers
{
    public class ArchivedSubProductFromProductMapper : BaseMapper<DALAppDTOOrderArchived.ArchivedSubProductFromProduct, Domain.App.Archive.ArchivedSubProductFromProduct>
    {
        public ArchivedSubProductFromProductMapper(IMapper mapper) : base(mapper)
        {
        }

        public static DALAppDTOOrderArchived.ArchivedSubProductFromProductClient MapToDalClient(
            Domain.App.Archive.ArchivedSubProductFromProduct subProduct)
        {
            return new()
            {
                Name = subProduct.ProductsSubCategoryProduct?.Name ?? "",
                Price = subProduct.ProductsSubCategoryProduct?.Price ?? 0,
                SubCategoryName = subProduct.ProductsSubCategoryProduct?.ProductsSubCategory.Name ?? ""
            };
        }
    }
}