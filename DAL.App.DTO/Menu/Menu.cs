﻿using System;
using System.Collections.Generic;

namespace DAL.App.DTO.Menu
{
    public class Menu : MenuOnlyName
    {
        public bool IsTemporary { get; set; }
        
        public DateTime? TemporaryMenuStart { get; set; }
        
        public DateTime? TemporaryMenuEnd { get; set; }
        
        public int CompanyId { get; set; }
        
        // public Company Company { get; set; } = default!;
        
        public List<MenuCategoryOnlyName>? MenuCategories { get; set; }

        public MenuCategory? CurrentCategory { get; set; }

        // public ICollection<MenuInRestaurant>? RestaurantsWithTheMenu { get; set; }
    }
}