﻿using Domain.Base;

namespace DAL.App.DTO.Menu
{
    public class MenuOnlyName : DomainEntity
    {
        public string Name { get; set; } = default!;
    }
}