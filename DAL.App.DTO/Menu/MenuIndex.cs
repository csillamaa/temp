﻿using System.Collections.Generic;
using DAL.App.DTO.Restaurant;

namespace DAL.App.DTO.Menu
{
    public class MenuIndex : MenuOnlyName
    {
        public string CompanyName { get; set; } = default!;

        public List<RestaurantOnlyName>? RestaurantsWithTheMenu { get; set; }
    }
}