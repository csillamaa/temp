﻿using Domain.Base;

namespace DAL.App.DTO.Menu
{
    public class MenuInRestaurant : DomainEntity
    {
        public int RestaurantId { get; set; }

        public int MenuId { get; set; }
    }
}