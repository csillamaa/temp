﻿using Domain.Base;

namespace DAL.App.DTO.Menu
{
    public class MenuCategoryData : DomainEntity
    {
        public string Name { get; set; } = default!;

        public int MenuId { get; set; }
    }
}