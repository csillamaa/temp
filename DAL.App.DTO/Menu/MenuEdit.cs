﻿using System.Collections.Generic;

namespace DAL.App.DTO.Menu
{
    public class MenuEdit : MenuData
    {
        public string CompanyName { get; set; } = default!;
        
        public List<MenuCategoryOnlyName>? MenuCategories { get; set; }

        public MenuCategory? CurrentCategory { get; set; }
    }
}