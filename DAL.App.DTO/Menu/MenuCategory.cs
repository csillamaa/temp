﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace DAL.App.DTO.Menu
{
    public class MenuCategory : DomainEntity
    {
        public int MenuId { get; set; }
        // public DTO.Menu.Menu Menu { get; set; } = default!;

        [MaxLength(35)]
        public string Name { get; set; } = default!;

        public int Position { get; set; }

        public ICollection<Product.ProductData>? Products { get; set; }
    }
}