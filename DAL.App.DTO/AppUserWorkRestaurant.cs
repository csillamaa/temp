﻿using Contracts.Domain.Base;
using DAL.App.DTO.Identity;
using Domain.Base;

namespace DAL.App.DTO
{
    public class AppUserWorkRestaurant: DomainEntity, IDomainAppUserId
    {
        public int RestaurantId { get; set; }

        public int AppUserId { get; set; }
    }
}