﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.App.DTO.Identity;
using DAL.App.DTO.Restaurant;
using Domain.Base;

namespace DAL.App.DTO
{
    public class Company : CompanyOnlyName
    {
        public DateTime DateJoined { get; set; }
        
        [MaxLength(60)]
        public string? Website { get; set; }

        [MaxLength(20)]
        public string PhoneNumber { get; set; } = default!;

        [MaxLength(60)]
        public string Email { get; set; } = default!;
        
        public ICollection<RestaurantOnlyName>? Restaurants { get; set; }
        public ICollection<AppUserOnlyName>? CompanyUsers { get; set; }
        // public ICollection<Menu>? Menus { get; set; }
        
    }
}