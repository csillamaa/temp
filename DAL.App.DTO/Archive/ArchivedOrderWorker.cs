﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.App.DTO.Identity;
using DAL.App.DTO.Order;
using Domain.Base;

namespace DAL.App.DTO.Archive
{
    public class ArchivedOrderWorker : DomainEntity
    {
        public DateTime SubmitTime { get; set; }

        public DateTime EstimatedCompleteTime { get; set; }
        public DateTime ActualCompleteTime { get; set; }

        public DateTime OrderHandedOver { get; set; }

        // public bool IsProcessed { get; set; }
        // public bool IsSelfPickup { get; set; }

        // public double DeliveryCost { get; set; }

        public double FoodCost { get; set; }
        
        // public bool IsReady { get; set; }

        public double AmountPaid { get; set; }
        
        [MaxLength(500)]
        public string? CustomerComment { get; set; }
        
        public int? OrderRating { get; set; }
        [MaxLength(350)]
        public string? CustomerOrderReviewComment { get; set; } = default!;
        
        [MaxLength(250)]
        public string? Comment { get; set; } = default!;

        // public int RestaurantId { get; set; }
        // public Restaurant.Restaurant Restaurant { get; set; } = default!;

        public ICollection<ArchivedProductInOrderClient> ProductsInOrder { get; set; } = default!;
        public ICollection<AppUserOnlyName> OrderAppUsers { get; set; } = default!;
    }
}