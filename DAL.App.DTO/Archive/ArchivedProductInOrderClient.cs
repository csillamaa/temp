﻿using System.Collections.Generic;
using DAL.App.DTO.Order;
using Domain.Base;

namespace DAL.App.DTO.Archive
{
    public class ArchivedProductInOrderClient: DomainEntity
    {
        public int Amount { get; set; }

        public string Name { get; set; } = default!;
        
        public double OneProductPrice { get; set; }

        public int? ProductId { get; set; }
        // public Product.Product? Product { get; set; }

        // public int OrderId { get; set; }
        // public DTO.Order.Order Order { get; set; } = default!;
        public ICollection<ArchivedSubProductFromProductClient>? SubProducts { get; set; }
    }
}