﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.App.DTO.Identity;
using Domain.Base;

namespace DAL.App.DTO
{
    public class CompanyOnlyName : DomainEntity
    {
        [MaxLength(60)]
        public string Name { get; set; } = default!;
    }
}