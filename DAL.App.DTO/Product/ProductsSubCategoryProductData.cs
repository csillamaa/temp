﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace DAL.App.DTO.Product
{
    public class ProductsSubCategoryProductData: DomainEntity
    {
        public int ProductsSubCategoryId { get; set; }
        public int ProductId { get; set; }
        
        public string Name { get; set; } = default!;
        
        public double Price { get; set; }
        
        public bool IsSelectedOnDefault { get; set; }
        
        public string? Description { get; set; }

    }
}