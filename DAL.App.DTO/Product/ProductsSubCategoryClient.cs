﻿using System.Collections.Generic;

using Domain.Base;

namespace DAL.App.DTO.Product
{
    public class ProductsSubCategoryClient: DomainEntity
    {
        public string Name { get; set; } = default!;
       
        public int MandatoryItemCount { get; set; }
        
        public int MaxItemCount { get; set; }
        
        public List<ProductsSubCategoryProductClient>? SubCategoryProducts { get; set; }
    }
}