﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace DAL.App.DTO.Product
{
    public class ProductsSubCategoryProductOnlyPrice : DomainEntity
    {
        public double Price { get; set; }
    }
}