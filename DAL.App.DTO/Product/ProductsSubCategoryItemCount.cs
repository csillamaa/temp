﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace DAL.App.DTO.Product
{
    public class ProductsSubCategoryItemCount : DomainEntity
    {
        public int MandatoryItemCount { get; set; }
        
        public int MaxItemCount { get; set; }
    }
}