﻿using Domain.Base;

namespace DAL.App.DTO.Product
{
    public class ProductsSubCategoryData: DomainEntity
    {
        public int ProductId { get; set; }
        
        public string Name { get; set; } = default!;
        
        public int MandatoryItemCount { get; set; }
        public int MaxItemCount { get; set; }
        
    }
}