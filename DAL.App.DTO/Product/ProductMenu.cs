﻿namespace DAL.App.DTO.Product
{
    public class ProductMenu: ProductData
    {
        public int Position { get; set; }
    }
}