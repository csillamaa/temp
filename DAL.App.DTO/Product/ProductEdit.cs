﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.App.DTO.Menu;
using Domain.Base;

namespace DAL.App.DTO.Product
{
    public class ProductEdit : ProductData
    {
        // [MaxLength(60)]
        // public string Name { get; set; } = default!;
        //
        // public double Price { get; set; }
        //
        // [MaxLength(500)]
        // public string Description { get; set; } = default!;
        //
        // public bool IsAlwaysPurchasable { get; set; }
        //
        // [MaxLength(60)]
        // public string? Picture { get; set; }
        //
        // public int Position { get; set; }
        //
        // public int MenuCategoryId { get; set; }
        
        
        // public List<MenuCategoryOnlyName> MenuCategories { get; set; } = default!;

        public ICollection<ProductsSubCategory>? ProductsSubCategories { get; set; }

        // public ICollection<Time>? SellingTimes { get; set; }
    }
}