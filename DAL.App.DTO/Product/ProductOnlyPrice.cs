﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.App.DTO.Menu;
using Domain.Base;

namespace DAL.App.DTO.Product
{
    public class ProductOnlyPrice : DomainEntity
    {
        public double Price { get; set; }
      
    }
}