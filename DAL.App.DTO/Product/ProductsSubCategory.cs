﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace DAL.App.DTO.Product
{
    public class ProductsSubCategory : ProductsSubCategoryData
    {
        // public DTO.Product.Product Product{ get; set; } = default!;
        
        public int Position { get; set; }
        public ICollection<ProductsSubCategoryProduct>? SubCategoryProducts { get; set; }
    }
}