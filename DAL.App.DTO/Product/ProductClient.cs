﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace DAL.App.DTO.Product
{
    public class ProductClient : DomainEntity
    {
        public string Name { get; set; } = default!;
        
        public double Price { get; set; }
        
        public string Description { get; set; } = default!;
        
        public bool IsAlwaysPurchasable { get; set; }
        
        [MaxLength(60)]
        public string? Picture { get; set; }

        public ICollection<ProductsSubCategoryClient>? ProductsSubCategories { get; set; }
    }
}