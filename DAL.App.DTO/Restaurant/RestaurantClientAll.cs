﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.App.DTO.Archive;
using DAL.App.DTO.Menu;
using Domain.Base;

namespace DAL.App.DTO.Restaurant
{
    public class RestaurantClientAll : DomainEntity
    {
        [MaxLength(60)]
        public string Name { get; set; } = default!;
        
        [MaxLength(60)]
        public string? Picture { get; set; } 
        
        // public bool IsPublic { get; set; }
        //
        // public DateTime DateJoined { get; set; }
        //
        // [MaxLength(700)]
        // public string Description { get; set; } = default!;
        //
        // public int CompanyId { get; set; }
        // public Company Company { get; set; } = default!;

        // Maybe need later on
        // [MaxLength(60)]
        // public string Address { get; set; } = default!;
        //
        // [MaxLength(20)]
        // public string PhoneNumber { get; set; } = default!;
        //
        // [MaxLength(60)]
        // public string Email { get; set; } = default!;

        public Time? OpeningTimes { get; set; } = default!;

        // public ICollection<AppUserWorkRestaurant>? Workers { get; set; }
        //
        // public ICollection<MenuInRestaurant>? Menus { get; set; }
        //
        // public ICollection<Order>? Orders { get; set; }
        //
        // public ICollection<ArchivedOrder>? CompletedOrders { get; set; }
    }
}