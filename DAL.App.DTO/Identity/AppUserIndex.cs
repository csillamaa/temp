﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.App.DTO.Restaurant;
using Domain.Base;

namespace DAL.App.DTO.Identity
{
    public class AppUserIndex: DomainEntity
    {
        [MaxLength(140)]
        public string FullName { get; set; } = default!;

        [MaxLength(140)]
        public string Email { get; set; } = default!;

        public string? CompanyName { get; set; }

        public IEnumerable<RestaurantOnlyName>? WorkRestaurants { get; set; }
    }
}