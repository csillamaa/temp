﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Contracts.Domain.Base;
using DAL.App.DTO.Archive;
using Microsoft.AspNetCore.Identity;

namespace DAL.App.DTO.Identity
{
    public class AppUser : IdentityUser<int>, IDomainEntity
    {
        [StringLength(65, MinimumLength = 1)]
        public string FirstName { get; set; } = default!;

        [StringLength(75, MinimumLength = 1)]
        public string LastName { get; set; } = default!;
        
        // [MaxLength(100)]
        // public string? Address { get; set; }

        public ICollection<BankCard>? Cards { get; set; }

        public ICollection<AppUserOrder>? Orders { get; set; }

        public ICollection<ArchivedAppUserOrder>? PastOrders { get; set; }

        public ICollection<AppUserFavoriteRestaurant>? FavoriteRestaurants { get; set; }

        public ICollection<AppUserWorkRestaurant>? WorkRestaurants { get; set; }

        public int? CompanyId { get; set; }
        public Company? Company { get; set; }
    }
}