﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.App.DTO.Restaurant;
using Domain.Base;

namespace DAL.App.DTO.Identity
{
    public class AppUserEdit : AppUserData
    {
        // [StringLength(65, MinimumLength = 1)]
        // public string FirstName { get; set; } = default!;
        //
        // [StringLength(75, MinimumLength = 1)]
        // public string LastName { get; set; } = default!;
        //
        // [MaxLength(140)]
        // public string Email { get; set; } = default!;
        //
        // [MaxLength(100)]
        // public string? Address { get; set; }
        // [MaxLength(20)]
        // public string PhoneNumber { get; set; } = default!;
        //
        // public int? CompanyId { get; set; }
        
        public DateTimeOffset? LockoutEnd { get; set; }

        public ICollection<RestaurantOnlyName>? WorkRestaurants { get; set; }
        
      
        public string? CompanyName { get; set; }
        
        // public ICollection<BankCard>? Cards { get; set; }

        // public ICollection<AppUserOrder>? Orders { get; set; }
        //
        // public ICollection<ArchivedAppUserOrder>? PastOrders { get; set; }

    }
}