﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace DAL.App.DTO.Identity
{
    public class AppUserOnlyName: DomainEntity
    {
        
        [MaxLength(140)]
        public string FullName { get; set; } = default!;
    }
}