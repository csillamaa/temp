﻿using System.ComponentModel.DataAnnotations;
using Contracts.Domain.Base;
using Domain.Base;
using Microsoft.AspNetCore.Identity;

namespace DAL.App.DTO.Identity
{
    public class AppRole: DomainEntity
    {
        [MaxLength(32)]
        public string Name { get; set; } = default!;
    }
}