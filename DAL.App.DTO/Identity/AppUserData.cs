﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace DAL.App.DTO.Identity
{
    public class AppUserData: DomainEntity
    {
        [StringLength(65, MinimumLength = 1)]
        public string FirstName { get; set; } = default!;

        [StringLength(75, MinimumLength = 1)]
        public string LastName { get; set; } = default!;

        [MaxLength(140)]
        public string Email { get; set; } = default!;
        
        // [MaxLength(100)]
        // public string? Address { get; set; }
        [MaxLength(20)]
        public string PhoneNumber { get; set; } = default!;
        
        public int? CompanyId { get; set; }
    }
}