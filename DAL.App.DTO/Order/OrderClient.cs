﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace DAL.App.DTO.Order
{
    public class OrderClient : DomainEntity
    {
        public DateTime SubmitTime { get; set; }

        public DateTime CompletedBy { get; set; }
        //
        // public DateTime OrderHandedOver { get; set; }
        //
        // public DateTime InitialCompleteTimeEstimate { get; set; }
        //
        // public bool IsProcessed { get; set; }
        public bool IsSelfPickup { get; set; }
        public bool IsReady { get; set; }


        // public double DeliveryCost { get; set; }

        public double FoodCost { get; set; }
        
        public double AmountPaid { get; set; }
        
        [MaxLength(500)]
        public string? CustomerComment { get; set; }

        public string RestaurantName { get; set; } = default!;
        public int RestaurantId { get; set; }
        
        // public Restaurant.Restaurant Restaurant { get; set; } = default!;

        public ICollection<ProductInOrderClient> ProductsInOrder { get; set; } = default!;
        
        // public ICollection<AppUserOrder> OrderAppUsers { get; set; } = default!;
    }
}