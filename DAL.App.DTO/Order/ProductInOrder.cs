﻿using System.Collections.Generic;
using Domain.Base;

namespace DAL.App.DTO.Order
{
    public class ProductInOrder : DomainEntity
    {
        public int Amount { get; set; }

        public double OneProductPrice { get; set; }

        public int? ProductId { get; set; }
        // public Product.Product? Product { get; set; }

        public int OrderId { get; set; }
        // public DTO.Order.Order Order { get; set; } = default!;
        public ICollection<SubProductFromProduct>? SubProducts { get; set; }
    }
}