﻿using DAL.App.DTO.Product;
using Domain.Base;

namespace DAL.App.DTO.Order
{
    public class SubProductFromProduct : DomainEntity
    {
        public int? ProductsSubCategoryProductId { get; set; }
        public ProductsSubCategoryProduct? ProductsSubCategoryProduct { get; set; } = default!;

        public int ProductInOrderId { get; set; }
        public ProductInOrder ProductInOrder { get; set; } = default!;

        public int Amount { get; set; }
    }
}