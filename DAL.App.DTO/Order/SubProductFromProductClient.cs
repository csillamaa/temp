﻿using DAL.App.DTO.Product;
using Domain.Base;

namespace DAL.App.DTO.Order
{
    public class SubProductFromProductClient
    {
        public string SubCategoryName { get; set; } = default!;
        
        public string Name { get; set; } = default!;
        
        // public int? SubCategoryProductId { get; set; }
        // public ProductsSubCategoryProduct? ProductsSubCategoryProduct { get; set; } = default!;

        public double Price { get; set; }
        // public int ProductInOrderId { get; set; }
        // public ProductInOrder ProductInOrder { get; set; } = default!;
        //
        // public int Amount { get; set; }
    }
}