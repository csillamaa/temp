﻿using Contracts.Domain.Base;
using DAL.App.DTO.Identity;
using Domain.Base;

namespace DAL.App.DTO
{
    public class AppUserFavoriteRestaurant: DomainEntity, IDomainAppUserId, IDomainAppUser<AppUser>
    {
        public int RestaurantId { get; set; }
        public Restaurant.Restaurant Restaurant { get; set; } = default!;

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; } = default!;
    }
}