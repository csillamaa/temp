﻿using System;
using Contracts.DAL.Base.Repositories;
using Contracts.Domain.Base;

namespace Contracts.BLL.Base.Services
{
    
   
    public interface IBaseEntityService<TBllEntity, TDalEntity>: IBaseEntityService<TBllEntity, TDalEntity, int>
        where TDalEntity: class, IDomainEntity
        where TBllEntity: class, IDomainEntity
    {
        
    }
    
    public interface IBaseEntityService<TBllEntity, TDalEntity, TKey> : IBaseService, IBaseRepository<TBllEntity, TKey>
        where TDalEntity: class, IDomainEntity<TKey>
        where TBllEntity: class, IDomainEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        
    }
}