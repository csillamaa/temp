﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.DTO.v1.Company
{
    public class CompanyData : CompanyOnlyName
    {
        public DateTime DateJoined { get; set; }
        
        [MaxLength(60)]
        public string? Website { get; set; }

        [MaxLength(20)]
        public string PhoneNumber { get; set; } = default!;

        [MaxLength(60)]
        public string Email { get; set; } = default!;
    }
}