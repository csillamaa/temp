﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace PublicApi.DTO.v1.Company
{
    public class CompanyOnlyName : DomainEntity
    {
        [MaxLength(60)]
        public string Name { get; set; } = default!;
    }
}