﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PublicApi.DTO.v1.Identity;
using PublicApi.DTO.v1.Restaurant;

namespace PublicApi.DTO.v1.Company
{
    public class Company : CompanyOnlyName
    {
        public DateTime DateJoined { get; set; }
        
        [MaxLength(60)]
        public string? Website { get; set; }

        [MaxLength(20)]
        public string PhoneNumber { get; set; } = default!;

        [MaxLength(60)]
        public string Email { get; set; } = default!;
        
        public ICollection<RestaurantOnlyName>? Restaurants { get; set; }
        public ICollection<AppUserCompany>? CompanyUsers { get; set; }
        // public ICollection<Menu>? Menus { get; set; }
        
    }
}