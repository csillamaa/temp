﻿using System.Collections.Generic;

namespace PublicApi.DTO.v1.Order
{
    public class OrderWorkerView
    {
        public int RestaurantId { get; set; }

        public string RestaurantName { get; set; } = default!;

        public ICollection<OrderWorker>? Orders { get; set; }
    }
}