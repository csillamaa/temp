﻿namespace PublicApi.DTO.v1.Order
{
    public class SubProductFromProductClient
    {
        public string SubCategoryName { get; set; } = default!;

        public string Name { get; set; } = default!;

        public double Price { get; set; }
    }
}