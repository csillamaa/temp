﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace PublicApi.DTO.v1.Order
{
    public class ProductInOrder : DomainEntity
    {
        public int Amount { get; set; }
        
        public double OneProductPrice { get; set; }

        public int? ProductId { get; set; }

        public int OrderId { get; set; }
  
        public ICollection<SubProductFromProduct>? SubProducts { get; set; }
    }
}