﻿using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Order;
using BLL.App.DTO.Product;
using Domain.Base;

namespace PublicApi.DTO.v1.Order
{
    public class SubProductFromProduct : DomainEntity
    {
        public int? ProductsSubCategoryProductId { get; set; }
        public ProductsSubCategoryProduct? ProductsSubCategoryProduct { get; set; } = default!;

        public int ProductInOrderId { get; set; }
        public ProductInOrder ProductInOrder { get; set; } = default!;

        public int Amount { get; set; }
    }
}