﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace PublicApi.DTO.v1.Order
{
    public class ProductInOrderClient: DomainEntity
    {
        public int Amount { get; set; }
        
        public double OneProductPrice { get; set; }
        
        public string Name { get; set; } = default!;

        public int? ProductId { get; set; }
        // public Product.Product? Product { get; set; }

        // public int OrderId { get; set; }
        // public DTO.Order.Order Order { get; set; } = default!;
        public List<SubProductFromProductClient>? SubProducts { get; set; }
    }
}