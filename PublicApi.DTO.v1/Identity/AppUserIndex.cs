﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;
using PublicApi.DTO.v1.Restaurant;

namespace PublicApi.DTO.v1.Identity
{
    public class AppUserIndex: DomainEntity
    {
        [MaxLength(140)]
        public string FullName { get; set; } = default!;

        [MaxLength(140)]
        public string Email { get; set; } = default!;

        public string? CompanyName { get; set; }

        public IEnumerable<RestaurantOnlyName>? WorkRestaurants { get; set; }
        
        public IEnumerable<string>? Roles { get; set; }
    }
}