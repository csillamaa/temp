﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace PublicApi.DTO.v1.Identity
{
    public class AppRole: DomainEntity
    {
    [MaxLength(32)]
    public string Name { get; set; } = default!;

    }
}