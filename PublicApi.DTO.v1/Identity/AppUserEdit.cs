﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;
using PublicApi.DTO.v1.Company;
using PublicApi.DTO.v1.Restaurant;

namespace PublicApi.DTO.v1.Identity
{
    public class AppUserEdit : AppUserData
    {
        public string? CompanyName { get; set; }

        public DateTimeOffset? LockoutEnd { get; set; }
        
        public ICollection<RestaurantOnlyName>? WorkRestaurants { get; set; }
        public ICollection<RestaurantOnlyName>? AddableRestaurants { get; set; }

        public IEnumerable<string>? Roles { get; set; }

        public IEnumerable<CompanyOnlyName>? AllCompanies { get; set; }
        
        // public ICollection<BankCard>? Cards { get; set; }

        // public ICollection<AppUserOrder>? Orders { get; set; }
        //
        // public ICollection<ArchivedAppUserOrder>? PastOrders { get; set; }

    }
}