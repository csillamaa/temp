﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace PublicApi.DTO.v1.Identity
{
    public class AppUserCompany: DomainEntity
    {
        [MaxLength(140)]
        public string FullName { get; set; } = default!;

        public bool IsCompanyAdmin { get; set; }

        public int[]? WorkRestaurantIds { get; set; }
    }
}