﻿using System;
using Domain.App;
using Domain.Base;

namespace PublicApi.DTO.v1
{
    public class Time 
    {
        // public int StartingDayOfWeek { get; set; }
        // public DateTime StartingTime { get; set; }
        //
        // public int EndingDayOfWeek { get; set; }
        // public DateTime EndingTime { get; set; }
        
       
        
        public int DayOfWeek { get; set; }
        
        public DateTime StartingTime { get; set; }

        public DateTime EndingTime { get; set; }
        //
        // public int? RestaurantId { get; set; }
        // public Domain.App.Restaurant? Restaurant { get; set; }
        //
        // public int? ProductId { get; set; }
        // public Product? Product { get; set; }
          
    }
}