﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;
using PublicApi.DTO.v1.Menu;

namespace PublicApi.DTO.v1.Restaurant
{
    public class Restaurant : RestaurantData
    {
        public string? CompanyName { get; set; }

        public ICollection<Time>? OpeningTimes { get; set; }
        
        public ICollection<AppUserWorkRestaurant>? Workers { get; set; }

        public ICollection<MenuInRestaurant>? Menus { get; set; }
    }
}