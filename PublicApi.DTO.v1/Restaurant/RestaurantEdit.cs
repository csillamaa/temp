﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;
using Microsoft.AspNetCore.Mvc.Rendering;
using PublicApi.DTO.v1.Menu;

namespace PublicApi.DTO.v1.Restaurant
{
    public class RestaurantEdit: RestaurantData
    {
        public string CompanyName { get; set; } = default!;

        public ICollection<Time>? OpeningTimes { get; set; }
        public ICollection<Identity.AppUserOnlyName>? Workers { get; set; }

        // public ICollection<CompanyJustName>? AllCompanies { get; set; }
        
        public ICollection<MenuOnlyName>? RestaurantMenus { get; set; }

        public List<MenuOnlyName>? CompanyMenus { get; set; }
    }
}