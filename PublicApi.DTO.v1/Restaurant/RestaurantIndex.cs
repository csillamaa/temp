﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace PublicApi.DTO.v1.Restaurant
{
    public class RestaurantIndex: RestaurantOnlyName
    {
        public DateTime DateJoined { get; set; }

        public string CompanyName { get; set; } = default!;

        [MaxLength(60)]
        public string Address { get; set; } = default!;

        public int WorkerCount { get; set; }
    }
}