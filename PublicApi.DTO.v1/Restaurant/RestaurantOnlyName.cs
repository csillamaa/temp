﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace PublicApi.DTO.v1.Restaurant
{
    public class RestaurantOnlyName: DomainEntity
    {
        [MaxLength(60)]
        public string Name { get; set; } = default!;

        public bool IsPublic { get; set; }
    }
}