﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace PublicApi.DTO.v1.Restaurant
{
    public class RestaurantData: DomainEntity
    {
        [MaxLength(60)]
        public string Name { get; set; } = default!;
        
        [MaxLength(60)]
        public string? Picture { get; set; } 
        
        public bool IsPublic { get; set; }

        public DateTime DateJoined { get; set; }

        [MaxLength(700)]
        public string Description { get; set; } = default!;

        public int CompanyId { get; set; }

        [MaxLength(60)]
        public string Address { get; set; } = default!;

        [MaxLength(20)]
        public string PhoneNumber { get; set; } = default!;

        [MaxLength(60)]
        public string Email { get; set; } = default!;

        // public ICollection<Time>? OpeningTimes { get; set; }
    }
}