﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace PublicApi.DTO.v1.Restaurant
{
    public class RestaurantClientAll : DomainEntity
    {
        [MaxLength(60)]
        public string Name { get; set; } = default!;
        
        [MaxLength(60)]
        public string? Picture { get; set; }

        public TimeCurrentDay OpeningTimes { get; set; } = default!;

    }
}