﻿using System.Collections.Generic;

namespace PublicApi.DTO.v1.Restaurant
{
    public class RestaurantClientAllLists
    {
        public List<RestaurantClientAll> AllRestaurants { get; set; } = default!;
        
        public List<RestaurantClientAll>? UserFavoriteRestaurants { get; set; }
    }
}