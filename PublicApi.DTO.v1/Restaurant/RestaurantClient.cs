﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace PublicApi.DTO.v1.Restaurant
{
    public class RestaurantClient : DomainEntity
    {
        public string Name { get; set; } = default!;
        
        public string? Picture { get; set; }
        
        public string Description { get; set; } = default!;
        
        public string Address { get; set; } = default!;

        public string PhoneNumber { get; set; } = default!;
        
        public string Email { get; set; } = default!;

        public ICollection<Time>? OpeningTimes { get; set; }

        public ICollection<Menu.Menu>? Menus { get; set; }
        
    }
}