﻿using System;

namespace PublicApi.DTO.v1
{
    public class TimeCurrentDay
    {
        public DateTime StartingTime { get; set; }

        public DateTime EndingTime { get; set; }
    }
}