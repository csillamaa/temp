﻿using Contracts.Domain.Base;
using Domain.Base;

namespace PublicApi.DTO.v1
{
    public class AppUserOrder: DomainEntity, IDomainAppUserId
    {
        public int OrderId { get; set; }
        
        public int AppUserId { get; set; }
    }
}