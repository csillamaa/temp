using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PublicApi.DTO.v1.Menu;

namespace PublicApi.DTO.v1.Product
{
    public class ProductCreate : ProductData
    {
        // TODO: do i need it?
        public int MenuId { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Product), Name = "MenuCategory")]
        public List<MenuCategoryOnlyName> MenuCategories { get; set; } = default!;
    }
}