﻿using Domain.Base;

namespace PublicApi.DTO.v1.Product
{
    public class ProductsSubCategoryData: DomainEntity
    {
        public int ProductId { get; set; }
        
        public string Name { get; set; } = default!;
      
        public int MandatoryItemCount { get; set; }
      
        public int MaxItemCount { get; set; }

    }
}