﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace PublicApi.DTO.v1.Product
{
    public class ProductsSubCategoryProduct : DomainEntity
    {
        public int ProductsSubCategoryId { get; set; }
        
        public PublicApi.DTO.v1.Product.ProductsSubCategory ProductsSubCategory { get; set; } = default!;
        
        [MaxLength(50)]
        public string Name { get; set; } = default!;
        
        public double Price { get; set; }
        
        public int Position { get; set; }
        
        public bool IsSelectedOnDefault { get; set; }
        
        public string? Description { get; set; }
    }
}