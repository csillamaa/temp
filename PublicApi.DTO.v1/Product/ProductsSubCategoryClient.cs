﻿using System.Collections.Generic;
using DAL.App.DTO.Product;
using Domain.Base;

namespace PublicApi.DTO.v1.Product
{
    public class ProductsSubCategoryClient: DomainEntity
    {
        public string Name { get; set; } = default!;
       
        public int MandatoryItemCount { get; set; }
        
        public int MaxItemCount { get; set; }
        
        public List<ProductsSubCategoryProductClient>? SubCategoryProducts { get; set; }
    }
}