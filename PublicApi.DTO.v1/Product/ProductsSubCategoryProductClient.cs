﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace PublicApi.DTO.v1.Product
{
    public class ProductsSubCategoryProductClient : DomainEntity
    {
        public string Name { get; set; } = default!;
        
        public double Price { get; set; }
        
        public bool IsSelectedOnDefault { get; set; }

        [MaxLength(250)]
        public string? Description { get; set; }
    }
}