﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace PublicApi.DTO.v1.Product
{
    public class ProductsSubCategory : DomainEntity
    {
        public int ProductId { get; set; }
        
        // public PublicApi.DTO.v1.Product.Product Product{ get; set; } = default!;
        
        [MaxLength(60)]
        public string Name { get; set; } = default!;
        
        public int Position { get; set; }
        
        public int MandatoryItemCount { get; set; }
        
        public int MaxItemCount { get; set; }

        public ICollection<ProductsSubCategoryProduct>? SubCategoryProducts { get; set; }
    }
}