﻿using System.Collections.Generic;
using PublicApi.DTO.v1.Menu;

namespace PublicApi.DTO.v1.Product
{
    public class ProductEdit : ProductData
    {
        // public int MenuId { get; set; }
        
        public List<MenuCategoryOnlyName>? MenuCategories { get; set; } = default!;

        public List<ProductsSubCategory>? ProductsSubCategories { get; set; }

        // public ICollection<Time>? SellingTimes { get; set; }
    }
}