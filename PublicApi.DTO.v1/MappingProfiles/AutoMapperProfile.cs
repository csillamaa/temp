﻿using AutoMapper;

namespace PublicApi.DTO.v1.MappingProfiles
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            // CreateMap<PublicApi.DTO.v1.Archive.ArchivedOrder, BLL.App.DTO.Archive.ArchivedOrder>().ReverseMap();
            // CreateMap<PublicApi.DTO.v1.Archive.ArchivedAppUserOrder, BLL.App.DTO.Archive.ArchivedAppUserOrder>().ReverseMap();
            // CreateMap<PublicApi.DTO.v1.Archive.ArchivedProductInOrder, BLL.App.DTO.Archive.ArchivedProductInOrder>().ReverseMap();
            // CreateMap<PublicApi.DTO.v1.Archive.ArchivedSubProductFromProduct, BLL.App.DTO.Archive.ArchivedSubProductFromProduct>().ReverseMap();
            
            CreateMap<PublicApi.DTO.v1.Identity.AppUser, BLL.App.DTO.Identity.AppUser>().ReverseMap();
            CreateMap<PublicApi.DTO.v1.Identity.AppRole, Domain.App.Identity.AppRole>().ReverseMap();
            
            // CreateMap<PublicApi.DTO.v1.AppUserFavoriteRestaurant, BLL.App.DTO.AppUserFavoriteRestaurant>().ReverseMap();
            // CreateMap<PublicApi.DTO.v1.AppUserOrder, BLL.App.DTO.AppUserOrder>().ReverseMap();
            CreateMap<PublicApi.DTO.v1.AppUserWorkRestaurant, BLL.App.DTO.AppUserWorkRestaurant>().ReverseMap();
            // CreateMap<PublicApi.DTO.v1.BankCard, BLL.App.DTO.BankCard>().ReverseMap();
            CreateMap<Company.Company, BLL.App.DTO.Company>().ReverseMap();
            CreateMap<PublicApi.DTO.v1.Menu.Menu, BLL.App.DTO.Menu.Menu>().ReverseMap();
            CreateMap<PublicApi.DTO.v1.Menu.MenuCategory, BLL.App.DTO.Menu.MenuCategory>().ReverseMap();
            CreateMap<PublicApi.DTO.v1.Menu.MenuInRestaurant, BLL.App.DTO.Menu.MenuInRestaurant>().ReverseMap();
            // CreateMap<PublicApi.DTO.v1.Order, BLL.App.DTO.Order>().ReverseMap();
            CreateMap<PublicApi.DTO.v1.Product.Product, BLL.App.DTO.Product.Product>().ReverseMap();
            // CreateMap<PublicApi.DTO.v1.ProductInOrder, BLL.App.DTO.ProductInOrder>().ReverseMap();
            
            CreateMap<PublicApi.DTO.v1.Product.ProductsSubCategory, BLL.App.DTO.Product.ProductsSubCategory>().ReverseMap();
            
            CreateMap<Restaurant.Restaurant, BLL.App.DTO.Restaurant.Restaurant>().ReverseMap();
            
            CreateMap<PublicApi.DTO.v1.Product.ProductsSubCategoryProduct, BLL.App.DTO.Product.ProductsSubCategoryProduct>().ReverseMap();
            // CreateMap<PublicApi.DTO.v1.SubProductFromProduct, BLL.App.DTO.SubProductFromProduct>().ReverseMap();
            CreateMap<PublicApi.DTO.v1.Time, BLL.App.DTO.Time>().ReverseMap();
        }
    }
}