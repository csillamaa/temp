﻿using Contracts.Domain.Base;
using Domain.Base;

namespace PublicApi.DTO.v1.Archive
{
    public class ArchivedAppUserOrder: DomainEntity, IDomainAppUserId
    {
        public int ArchivedOrderId { get; set; }
        // public ArchivedOrder ArchivedOrder { get; set; } = default!;

        public int AppUserId { get; set; }
        // public AppUser AppUser { get; set; } = default!;
    }
}