﻿using Domain.Base;
using PublicApi.DTO.v1.Product;

namespace PublicApi.DTO.v1.Archive
{
    public class ArchivedSubProductFromProduct : DomainEntity
    {
        public int ProductsSubCategoryProductId { get; set; }
        public ProductsSubCategoryProduct ProductsSubCategoryProduct { get; set; } = default!;

        public int ArchivedProductInOrderId { get; set; }
        public ArchivedProductInOrder ArchivedProductInOrder { get; set; } = default!;

        public int Amount { get; set; }
    }
}