﻿using System.Collections.Generic;
using PublicApi.DTO.v1.Order;

namespace PublicApi.DTO.v1.Archive
{
    public class ArchivedOrderWorkerView
    {
        public int RestaurantId { get; set; }

        public string RestaurantName { get; set; } = default!;

        public ICollection<ArchivedOrderWorker>? Orders { get; set; }
    }
}