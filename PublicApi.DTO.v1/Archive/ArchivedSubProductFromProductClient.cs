﻿namespace PublicApi.DTO.v1.Archive
{
    public class ArchivedSubProductFromProductClient
    {
        public string SubCategoryName { get; set; } = default!;

        public string Name { get; set; } = default!;

        public double Price { get; set; }
    }
}