﻿using Contracts.Domain.Base;
using Domain.Base;

namespace PublicApi.DTO.v1
{
    public class AppUserWorkRestaurant: DomainEntity, IDomainAppUserId
    {
        public int RestaurantId { get; set; }

        public int AppUserId { get; set; }
    }
}