﻿using AutoMapper;
using BLL.App.DTO.Mappers;

namespace PublicApi.DTO.v1.Mappers
{
    public class AppRoleMapper: BaseMapper<PublicApi.DTO.v1.Identity.AppRole, Domain.App.Identity.AppRole>
    {
        public AppRoleMapper(IMapper mapper) : base(mapper)
        {
        }
    }
}