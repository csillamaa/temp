﻿using System.Linq;
using AutoMapper;
using BLL.App.DTO.Mappers;
using PublicApi.DTO.v1.Company;
using PublicApiDTOv1 = PublicApi.DTO.v1;
using BLLAppDTO = BLL.App.DTO; 

namespace PublicApi.DTO.v1.Mappers
{
    public class CompanyMapper: BaseMapper<Company.Company, BLLAppDTO.Company>
    {
        public CompanyMapper(IMapper mapper) : base(mapper)
        {
        }
        public static CompanyOnlyName MapToPublicApiV1(BLLAppDTO.CompanyOnlyName company)
        {
            return new()
            {
                Id = company.Id,
                Name = company.Name
            };
        }
        
        public static Company.Company MapToPublicApiV1(BLLAppDTO.Company company)
        {
            return new()
            {
                Id = company.Id,
                Name = company.Name,
                Email = company.Email,
                PhoneNumber = company.PhoneNumber,
                DateJoined = company.DateJoined,
                Website = company.Website,
                Restaurants = company.Restaurants?.Select(RestaurantMapper.MapToApi).ToList(),
                CompanyUsers = company.CompanyUsers?.Select(AppUserMapper.MapToPublicApiV1).ToList()
            };
        }
        
        public static BLLAppDTO.Company MapToBLL(PublicApi.DTO.v1.Company.CompanyData company)
        {
            return new()
            {
                Id = company.Id,
                Name = company.Name,
                Email = company.Email,
                PhoneNumber = company.PhoneNumber,
                DateJoined = company.DateJoined,
                Website = company.Website,
            };
        }
    }
}