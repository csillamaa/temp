﻿using System.Linq;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTORestaurant = BLL.App.DTO.Restaurant;
using PublicApiDTOv1Restaurant = PublicApi.DTO.v1.Restaurant;

namespace PublicApi.DTO.v1.Mappers
{
    public class RestaurantMapper: BaseMapper<PublicApiDTOv1Restaurant.Restaurant, BLLAppDTORestaurant.Restaurant>
    { 
        public RestaurantMapper(IMapper mapper) : base(mapper)
        {
        }

        public static PublicApiDTOv1Restaurant.RestaurantIndex MapToApi(BLLAppDTORestaurant.RestaurantIndex restaurant)
        {
            return new()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                IsPublic = restaurant.IsPublic,
                Address = restaurant.Address,
                CompanyName = restaurant.CompanyName,
                WorkerCount = restaurant.WorkerCount,
                DateJoined = restaurant.DateJoined
            };
        }
        public static PublicApiDTOv1Restaurant.RestaurantEdit MapToApi(BLLAppDTORestaurant.RestaurantEdit restaurant)
        {
            return new ()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                Email = restaurant.Email,
                PhoneNumber = restaurant.PhoneNumber,
                DateJoined = restaurant.DateJoined,
                Address = restaurant.Address,
                CompanyId = restaurant.CompanyId,
                CompanyName = restaurant.CompanyName,
                Description = restaurant.Description,
                Picture = restaurant.Picture,
                Workers = restaurant.Workers?.Select(AppUserMapper.MapToPublicApiV1).ToList(),
                OpeningTimes = restaurant.OpeningTimes?.Select(TimeMapper.Map).ToList(),
                RestaurantMenus = restaurant.RestaurantMenus?.Select(MenuMapper.Map).ToList(),
                CompanyMenus = restaurant.CompanyMenus?.Select(MenuMapper.Map).ToList()
            };
        }
        
        public static BLLAppDTORestaurant.Restaurant MapToBLL(PublicApiDTOv1Restaurant.RestaurantData restaurant)
        {
            return new ()
            {
                CompanyId = restaurant.CompanyId,
                DateJoined = restaurant.DateJoined,
                Name = restaurant.Name,
                Description = restaurant.Description,
                Address = restaurant.Address,
                PhoneNumber = restaurant.PhoneNumber,
                Picture = restaurant.Picture,
                Email = restaurant.Email
            };
        }
        
        public static PublicApiDTOv1Restaurant.RestaurantOnlyName MapToApi(BLLAppDTORestaurant.RestaurantOnlyName restaurant)
        {
            return new()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                IsPublic = restaurant.IsPublic,
            };
        }
        public static PublicApiDTOv1Restaurant.RestaurantClientAll MapToApiClientAll(BLLAppDTORestaurant.RestaurantClientAll restaurant)
        {
            return new ()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                Picture = restaurant.Picture,
                OpeningTimes = TimeMapper.MapToApiCurrentDay(restaurant.OpeningTimes!)
            };
        }
        
        
        public static PublicApiDTOv1Restaurant.RestaurantClient MapToApiClient(BLLAppDTORestaurant.RestaurantClient restaurant)
        {
           return new()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                Address = restaurant.Address,
                PhoneNumber = restaurant.PhoneNumber,
                Picture = restaurant.Picture,
                Description = restaurant.Description,
                Email = restaurant.Email,
                OpeningTimes = restaurant.OpeningTimes?.Select(TimeMapper.Map).ToList(),
                Menus = restaurant.Menus?.Select(MenuMapper.MapToApiMenu).ToList()
            };
        }
        
        public static PublicApiDTOv1Restaurant.RestaurantClientAllLists MapToApiClientAllLists(BLLAppDTORestaurant.RestaurantClientAllLists restaurant)
        {
            return new ()
            {
               AllRestaurants = restaurant.AllRestaurants.Select(MapToApiClientAll).ToList(),
               UserFavoriteRestaurants = restaurant.UserFavoriteRestaurants?.Select(MapToApiClientAll).ToList()
            };
        }
    }
}