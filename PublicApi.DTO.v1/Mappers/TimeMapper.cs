﻿using AutoMapper;
using BLL.App.DTO.Mappers;

namespace PublicApi.DTO.v1.Mappers
{
    public class TimeMapper: BaseMapper<PublicApi.DTO.v1.Time, BLL.App.DTO.Time>
    {
        public TimeMapper(IMapper mapper) : base(mapper)
        {
        }

        public new static Time Map(BLL.App.DTO.Time time)
        {
            return new()
            {
                DayOfWeek = time.DayOfWeek,
                StartingTime = time.StartingTime,
                EndingTime = time.EndingTime
            };
        }
        
        public new static BLL.App.DTO.Time Map(Time time)
        {
            return new()
            {
                DayOfWeek = time.DayOfWeek,
                StartingTime = time.StartingTime,
                EndingTime = time.EndingTime
            };
        }
        
        public static TimeCurrentDay MapToApiCurrentDay(BLL.App.DTO.TimeCurrentDay time)
        {
            return new()
            {
                EndingTime = time.EndingTime,
                StartingTime = time.StartingTime
            };
        }
    }
}