﻿using System.Linq;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTOMenu = BLL.App.DTO.Menu;
using PublicApiV1Menu = PublicApi.DTO.v1.Menu;

namespace PublicApi.DTO.v1.Mappers
{
    public class MenuMapper : BaseMapper<PublicApiV1Menu.Menu, BLLAppDTOMenu.Menu>
    {
        public MenuMapper(IMapper mapper) : base(mapper)
        {
        }

        public static PublicApiV1Menu.MenuIndex Map(BLLAppDTOMenu.MenuIndex menu)
        {
            return new()
            {
                
                Id = menu.Id,
                CompanyName = menu.CompanyName,
                Name = menu.Name,
                RestaurantsWithTheMenu = menu.RestaurantsWithTheMenu?
                    .Select(RestaurantMapper.MapToApi).ToList()
            };
        }
        
        public static PublicApiV1Menu.MenuEdit Map(BLLAppDTOMenu.MenuEdit menu)
        {
            return new()
            {
                Id = menu.Id,
                CompanyName = menu.CompanyName,
                Name = menu.Name,
                CompanyId = menu.CompanyId,
                MenuCategories = menu.MenuCategories?
                    .Select(MenuCategoryMapper.MapToOnlyName)
                    .OrderBy(c => c.Position)
                    .ToList(),
                CurrentCategory = MenuCategoryMapper.Map(menu.CurrentCategory),
            };
        }
        
        public static PublicApiV1Menu.MenuData Map(BLLAppDTOMenu.MenuData menu)
        {
            return new()
            {
                
                Id = menu.Id,
                Name = menu.Name,
                CompanyId = menu.CompanyId
            };
        }
        
        public static BLLAppDTOMenu.MenuData Map(PublicApiV1Menu.MenuData menu)
        {
            return new()
            {
                Id = menu.Id,
                Name = menu.Name,
                CompanyId = menu.CompanyId
            };
        }
        public static PublicApiV1Menu.MenuOnlyName Map(BLLAppDTOMenu.MenuOnlyName menu)
        {
            return new()
            {
                Id = menu.Id,
                Name = menu.Name,
            };
        }
        
        public static BLLAppDTOMenu.Menu Map(PublicApiV1Menu.MenuOnlyName menu)
        {
            return new()
            {
                Id = menu.Id,
                Name = menu.Name,
                CompanyId = menu.Id
            };
        }
        
        public static PublicApiV1Menu.Menu MapToApiMenu(BLLAppDTOMenu.Menu menu)
        {
            return new()
            {
                Id = menu.Id,
                Name = menu.Name,
                // CompanyId = menu.CompanyId,
                MenuCategories = menu.MenuCategories?
                    .Select(MenuCategoryMapper.MapToOnlyName)
                    .OrderBy(c => c.Position)
                    .ToList(),
                CurrentCategory = MenuCategoryMapper.Map(menu.CurrentCategory)
            };
        }
    }
}