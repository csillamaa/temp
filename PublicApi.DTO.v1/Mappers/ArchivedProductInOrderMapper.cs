﻿using System.Linq;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;
using PublicApiDTOv1OrderArchived = PublicApi.DTO.v1.Archive;

namespace PublicApi.DTO.v1.Mappers
{
    public class ArchivedProductInOrderMapper : BaseMapper<PublicApiDTOv1OrderArchived.ArchivedProductInOrder, BLLAppDTOOrderArchived.ArchivedProductInOrder>
    {
        public ArchivedProductInOrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static PublicApiDTOv1OrderArchived.ArchivedProductInOrderClient MapToApiClient(BLLAppDTOOrderArchived.ArchivedProductInOrderClient productInOrder)
        {
            return new()
            {
                Id = productInOrder.Id,
                ProductId = productInOrder.ProductId,
                Amount = productInOrder.Amount,
                OneProductPrice = productInOrder.OneProductPrice,
                Name = productInOrder.Name,
                SubProducts = productInOrder.SubProducts?.Select(ArchivedSubProductFromProductMapper.MapToApiClient).ToList()
            };
        }
    }
}