﻿using AutoMapper;
using BLL.App.DTO.Mappers;

namespace PublicApi.DTO.v1.Mappers
{
    public class AppUserFavoriteRestaurantMapper: BaseMapper<PublicApi.DTO.v1.AppUserFavoriteRestaurant, BLL.App.DTO.AppUserFavoriteRestaurant>
    {
        public AppUserFavoriteRestaurantMapper(IMapper mapper) : base(mapper)
        {
        }
    }
}