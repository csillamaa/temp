﻿using System.Linq;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;
using PublicApiDTOv1OrderArchived = PublicApi.DTO.v1.Archive;

namespace PublicApi.DTO.v1.Mappers
{
    public class ArchivedOrderMapper : BaseMapper<PublicApiDTOv1OrderArchived.ArchivedOrder, BLLAppDTOOrderArchived.ArchivedOrder>
    {
        public ArchivedOrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static PublicApiDTOv1OrderArchived.ArchivedOrderClient? MapToApiOrderClient(BLLAppDTOOrderArchived.ArchivedOrderClient? order)
        {
            if (order == null)
            {
                return null;
            }

            return new()
            {
                Id = order.Id,
                SubmitTime = order.SubmitTime,
                CompletedBy = order.CompletedBy,
                RestaurantId = order.RestaurantId,
                RestaurantName = order.RestaurantName,
                CustomerComment = order.CustomerComment,
                OrderRating = order.OrderRating,
                CustomerOrderReviewComment = order.CustomerOrderReviewComment,
                IsSelfPickup = order.IsSelfPickup,
                AmountPaid = order.AmountPaid,
                FoodCost = order.FoodCost,
                ProductsInOrder = order.ProductsInOrder.Select(ArchivedProductInOrderMapper.MapToApiClient).ToList()
            };
        }
        
        public static PublicApiDTOv1OrderArchived.ArchivedOrderWorker MapToApiOrderWorker2(BLLAppDTOOrderArchived.ArchivedOrderWorker order)
        {
            return new()
            {
                Id = order.Id,
                SubmitTime = order.SubmitTime,
                ActualCompleteTime = order.ActualCompleteTime,
                EstimatedCompleteTime = order.EstimatedCompleteTime,
                OrderHandedOver = order.OrderHandedOver,
                CustomerComment = order.CustomerComment,
                Comment = order.Comment,
                OrderRating = order.OrderRating,
                CustomerOrderReviewComment = order.CustomerOrderReviewComment,
                AmountPaid = order.AmountPaid,
                FoodCost = order.FoodCost,
                ProductsInOrder = order.ProductsInOrder.Select(ArchivedProductInOrderMapper.MapToApiClient).ToList(),
                OrderAppUsers = order.OrderAppUsers.Select(AppUserMapper.MapToPublicApiV1).ToList()
            };
        }
        
        public static PublicApiDTOv1OrderArchived.ArchivedOrderWorkerView MapToApiOrderWorkerView(BLLAppDTOOrderArchived.ArchivedOrderWorkerView order)
        {
            return new()
            {
               RestaurantId = order.RestaurantId,
               RestaurantName = order.RestaurantName,
               Orders = order.Orders?.Select(MapToApiOrderWorker2).ToList()
            };
        }
    }
}