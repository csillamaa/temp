﻿using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;
using PublicApiDTOv1OrderArchived = PublicApi.DTO.v1.Archive;

namespace PublicApi.DTO.v1.Mappers
{
    public class ArchivedSubProductFromProductMapper : BaseMapper<PublicApiDTOv1OrderArchived.ArchivedSubProductFromProduct, BLLAppDTOOrderArchived.ArchivedSubProductFromProduct>
    {
        public ArchivedSubProductFromProductMapper(IMapper mapper) : base(mapper)
        {
        }

        public static PublicApiDTOv1OrderArchived.ArchivedSubProductFromProductClient MapToApiClient(
            BLLAppDTOOrderArchived.ArchivedSubProductFromProductClient subProduct)
        {
            return new()
            {
                Name = subProduct.Name,
                Price = subProduct.Price,
                SubCategoryName = subProduct.SubCategoryName
            };
        }
    }
}