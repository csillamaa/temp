﻿using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTOProduct = BLL.App.DTO.Product;
using PublicApiV1DTOProduct = PublicApi.DTO.v1.Product;

namespace PublicApi.DTO.v1.Mappers
{
    public class ProductsSubCategoryProductMapper: BaseMapper<PublicApiV1DTOProduct.ProductsSubCategoryProduct, BLLAppDTOProduct.ProductsSubCategoryProduct>
    {
        public ProductsSubCategoryProductMapper(IMapper mapper) : base(mapper)
        {
        }

        public new static PublicApiV1DTOProduct.ProductsSubCategoryProduct Map(BLLAppDTOProduct.ProductsSubCategoryProduct subProduct)
        {
            return new()
            {
                Id = subProduct.Id,
                Name = subProduct.Name,
                Price = subProduct.Price,
                ProductsSubCategoryId = subProduct.ProductsSubCategoryId,
                Position = subProduct.Position,
                Description = subProduct.Description,
                IsSelectedOnDefault = subProduct.IsSelectedOnDefault
            };
        }

        public static BLLAppDTOProduct.ProductsSubCategoryProduct MapToRegBLL(PublicApiV1DTOProduct.ProductsSubCategoryProductData subProduct)
        {
            return new()
            {
                Id = subProduct.Id,
                Name = subProduct.Name,
                Price = subProduct.Price,
                ProductsSubCategoryId = subProduct.ProductsSubCategoryId,
                Description = subProduct.Description,
                IsSelectedOnDefault = subProduct.IsSelectedOnDefault,
            };
        }
        
        public static PublicApiV1DTOProduct.ProductsSubCategoryProductData? Map(BLLAppDTOProduct.ProductsSubCategoryProductData? subProduct)
        {
            if (subProduct == null)
            {
                return null;
            }
            return new()
            {
                Id = subProduct.Id,
                Name = subProduct.Name,
                Price = subProduct.Price,
                ProductsSubCategoryId = subProduct.ProductsSubCategoryId,
                Description = subProduct.Description,
                IsSelectedOnDefault = subProduct.IsSelectedOnDefault,
            };
        }
        
        public static PublicApiV1DTOProduct.ProductsSubCategoryProductClient MapToApiClient(BLLAppDTOProduct.ProductsSubCategoryProductClient subProduct)
        {
            return new()
            {
                Id = subProduct.Id,
                Price = subProduct.Price,
                Name = subProduct.Name,
                Description = subProduct.Description,
                IsSelectedOnDefault = subProduct.IsSelectedOnDefault,
            };
        }
    }
}