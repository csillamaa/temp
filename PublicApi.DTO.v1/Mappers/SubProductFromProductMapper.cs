﻿using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTOOrder = BLL.App.DTO.Order;
using PublicApiDTOv1Order = PublicApi.DTO.v1.Order;

namespace PublicApi.DTO.v1.Mappers
{
    public class SubProductFromProductMapper : BaseMapper<PublicApiDTOv1Order.SubProductFromProduct, BLLAppDTOOrder.SubProductFromProduct>
    {
        public SubProductFromProductMapper(IMapper mapper) : base(mapper)
        {
        }

        public static PublicApiDTOv1Order.SubProductFromProductClient MapToApiClient(
            BLLAppDTOOrder.SubProductFromProductClient subProduct)
        {
            return new()
            {
                Name = subProduct.Name,
                Price = subProduct.Price,
                SubCategoryName = subProduct.SubCategoryName,
            };
        }
    }
}