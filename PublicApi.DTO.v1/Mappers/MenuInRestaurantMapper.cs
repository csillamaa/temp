﻿using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTOMenu = BLL.App.DTO.Menu;
using PublicApiV1Menu = PublicApi.DTO.v1.Menu;

namespace PublicApi.DTO.v1.Mappers
{
    public class MenuInRestaurantMapper : BaseMapper<PublicApiV1Menu.MenuInRestaurant, BLLAppDTOMenu.MenuInRestaurant>
    {
        public MenuInRestaurantMapper(IMapper mapper) : base(mapper)
        {
        }

        public new static BLLAppDTOMenu.MenuInRestaurant? Map(PublicApiV1Menu.MenuInRestaurant? menuInRestaurant)
        {
            if (menuInRestaurant == null)
            {
                return null;
            }

            return new()
            {
                MenuId = menuInRestaurant.MenuId,
                RestaurantId = menuInRestaurant.RestaurantId
            };
        }
    }
}