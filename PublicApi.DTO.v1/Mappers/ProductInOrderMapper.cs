﻿using System.Linq;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTOOrder = BLL.App.DTO.Order;
using PublicApiDTOv1Order = PublicApi.DTO.v1.Order;

namespace PublicApi.DTO.v1.Mappers
{
    public class ProductInOrderMapper : BaseMapper<PublicApiDTOv1Order.ProductInOrder, BLLAppDTOOrder.ProductInOrder>
    {
        public ProductInOrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static PublicApiDTOv1Order.ProductInOrderClient MapToApiClient(BLLAppDTOOrder.ProductInOrderClient productInOrder)
        {
            return new()
            {
                Id = productInOrder.Id,
                ProductId = productInOrder.ProductId,
                Amount = productInOrder.Amount,
                OneProductPrice = productInOrder.OneProductPrice,
                Name = productInOrder.Name,
                SubProducts = productInOrder.SubProducts?.Select(SubProductFromProductMapper.MapToApiClient).ToList()
            };
        }
    }
}