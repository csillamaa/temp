﻿using System.Linq;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTOProduct = BLL.App.DTO.Product;
using PublicApiV1DTOProduct = PublicApi.DTO.v1.Product;

namespace PublicApi.DTO.v1.Mappers
{
    public class ProductMapper : BaseMapper<PublicApiV1DTOProduct.Product, BLLAppDTOProduct.Product>
    {
        public ProductMapper(IMapper mapper) : base(mapper)
        {
        }
        public new static PublicApiV1DTOProduct.Product Map(BLLAppDTOProduct.Product product)
        {
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Price = product.Price,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                Position = product.Position
            };
        }
        
        public static BLLAppDTOProduct.Product MapToRegBLL(PublicApiV1DTOProduct.ProductData product)
        {
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Price = product.Price,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                Picture = product.Picture
            };
        }
        
        public static PublicApiV1DTOProduct.ProductEdit MapToEdit(BLLAppDTOProduct.ProductEdit product)
        {
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Price = product.Price,
                Picture = product.Picture,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                ProductsSubCategories = product.ProductsSubCategories?
                    .Select(ProductsSubCategoryMapper.Map)
                    .OrderBy(c => c.Position)
                    .ToList(),
                MenuCategories = product.MenuCategories?.Select(MenuCategoryMapper.MapToOnlyName).ToList()
            };
        }
        
        public static PublicApiV1DTOProduct.ProductEdit MapToEditBLL(PublicApiV1DTOProduct.ProductData product)
        {
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Price = product.Price,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                Picture = product.Picture
            };
        }
        public static PublicApiV1DTOProduct.ProductData MapToApiData(BLLAppDTOProduct.ProductData product)
        {
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Price = product.Price,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                Picture = product.Picture
            };
        }
        
        public static PublicApiV1DTOProduct.ProductClient MapToApiClient(BLLAppDTOProduct.ProductClient product)
        {
            return new()
            {
                Id = product.Id,
                Name = product.Name,
                Price = product.Price,
                Picture = product.Picture,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                ProductsSubCategories = product.ProductsSubCategories?
                    .Select(ProductsSubCategoryMapper.MapToApiClient)
                    .ToList()
            };
        }
    }
}