﻿using System.Linq;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTOIdentity = BLL.App.DTO.Identity;
using PublicApiDTOv1Identity = PublicApi.DTO.v1.Identity;

namespace PublicApi.DTO.v1.Mappers

{
    public class AppUserMapper: BaseMapper<PublicApiDTOv1Identity.AppUser, BLLAppDTOIdentity.AppUser>
    {
        public AppUserMapper(IMapper mapper) : base(mapper)
        {
        }
        public static PublicApiDTOv1Identity.AppUserIndex MapToPublicApiV1(BLLAppDTOIdentity.AppUserIndex user)
        {
            return new()
            {
                FullName = user.FullName,
                Id = user.Id,
                Email = user.Email,
                CompanyName = user.CompanyName,
                WorkRestaurants = user.WorkRestaurants?.Select(RestaurantMapper.MapToApi),
            };
        }
        public static PublicApiDTOv1Identity.AppUserOnlyName MapToPublicApiV1(BLLAppDTOIdentity.AppUserOnlyName user)
        {
            return new()
            {
                FullName = user.FullName,
                Id = user.Id,
            };
        }
        
        public static PublicApiDTOv1Identity.AppUserEdit MapToPublicApiV1(BLLAppDTOIdentity.AppUserEdit user)
        {
            return new()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                // Address = user.Address,
                CompanyName = user.CompanyName,
                CompanyId = user.CompanyId,
                LockoutEnd = user.LockoutEnd,
                Roles = user.Roles,
                WorkRestaurants = user.WorkRestaurants?.Select(RestaurantMapper.MapToApi).ToList(),
                AddableRestaurants = user.AddableRestaurants?.Select(RestaurantMapper.MapToApi).ToList(),
                AllCompanies = user.AllCompanies?.Select(CompanyMapper.MapToPublicApiV1)
            };
        }
        
        public static PublicApiDTOv1Identity.AppUserData MapToPublicApiV1(BLLAppDTOIdentity.AppUserData user)
        {
            return new()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                // Address = user.Address,
                CompanyId = user.CompanyId,
            };
        }
        
        public static BLLAppDTOIdentity.AppUserData MapToBLL(PublicApiDTOv1Identity.AppUserData user)
        {
            return new()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                // Address = user.Address,
                CompanyId = user.CompanyId,
            };
        }
        
        public static PublicApiDTOv1Identity.AppUserCompany MapToPublicApiV1(BLLAppDTOIdentity.AppUserCompany user)
        {
            return new()
            {
                Id = user.Id,
                FullName = user.FullName,
                IsCompanyAdmin = user.IsCompanyAdmin,
                WorkRestaurantIds = user.WorkRestaurantIds
            };
        }
    }
}