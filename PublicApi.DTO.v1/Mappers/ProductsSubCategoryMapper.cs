﻿using System.Linq;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTOProduct = BLL.App.DTO.Product;
using PublicApiV1DTOProduct = PublicApi.DTO.v1.Product;

namespace PublicApi.DTO.v1.Mappers
{
    public class ProductsSubCategoryMapper: BaseMapper<PublicApiV1DTOProduct.ProductsSubCategory, BLLAppDTOProduct.ProductsSubCategory>
    {
        public ProductsSubCategoryMapper(IMapper mapper) : base(mapper)
        {
        }

        public new static PublicApiV1DTOProduct.ProductsSubCategory Map(BLLAppDTOProduct.ProductsSubCategory productsSubCategory)
        {
            return new()
            {
                Id = productsSubCategory.Id,
                Name = productsSubCategory.Name,
                Position = productsSubCategory.Position,
                MandatoryItemCount = productsSubCategory.MandatoryItemCount,
                MaxItemCount = productsSubCategory.MaxItemCount,
                SubCategoryProducts = productsSubCategory.SubCategoryProducts?
                    .Select(ProductsSubCategoryProductMapper.Map)
                    .OrderBy(p => p.Position)
                    .ToList()
            };
        }
        
        public static BLLAppDTOProduct.ProductsSubCategory MapToRegBLL(PublicApiV1DTOProduct.ProductsSubCategoryData productsSubCategoryData)
        {
            return new()
            {
                Id = productsSubCategoryData.Id,
                Name = productsSubCategoryData.Name,
                ProductId = productsSubCategoryData.ProductId,
                MandatoryItemCount = productsSubCategoryData.MandatoryItemCount,
                MaxItemCount = productsSubCategoryData.MaxItemCount,
                
            };
        }
        
        public static PublicApiV1DTOProduct.ProductsSubCategoryData? Map(BLLAppDTOProduct.ProductsSubCategoryData? productsSubCategoryData)
        {
            if (productsSubCategoryData == null)
            {
                return null;
            }
            return new()
            {
                Id = productsSubCategoryData.Id,
                Name = productsSubCategoryData.Name,
                ProductId = productsSubCategoryData.ProductId,
                MandatoryItemCount = productsSubCategoryData.MandatoryItemCount,
                MaxItemCount = productsSubCategoryData.MaxItemCount,
            };
        }
        
        public static PublicApiV1DTOProduct.ProductsSubCategoryClient MapToApiClient(BLLAppDTOProduct.ProductsSubCategoryClient subCategory)
        {

            return new()
            {
                Id = subCategory.Id,
                Name = subCategory.Name,
                MandatoryItemCount = subCategory.MandatoryItemCount,
                MaxItemCount = subCategory.MaxItemCount,
                SubCategoryProducts = subCategory.SubCategoryProducts?.Select(ProductsSubCategoryProductMapper.MapToApiClient).ToList()
            };
        }
    }
}