﻿using System.Linq;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTOOrder = BLL.App.DTO.Order;
using PublicApiDTOv1Order = PublicApi.DTO.v1.Order;

namespace PublicApi.DTO.v1.Mappers
{
    public class OrderMapper : BaseMapper<PublicApiDTOv1Order.Order, BLLAppDTOOrder.Order>
    {
        public OrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static PublicApiDTOv1Order.OrderClient? MapToApiOrderClient(BLLAppDTOOrder.OrderClient? order)
        {
            if (order == null)
            {
                return null;
            }

            return new()
            {
                Id = order.Id,
                SubmitTime = order.SubmitTime,
                CompletedBy = order.CompletedBy,
                RestaurantId = order.RestaurantId,
                RestaurantName = order.RestaurantName,
                CustomerComment = order.CustomerComment,
                IsSelfPickup = order.IsSelfPickup,
                AmountPaid = order.AmountPaid,
                IsReady = order.IsReady,
                FoodCost = order.FoodCost,
                ProductsInOrder = order.ProductsInOrder.Select(ProductInOrderMapper.MapToApiClient).ToList()
            };
        }
        
        public static PublicApiDTOv1Order.OrderWorker MapToApiOrderWorker(BLLAppDTOOrder.OrderWorker order)
        {
            return new()
            {
                Id = order.Id,
                SubmitTime = order.SubmitTime,
                CompletedBy = order.CompletedBy,
                InitialCompleteTimeEstimate = order.InitialCompleteTimeEstimate,
                CustomerComment = order.CustomerComment,
                AmountPaid = order.AmountPaid,
                FoodCost = order.FoodCost,
                // IsReady = order.IsReady,
                ProductsInOrder = order.ProductsInOrder.Select(ProductInOrderMapper.MapToApiClient).ToList(),
                OrderAppUsers = order.OrderAppUsers.Select(AppUserMapper.MapToPublicApiV1).ToList()
            };
        }
        
        public static PublicApiDTOv1Order.OrderWorkerView MapToApiOrderWorkerView(BLLAppDTOOrder.OrderWorkerView order)
        {
            return new()
            {
               RestaurantId = order.RestaurantId,
               RestaurantName = order.RestaurantName,
               Orders = order.Orders?.Select(MapToApiOrderWorker).ToList()
            };
        }
    }
}