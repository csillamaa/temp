﻿using System.Linq;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLLAppDTOMenu = BLL.App.DTO.Menu;
using PublicApiV1Menu = PublicApi.DTO.v1.Menu;

namespace PublicApi.DTO.v1.Mappers
{
    public class MenuCategoryMapper: BaseMapper<PublicApiV1Menu.MenuCategory, BLLAppDTOMenu.MenuCategory>
    {
        public MenuCategoryMapper(IMapper mapper) : base(mapper)
        {
        }
        public new static PublicApiV1Menu.MenuCategory? Map(BLLAppDTOMenu.MenuCategory? category)
        {
            if (category == null)
            {
                return null;
            }
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                Position = category.Position,
                Products = category.Products?.Select(ProductMapper.MapToApiData).ToList()
            };
        }
        
        public new static BLLAppDTOMenu.MenuCategory? Map(PublicApiV1Menu.MenuCategory? category)
        {
            if (category == null)
            {
                return null;
            }
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                MenuId = category.MenuId,
                Position = category.Position,
            };
        }
        
        public static PublicApiV1Menu.MenuCategoryOnlyName MapToOnlyName(BLLAppDTOMenu.MenuCategoryOnlyName category)
        {
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                Position = category.Position
            };
        }
        
        public static PublicApiV1Menu.MenuCategoryData? Map(BLLAppDTOMenu.MenuCategoryData? category)
        {
            if (category == null)
            {
                return null;
            }
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                MenuId = category.MenuId
            };
        }
        
        public static BLLAppDTOMenu.MenuCategoryData Map(PublicApiV1Menu.MenuCategoryData category)
        {
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                MenuId = category.MenuId
            };
        }
        
        public static PublicApiV1Menu.MenuCategory MapToRegPublicApiV1(PublicApiV1Menu.MenuCategoryData category)
        {
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                MenuId = category.MenuId,
            };
        }
        
    }
}