﻿using AutoMapper;
using BLL.App.DTO.Mappers;

namespace PublicApi.DTO.v1.Mappers
{
    public class AppUserWorkRestaurantMapper :  BaseMapper<BLL.App.DTO.AppUserWorkRestaurant, DAL.App.DTO.AppUserWorkRestaurant>
    {
        public AppUserWorkRestaurantMapper(IMapper mapper) : base(mapper)
        {
        }

        public static PublicApi.DTO.v1.AppUserWorkRestaurant MapToApi(
            BLL.App.DTO.AppUserWorkRestaurant appUserWorkRestaurant)
        {
            return new()
            {
                Id = appUserWorkRestaurant.Id,
                AppUserId = appUserWorkRestaurant.AppUserId,
                RestaurantId = appUserWorkRestaurant.RestaurantId
            };
        }

       
    }
}