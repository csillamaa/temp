﻿using Domain.Base;

namespace PublicApi.DTO.v1.Menu
{
    public class MenuInRestaurant
    {
        public int RestaurantId { get; set; }

        public int MenuId { get; set; }
    }
}