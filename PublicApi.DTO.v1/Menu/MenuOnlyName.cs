﻿using Domain.Base;

namespace PublicApi.DTO.v1.Menu
{
    public class MenuOnlyName : DomainEntity
    {
        public string Name { get; set; } = default!;
    }
}