﻿using System.Collections.Generic;

namespace PublicApi.DTO.v1.Menu
{
    public class MenuEdit : MenuData
    {
        public string CompanyName { get; set; } = default!;
        
        public List<MenuCategoryOnlyName>? MenuCategories { get; set; }

        public MenuCategory? CurrentCategory { get; set; }
        
        public List<MenuOnlyName>? CompanyMenus { get; set; }
    }
}