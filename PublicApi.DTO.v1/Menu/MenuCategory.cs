﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.DTO.v1.Menu
{
    public class MenuCategory : MenuCategoryOnlyName
    {
        public int MenuId { get; set; }
        
        // [Display(ResourceType = typeof(Resources.BLL.App.DTO.Menu), Name = "MenuString")]
        // public DTO.Menu.Menu Menu { get; set; } = default!;
        //
        
        public List<Product.ProductData>? Products { get; set; }
    }
}