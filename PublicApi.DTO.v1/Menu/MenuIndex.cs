﻿using System.Collections.Generic;
using PublicApi.DTO.v1.Restaurant;

namespace PublicApi.DTO.v1.Menu
{
    public class MenuIndex : MenuOnlyName
    {
          public string CompanyName { get; set; } = default!;

          public List<RestaurantOnlyName>? RestaurantsWithTheMenu { get; set; }
    }
}