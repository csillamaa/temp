﻿using Domain.Base;

namespace PublicApi.DTO.v1.Menu
{
    public class MenuCategoryOnlyName : DomainEntity
    {
        public string Name { get; set; } = default!;
        
        public int Position { get; set; }

    }
}