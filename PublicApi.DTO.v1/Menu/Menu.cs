﻿using System;
using System.Collections.Generic;

namespace PublicApi.DTO.v1.Menu
{
    public class Menu : MenuOnlyName
    {
        public bool IsTemporary { get; set; }
        
        public DateTime? TemporaryMenuStart { get; set; }
       
        public DateTime? TemporaryMenuEnd { get; set; }
        
        // public int CompanyId { get; set; }
        
        // public Company.Company Company { get; set; } = default!;
        
        public List<MenuCategoryOnlyName>? MenuCategories { get; set; }

        public MenuCategory? CurrentCategory { get; set; }
        
        public ICollection<MenuInRestaurant>? RestaurantsWithTheMenu { get; set; }

        // public int? CurrentCategoryId { get; set; }
    }
}