﻿using Domain.Base;

namespace PublicApi.DTO.v1.Menu
{
    public class MenuCategoryData: DomainEntity
    {
        public string Name { get; set; } = default!;
        public int MenuId { get; set; }
    }
}