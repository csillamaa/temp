﻿using BLL.App.DTO.Menu;
using Contracts.BLL.App.Services;
using Contracts.BLL.Base;
using Contracts.BLL.Base.Services;
using DAL.App.DTO.Order;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;
using MenuCategory = DAL.App.DTO.Menu.MenuCategory;

namespace Contracts.BLL.App
{
    public interface IAppBLL : IBaseBLL
    {
        IBaseEntityService<BLLAppDTO.AppUserOrder, DALAppDTO.AppUserOrder> AppUserOrders { get; }
        IBaseEntityService<BLLAppDTO.Archive.ArchivedAppUserOrder, DALAppDTO.Archive.ArchivedAppUserOrder> ArchivedAppUserOrders { get; }
        IAppUserFavoriteRestaurantService AppUserFavoriteRestaurants { get; }
        IAppUserWorkRestaurantService AppUserWorkRestaurants { get; }
        
        IAppUserService AppUsers { get; }

        IBaseEntityService<BLLAppDTO.BankCard, DALAppDTO.BankCard> BankCards { get; }

        IArchivedProductInOrderService ArchivedProductsInOrder { get; }
        IArchivedSubProductFromProductService ArchivedSubProductsFromProduct { get; }
        IArchivedOrderService ArchivedOrders { get; }
        ICompanyService Companies { get; }
        IMenuService Menus { get; }
        IMenuCategoryService MenuCategories { get; }
        IMenuInRestaurantService MenusInRestaurant{ get; }
        IOrderService Orders { get; }
        IProductService Products { get; }
        IProductInOrderService ProductsInOrder { get; }
        IProductsSubCategoryService ProductsSubCategories { get; }
        IRestaurantService Restaurants { get; }
        IProductsSubCategoryProductService ProductsSubCategoryProducts{ get; }
        ISubProductFromProductService SubProductsFromProduct { get; }
        ITimeService Times { get; }
    }
}