﻿using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;

namespace Contracts.BLL.App.Services
{
    public interface IArchivedProductInOrderService: IBaseEntityService<BLLAppDTOOrderArchived.ArchivedProductInOrder, DALAppDTOOrderArchived.ArchivedProductInOrder>,
        IArchivedProductInOrderRepositoryCustom<BLLAppDTOOrderArchived.ArchivedProductInOrder>
    {
        
    }
}