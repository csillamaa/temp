﻿using System;
using System.Threading.Tasks;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace Contracts.BLL.App.Services
{
    public interface ITimeService: IBaseEntityService<BLLAppDTO.Time, DALAppDTO.Time>,
        ITimeRepositoryCustom<BLLAppDTO.Time>
    {
        Task<string?> ValidateAndSaveOpeningTime(int restaurantId, string dayString, int dayOfWeek, 
            string? timeOpen, string? timeClose);

        bool IsOpenTimeBeforeClose(DateTime openTime, DateTime closeTime);
    }
}