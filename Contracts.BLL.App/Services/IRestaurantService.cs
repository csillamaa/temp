﻿using System.Threading.Tasks;
using BLL.App.DTO.Restaurant;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;
using Restaurant = DAL.App.DTO.Restaurant.Restaurant;

namespace Contracts.BLL.App.Services
{
    public interface IRestaurantService: IBaseEntityService<global::BLL.App.DTO.Restaurant.Restaurant, Restaurant>,
        IRestaurantRepositoryCustom<global::BLL.App.DTO.Restaurant.Restaurant>
    {
        Task<RestaurantClientAllLists> GetAllRestaurantViewModel(int? userId, bool noTracking = true);
    }
}