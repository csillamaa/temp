﻿using System.Threading.Tasks;
using BLL.App.DTO.Menu;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace Contracts.BLL.App.Services
{
    public interface IMenuService: IBaseEntityService<BLLAppDTO.Menu.Menu, DALAppDTO.Menu.Menu>,
        IMenuRepositoryCustom<BLLAppDTO.Menu.Menu>
    {
        // Task<BLLAppDTO.Menu.Menu> RemoveMenu(BLLAppDTO.Menu.Menu menu, int userId = default); 
        // void AddMenu(MenuData menu);

    }
}