﻿using System.Threading.Tasks;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using BLLAppDTOArchived = BLL.App.DTO.Archive;
using DALAppDTOArchived = DAL.App.DTO.Archive;

namespace Contracts.BLL.App.Services
{
    public interface IArchivedOrderService: 
        IBaseEntityService<BLLAppDTOArchived.ArchivedOrder, DALAppDTOArchived.ArchivedOrder>,
        IArchivedOrderRepositoryCustom<BLLAppDTOArchived.ArchivedOrder>
    {
        Task<BLLAppDTOArchived.ArchivedOrderWorkerView> GetWorkerOrderView(int restaurantId, bool noTracking = true);
    }
}