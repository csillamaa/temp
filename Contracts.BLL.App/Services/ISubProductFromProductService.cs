﻿using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using BLLAppDTOOrder = BLL.App.DTO.Order;
using DALAppDTOOrder = DAL.App.DTO.Order;

namespace Contracts.BLL.App.Services
{
    public interface ISubProductFromProductService: IBaseEntityService<BLLAppDTOOrder.SubProductFromProduct, DALAppDTOOrder.SubProductFromProduct>,
        ISubProductFromProductRepositoryCustom<BLLAppDTOOrder.SubProductFromProduct>
    {
        
    }
}