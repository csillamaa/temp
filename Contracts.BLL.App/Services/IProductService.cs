﻿using System.Threading.Tasks;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Product;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace Contracts.BLL.App.Services
{
    public interface IProductService: IBaseEntityService<BLLAppDTO.Product.Product, Product>,
        IProductRepositoryCustom<BLLAppDTO.Product.Product>
    {
    }
}