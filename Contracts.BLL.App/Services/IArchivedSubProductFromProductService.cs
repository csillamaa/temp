﻿using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;

namespace Contracts.BLL.App.Services
{
    public interface IArchivedSubProductFromProductService: IBaseEntityService<BLLAppDTOOrderArchived.ArchivedSubProductFromProduct, DALAppDTOOrderArchived.ArchivedSubProductFromProduct>,
        IArchivedSubProductFromProductRepositoryCustom<BLLAppDTOOrderArchived.ArchivedSubProductFromProduct>
    {
        
    }
}