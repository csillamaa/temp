﻿using System.Threading.Tasks;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using BLLAppDTOOrder = BLL.App.DTO.Order;
using DALAppDTOOrder = DAL.App.DTO.Order;

namespace Contracts.BLL.App.Services
{
    public interface IOrderService: IBaseEntityService<BLLAppDTOOrder.Order, DALAppDTOOrder.Order>,
        IOrderRepositoryCustom<BLLAppDTOOrder.Order>
    {

        Task<BLLAppDTOOrder.OrderWorkerView> GetWorkerOrderView(int restaurantId, bool isReady, bool noTracking = true);
        
        Task<string?> AddProductToOrder(int userId, int productId, int restaurantId, int amount, int[] subProductId);

        int[] AddSubProductIdsToOneArray(int[] subCategory0, int[] subCategory1, int[] subCategory2, int[] subCategory3,
            int[] subCategory4);
    }
}