﻿#pragma warning disable 1591
using System;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.DTO.Identity;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin, Company Admin")]
    public class UserController: Controller
    {

        private readonly IAppBLL _bll;
        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;
        private readonly RoleManager<Domain.App.Identity.AppRole> _roleManager;
        
        
        public UserController(UserManager<Domain.App.Identity.AppUser> userManager, IAppBLL bll, 
            RoleManager<Domain.App.Identity.AppRole> roleManager)
        {
            _userManager = userManager;
            _bll = bll;
            _roleManager = roleManager;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _bll.AppUsers.GetAllIndexAsyncBLL(User.GetUserId()!.Value));
        }
        
        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Email, FirstName, LastName, PhoneNumber")] AppUserData appUser)
        {
            if (User.IsInRole("Company Admin"))
            {
                ModelState.Remove("Address");
            }
            if (ModelState.IsValid)
            {
                if (User.IsInRole("Company Admin"))
                {
                    appUser.CompanyId = await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value);;
                }

                var newUserId = await _bll.AppUsers.AddUserBLL(appUser);
                if (newUserId == null)
                {
                    return BadRequest();
                }
                return RedirectToAction(nameof(Edit), new {id = newUserId});;
            }

            return View();
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _bll.AppUsers.FirstOrDefaultEditAsyncBLL(id.Value);
           
      
            if (user == null || !await User.DoesUserHaveAuthorityToEdit(user.CompanyId, _userManager)) 
            {
                return NotFound();
            }
            
            return View(user);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id, Email, PhoneNumber, FirstName, LastName, NormalizedEmail, ConcurrencyStamp")] AppUserData appUser)
        {
            if (id != appUser.Id)
            {
                return NotFound();
            }
            
            if (ModelState.IsValid)
            {
                var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(id));
                if (user == null || !await User.DoesUserHaveAuthorityToEdit(user.CompanyId, _userManager))
                {
                    return NotFound();
                }
                // user.Address = appUser.Address;
                user.Email = appUser.Email;
                user.UserName = appUser.Email;
                user.FirstName = appUser.FirstName;
                user.LastName = appUser.LastName;
                user.PhoneNumber = appUser.PhoneNumber;

                await _userManager.UpdateAsync(user);
               
                return RedirectToAction(nameof(Index));
            }
     
            var bllUser = await _bll.AppUsers.FirstOrDefaultEditAsyncBLL(id);
            // appUser.Address = appUser.Address;
            appUser.Email = appUser.Email;
            appUser.FirstName = appUser.FirstName;
            appUser.LastName = appUser.LastName;
            appUser.PhoneNumber = appUser.PhoneNumber;

            return View(bllUser);
        }

        [HttpPost]
        public async Task<IActionResult> AddRole(string roleName, int id)
        {
            if (!await _roleManager.RoleExistsAsync(roleName))
            {
                return BadRequest();
            }
            
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(id));
            if (user == null)
            {
                return NotFound();
            }

            if (roleName.Equals("Admin"))
            {
                if (!User.IsInRole("Admin"))
                {
                    return Unauthorized();
                }
                await _userManager.AddToRoleAsync(user, roleName);
                return RedirectToAction(nameof(Edit), new {id});
            }
            
            if (!await User.DoesUserHaveAuthorityToEdit(user.CompanyId, _userManager))
            {
                return NotFound();
            }

            await _userManager.AddToRoleAsync(user, roleName);
            return RedirectToAction(nameof(Edit), new {id});
        }
        
        [HttpPost]
        public async Task<IActionResult> AddToRestaurant(int? restaurantId, int userId)
        {
            if (restaurantId == null)
            {
                RedirectToAction(nameof(Edit), new {id = userId});
            }
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
            if (user == null)
            {
                return NotFound();
            }
            
            if (!await User.DoesUserHaveAuthorityToEdit(user.CompanyId, _userManager))
            {
                return NotFound();
            }

            if (!await _bll.Restaurants.IsRestaurantOwnedByCompany(restaurantId!.Value, user.CompanyId!.Value))
            {
                return NotFound();
            }
            
            await _userManager.AddToRoleAsync(user, "Restaurant Worker");

            var restaurantWorker = new AppUserWorkRestaurant
            {
                AppUserId = userId,
                RestaurantId = restaurantId.Value
            };
            
            _bll.AppUserWorkRestaurants.Add(restaurantWorker);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Edit), new {id = userId});
        }
       
        [HttpPost]
        public async Task<IActionResult> RemoveAppUserFromRestaurant(int restaurantId, int userId, 
            bool rerouteToRestaurant = false, bool rerouteToCompany = false)
        {
            var companyId = await _bll.AppUsers.GetUserCompanyId(userId);
            if (companyId == null)
            {
                return BadRequest();
            }
     
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            
            var appUserWorkRestaurant = await _bll.AppUserWorkRestaurants.FirstOrDefaultAsync(restaurantId, userId);
            if (appUserWorkRestaurant == null)
            {
                return NotFound();
            }
           
            _bll.AppUserWorkRestaurants.Remove(appUserWorkRestaurant);
            await _bll.SaveChangesAsync();
            
            if (rerouteToRestaurant)
            {
                return RedirectToAction(nameof(RestaurantController.Edit), "Company",new{id = restaurantId});
            }
            if (rerouteToCompany)
            {
                return RedirectToAction(nameof(CompanyController.Edit), "Company",new{id = companyId});
            }
            return RedirectToAction(nameof(Edit), new {id = userId});
        }
        
        [HttpPost]
        public async Task<IActionResult> RemoveRole(string roleName, int id, int? companyId, bool rerouteToCompany = false)
        {            
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(id));
            if (user == null)
            {
                return NotFound();
            }

            if (roleName.Equals("Admin") && !User.IsInRole("Admin"))
            {
                return NotFound();
            }

            if (!await User.DoesUserHaveAuthorityToEdit(user.CompanyId, _userManager))
            {
                return NotFound();
            }
            
            await _userManager.RemoveFromRoleAsync(user, roleName);
            await _userManager.UpdateAsync(user);

            if (roleName.Equals("Restaurant Worker"))
            {
                await _bll.AppUserWorkRestaurants.RemoveFromAllRestaurants(id);
            }
            
            if (rerouteToCompany)
            {
                return RedirectToAction(nameof(CompanyController.Edit), "Company", new{id = companyId});
            }
            return RedirectToAction(nameof(Edit), new {id});
        }
        
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            
            if (!await User.DoesUserHaveAuthorityToEdit(user.CompanyId, _userManager))
            {
                return NotFound();
            }
            
            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(id));
   
            if (!await User.DoesUserHaveAuthorityToEdit(user.CompanyId, _userManager))
            {
                return Unauthorized();
            }
            
            await _bll.AppUserWorkRestaurants.RemoveFromAllRestaurants(id);
            await _bll.AppUserFavoriteRestaurants.RemoveAllUserFavoriteRestaurants(id);
            await _bll.AppUsers.RemoveAllUserOrders(id);
            var roles = await _userManager.GetRolesAsync(user);
            await _userManager.RemoveFromRolesAsync(user, roles);
            
            await _userManager.DeleteAsync(user);
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> AddToCompany(int userId, int? companyId)
        {
            if (companyId == null)
            {
                return RedirectToAction(nameof(Edit), new {id = userId});
            }
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
            if (user == null || user.CompanyId != null)
            {
                return NotFound();
            }

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }
    
            user.CompanyId = companyId;
            await _userManager.UpdateAsync(user);
            
            return RedirectToAction(nameof(Edit), new {id = userId});
        }
        
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UnbanUser(int id)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(id));
            if (user == null)
            {
                return NotFound();
            }
            user.LockoutEnd = null;
            await _userManager.UpdateAsync(user);

            return RedirectToAction(nameof(Edit), new {id});
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> BanUser(int id, string lockoutEnd)
        {
      
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(id));
            if (user == null)
            {
                return NotFound();
            }
            user.LockoutEnd = DateTimeOffset.Parse(lockoutEnd);
            await _userManager.UpdateAsync(user);
            
            return RedirectToAction(nameof(Edit), new {id});
        }
        
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> RemoveCompany(int id)
        {
      
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(id));
            
            user.CompanyId = null;
            
            await _bll.AppUserWorkRestaurants.RemoveFromAllRestaurants(id);
            await _bll.SaveChangesAsync();
            
            await _userManager.RemoveFromRoleAsync(user, "Company Admin");
            await _userManager.RemoveFromRoleAsync(user, "Restaurant Worker");
            await _userManager.UpdateAsync(user);
            
            return RedirectToAction(nameof(Edit), new {id});
        }
    }
}