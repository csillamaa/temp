#pragma warning disable 1591
using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin, Company Admin")]
    public class CompanyController : Controller
    {
        private readonly IAppBLL _bll;

        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;
        
        public CompanyController(UserManager<Domain.App.Identity.AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Companies.GetAllAsync());
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _bll.Companies
                .FirstOrDefaultAsync((int)id);
            if (company == null)
            {
                return NotFound();
            }

            return View(company);
        }

        [Authorize(Roles = "Admin")]

        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Name,DateJoined,Website,PhoneNumber,Email,Id")] BLL.App.DTO.Company company)
        {
            if (ModelState.IsValid)
            {
                _bll.Companies.Add(company);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(company);
        }
        
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            
            if (!await User.DoesUserHaveAuthorityToEdit(id, _userManager))
            {
                return NotFound();
            }
            
            var company = await _bll.Companies.FirstOrDefaultAsyncBLL(id.Value);
            if (company == null)
            {
                return NotFound();
            }
            
            return View(company);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind("Name,DateJoined,Website,PhoneNumber,Email,Id")] BLL.App.DTO.Company company)
        {
            if (id != company.Id)
            {
                return NotFound();
            }
            if (!await User.DoesUserHaveAuthorityToEdit(id, _userManager))
            {
                return NotFound();
            }
            var dbCompany = await _bll.Companies.FirstOrDefaultAsyncBLL(id);
            if (dbCompany == null)
            {
                return NotFound();
            }
            dbCompany.Name = company.Name;
            dbCompany.DateJoined = company.DateJoined;
            dbCompany.Website = company.Website;
            dbCompany.PhoneNumber = company.PhoneNumber;
            dbCompany.Email = company.Email;
            
            if (ModelState.IsValid)
            {
                _bll.Companies.Update(company);
                await _bll.SaveChangesAsync();
            }
            return View(dbCompany);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _bll.Companies
                .FirstOrDefaultAsync(id.Value);
            if (company == null)
            {
                return NotFound();
            }

            return View(company);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var company = await _bll.Companies.FirstOrDefaultAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            var companyUserIds = await _bll.AppUsers.GetAllCompanyUserIds(id);
            
            _bll.Companies.Remove(company);
            await _bll.SaveChangesAsync();
            
            await _bll.AppUsers.RemoveAllCompanyUsers(companyUserIds);
            
            return RedirectToAction(nameof(Index));
        }
    }
}
