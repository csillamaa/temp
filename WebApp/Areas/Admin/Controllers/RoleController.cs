#pragma warning disable 1591
using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Identity;
using BLL.App.DTO.Mappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class RoleController : Controller
    {

        private readonly AppRoleMapper _appRoleMapper;
        private readonly RoleManager<Domain.App.Identity.AppRole> _roleManager;
        
        public RoleController(RoleManager<Domain.App.Identity.AppRole> roleManager, IMapper mapper)
        {
            _roleManager = roleManager;
            _appRoleMapper = new AppRoleMapper(mapper);
        }

        public async Task<IActionResult> Index()
        {
            var roles = await _roleManager.Roles
                .Select(r =>_appRoleMapper.Map(r))
                .ToListAsync();
            
            return View(roles);
        }
        
        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(string Name)
        {
            if (ModelState.IsValid)
            {
                var role = new Domain.App.Identity.AppRole
                {
                    Name = Name
                };
                await _roleManager.CreateAsync(role);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appRole = _appRoleMapper.Map(await _roleManager.Roles
                .FirstOrDefaultAsync(r => r.Id.Equals(id)));
            if (appRole == null)
            {
                return NotFound();
            }
            return View(appRole);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Name,Id")] AppRole appRole)
        {
            if (id != appRole.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var role = await _roleManager.Roles.FirstOrDefaultAsync(r => r.Id.Equals(id));
                if (role == null)
                {
                    return NotFound();
                }
                role.Name = appRole.Name;
                await _roleManager.UpdateAsync(role);
                return RedirectToAction(nameof(Index));
            }
            return View(appRole);
        }
        
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appRole = _appRoleMapper.Map(await _roleManager.Roles
                .FirstOrDefaultAsync(r => r.Id.Equals(id)));
            if (appRole == null)
            {
                return NotFound();
            }

            return View(appRole);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var appRole = await _roleManager.Roles
                .FirstOrDefaultAsync(m => m.Id == id);
            await _roleManager.DeleteAsync(appRole);
            return RedirectToAction(nameof(Index));
        }
    }
}
