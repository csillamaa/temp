#pragma warning disable 1591
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.DTO.Mappers;
using BLL.App.DTO.Menu;
using BLL.App.DTO.Restaurant;
using Contracts.BLL.App;
using Domain.App.Identity;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin, Company Admin")]
    public class RestaurantController : Controller
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<AppUser> _userManager;
        
        public RestaurantController(UserManager<AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Restaurants.GetAllIndexAsyncBLL());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _bll.Restaurants
                .FirstOrDefaultEditAsync((int)id);
            if (restaurant == null)
            {
                return NotFound();
            }

            // return View(restaurant);
            return View();

        }

        public async Task<IActionResult> Create()
        {
            var restaurant = new RestaurantEdit();
            if (User.IsInRole("Admin"))
            {
                restaurant.AllCompanies = new SelectList(await _bll.Companies.GetAllJustNameAsyncBLL(),
                    nameof(CompanyOnlyName.Id), nameof(CompanyOnlyName.Name));
            }
            else
            {
                restaurant.CompanyId = (await _userManager.GetUserAsync(User)).CompanyId!.Value;
            }
           
            return View(restaurant);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Picture,IsPublic,DateJoined,Description,CompanyId,Address,PhoneNumber,Email,Id")] RestaurantData restaurant)
        {
            if (!await User.DoesUserHaveAuthorityToEdit(restaurant.CompanyId, _userManager))
            {
                NotFound();
            }
            
            if (ModelState.IsValid)
            {
                _bll.Restaurants.Add(RestaurantMapper.MapToBLLRegularFromBLLData(restaurant));
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            var restaurantCreate = RestaurantMapper.MapToBLLEditFromBLLData(restaurant);
            
            if (User.IsInRole("Admin"))
            {
                restaurantCreate.AllCompanies = new SelectList(await _bll.Companies.GetAllJustNameAsyncBLL(),
                    nameof(CompanyOnlyName.Id), nameof(CompanyOnlyName.Name));
            }

            return View(restaurantCreate);
        }

        public async Task<IActionResult> Edit(int? id, string? errorMessage)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _bll.Restaurants.FirstOrDefaultEditAsyncBLL(id.Value);
         
            if (restaurant == null)
            {
                return NotFound();
            }

            if (!await User.DoesUserHaveAuthorityToEdit(await _bll.Restaurants.GetRestaurantCompanyId(id.Value), _userManager))
            {
                return Unauthorized();
            }

            restaurant.ErrorMessage = errorMessage;
            
            return View(restaurant);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,  [Bind("CompanyId, Name, Picture, IsPublic, DateJoined, Description, Address, PhoneNumber, Email, Id")] RestaurantData restaurant)
        {
            if (id != restaurant.Id)
            {
                return NotFound();
            }
            
            if (!await User.DoesUserHaveAuthorityToEdit(id, _userManager))
            {
                return NotFound();
            }
            var dbRestaurantEdit = await _bll.Restaurants.FirstOrDefaultEditAsyncBLL(id);
            if (dbRestaurantEdit == null)
            {
                return NotFound();
            }
            ModelState.Remove("CompanyName");
            
            if (User.IsInRole("Company Admin"))
            {
                ModelState.Remove("DateJoined");
            }
            if (ModelState.IsValid)
            {
                _bll.Restaurants.Update(BLL.App.DTO.Mappers.RestaurantMapper.MapToBLLRegularFromBLLData(restaurant));
                await _bll.SaveChangesAsync();
                return View(dbRestaurantEdit);
            }
            
            dbRestaurantEdit.DateJoined = restaurant.DateJoined;
            dbRestaurantEdit.Name = restaurant.Name;
            dbRestaurantEdit.Address = restaurant.Address;
            dbRestaurantEdit.Description = restaurant.Description;
            dbRestaurantEdit.Email = restaurant.Email;
            dbRestaurantEdit.Picture = restaurant.Picture;
            dbRestaurantEdit.IsPublic = restaurant.IsPublic;
            dbRestaurantEdit.PhoneNumber = restaurant.PhoneNumber;

           
            return View(dbRestaurantEdit);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var restaurant = await _bll.Restaurants
                .FirstOrDefaultEditAsyncBLL((int)id);
            if (restaurant == null)
            {
                return NotFound();
            }

            if (!await User.DoesUserHaveAuthorityToEdit(restaurant.CompanyId, _userManager))
            {
                return NotFound();
            }
            return View(restaurant);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var restaurant = await _bll.Restaurants.FirstOrDefaultDeleteAsync(id);
            if (restaurant == null)
            {
                return NotFound();
            }

            if (!await User.DoesUserHaveAuthorityToEdit(restaurant.CompanyId, _userManager))
            {
                return NotFound();
            }

            await _bll.MenusInRestaurant.RemoveAllMenusFromRestaurant(id);
            
            _bll.Restaurants.Remove(restaurant);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        
        [HttpPost]
        public async Task<IActionResult> SaveOpeningTimes(int companyId, int restaurantId, string? mondayOpen, 
            string? mondayClose, string? tuesdayOpen, string? tuesdayClose, string? wednesdayOpen, string? wednesdayClose
            , string? thursdayOpen, string? thursdayClose, string? fridayOpen, string? fridayClose, string? sundayOpen, 
            string? sundayClose, string? saturdayOpen, string? saturdayClose)
        {
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }

            if (!await _bll.Restaurants.ExistsAsync(restaurantId))
            {
                return NotFound();
            }

            var errorMessage = new StringBuilder(Resources.Areas.Admin.Restaurants.TimeErrorMessage);
            
            errorMessage.Append(await _bll.Times.ValidateAndSaveOpeningTime(restaurantId, 
                Resources.BLL.App.DTO.Time.Monday,1, mondayOpen, mondayClose));
            errorMessage.Append(await _bll.Times.ValidateAndSaveOpeningTime(restaurantId, 
                Resources.BLL.App.DTO.Time.Tuesday,2, tuesdayOpen, tuesdayClose));
            errorMessage.Append(await _bll.Times.ValidateAndSaveOpeningTime(restaurantId, 
                Resources.BLL.App.DTO.Time.Wednesday, 3, wednesdayOpen, wednesdayClose));
            errorMessage.Append(await _bll.Times.ValidateAndSaveOpeningTime(restaurantId, 
                Resources.BLL.App.DTO.Time.Thursday, 4, thursdayOpen, thursdayClose));
            errorMessage.Append(await _bll.Times.ValidateAndSaveOpeningTime(restaurantId, 
                Resources.BLL.App.DTO.Time.Friday,5, fridayOpen, fridayClose));
            errorMessage.Append(await _bll.Times.ValidateAndSaveOpeningTime(restaurantId, 
                Resources.BLL.App.DTO.Time.Saturday, 6, saturdayOpen, saturdayClose));
            errorMessage.Append(await _bll.Times.ValidateAndSaveOpeningTime(restaurantId, 
                Resources.BLL.App.DTO.Time.Sunday, 0, sundayOpen, sundayClose));

            if (errorMessage.ToString().Equals(Resources.Areas.Admin.Restaurants.TimeErrorMessage))
            {
                errorMessage = null;
            }

            return RedirectToAction(nameof(Edit), new {id = restaurantId, errorMessage = errorMessage?.ToString()});
        }

        public async Task<IActionResult> AddMenu([Bind("MenuId, RestaurantId")] MenuInRestaurant menuInRestaurant)
        {
            if (!await _bll.Menus.ExistsAsync(menuInRestaurant.MenuId) || !await _bll.Restaurants.ExistsAsync(menuInRestaurant.RestaurantId))
            {
                return NotFound();
            }

            if (!await _bll.MenusInRestaurant.IsMenuAddableToRestaurant(menuInRestaurant))
            {
                return BadRequest();
            }
            
            var restaurantCompanyId = await _bll.Restaurants.GetRestaurantCompanyId(menuInRestaurant.RestaurantId);
            if (!await User.DoesUserHaveAuthorityToEdit(restaurantCompanyId, _userManager))
            {
                return Unauthorized();
            }
            
            _bll.MenusInRestaurant.Add(menuInRestaurant);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Edit), new {id = menuInRestaurant.RestaurantId});
        }
       
        public async Task<IActionResult> RemoveMenu([Bind("MenuId, RestaurantId")] MenuInRestaurant menuInRestaurant)
        {
            var restaurantCompanyId = await _bll.Restaurants.GetRestaurantCompanyId(menuInRestaurant.RestaurantId);
            
            if (!await User.DoesUserHaveAuthorityToEdit(restaurantCompanyId, _userManager))
            {
                return Unauthorized();
            }
            
            if (await _bll.MenusInRestaurant.RemoveMenuFromRestaurant(menuInRestaurant) == null)
            {
                return NotFound();
            }
            
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Edit), new {id = menuInRestaurant.RestaurantId});
        }
    }
}