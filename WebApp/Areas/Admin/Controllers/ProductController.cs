#pragma warning disable 1591
using System;
using System.Threading.Tasks;
using BLL.App.DTO.Mappers;
using BLL.App.DTO.Product;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin, Company Admin")]
    public class ProductController : Controller
    {
        private readonly IAppBLL _bll;

        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;
        
        public ProductController(UserManager<Domain.App.Identity.AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }
        
        public async Task<IActionResult> Create(int menuId)
        {
            if (!await _bll.Menus.ExistsAsync(menuId))
            {
                return NotFound();
            }
            
            var companyId = await _bll.Menus.GetMenuCompanyId(menuId);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            
            var product = new ProductCreate
            {
                MenuId = menuId,
                MenuCategories = await _bll.MenuCategories.GetAllMenuCategoriesOnlyNameBLL(menuId)
            };
            
            return View(product);
        }
        
        [HttpPost]
        public async Task<IActionResult> Create(int menuId, [Bind("Name,IsAlwaysPurchasable,Price,MenuCategoryId,Picture,Description")] ProductData productData)
        {
            var companyId = await _bll.MenuCategories.GetCategoryCompanyId(productData.MenuCategoryId);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var regBLLProduct = ProductMapper.MapToRegBLL(productData);
                regBLLProduct.Position = _bll.Products.GetProductCountInCategory(regBLLProduct.MenuCategoryId);
                
                _bll.Products.Add(regBLLProduct);
                await _bll.SaveChangesAsync();
                
                var id = _bll.Products.GetUpdatedEntityAfterSaveChanges(regBLLProduct).Id;
                return RedirectToAction(nameof(Edit), new{id});
            }
            
            return RedirectToAction(nameof(Create), new {menuId});
        }
        
        public async Task<IActionResult> Edit(int? id)
        {
            Console.WriteLine(id);
            if (id == null)
            {
                return NotFound();
            }

            var product = await _bll.Products.FirstOrDefaultEditAsyncBLL(id.Value);
            if (product == null)
            {
                return NotFound();
            }

            var companyId = await _bll.Products.GetProductCompanyId(id.Value);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            
            return View(product);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int menuId,
            [Bind("Id,Name,IsAlwaysPurchasable,Price,MenuCategoryId,Picture,Description")] ProductData productData)
        {
            var companyId = await _bll.Products.GetProductCompanyId(productData.Id);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            
            var dbProduct = await _bll.Products.FirstOrDefaultAsync(productData.Id);
            if (dbProduct == null)
            {
                return NotFound();
            }
           
            if (ModelState.IsValid)
            {
                dbProduct.Name = productData.Name;
                dbProduct.IsAlwaysPurchasable = productData.IsAlwaysPurchasable;
                dbProduct.Price = productData.Price;
                dbProduct.Picture = productData.Picture;
                dbProduct.Description = productData.Description;
                
                if (dbProduct.MenuCategoryId != productData.MenuCategoryId)
                {
                    if (!await _bll.MenuCategories.ExistsAsync(productData.MenuCategoryId))
                    {
                        return NotFound();
                    }
                    dbProduct.MenuCategoryId = productData.MenuCategoryId;
                    dbProduct.Position = _bll.Products.GetProductCountInCategory(productData.MenuCategoryId);
                    await _bll.ProductsSubCategories.MoveSubCategoriesUpWhenDelete(dbProduct.Id);
                }

                _bll.Products.Update(dbProduct);
                await _bll.SaveChangesAsync();
            }

            var productEdit = ProductMapper.MapToEditBLL(productData);
            productEdit.MenuCategories = await _bll.MenuCategories.GetAllMenuCategoriesOnlyNameBLL(menuId);
            productEdit.MenuId = menuId;

            return View(productEdit);
        }
        
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _bll.Products
                .FirstOrDefaultEditAsyncBLL(id.Value);
            if (product == null)
            {
                return NotFound();
            }

            var companyId = await _bll.Products.GetProductCompanyId(id.Value);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }

            return View(product);
        }

        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (!await _bll.Products.ExistsAsync(id))
            {
                return NotFound();
            }

            var companyId = await _bll.Products.GetProductCompanyId(id);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            
            var menuId = await _bll.Products.GetProductMenuId(id);
            
            await _bll.Products.MoveProductsUpWhenDelete(id);

            await _bll.Products.RemoveAsync(id, default);
            await _bll.SaveChangesAsync();
            
            return RedirectToAction(nameof(Edit), "Menu", new {id = menuId});
        }
        
        public async Task<IActionResult> MoveProduct(int productId, int menuId, bool moveUp)
        {
            if (!await _bll.Products.ExistsAsync(productId))
            {
                return NotFound();
            }
            
            var categoryCompanyId = await _bll.Products.GetProductCompanyId(productId);
            if (!await User.DoesUserHaveAuthorityToEdit(categoryCompanyId, _userManager))
            {
                return NotFound();
            }
         
            var result = await _bll.Products.ChangeProductPosition(productId, moveUp);
            if (!result)
            {
                return BadRequest();
            }
            
            return RedirectToAction(nameof(Edit), "Menu", new {id = menuId});
        }
    }
}
