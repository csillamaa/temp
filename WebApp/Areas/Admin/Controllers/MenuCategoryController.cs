#pragma warning disable 1591
using System;
using System.Threading.Tasks;
using BLL.App.DTO.Mappers;
using BLL.App.DTO.Menu;
using Contracts.BLL.App;
using Domain.App.Identity;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;


namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin, Company Admin")]
    public class MenuCategoryController : Controller
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<AppUser> _userManager;
        

        public MenuCategoryController(UserManager<AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name, MenuId")] MenuCategoryData category)
        {
            var companyId = await _bll.Menus.GetMenuCompanyId(category.MenuId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                NotFound();
            }
            
            if (ModelState.IsValid)
            {
                var menuCategory = MenuCategoryMapper.MapToRegBLL(category);
                menuCategory.Position = _bll.MenuCategories.GetCategoryCountInMenu(menuCategory.MenuId);
                _bll.MenuCategories.Add(menuCategory);
                await _bll.SaveChangesAsync();
            }

            return RedirectToAction(nameof(MenuController.Edit), "Menu",new {id = category.MenuId});
        }

        public async Task<IActionResult> Edit(int? id, int companyId, int menuId)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var categoryCompanyId = await _bll.MenuCategories.GetCategoryCompanyId(id.Value);

            if (!await User.DoesUserHaveAuthorityToEdit(categoryCompanyId, _userManager))
            {
                return NotFound();
            }

            var menuCategory = await _bll.MenuCategories.FirstOrDefaultEditAsyncBLL(id.Value);

            if (menuCategory == null)
            {
                return NotFound();
            }
            
            menuCategory.MenuId = menuId;
            
            return View(menuCategory);

        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id, Name, MenuId")] MenuCategoryData category)
        {
            if (id != category.Id)
            {
                return BadRequest();
            }
           
            var categoryCompanyId = await _bll.MenuCategories.GetCategoryCompanyId(id);

            if (!await User.DoesUserHaveAuthorityToEdit(categoryCompanyId, _userManager))
            {
                return NotFound();
            }
            
            var menuCategory = await _bll.MenuCategories.FirstOrDefaultEditAsyncBLL(id);
            
            if (menuCategory == null)
            {
                return NotFound();
            }

            menuCategory.Name = category.Name;
            _bll.MenuCategories.Update(MenuCategoryMapper.MapToRegBLL(menuCategory));
            await _bll.SaveChangesAsync();

            return RedirectToAction(nameof(MenuController.Edit), "Menu",new {id = category.MenuId});
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            

            var menu = await _bll.MenuCategories.FirstOrDefaultEditAsyncBLL(id.Value);
            if (menu == null)
            {
                return NotFound();
            }

            var companyId = await _bll.MenuCategories.GetCategoryCompanyId(id.Value);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }


            return View(menu);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (!await _bll.MenuCategories.ExistsAsync(id))
            {
                return NotFound();
            }

            var companyId = await _bll.MenuCategories.GetCategoryCompanyId(id);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }

            var menuId = await _bll.MenuCategories.GetCategoryMenuId(id);

            await _bll.MenuCategories.MoveCategoriesUpWhenDelete(id);

            await _bll.MenuCategories.RemoveAsync(id, default);
            await _bll.SaveChangesAsync();
            
            return RedirectToAction(nameof(MenuController.Edit), "Menu",new {id = menuId});
        }
    }
}