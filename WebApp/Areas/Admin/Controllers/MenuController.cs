#pragma warning disable 1591
using System;
using System.Threading.Tasks;
using BLL.App.DTO.Mappers;
using BLL.App.DTO.Menu;
using Contracts.BLL.App;
using Domain.App.Identity;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin, Company Admin")]
    public class MenuController : Controller
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<AppUser> _userManager;

        public MenuController(UserManager<AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _bll.Menus.GetAllIndexASyncBLL(await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value)));
        }
        

        [Authorize(Roles = "Company Admin")]
        public async Task<IActionResult> Create()
        {
            var newMenu = new MenuData
            {
                CompanyId = (await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value))!.Value
            };
            return View(newMenu);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,CompanyId")] MenuData menu)
        {
            if (!await User.DoesUserHaveAuthorityToEdit(menu.CompanyId, _userManager))
            {
                return NotFound();
            }
            
            if (ModelState.IsValid)
            {
                _bll.Menus.Add(MenuMapper.MapToRegBLL(menu));
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(menu);
        }

        public async Task<IActionResult> Edit(int? id, int? currentCategoryId)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            if (currentCategoryId != null && !await _bll.MenuCategories.ExistsAsync(currentCategoryId.Value))
            {
                return NotFound();
            }

            var menu = await _bll.Menus.FirstOrDefaultEditAsyncBLL(id.Value, currentCategoryId);
         
            if (menu == null)
            {
                return NotFound();
            }

            if (!await User.DoesUserHaveAuthorityToEdit(menu.CompanyId, _userManager))
            {
                return NotFound();
            }
            
            return View(menu);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, int? currentCategoryId, string name)
        {
            var menu = await _bll.Menus.FirstOrDefaultAsync(id);

            if (menu == null)
            {
                return NotFound();
            }

            if (!await User.DoesUserHaveAuthorityToEdit(menu.CompanyId, _userManager))
            {
                return NotFound();
            }

            menu.Name = name;
            _bll.Menus.Update(menu);
            await _bll.SaveChangesAsync();

            return RedirectToAction(nameof(Edit), new {id, currentCategoryId});
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var menu = await _bll.Menus.FirstOrDefaultAsync(id.Value);
            if (menu == null)
            {
                return NotFound();
            }
            
            if (!await User.DoesUserHaveAuthorityToEdit(menu.CompanyId, _userManager))
            {
                return NotFound();
            }


            return View(menu);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {

            var menu = await _bll.Menus.FirstOrDefaultAsync(id);
            if (menu == null)
            {
                return NotFound();
            }

            if (!await User.DoesUserHaveAuthorityToEdit(menu.CompanyId, _userManager))
            {
                return NotFound();
            }

            _bll.Menus.Remove(menu);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> MoveCategory(int categoryId, int menuId, bool moveUp)
        {
            if (!await _bll.MenuCategories.ExistsAsync(categoryId))
            {
                return NotFound();
            }
            var categoryCompanyId = await _bll.MenuCategories.GetCategoryCompanyId(categoryId);
            if (!await User.DoesUserHaveAuthorityToEdit(categoryCompanyId, _userManager))
            {
                return NotFound();
            }
         
            var result = await _bll.MenuCategories.ChangeCategoryPosition(categoryId, moveUp);
            if (!result)
            {
                return BadRequest();
            }
            
            return RedirectToAction(nameof(Edit), new {id = menuId});

        }
        
        public async Task<IActionResult> CopyOtherMenuItems(int currentMenuId, int otherMenuId)
        {
            if (!await _bll.Menus.ExistsAsync(currentMenuId) && !await _bll.Menus.ExistsAsync(otherMenuId))
            {
                return NotFound();
            }
            
            var menuCompanyId = await _bll.Menus.GetMenuCompanyId(currentMenuId);
            var otherMenuCompanyId = await _bll.Menus.GetMenuCompanyId(otherMenuId);
            if (menuCompanyId != otherMenuCompanyId || !await User.DoesUserHaveAuthorityToEdit(menuCompanyId, _userManager))
            {
                return NotFound();
            }

            await _bll.Menus.CopyMenuItemsToOtherMenu(currentMenuId, otherMenuId);
            
            return RedirectToAction(nameof(Edit), new {id = currentMenuId});
        }
    }
}