#pragma warning disable 1591
using System;
using System.Threading.Tasks;
using BLL.App.DTO.Mappers;
using BLL.App.DTO.Product;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin, Company Admin")]
    public class SubProductController : Controller
    {
        private readonly IAppBLL _bll;

        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;
        
        public SubProductController(UserManager<Domain.App.Identity.AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }
        
        public async Task<IActionResult> Create(int subCategoryId)
        {
            
            if (!await _bll.ProductsSubCategories.ExistsAsync(subCategoryId))
            {
                return NotFound();
            }
            
            var companyId = await _bll.ProductsSubCategories.GetSubCategoryCompanyId(subCategoryId);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }

            var productId = await _bll.ProductsSubCategories.GetProductId(subCategoryId);

            var subProduct = new ProductsSubCategoryProductData
            {
                ProductsSubCategoryId = subCategoryId,
                ProductId = productId
            };
            
            return View(subProduct);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("Name,ProductId,ProductsSubCategoryId,Price,IsSelectedOnDefault,Description")]
            ProductsSubCategoryProductData subProductData)
        {
            foreach (var state in ModelState)
            {
                Console.WriteLine(state.Key + state.Value.Errors.Count);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var productCompanyId = await _bll.Products.GetProductCompanyId(subProductData.ProductId);
            var subCategoryCompanyId =
                await _bll.ProductsSubCategories.GetSubCategoryCompanyId(subProductData.ProductsSubCategoryId);

            if (productCompanyId != subCategoryCompanyId || !await User.DoesUserHaveAuthorityToEdit(productCompanyId, _userManager))
            {
                return Unauthorized();
            }


            var subProduct = ProductsSubCategoryProductMapper.MapToRegBLL(subProductData);
            subProduct.Position = _bll.ProductsSubCategoryProducts
                .GetProductSubCategoryProductCountInSubCategory(subProductData.ProductsSubCategoryId);
            
            _bll.ProductsSubCategoryProducts.Add(subProduct);
            await _bll.SaveChangesAsync();

            return RedirectToAction(nameof(Edit), "Product", new {id = subProductData.ProductId});
        }
        
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyId = await _bll.ProductsSubCategoryProducts.GetProductsSubCategoryProductCompanyId(id.Value);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            
            var subProduct = await _bll.ProductsSubCategoryProducts.FirstOrDefaultEditAsyncBLL(id.Value);
            if (subProduct == null)
            {
                return NotFound();
            }
            
            return View(subProduct);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("Id,Name,ProductId,ProductsSubCategoryId,Price,IsSelectedOnDefault")]
            ProductsSubCategoryProductData subProductData)
        {
            var companyId = await _bll.Products.GetProductCompanyId(subProductData.ProductId);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
         
            var subProduct = await _bll.ProductsSubCategoryProducts.FirstOrDefaultAsync(subProductData.Id);
            if (subProduct == null)
            {
                return NotFound();
            }
            subProduct.Name = subProductData.Name;
            subProduct.Price = subProductData.Price;
            subProduct.IsSelectedOnDefault = subProductData.IsSelectedOnDefault;
            subProduct.Description = subProductData.Description;
            
            _bll.ProductsSubCategoryProducts.Update(subProduct);
            await _bll.SaveChangesAsync();
            
            return RedirectToAction(nameof(Edit), "Product", new {id = subProductData.ProductId});
        }
        
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var companyId = await _bll.ProductsSubCategoryProducts.GetProductsSubCategoryProductCompanyId(id.Value);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            var subProduct = await _bll.ProductsSubCategoryProducts
                .FirstOrDefaultEditAsyncBLL(id.Value);
            if (subProduct == null)
            {
                return NotFound();
            }

            return View(subProduct);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var subProduct = await _bll.ProductsSubCategoryProducts.FirstOrDefaultAsync(id);
            if (subProduct == null)
            {
                return NotFound();
            }
            
            var companyId = await _bll.ProductsSubCategoryProducts.GetProductsSubCategoryProductCompanyId(id);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            var productId = await _bll.ProductsSubCategoryProducts.GetProductId(id);
          
            await _bll.ProductsSubCategoryProducts.MoveSubCategoryProductsUpWhenDelete(id);

            _bll.ProductsSubCategoryProducts.Remove(subProduct);
            await _bll.SaveChangesAsync();
            
            return RedirectToAction(nameof(Edit), "Product", new {id = productId});
        }

        public async Task<IActionResult> MoveSubProduct(int id, bool moveUp)
        {
            var companyId = await _bll.ProductsSubCategoryProducts.GetProductsSubCategoryProductCompanyId(id);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }

            if (!await _bll.ProductsSubCategoryProducts.ExistsAsync(id))
            {
                return NotFound();
            }

            if (!await _bll.ProductsSubCategoryProducts.ChangeSubCategoryProductPosition(id, moveUp))
            {
                return BadRequest();
            }
            
            var productId = await _bll.ProductsSubCategoryProducts.GetProductId(id);
            
            return RedirectToAction(nameof(Edit), "Product", new {id = productId});
        }
    }
}
