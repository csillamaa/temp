#pragma warning disable 1591
using System;
using System.Threading.Tasks;
using BLL.App.DTO.Mappers;
using BLL.App.DTO.Product;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin, Company Admin")]
    public class SubCategoryController : Controller
    {
        private readonly IAppBLL _bll;

        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;
        
        public SubCategoryController(UserManager<Domain.App.Identity.AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }
        
        public async Task<IActionResult> Create(int productId)
        {
            
            if (!await _bll.Products.ExistsAsync(productId))
            {
                return NotFound();
            }
            
            var companyId = await _bll.Products.GetProductCompanyId(productId);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            
            var subCategory = new ProductsSubCategoryData
            {
                ProductId = productId
            };
            
            return View(subCategory);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("Name,ProductId,MaxItemCount, MandatoryItemCount")] ProductsSubCategoryData subCategoryData)
        {
            foreach (var state in ModelState)
            {
                Console.WriteLine(state.Key + state.Value.Errors.Count);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var companyId = await _bll.Products.GetProductCompanyId(subCategoryData.ProductId);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }

            var subCategory = ProductsSubCategoryMapper.MapToRegBLL(subCategoryData);
            subCategory.Position = _bll.ProductsSubCategories.GetSubCategoryCountInProduct(subCategoryData.ProductId);
            _bll.ProductsSubCategories.Add(subCategory);
            await _bll.SaveChangesAsync();

            return RedirectToAction(nameof(Edit), "Product", new {id = subCategoryData.ProductId});
        }
        
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyId = await _bll.ProductsSubCategories.GetSubCategoryCompanyId(id.Value);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            
            var subCategory = await _bll.ProductsSubCategories.FirstOrDefaultEditAsyncBLL(id.Value);
            if (subCategory == null)
            {
                return NotFound();
            }
            
            return View(subCategory);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("Id,Name,ProductId,MaxItemCount,MandatoryItemCount")]
            ProductsSubCategoryData subCategoryData)
        {
            var companyId = await _bll.ProductsSubCategories.GetSubCategoryCompanyId(subCategoryData.Id);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
         
            var subCategory = await _bll.ProductsSubCategories.FirstOrDefaultAsync(subCategoryData.Id);
            if (subCategory == null)
            {
                return NotFound();
            }
            subCategory.Name = subCategoryData.Name;
            subCategory.MaxItemCount = subCategoryData.MaxItemCount;
            subCategory.MandatoryItemCount = subCategoryData.MandatoryItemCount;
            
            // TODO: VALIDATE DATA

            _bll.ProductsSubCategories.Update(subCategory);
            await _bll.SaveChangesAsync();
            
            return RedirectToAction(nameof(Edit), "Product", new {id = subCategory.ProductId});
        }
        
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var companyId = await _bll.ProductsSubCategories.GetSubCategoryCompanyId(id.Value);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            var subCategory = await _bll.ProductsSubCategories
                .FirstOrDefaultEditAsyncBLL(id.Value);
            if (subCategory == null)
            {
                return NotFound();
            }

            return View(subCategory);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var subCategory = await _bll.ProductsSubCategories.FirstOrDefaultAsync(id);
            if (subCategory == null)
            {
                return NotFound();
            }
            
            var companyId = await _bll.ProductsSubCategories.GetSubCategoryCompanyId(id);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            var productId = await _bll.ProductsSubCategories.GetProductId(id);
          
            await _bll.ProductsSubCategories.MoveSubCategoriesUpWhenDelete(id);

            _bll.ProductsSubCategories.Remove(subCategory);
            await _bll.SaveChangesAsync();
            
            return RedirectToAction(nameof(Edit), "Product", new {id = productId});
        }
        
        public async Task<IActionResult> MoveCategory(int id, bool moveUp)
        {
            var companyId = await _bll.ProductsSubCategories.GetSubCategoryCompanyId(id);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }

            if (!await _bll.ProductsSubCategories.ExistsAsync(id))
            {
                return NotFound();
            }

            if (!await _bll.ProductsSubCategories.ChangeSubCategoryPosition(id, moveUp))
            {
                return BadRequest();
            }
            
            var productId = await _bll.ProductsSubCategories.GetProductId(id);
            
            return RedirectToAction(nameof(Edit), "Product", new {id = productId});
        }
    }
}
