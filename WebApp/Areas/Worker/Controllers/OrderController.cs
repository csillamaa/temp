﻿#pragma warning disable 1591
using System;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Domain.App.Identity;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.Worker.Controllers
{
    [Area("Worker")]
    [Authorize(Roles = "Restaurant Worker")]
    public class OrderController: Controller
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<AppUser> _userManager;
        

        public OrderController(UserManager<AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }
        
        public async Task<IActionResult> Index(int restaurantId)
        {
            if (!await _bll.Restaurants.ExistsAsync(restaurantId))
            {
                return NotFound();
            }

            var companyId = await _bll.Restaurants.GetRestaurantCompanyId(restaurantId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            var orders = await _bll.Orders.GetWorkerOrderView(restaurantId, false);

            return View(orders);
        }
        
        public async Task<IActionResult> Ready(int restaurantId)
        {
            if (!await _bll.Restaurants.ExistsAsync(restaurantId))
            {
                return NotFound();
            }
            
            var companyId = await _bll.Restaurants.GetRestaurantCompanyId(restaurantId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            
            var orders = await _bll.Orders.GetWorkerOrderView(restaurantId, true);
            
            return View(orders);
        }
        
        public async Task<IActionResult> Completed(int restaurantId)
        {
            if (!await _bll.Restaurants.ExistsAsync(restaurantId))
            {
                return NotFound();
            }
            
            var companyId = await _bll.Restaurants.GetRestaurantCompanyId(restaurantId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            
            var orders = await _bll.ArchivedOrders.GetWorkerOrderView(restaurantId);
            
            return View(orders);
        }
        
        public async Task<IActionResult> ChangeCompleteTime(int orderId, string newCompleteTime)
        {
            if (!await _bll.Orders.ExistsAsync(orderId))
            {
                return NotFound();
            }
            
            var companyId = await _bll.Orders.GetCompanyId(orderId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }
            var restaurantId = await _bll.Orders.GetRestaurantId(orderId);
            
            if (!DateTime.TryParse(newCompleteTime, out var newCompleteDateTime))
            {
                return RedirectToAction(nameof(Index), new {restaurantId});
            }
            
            if (!await _bll.Orders.ChangeOrderCompleteTime(orderId, newCompleteDateTime))
            {
                return NotFound();
            }
            
            return RedirectToAction(nameof(Index), new {restaurantId});
        }
        
        public async Task<IActionResult> ChangeOrderToReady(int orderId)
        {
            if (!await _bll.Orders.ExistsAsync(orderId))
            {
                return NotFound();
            }
            
            var companyId = await _bll.Orders.GetCompanyId(orderId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }

            if (!await _bll.Orders.ChangeOrderToReady(orderId))
            {
                return NotFound();
            }
            
            var restaurantId = await _bll.Orders.GetRestaurantId(orderId);
            
            return RedirectToAction(nameof(Index), new {restaurantId});
        }
        
        public async Task<IActionResult> ArchiveOrder(int orderId)
        {
            if (!await _bll.Orders.ExistsAsync(orderId))
            {
                return NotFound();
            }
            
            var companyId = await _bll.Orders.GetCompanyId(orderId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }

            var restaurantId = await _bll.Orders.GetRestaurantId(orderId);
            
            if (!await _bll.Orders.ArchiveOrder(orderId))
            {
                return BadRequest();
            }

            return RedirectToAction(nameof(Ready), new {restaurantId});
        }
        
        public async Task<IActionResult> AddCommentToArchivedOrder(int orderId, string comment)
        {
            if (!await _bll.ArchivedOrders.ExistsAsync(orderId))
            {
                return NotFound();
            }

            var companyId = await _bll.ArchivedOrders.GetCompanyId(orderId);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return NotFound();
            }

            await _bll.ArchivedOrders.AddCommentToOrder(orderId, comment, false);
            
            var restaurantId = await _bll.ArchivedOrders.GetRestaurantId(orderId);

            return RedirectToAction(nameof(Completed), new {restaurantId});
        }
    }
    
}