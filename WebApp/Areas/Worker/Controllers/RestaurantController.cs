﻿#pragma warning disable 1591
using System.Threading.Tasks;
using Contracts.BLL.App;
using Domain.App.Identity;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.Worker.Controllers
{
    [Area("Worker")]
    [Authorize(Roles = "Restaurant Worker")]
    public class RestaurantController: Controller
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<AppUser> _userManager;
        

        public RestaurantController(UserManager<AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }
        
        public async Task<IActionResult> Index()
        {
            var restaurants = await _bll.Restaurants.GetAllRestaurantsAppUserWorksInAsyncBLL(User.GetUserId()!.Value);
            if (restaurants == null)
            {
                return NotFound();
            }
            if (restaurants.Count == 1)
            {
                return RedirectToAction(nameof(Index), "Order", new {restaurantId = restaurants[0].Id});
            }
            
            return View(restaurants);
        }
    }
    
}