﻿#pragma warning disable 1591
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Contracts.DAL.App;
using Domain.App;
using Domain.App.Identity;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Areas.CompanyAdmin.Controllers
{
        [Area("CompanyAdmin")]
        [Authorize(Roles = "Company Admin")]
        public class RestaurantController: Controller
        {
            private readonly IAppBLL _bll;

            private readonly UserManager<AppUser> _userManager;
        

            public RestaurantController(UserManager<AppUser> userManager, IAppBLL bll)
            {
                _userManager = userManager;
                _bll = bll;
            }
        
            public async Task<IActionResult> Index()
            {
                var companyId = await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value);
                var restaurants = await _bll.Restaurants.GetAllCompanyRestaurantsBLL(companyId!.Value);

                return View(restaurants);

            }
        }
}