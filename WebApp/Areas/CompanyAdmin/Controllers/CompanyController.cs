﻿#pragma warning disable 1591
using System.Threading.Tasks;
using Contracts.BLL.App;
using Domain.App.Identity;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.CompanyAdmin.Controllers
{
    [Area("CompanyAdmin")]
    [Authorize(Roles = "Company Admin")]
    public class CompanyController: Controller
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<AppUser> _userManager;
        

        public CompanyController(UserManager<AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }
        
        public async Task<IActionResult> Index()
        {
            var companyId = await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value);
            var company = await _bll.Companies.FirstOrDefaultAsyncBLL(companyId!.Value);
            return View(company);
        }
    }
    
}