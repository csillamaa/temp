﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Domain.App.Identity;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using PublicApi.DTO.v1.Restaurant;

namespace WebApp.ApiControllers.Worker
{
    /// <summary>
    /// Represent a RESTful Worker's Restaurant service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Restaurant Worker", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class WorkerRestaurantController: ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<AppUser> _userManager;
        

        /// <summary>
        /// Constructor for controller.
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="bll"></param>
        public WorkerRestaurantController(UserManager<AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }
        
        /// <summary>
        /// Gets all restaurant's user works in.
        /// </summary>
        /// <returns>The user's restaurants.</returns>
        /// <response code="200">The user's restaurants were retrieved.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The user doesn't work in any restaurant.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<RestaurantOnlyName>),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<RestaurantOnlyName>>> GetAllRestaurants()
        {
            var restaurants = await _bll.Restaurants.GetAllRestaurantsAppUserWorksInAsyncBLL(User.GetUserId()!.Value);
            if (restaurants == null)
            {
                return NotFound();
            }
            
            return restaurants.Select(RestaurantMapper.MapToApi).ToList();
        }
    }
    
}