﻿using System;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Domain.App.Identity;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Archive;
using PublicApi.DTO.v1.Mappers;
using PublicApi.DTO.v1.Order;

namespace WebApp.ApiControllers.Worker
{
    /// <summary>
    /// Represent a RESTful Worker's Order service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Restaurant Worker", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class WorkerOrderController: ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<AppUser> _userManager;
        

        /// <summary>
        /// Constructor for controller/
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="bll"></param>
        public WorkerOrderController(UserManager<AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }
        
        /// <summary>
        /// Gets all restaurant's active orders.
        /// </summary>
        /// <param name="restaurantId">Id of the restaurant, int.</param>
        /// <returns>Restaurant's active orders.</returns>
        /// <response code="200">The restaurant's active orders were retrieved.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The restaurant doesn't exist.</response>
        [HttpGet("{restaurantId:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(OrderWorkerView),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<OrderWorkerView>> GetActiveOrders(int restaurantId)
        {
            if (!await _bll.Restaurants.ExistsAsync(restaurantId))
            {
                return NotFound();
            }

            var companyId = await _bll.Restaurants.GetRestaurantCompanyId(restaurantId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }
            var orders = await _bll.Orders.GetWorkerOrderView(restaurantId, false);

            return OrderMapper.MapToApiOrderWorkerView(orders);
        }
        
        /// <summary>
        /// Gets all restaurant's ready orders.
        /// </summary>
        /// <param name="restaurantId">Id of the restaurant, int.</param>
        /// <returns>Restaurant's ready orders.</returns>
        /// <response code="200">The restaurant's ready orders were retrieved.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The restaurant doesn't exist.</response>
        [HttpGet("{restaurantId:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(OrderWorkerView),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<OrderWorkerView>> GetReadyOrders(int restaurantId)
        {
            if (!await _bll.Restaurants.ExistsAsync(restaurantId))
            {
                return NotFound();
            }
            
            var companyId = await _bll.Restaurants.GetRestaurantCompanyId(restaurantId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }
            
            var orders = await _bll.Orders.GetWorkerOrderView( restaurantId, true);
            
            return OrderMapper.MapToApiOrderWorkerView(orders);

        }
        
        /// <summary>
        /// Gets all restaurant's completed orders.
        /// </summary>
        /// <param name="restaurantId">Id of the restaurant, int.</param>
        /// <returns>Restaurant's completed orders.</returns>
        /// <response code="200">The restaurant's completed orders were retrieved.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The restaurant doesn't exist.</response>
        [HttpGet("{restaurantId:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ArchivedOrderWorkerView),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ArchivedOrderWorkerView>> GetCompletedOrders(int restaurantId)
        {
            if (!await _bll.Restaurants.ExistsAsync(restaurantId))
            {
                return NotFound();
            }
            
            var companyId = await _bll.Restaurants.GetRestaurantCompanyId(restaurantId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }
            
            var orders = await _bll.ArchivedOrders.GetWorkerOrderView(restaurantId);
            
            return ArchivedOrderMapper.MapToApiOrderWorkerView(orders);
        }
        
        /// <summary>
        /// Changes order completed by time.
        /// </summary>
        /// <param name="orderId">Id of the order, int.</param>
        /// <param name="newCompleteTime">New completed by time.</param>
        /// <returns></returns>
        /// <response code="204">The order's completed by time was changed.</response>
        /// <response code="400">The newCompletedTime string wasn't in datetime parsable format or the order's complete
        /// time couldn't be changed because it is already ready or it isn't submitted by the user yet.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The order doesn't exist.</response>
        [HttpPost("{orderId:int}, {newCompleteTime}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ChangeCompleteTime(int orderId, string newCompleteTime)
        {
            if (!await _bll.Orders.ExistsAsync(orderId))
            {
                return NotFound();
            }
            
            var companyId = await _bll.Orders.GetCompanyId(orderId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }
            
            if (!DateTime.TryParse(newCompleteTime, out var newCompleteDateTime))
            {
               return BadRequest();
            }
            
            if (!await _bll.Orders.ChangeOrderCompleteTime(orderId, newCompleteDateTime))
            {
                return BadRequest();
            }
            
            return NoContent();
        }
        
        /// <summary>
        /// Changes order to ready state.
        /// </summary>
        /// <param name="orderId">Id of the order, int.</param>
        /// <returns></returns>
        /// <response code="204">The order was changed to ready state.</response>
        /// <response code="400">The order couldn't be switched to ready state because it is already ready
        /// or it isn't submitted by the user yet.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The order doesn't exist.</response>
        [HttpPost("{orderId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ChangeOrderToReady(int orderId)
        {
            if (!await _bll.Orders.ExistsAsync(orderId))
            {
                return NotFound();
            }
            
            var companyId = await _bll.Orders.GetCompanyId(orderId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }

            if (!await _bll.Orders.ChangeOrderToReady(orderId))
            {
                return BadRequest();
            }
            
            return NoContent();
        }
        
        /// <summary>
        /// After order is ready and given to the client, then this method will archive the order
        /// and delete it from order table.
        /// </summary>
        /// <param name="orderId">Id of the order, int.</param>
        /// <returns></returns>
        /// <response code="204">The order was archived.</response>
        /// <response code="400">The order couldn't be archived because it isn't submitted by the user yet
        /// or it isn't ready yet.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The order doesn't exist.</response>
        [HttpPost("{orderId:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ArchiveOrder(int orderId)
        {
            if (!await _bll.Orders.ExistsAsync(orderId))
            {
                return NotFound();
            }
            
            var companyId = await _bll.Orders.GetCompanyId(orderId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }

            if (!await _bll.Orders.ArchiveOrder(orderId))
            {
                return BadRequest();
            }

            return NoContent();
        }
        
        /// <summary>
        /// Adds a comment by restaurant worker to a completed order.
        /// </summary>
        /// <param name="orderId">Id of the order, int.</param>
        /// <param name="comment">Comment string.</param>
        /// <returns></returns>
        /// <response code="204">The comment was added to the order..</response>
        /// <response code="400">The order couldn't be archived because it isn't submitted by the user yet
        /// or it isn't ready yet.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The order doesn't exist.</response>
        [HttpPost("{orderId:int}, {comment}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> AddCommentToArchivedOrder(int orderId, string comment)
        {
            if (!await _bll.ArchivedOrders.ExistsAsync(orderId))
            {
                return NotFound();
            }

            var companyId = await _bll.ArchivedOrders.GetCompanyId(orderId);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }

            await _bll.ArchivedOrders.AddCommentToOrder(orderId, comment, false);
            
            return NoContent();
        }
        
    }
}