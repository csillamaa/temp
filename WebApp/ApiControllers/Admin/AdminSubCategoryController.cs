using System;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using PublicApi.DTO.v1.Product;

namespace WebApp.ApiControllers.Admin
{
    /// <summary>
    /// Represent a RESTful SubCategory service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Admin, Company Admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdminSubCategoryController : ControllerBase
    {
        private readonly IAppBLL _bll;

        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;
        
        /// <summary>
        /// Constructor for controller.
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="bll"></param>
        public AdminSubCategoryController(UserManager<Domain.App.Identity.AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }
        
        
        /// <summary>
        /// Creates a sub category.
        /// </summary>
        /// <param name="subCategoryData">Sub category with data that is being cteated.</param>
        /// <returns></returns>
        /// <response code="201">The sub category was created.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ProductsSubCategoryData), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<ProductsSubCategoryData>> PostSubCategory(ProductsSubCategoryData subCategoryData)
        {
            var companyId = await _bll.Products.GetProductCompanyId(subCategoryData.ProductId);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }

            var subCategory = ProductsSubCategoryMapper.MapToRegBLL(subCategoryData);
            subCategory.Position = _bll.ProductsSubCategories.GetSubCategoryCountInProduct(subCategoryData.ProductId);
            _bll.ProductsSubCategories.Add(subCategory);
            await _bll.SaveChangesAsync();
            
            subCategory.Id = _bll.ProductsSubCategories.GetUpdatedEntityAfterSaveChanges(subCategory).Id;

            return CreatedAtAction(nameof(GetSubCategory), new {id = subCategory.Id}, subCategory);
        }
        
        /// <summary>
        /// Gets a single sub category.
        /// </summary>
        /// <param name="id">Id of the sub product being retrieved.</param>
        /// <returns>The retrieved sub category.</returns>
        /// <response code="200">The sub category was retrieved.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The sub category doesn't exist.</response>
        [HttpGet("{id:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ProductsSubCategoryData), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)] 
        [ProducesResponseType(StatusCodes.Status404NotFound)] 
        public async Task<ActionResult<ProductsSubCategoryData>> GetSubCategory(int id)
        {
            var companyId = await _bll.ProductsSubCategories.GetSubCategoryCompanyId(id);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }
            
            var subCategory = await _bll.ProductsSubCategories.FirstOrDefaultEditAsyncBLL(id);
            if (subCategory == null)
            {
                return NotFound();
            }
            
            return ProductsSubCategoryMapper.Map(subCategory)!;
        }
        
        /// <summary>
        /// Updates a sub category.
        /// </summary>
        /// <param name="subCategoryData">Sub category with data being updated.</param>
        /// <returns></returns>
        /// <response code="204">The sub category was updated.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The sub category doesn't exist.</response>
        [HttpPut]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)] 
        [ProducesResponseType(StatusCodes.Status404NotFound)] 
        public async Task<ActionResult> PutSubCategory(ProductsSubCategoryData subCategoryData)
        {
            var companyId = await _bll.ProductsSubCategories.GetSubCategoryCompanyId(subCategoryData.Id);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }
         
            var subCategory = await _bll.ProductsSubCategories.FirstOrDefaultAsync(subCategoryData.Id);
            if (subCategory == null)
            {
                return NotFound();
            }
            
            subCategory.Name = subCategoryData.Name;
            subCategory.MaxItemCount = subCategoryData.MaxItemCount;
            subCategory.MandatoryItemCount = subCategoryData.MandatoryItemCount;
            
            // TODO: VALIDATE DATA

            _bll.ProductsSubCategories.Update(subCategory);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
       

        /// <summary>
        /// Deletes a sub category.
        /// </summary>
        /// <param name="id">Id of the sub product being deleted.</param>
        /// <returns></returns>
        /// <response code="204">The sub category was deleted.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The sub category doesn't exist.</response>
        [HttpDelete("{id:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)] 
        [ProducesResponseType(StatusCodes.Status404NotFound)] 
        public async Task<ActionResult> DeleteSubCategory(int id)
        {
            var subCategory = await _bll.ProductsSubCategories.FirstOrDefaultAsync(id);
            if (subCategory == null)
            {
                return NotFound();
            }
            
            var companyId = await _bll.ProductsSubCategories.GetSubCategoryCompanyId(id);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }
            

            await _bll.ProductsSubCategories.MoveSubCategoriesUpWhenDelete(id);

            _bll.ProductsSubCategories.Remove(subCategory);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
        
        /// <summary>
        /// Moves a sub category up or down on product.
        /// </summary>
        /// <param name="id">Id of the sub category being moved, int.</param>
        /// <param name="moveUp">Direction of the sub category being moved: true - up, false - down.</param>
        /// <returns></returns>
        /// <response code="204">The sub category was moved.</response>
        /// <response code="400">The sub category can't be moved that way.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The sub category doesn't exist.</response>
        [HttpPost("{id:int}, {moveUp:bool}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)] 
        [ProducesResponseType(StatusCodes.Status404NotFound)] 
        public async Task<ActionResult> MoveCategory(int id, bool moveUp)
        {
            var companyId = await _bll.ProductsSubCategories.GetSubCategoryCompanyId(id);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }

            if (!await _bll.ProductsSubCategories.ExistsAsync(id))
            {
                return NotFound();
            }

            if (!await _bll.ProductsSubCategories.ChangeSubCategoryPosition(id, moveUp))
            {
                return BadRequest();
            }
     
            return NoContent();
        }
    }
}
