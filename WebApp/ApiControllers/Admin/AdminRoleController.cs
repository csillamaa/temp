using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;

namespace WebApp.ApiControllers.Admin
{ 
    /// <summary>
    /// Represent a RESTful admin role service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Microsoft.AspNetCore.Authorization.Authorize(Roles = "Admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdminRolesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly AppRoleMapper _roleMapper;
        private readonly RoleManager<Domain.App.Identity.AppRole> _roleManager;

        /// <summary>
        /// Constructor for controller.
        /// </summary>
        /// <param name="roleManager"></param>
        /// <param name="mapper"></param>
        public AdminRolesController(RoleManager<Domain.App.Identity.AppRole> roleManager, IMapper mapper)
        {
            _roleManager = roleManager;
            _mapper = mapper;
            _roleMapper = new AppRoleMapper(mapper);
        }

   
        /// <summary>
        /// Gets all existing user roles.
        /// </summary>
        /// <returns>List of roles.</returns>
        /// <response code="200">Roles were retrieved.</response>
        /// <response code="401">Logged in user doesn't have authority to access this info.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<PublicApi.DTO.v1.Identity.AppRole>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<List<PublicApi.DTO.v1.Identity.AppRole>> GetRoles()
        {
            var roles = await _roleManager.Roles
                .Select(role => _roleMapper.Map(role))
                .ToListAsync();
            return roles!;
        }

        /// <summary>
        /// Gets a single role.
        /// </summary>
        /// <param name="id">Id of the role, int.</param>
        /// <returns></returns>
        /// <response code="200">The role was retrieved.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The role doesn't exist.</response>
        [HttpGet("{id:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(PublicApi.DTO.v1.Identity.AppRole), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        
        public async Task<ActionResult<PublicApi.DTO.v1.Identity.AppRole>> GetAppRole(int id)
        {
            var appRole = _roleMapper.Map(await _roleManager.Roles
                .FirstOrDefaultAsync(r => r.Id.Equals(id)));
            if (appRole == null)
            {
                return NotFound();
            }

            return appRole;
        }
        
        /// <summary>
        /// Updates a role.
        /// </summary>
        /// <param name="id">The id of the role being updated.</param>
        /// <param name="appRole">The new role data.</param>
        /// <returns>The updated role.</returns>
        /// <response code="204">The role was updated.</response>
        /// <response code="400">The parameter id doesn't match the role's id or a role with the same name exist already.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The role doesn't exist.</response>
        [HttpPut("{id:int}")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        public async Task<IActionResult> PutAppRole(int id, PublicApi.DTO.v1.Identity.AppRole appRole)
        {
            if (id != appRole.Id)
            {
                return BadRequest();
            }

            if (await _roleManager.RoleExistsAsync(appRole.Name))
            {
                return BadRequest();
            }
            var role = await _roleManager.Roles.FirstOrDefaultAsync(r => r.Id.Equals(id));
            if (role == null)
            {
                return NotFound();
            }
            role.Name = appRole.Name;
            await _roleManager.UpdateAsync(role);
        
            return NoContent();
        }
        
        /// <summary>
        /// Creates a new role.
        /// </summary>
        /// <param name="appRole">The new role's data</param>
        /// <returns>The created role.</returns>
        /// <response code="201">The role was created.</response>
        /// <response code="400">A role with the same name already exists.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(PublicApi.DTO.v1.Identity.AppRole), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<PublicApi.DTO.v1.Identity.AppRole>> PostAppRole(PublicApi.DTO.v1.Identity.AppRole appRole)
        {
            if (await _roleManager.RoleExistsAsync(appRole.Name))
            {
                return BadRequest();
            }
            var role = new Domain.App.Identity.AppRole
            {
                Name = appRole.Name
            };
            await _roleManager.CreateAsync(role);

            appRole.Id = role.Id;
            
            return CreatedAtAction("GetAppRole", new { id = appRole.Id }, appRole);
        }

        /// <summary>
        /// Deletes a role.
        /// </summary>
        /// <param name="id">The id of the role being deleted.</param>
        /// <returns></returns>
        /// <response code="204">The role was deleted.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The role doesn't exist.</response>
        [HttpDelete("{id:int}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteAppRole(int id)
        {
            var appRole = await _roleManager.Roles.FirstOrDefaultAsync(r => r.Id.Equals(id));
            if (appRole == null)
            {
                return NotFound();
            }

            await _roleManager.DeleteAsync(appRole);

            return NoContent();
        }
    }
}