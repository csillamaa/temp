using System;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using PublicApi.DTO.v1.Product;

namespace WebApp.ApiControllers.Admin
{
    /// <summary>
    /// Represent a RESTful Product service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Admin, Company Admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdminProductController : ControllerBase
    {
        private readonly IAppBLL _bll;

        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;
        
        /// <summary>
        /// Constructor for controller.
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="bll"></param>
        public AdminProductController(UserManager<Domain.App.Identity.AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }
        
        
        /// <summary>
        /// Creates a product.
        /// </summary>
        /// <param name="productData">Product with data being created.</param>
        /// <returns></returns>
        /// <response code="201">Product was created.</response>
        /// <response code="401">Logged in user doesn't have authority to access this info.</response>
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ProductData), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<ProductData>> PostProduct(ProductData productData)
        {
            var companyId = await _bll.MenuCategories.GetCategoryCompanyId(productData.MenuCategoryId);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }

            var regBLLProduct = ProductMapper.MapToRegBLL(productData);
            
            regBLLProduct.Position = _bll.Products.GetProductCountInCategory(regBLLProduct.MenuCategoryId);
            
            _bll.Products.Add(regBLLProduct);
            await _bll.SaveChangesAsync();
            
            regBLLProduct.Id = _bll.Products.GetUpdatedEntityAfterSaveChanges(regBLLProduct).Id;

            return CreatedAtAction(nameof(GetProduct), new {id = regBLLProduct.Id}, regBLLProduct);
        }
        
        /// <summary>
        /// Gets a single product.
        /// </summary>
        /// <param name="id">Id of the product being retrieved, int.</param>
        /// <returns>The retrieved product.</returns>
        /// <response code="200">Product was retrieved.</response>
        /// <response code="401">Logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The product doesn't exist.</response>
        [HttpGet("{id:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ProductEdit), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ProductEdit>> GetProduct(int id)
        {
            var product = await _bll.Products.FirstOrDefaultEditAsyncBLL(id);
                
            if (product == null)
            {
                return NotFound();
            }

            var companyId = await _bll.Products.GetProductCompanyId(id);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }
            
            return ProductMapper.MapToEdit(product);
        }
        
        /// <summary>
        /// Updates a product.
        /// </summary>
        /// <param name="productData">Product with data being updated.</param>
        /// <returns></returns>
        /// <response code="204">Product was updated.</response>
        /// <response code="401">Logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The product or the new assigned product's menu category doesn't exist.</response>
        [HttpPut]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutProduct(ProductData productData)
        {
            var dbProduct = await _bll.Products.FirstOrDefaultAsync(productData.Id);
            
            if (dbProduct == null)
            {
                return NotFound();
            }
            
            var companyId = await _bll.Products.GetProductCompanyId(productData.Id);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }
            
            dbProduct.Name = productData.Name;
            dbProduct.IsAlwaysPurchasable = productData.IsAlwaysPurchasable;
            dbProduct.Price = productData.Price;
            dbProduct.Picture = productData.Picture;
            dbProduct.Description = productData.Description;
            
            if (dbProduct.MenuCategoryId != productData.MenuCategoryId)
            {
                if (!await _bll.MenuCategories.ExistsAsync(productData.MenuCategoryId))
                {
                    return NotFound();
                }
                dbProduct.MenuCategoryId = productData.MenuCategoryId;
                dbProduct.Position = _bll.Products.GetProductCountInCategory(productData.MenuCategoryId);
                
                await _bll.ProductsSubCategories.MoveSubCategoriesUpWhenDelete(dbProduct.Id);
            }
            
            _bll.Products.Update(dbProduct);
            await _bll.SaveChangesAsync();
            
            return NoContent();
        }


        /// <summary>
        /// Deletes a product.
        /// </summary>
        /// <param name="id">Id of the product being deleted, int.</param>
        /// <returns></returns>
        /// <response code="204">Product was deleted.</response>
        /// <response code="401">Logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The product doesn't exist.</response>
        [HttpDelete("{id:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteProduct(int id)
        {
            if (!await _bll.Products.ExistsAsync(id))
            {
                return NotFound();
            }

            var companyId = await _bll.Products.GetProductCompanyId(id);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }
            
            await _bll.Products.MoveProductsUpWhenDelete(id);

            await _bll.Products.RemoveAsync(id, default);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
        
        /// <summary>
        /// Moves a product in menu category.
        /// </summary>
        /// <param name="id">Id of the product being moved, int.</param>
        /// <param name="moveUp">Direction of the product being moved: true - up, false - down</param>
        /// <returns></returns>
        /// <response code="204">Product was moved.</response>
        /// <response code="400">The product can't be moved that way.</response>
        /// <response code="401">Logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The product doesn't exist.</response>
        [HttpPost("{id:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)] 
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> MoveProduct(int id, bool moveUp)
        {
            if (!await _bll.Products.ExistsAsync(id))
            {
                return NotFound();
            }
            
            var categoryCompanyId = await _bll.Products.GetProductCompanyId(id);
            
            if (!await User.DoesUserHaveAuthorityToEdit(categoryCompanyId, _userManager))
            {
                return Unauthorized();
            }
            
            if (!await _bll.Products.ChangeProductPosition(id, moveUp))
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
