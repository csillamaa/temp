﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Identity;
using PublicApi.DTO.v1.Mappers;

namespace WebApp.ApiControllers.Admin
{
    /// <summary>
    /// Represent a RESTful admin users service.
    /// </summary>
    [ApiVersion("1.0")]
    // ReSharper disable once RouteTemplates.RouteParameterConstraintNotResolved
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Admin, Company Admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdminUserController: ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;
        private readonly RoleManager<Domain.App.Identity.AppRole> _roleManager;

       /// <summary>
       /// Constructor for controller.
       /// </summary>
       /// <param name="bll"></param>
       /// <param name="userManager"></param>
       /// <param name="roleManager"></param>
        public AdminUserController(IAppBLL bll, UserManager<Domain.App.Identity.AppUser> userManager, 
            RoleManager<Domain.App.Identity.AppRole> roleManager)
        {
            _bll = bll;
            _userManager = userManager;
            _roleManager = roleManager;
        }
        
        /// <summary>
        /// Gets all the users that the logged in user has authority over.
        /// </summary>
        /// <returns>List of users.</returns>
        /// <response code="200">Users were retrieved.</response>
        /// <response code="401">Logged in user doesn't have authority to view these users.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<AppUserIndex>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<List<AppUserIndex>> GetUsers()
        {
            var users = await _bll.AppUsers.GetAllIndexAsyncBLL(User.GetUserId()!.Value);

            return users.Select(AppUserMapper.MapToPublicApiV1).ToList();
        }

        /// <summary>
        /// Gets a single user.
        /// </summary>
        /// <param name="id">Id of the user being retrieved, int.</param>
        /// <returns>One user with all his roles, company, work restaurants and roles.</returns>
        /// <response code="200">User was retrieved.</response>
        /// <response code="401">Logged in user doesn't have authority to view this user.</response>
        /// <response code="404">The user does not exist.</response>
        [HttpGet("{id:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(AppUserEdit), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AppUserEdit>> GetUser(int id)
        {
            var user = await _bll.AppUsers.FirstOrDefaultEditAsyncBLL(id);

            if (user == null) 
            {
                return NotFound();
            }

            if (!await User.DoesUserHaveAuthorityToEdit(user.CompanyId, _userManager))
            {
                return Unauthorized();
            }

            var apiUser = AppUserMapper.MapToPublicApiV1(user);
            
            return apiUser;
        }
        
        /// <summary>
        /// Updates users data.
        /// </summary>
        /// <param name="id">Id of the user being changed, int.</param>
        /// <param name="appUser">User that is being changed with new data.</param>
        /// <returns>The updated user.</returns>
        /// <response code="200">User was updated.</response>
        /// <response code="400">User's id and parameter "id" didn't match.</response>
        /// <response code="401">Logged in user doesn't have authority to edit this user.</response>
        /// <response code="404">The user doesn't exist.</response>
        [HttpPut("{id:int}")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(AppUserEdit), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutUser(int id, AppUserData appUser)
        {
            if (id != appUser.Id)
            {
                return BadRequest();
            }
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(id));
            
            if (user == null)
            {
                return NotFound();
            }

            if (!await User.DoesUserHaveAuthorityToEdit(user.CompanyId, _userManager))
            {
                return Unauthorized();
            }
       
            // TODO check if data is valid
            // user.Address = appUser.Address;
            user.Email = appUser.Email;
            user.UserName = appUser.Email;
            user.FirstName = appUser.FirstName;
            user.LastName = appUser.LastName;
            user.PhoneNumber = appUser.PhoneNumber;
            await _userManager.UpdateAsync(user);
        
            return NoContent();
        }
        
        /// <summary>
        /// Creates a new user.
        /// </summary>
        /// <param name="appUser">User that is being created.</param>
        /// <returns>The Created user.</returns>
        /// <response code="201">User was created.</response>
        /// <response code="400">User couldn't be created.</response>
        /// <response code="401">Logged in user doesn't have authority to create a new user.</response>
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(AppUserData), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<AppUserData>> PostUser(AppUserData appUser)
        {
            if (User.IsInRole("Company Admin"))
            {
                appUser.CompanyId = await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value);
            }

            var bllAppUser = AppUserMapper.MapToBLL(appUser);

            var newUserId = await _bll.AppUsers.AddUserBLL(bllAppUser);
            
            if (newUserId == null)
            {
                return BadRequest();
            }

            appUser.Id = newUserId.Value;
            
            return CreatedAtAction("GetUser", new { id = appUser.Id }, appUser);
        }
        

        /// <summary>
        /// Deletes a user.
        /// </summary>
        /// <param name="id">Id of user being deleted, int.</param>
        /// <returns></returns>
        /// <response code="200">User was deleted.</response>
        /// <response code="401">Logged in user doesn't have the authority to delete this user.</response>
        /// <response code="404">The user doesn't exist.</response>
        [HttpDelete("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(r => r.Id.Equals(id));
            if (user == null)
            {
                return NotFound();
            }

            if (!await User.DoesUserHaveAuthorityToEdit(await _bll.AppUsers.GetUserCompanyId(id), _userManager))
            {
                return Unauthorized();
            }
            
            await _bll.AppUserWorkRestaurants.RemoveFromAllRestaurants(id);
            await _bll.AppUserFavoriteRestaurants.RemoveAllUserFavoriteRestaurants(id);
            await _bll.AppUsers.RemoveAllUserOrders(id);
            var roles = await _userManager.GetRolesAsync(user);
            await _userManager.RemoveFromRolesAsync(user, roles);
            
            await _userManager.DeleteAsync(user);
        
            return NoContent();
        }
        
        /// <summary>
        /// Adds a user to a role.
        /// </summary>
        /// <param name="roleName">Name of the role you want the user to be added to.</param>
        /// <param name="userId">Id of the user being added to the role, int.</param>
        /// <returns></returns>
        /// <response code="204">The user was successfully added to the role.</response>
        /// <response code="400">The role does not exist.</response>
        /// <response code="401">The logged in user doesn't have the authority to add this user to that role.</response>
        /// <response code="404">The user or role doesn't exist.</response>
        [HttpPost("{userId:int}, {roleName}")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> AddRole(int userId, string roleName)
        {
            if (!await _roleManager.RoleExistsAsync(roleName))
            {
                return NotFound();
            }
            
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
            
            if (user == null)
            {
                return NotFound();
            }

            if (roleName.Equals("Admin"))
            {
                if (!User.IsInRole("Admin"))
                {
                    return Unauthorized();
                }
                await _userManager.AddToRoleAsync(user, roleName);
                return NoContent();
            }

            if (user.CompanyId == null)
            {
                return BadRequest();
            }
            
            if (!await User.DoesUserHaveAuthorityToEdit(user.CompanyId, _userManager))
            {
                return Unauthorized();
            }

            await _userManager.AddToRoleAsync(user, roleName);
            
            return NoContent();
        }
        
        /// <summary>
        /// Removes a user from a role.
        /// </summary>
        /// <param name="roleName">Name of the role you want the user to be removed from.</param>
        /// <param name="userId">Id of the user that is being removed from a role, int.</param>
        /// <returns></returns>
        /// <response code="204">The user was successfully removed from the role.</response>
        /// <response code="400">The user wasn't in the role that it was tried to be removed from.</response>
        /// <response code="401">The logged in user doesn't have the authority to remove this user form that role.</response>
        /// <response code="404">The user or role doesn't exist.</response>
        [HttpPost("{userId:int}, {roleName}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RemoveRole(string roleName, int userId)
        {      
            if (!await _roleManager.RoleExistsAsync(roleName))
            {
                return BadRequest();
            }
            
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
            if (user == null)
            {
                return NotFound();
            }
        
            if (roleName.Equals("Admin") && !User.IsInRole("Admin"))
            {
                return Unauthorized();
            }
        
            if (!await User.DoesUserHaveAuthorityToEdit(user.CompanyId, _userManager))
            {
                return Unauthorized();
            }

            if (!await _userManager.IsInRoleAsync(user, roleName))
            {
                return BadRequest();
            }
            
            await _userManager.RemoveFromRoleAsync(user, roleName);
            await _userManager.UpdateAsync(user);
        
            if (roleName.Equals("Restaurant Worker"))
            {
                await _bll.AppUserWorkRestaurants.RemoveFromAllRestaurants(userId);
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a user to a restaurant as a worker.
        /// </summary>
        /// <param name="userId">Id of the user being added to the restaurant, int.</param>
        /// <param name="restaurantId">Id of the restaurant the user is being added to, int.</param>
        /// <returns></returns>
        /// <response code="204">The user was successfully added to the restaurant.</response>
        /// <response code="400">The restaurant doesn't belong to the user's company.</response>
        /// <response code="401">The logged in user doesn't have the authority to add this user to that restaurant.</response>
        /// <response code="404">The user or restaurant doesn't exist.</response>
        [HttpPost("{userId:int}, {restaurantId:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> AddUserToRestaurant(int userId, int restaurantId)
        {
            if (!await _bll.Restaurants.ExistsAsync(restaurantId))
            {
                return NotFound();
            }
            
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
            if (user == null)
            {
                return NotFound();
            }
            
            if (!await User.DoesUserHaveAuthorityToEdit(user.CompanyId, _userManager))
            {
                return Unauthorized();
            }
        
            if (!await _bll.Restaurants.IsRestaurantOwnedByCompany(restaurantId, user.CompanyId!.Value))
            {
                return BadRequest();
            }
            
            await _userManager.AddToRoleAsync(user, "Restaurant Worker");
        
            var restaurantWorker = new BLL.App.DTO.AppUserWorkRestaurant
            {
                AppUserId = userId,
                RestaurantId = restaurantId
            };
            
            _bll.AppUserWorkRestaurants.Add(restaurantWorker);
            await _bll.SaveChangesAsync();
        
            return NoContent();
        }
        /// <summary>
        /// Removes a user from a restaurant.
        /// </summary>
        /// <param name="restaurantId">Id of the restaurant the user is being removed from, int.</param>
        /// <param name="userId">Id of the user being removed from a restaurant, int.</param>
        /// <returns></returns>
        /// <response code="204">The user was successfully removed from the restaurant.</response>
        /// <response code="400">The user isn't any companies user..</response>
        /// <response code="401">The logged in user doesn't have the authority to remove this from that restaurant.</response>
        /// <response code="404">The user or restaurant doesn't exist or the user doesn't work in that restaurant.</response>
        [HttpPost("{userId:int}, {restaurantId:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RemoveUserFromRestaurant(int userId, int restaurantId)
        {
            var companyId = await _bll.AppUsers.GetUserCompanyId(userId);
            if (companyId == null)
            {
                return BadRequest();
            }
         
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }
        
            var appUserWorkRestaurant = await _bll.AppUserWorkRestaurants.FirstOrDefaultAsync(restaurantId, userId);
            if (appUserWorkRestaurant == null)
            {
                return NotFound();
            }
         
            _bll.AppUserWorkRestaurants.Remove(appUserWorkRestaurant);
            await _bll.SaveChangesAsync();
        
            return NoContent();
        }
        
        /// <summary>
        /// Adds user to a company.
        /// </summary>
        /// <param name="userId">Id of the user being added to the company, int.</param>
        /// <param name="companyId">If of the company the user is being added to, int.</param>
        /// <returns></returns>
        /// <response code="204">The user was added to the company.</response>
        /// <response code="400">The user already has a company.</response>
        /// <response code="401">The logged in user doesn't have the authority to add a user to a company.</response>
        /// <response code="404">The user or company doesn't exist.</response>
        [Authorize(Roles = "Admin")]
        [HttpPost("{userId:int}, {companyId:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> AddUserToCompany(int userId, int companyId)
        {
            if (!await _bll.Companies.ExistsAsync(companyId))
            {
                return NotFound();
            }
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
            if (user == null)
            {
                return NotFound();
            }
        
            if (user.CompanyId != null)
            {
                return BadRequest();
            }
            
            user.CompanyId = companyId;
            await _userManager.UpdateAsync(user);
            
            return NoContent();
        }
        
        /// <summary>
        /// Removes a user from a company.
        /// </summary>
        /// <param name="userId">Id of the user being from the the company, int.</param>
        /// <returns></returns>
        /// <response code="204">The user was removed from the company.</response>
        /// <response code="400">The user doesn't have a company.</response>
        /// <response code="401">The logged in user doesn't have the authority to remove a user from a company.</response>
        /// <response code="404">The user doesn't exist.</response>
        [Authorize(Roles = "Admin")]
        [HttpPost("{userId:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RemoveUsersCompany(int userId)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
        
            if (user == null)
            {
                return NotFound();
            }
        
            if (user.CompanyId == null)
            {
                return BadRequest();
            }
            
            user.CompanyId = null;
            
            await _bll.AppUserWorkRestaurants.RemoveFromAllRestaurants(userId);
            await _bll.SaveChangesAsync();
            
            await _userManager.RemoveFromRoleAsync(user, "Company Admin");
            await _userManager.RemoveFromRoleAsync(user, "Restaurant Worker");
            await _userManager.UpdateAsync(user);
            
            return NoContent();
        }
        
        /// <summary>
        /// Removes ban on a user.
        /// </summary>
        /// <param name="userId">Id of the user being unbanned, int.</param>
        /// <returns></returns>
        /// <response code="204">The user was unbanned</response>
        /// <response code="401">The logged in user doesn't have the authority to unban a user.</response>
        /// <response code="404">The user doesn't exist.</response>
        [Authorize(Roles = "Admin")]
        [HttpPost("{userId:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UnBanUser(int userId)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
            if (user == null)
            {
                return NotFound();
            }
            user.LockoutEnd = null;
            await _userManager.UpdateAsync(user);
        
            return NoContent();
        }
        
        /// <summary>
        /// Bans a user.
        /// </summary>
        /// <param name="id">Id of the user being banned, int.</param>
        /// <param name="lockoutEnd">The UTC date that the user is being banned until.
        /// yyyy-mm-ddThh:mm:ss</param>
        /// <returns></returns>
        /// <response code="204">The user was banned</response>
        /// <response code="401">The logged in user doesn't have the authority to ban a user.</response>
        /// <response code="404">The user doesn't exist.</response>
        [Authorize(Roles = "Admin")]
        [HttpPost("{id:int}, {lockoutEnd}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> BanUser(int id, DateTimeOffset lockoutEnd)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(id));
            if (user == null)
            {
                return NotFound();
            }

            user.LockoutEnd = lockoutEnd;
            await _userManager.UpdateAsync(user);
            
            return NoContent();
        }
    }
}