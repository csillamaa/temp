using System;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Domain.App.Identity;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using PublicApi.DTO.v1.Menu;

namespace WebApp.ApiControllers.Admin
{
    /// <summary>
    /// Represent a RESTful admin menu category service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin, Company Admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdminMenuCategoryController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<AppUser> _userManager;

        /// <summary>
        /// Constructor for controller.
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="bll"></param>
        public AdminMenuCategoryController(UserManager<AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }
        
        /// <summary>
        /// Creates a new category for a menu.
        /// </summary>
        /// <param name="category">Object with category name and menuId, int.</param>
        /// <returns>The created menu category.</returns>
        /// <response code="201">The menu category was created.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(MenuCategoryOnlyName), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<MenuCategoryOnlyName>> PostMenuCategory(MenuCategoryData category)
        {

            var companyId = await _bll.Menus.GetMenuCompanyId(category.MenuId);
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }

            var menuCategory = MenuCategoryMapper.Map(MenuCategoryMapper.MapToRegPublicApiV1(category))!;
            menuCategory.Position = _bll.MenuCategories.GetCategoryCountInMenu(menuCategory.MenuId);

            _bll.MenuCategories.Add(menuCategory);
            await _bll.SaveChangesAsync();
            
            var id = _bll.MenuCategories.GetUpdatedEntityAfterSaveChanges(menuCategory).Id;
 
            menuCategory.Id = id;

            var publicApiV1Menu = MenuCategoryMapper.MapToOnlyName(menuCategory);

            return CreatedAtAction("GetMenuCategory", new{id},publicApiV1Menu );
        }

        /// <summary>
        /// Gets a single menu category.
        /// </summary>
        /// <param name="id">Id of the category, int.</param>
        /// <returns>Returns the retrieved menu category.</returns>
        /// <response code="200">The menu category was retrieved.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The menu category doesn't exist.</response>
        [HttpGet("{id:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(MenuCategoryData), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<MenuCategoryData>> GetMenuCategory(int id)
        {

            var menuCategory = await _bll.MenuCategories.FirstOrDefaultEditAsyncBLL(id);

            if (menuCategory == null)
            {
                return NotFound();
            }

            var categoryCompanyId = await _bll.MenuCategories.GetCategoryCompanyId(id);

            if (!await User.DoesUserHaveAuthorityToEdit(categoryCompanyId, _userManager))
            {
                return Unauthorized();
            }
            
            return MenuCategoryMapper.Map(menuCategory)!;
        }

        /// <summary>
        /// Updates a menu category's data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        /// <response code="200">The menu category was retrieved.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The menu category doesn't exist.</response>
        [HttpPut("{id:int}")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutMenuCategory(int id, [Bind("Id, Name, MenuId")] MenuCategoryData category)
        {
            if (id != category.Id)
            {
                return BadRequest();
            }
           
            var categoryCompanyId = await _bll.MenuCategories.GetCategoryCompanyId(id);

            if (!await User.DoesUserHaveAuthorityToEdit(categoryCompanyId, _userManager))
            {
                return Unauthorized();
            }
            
            var menuCategory = await _bll.MenuCategories.FirstOrDefaultEditAsyncBLL(id);
            
            if (menuCategory == null)
            {
                return NotFound();
            }

            menuCategory.Name = category.Name;
            _bll.MenuCategories.Update(BLL.App.DTO.Mappers.MenuCategoryMapper.MapToRegBLL(menuCategory));
            await _bll.SaveChangesAsync();

            return NoContent();
        }
        
        /// <summary>
        /// Deletes a menu category.
        /// </summary>
        /// <param name="id">Id of the menu category, int.</param>
        /// <returns></returns>
        /// <response code="204">The menu category was deleted.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The menu category doesn't exist.</response>
        [HttpDelete("{id:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteMenuCategory(int id)
        {
            var menuCategory = await _bll.MenuCategories.FirstOrDefaultEditAsyncBLL(id);
            if (menuCategory == null)
            {
                return NotFound();
            }

            var companyId = await _bll.MenuCategories.GetCategoryCompanyId(id);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }

            await _bll.MenuCategories.MoveCategoriesUpWhenDelete(id);
            await _bll.MenuCategories.RemoveAsync(id, default);
            await _bll.SaveChangesAsync();
            return NoContent();

        }
    }
}