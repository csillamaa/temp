using System;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using PublicApi.DTO.v1.Product;

namespace WebApp.ApiControllers.Admin
{
    /// <summary>
    /// Represent a RESTful SubProduct service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Admin, Company Admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdminSubProductController : ControllerBase
    {
        private readonly IAppBLL _bll;

        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;
        
        /// <summary>
        /// Constructor for controller.
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="bll"></param>
        public AdminSubProductController(UserManager<Domain.App.Identity.AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }

        /// <summary>
        /// Creates a new sub product.
        /// </summary>
        /// <param name="subProductData">Sub product data that is being created.</param>
        /// <returns>The created sub product</returns>
        /// <response code="201">The sub product was created.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ProductsSubCategoryProduct), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<ProductsSubCategoryProduct>> PostSubProduct(ProductsSubCategoryProductData subProductData)
        {
            var subCategoryCompanyId =
                await _bll.ProductsSubCategories.GetSubCategoryCompanyId(subProductData.ProductsSubCategoryId);

            if (!await User.DoesUserHaveAuthorityToEdit(subCategoryCompanyId, _userManager))
            {
                return Unauthorized();
            }

            var subProduct = ProductsSubCategoryProductMapper.MapToRegBLL(subProductData);
            subProduct.Position = _bll.ProductsSubCategoryProducts
                .GetProductSubCategoryProductCountInSubCategory(subProductData.ProductsSubCategoryId);
            
            _bll.ProductsSubCategoryProducts.Add(subProduct);
            await _bll.SaveChangesAsync();
            
            subProduct.Id = _bll.ProductsSubCategoryProducts.GetUpdatedEntityAfterSaveChanges(subProduct).Id;

            return CreatedAtAction(nameof(GetSubProduct), new {id = subProduct.Id}, subProduct);
        }
        
        /// <summary>
        /// Gets a single sub product.
        /// </summary>
        /// <param name="id">Id of the sub product, int.</param>
        /// <returns>Sub product that was retrieved with info that is needed to edit it.</returns>
        /// <response code="200">The sub product was retrieved.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The sub product doesn't exist.</response>
        [HttpGet("{id:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ProductsSubCategoryProductData), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)] 
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ProductsSubCategoryProductData>> GetSubProduct(int id)
        {
            var subProduct = await _bll.ProductsSubCategoryProducts.FirstOrDefaultEditAsyncBLL(id);
            if (subProduct == null)
            {
                return NotFound();
            }
            
            var companyId = await _bll.ProductsSubCategoryProducts.GetProductsSubCategoryProductCompanyId(id);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }

            return ProductsSubCategoryProductMapper.Map(subProduct)!;
        }
        
        /// <summary>
        /// Updates a sub product.
        /// </summary>
        /// <param name="subProductData">Sub product with data being updated.</param>
        /// <returns></returns>
        /// <response code="204">The sub product was updated.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The sub product doesn't exist.</response>
        [HttpPut]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)] 
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutSubProduct(ProductsSubCategoryProductData subProductData)
        {
            var companyId = await _bll.ProductsSubCategories.GetSubCategoryCompanyId(subProductData.ProductsSubCategoryId);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }
         
            var subProduct = await _bll.ProductsSubCategoryProducts.FirstOrDefaultAsync(subProductData.Id);
            if (subProduct == null)
            {
                return NotFound();
            }
            subProduct.Name = subProductData.Name;
            subProduct.Price = subProductData.Price;
            subProduct.IsSelectedOnDefault = subProductData.IsSelectedOnDefault;
            subProduct.Description = subProductData.Description;
            
            _bll.ProductsSubCategoryProducts.Update(subProduct);
            await _bll.SaveChangesAsync();
            
            return NoContent();

        }

        /// <summary>
        /// Deletes a sub product.
        /// </summary>
        /// <param name="id">Id of th sub product being deleted, int.</param>
        /// <returns></returns>
        /// <response code="204">The sub product was deleted.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The sub product doesn't exist.</response>
        [HttpDelete("{id:int}")]
        [ProducesResponseType( StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)] 
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteSubProduct(int id)
        {
            var subProduct = await _bll.ProductsSubCategoryProducts.FirstOrDefaultAsync(id);
            if (subProduct == null)
            {
                return NotFound();
            }
            
            var companyId = await _bll.ProductsSubCategoryProducts.GetProductsSubCategoryProductCompanyId(id);
            
            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }

            await _bll.ProductsSubCategoryProducts.MoveSubCategoryProductsUpWhenDelete(id);

            _bll.ProductsSubCategoryProducts.Remove(subProduct);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Moves a sub product up or down in sub category.
        /// </summary>
        /// <param name="id">Id of the sub product being moved.</param>
        /// <param name="moveUp">Direction of the sub product move: true - up, false - down.</param>
        /// <returns></returns>
        /// <response code="204">The sub product was moved.</response>
        /// <response code="400">The sub product can't be moved that way.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The sub product doesn't exist.</response>
        [HttpPost("{id:int}, {moveUp:bool}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)] 
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> MoveSubProduct(int id, bool moveUp)
        {
            var companyId = await _bll.ProductsSubCategoryProducts.GetProductsSubCategoryProductCompanyId(id);

            if (!await User.DoesUserHaveAuthorityToEdit(companyId, _userManager))
            {
                return Unauthorized();
            }

            if (!await _bll.ProductsSubCategoryProducts.ExistsAsync(id))
            {
                return NotFound();
            }

            if (!await _bll.ProductsSubCategoryProducts.ChangeSubCategoryProductPosition(id, moveUp))
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
