using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1;
using PublicApi.DTO.v1.Company;
using PublicApi.DTO.v1.Mappers;

namespace WebApp.ApiControllers.Admin
{
    /// <summary>
    /// Represent a RESTful admin company service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin, Company Admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdminCompaniesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;

        /// <summary>
        /// Constructor for controller.
        /// </summary>
        /// <param name="bll"></param>
        /// <param name="userManager"></param>
        public AdminCompaniesController(IAppBLL bll, UserManager<Domain.App.Identity.AppUser> userManager)
        {
            _bll = bll;
            _userManager = userManager;
        }

   
        /// <summary>
        /// Gets all companies.
        /// </summary>
        /// <returns>List of companies.</returns>
        /// <response code="200">Companies were retrieved.</response>
        /// <response code="401">Logged in user doesn't have authority to access this info.</response>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<Company>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IEnumerable<Company>> GetCompanies()
        {
            var companies = await _bll.Companies.GetAllAsync();
            return companies.Select(CompanyMapper.MapToPublicApiV1);
        }

        /// <summary>
        /// Gets a single company.
        /// </summary>
        /// <param name="id">Id of the company, int.</param>
        /// <returns></returns>
        /// <response code="200">The company was retrieved.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The company doesn't exist.</response>
        [HttpGet("{id:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(Company), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Company>> GetCompany(int id)
        {
            var company = await _bll.Companies.FirstOrDefaultAsyncBLL(id);
                
            if (company == null)
            {
                return NotFound();
            }
            
            if (!await User.DoesUserHaveAuthorityToEdit(id, _userManager))
            {
                return Unauthorized();
            }

            return CompanyMapper.MapToPublicApiV1(company);
        }
        
        /// <summary>
        /// Updates a company.
        /// </summary>
        /// <param name="id">The id of the company being updated.</param>
        /// <param name="company">The new company's data.</param>
        /// <returns>The updated company.</returns>
        /// <response code="204">The company was updated.</response>
        /// <response code="400">The parameter id doesn't match the company's id.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The company doesn't exist.</response>
        [HttpPut("{id:int}")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutCompany(int id, CompanyData company)
        {
            if (id != company.Id)
            {
                return BadRequest();
            }
            if (!await User.DoesUserHaveAuthorityToEdit(id, _userManager))
            {
                return Unauthorized();
            }

            var dbCompany = await _bll.Companies.FirstOrDefaultAsyncBLL(id);
            if (dbCompany == null)
            {
                return NotFound();
            }
            
            // TODO check if data is valid
            
            dbCompany.Name = company.Name;
            dbCompany.Website = company.Website;
            dbCompany.PhoneNumber = company.PhoneNumber;
            dbCompany.Email = company.Email;
            if (User.IsInRole("Admin"))
            {
                dbCompany.DateJoined = company.DateJoined;
            }
            
            _bll.Companies.Update(dbCompany);
            await _bll.SaveChangesAsync();
        
            return NoContent();
        }
        
        /// <summary>
        /// Creates a new company.
        /// </summary>
        /// <param name="company">The new company's data</param>
        /// <returns>The created company.</returns>
        /// <response code="201">The company was created.</response>
        /// <response code="400">A company with the same name already exists.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(Company), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Company>> PostCompany(CompanyData company)
        {
            // TODO: check if data is valid.
            var bllCompany = CompanyMapper.MapToBLL(company);
            
            _bll.Companies.Add(bllCompany);
            await _bll.SaveChangesAsync();

            company.Id = _bll.Companies.GetUpdatedEntityAfterSaveChanges(bllCompany).Id;
            
            return CreatedAtAction("GetCompany", new { id = company.Id }, company);
        }

        /// <summary>
        /// Deletes a company.
        /// </summary>
        /// <param name="id">The id of the company being deleted.</param>
        /// <returns></returns>
        /// <response code="204">The company was deleted.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The company doesn't exist.</response>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteCompany(int id)
        {
            var company = await _bll.Companies.FirstOrDefaultAsyncBLL(id);
            
            if (company == null)
            {
                return NotFound();
            }

            var companyUserIds = await _bll.AppUsers.GetAllCompanyUserIds(id);
            
            _bll.Companies.Remove(company);
            await _bll.SaveChangesAsync();
            
            await _bll.AppUsers.RemoveAllCompanyUsers(companyUserIds);

            return NoContent();
        }
    }
}