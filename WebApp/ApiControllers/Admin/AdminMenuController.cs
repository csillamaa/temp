using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Domain.App.Identity;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using PublicApi.DTO.v1.Menu;

namespace WebApp.ApiControllers.Admin
{
    /// <summary>
    /// Represent a RESTful admin menu service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Admin, Company Admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdminMenuController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<AppUser> _userManager;
        

        /// <summary>
        /// Constructor for controller
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="bll"></param>
        public AdminMenuController(UserManager<AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }

        
        /// <summary>
        /// Gets all the menus.
        /// </summary>
        /// <returns>List of menus.</returns>
        /// <response code="200">The menus user has authority over were retrieved.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<MenuIndex>), StatusCodes.Status200OK)]
        public async Task<ActionResult<List<MenuIndex>>> GetMenus()
        {
            return (await _bll.Menus.GetAllIndexASyncBLL(await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value)))
                .Select(MenuMapper.Map)
                .ToList();
        }
        
        
        /// <summary>
        /// Creates a new menu.
        /// </summary>
        /// <param name="menu">Object with menu name.</param>
        /// <returns>The created menu.</returns>
        /// <response code="201">The menu was created.</response>
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(MenuOnlyName), StatusCodes.Status201Created)]
        public async Task<ActionResult<MenuOnlyName>> PostMenu(MenuOnlyName menu)
        {
            var companyId = await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value);
        
            var bllMenu = MenuMapper.Map(menu);
            bllMenu.CompanyId = companyId!.Value;
            
            _bll.Menus.Add(bllMenu);
            await _bll.SaveChangesAsync();
            
            bllMenu.Id = _bll.Menus.GetUpdatedEntityAfterSaveChanges(bllMenu).Id;
        
            var publicApiV1Menu = MenuMapper.Map(bllMenu);
            
            return CreatedAtAction("GetMenu", new {id = publicApiV1Menu.Id}, publicApiV1Menu);
        }
        
        
        /// <summary>
        /// Gets a single menu.
        /// </summary>
        /// <param name="id">Id of the menu being retrieved, int.</param>
        /// <param name="currentCategoryId">Id of the menu category that is wanted to be displayed, int.
        /// If left empty, will display first category.</param>
        /// <returns>The retrieved menu.</returns>
        /// <response code="200">The menu was retrieved.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The menu or menu category doesn't exist.</response>
        [HttpGet("{id:int}")]
        // [HttpGet("{id:int}, {currentCategoryId:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(MenuEdit), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<MenuEdit>> GetMenu(int id, int? currentCategoryId)
        {
            if (currentCategoryId != null && !await _bll.MenuCategories.ExistsAsync(currentCategoryId.Value))
            {
                return NotFound();
            }
            var menu = await _bll.Menus.FirstOrDefaultEditAsyncBLL(id, currentCategoryId);
         
            if (menu == null)
            {
                return NotFound();
            }
            
            if (!await User.DoesUserHaveAuthorityToEdit(menu.CompanyId, _userManager))
            {
                return Unauthorized();
            }
            
            return MenuMapper.Map(menu);
        
        }
        
        /// <summary>
        /// Updates a menu.
        /// </summary>
        /// <param name="id">Id of the menu being updated, int.</param>
        /// <param name="name">New name of the menu.</param>
        /// <returns></returns>
        /// <response code="204">The menu was updated.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The menu doesn't exist.</response>
        [HttpPut("{id:int}, {name}")]
        [ProducesResponseType( StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutMenu(int id, string name)
        {
            var menu = await _bll.Menus.FirstOrDefaultAsync(id);
        
            if (menu == null)
            {
                return NotFound();
            }
        
            if (!await User.DoesUserHaveAuthorityToEdit(menu.CompanyId, _userManager))
            {
                return Unauthorized();
            }
        
            menu.Name = name;
            _bll.Menus.Update(menu);
            await _bll.SaveChangesAsync();
        
            return NoContent();
        }
        
        
        /// <summary>
        /// Deletes a menu.
        /// </summary>
        /// <param name="id">Id of the menu being deleted, int.</param>
        /// <returns></returns>
        /// <response code="204">The menu was deleted.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The menu doesn't exist.</response>
        [HttpDelete("{id:int}")]
        [ProducesResponseType( StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteMenu(int id)
        {
            var menu = await _bll.Menus.FirstOrDefaultAsync(id);
            if (menu == null)
            {
                return NotFound();
            }
        
            if (!await User.DoesUserHaveAuthorityToEdit(menu.CompanyId, _userManager))
            {
                return Unauthorized();
            }
        
            _bll.Menus.Remove(menu);
            await _bll.SaveChangesAsync();
            return NoContent();
        }

        /// <summary>
        /// Moves a category up or down on the menu.
        /// </summary>
        /// <param name="categoryId">Id of the menu category being moved, int.</param>
        /// <param name="moveUp">Direction of the moving of the category: true - up, false - down</param>
        /// <returns></returns>
        /// <response code="204">The menu was moved.</response>
        /// <response code="400">The menu can't be moved that way.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The menu category doesn't exist.</response>
        [HttpPost("{categoryId:int}, {moveUp:bool}")]
        [ProducesResponseType( StatusCodes.Status204NoContent)]
        [ProducesResponseType( StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> MoveCategory(int categoryId, bool moveUp)
        {
            if (!await _bll.MenuCategories.ExistsAsync(categoryId))
            {
                return NotFound();
            }
            
            var categoryCompanyId = await _bll.MenuCategories.GetCategoryCompanyId(categoryId);
            if (!await User.DoesUserHaveAuthorityToEdit(categoryCompanyId, _userManager))
            {
                return Unauthorized();
            }
         
            var result = await _bll.MenuCategories.ChangeCategoryPosition(categoryId, moveUp);
            if (!result)
            {
                return BadRequest();
            }
            
            return NoContent();
        }
        
        
        /// <summary>
        /// Moves product up or down in the menu.
        /// </summary>
        /// <param name="productId">Id of the product being moved, int.</param>
        /// <param name="moveUp">Direction of the moving of the product: true - up, false - down</param>
        /// <returns></returns>
        /// <response code="204">The product was moved.</response>
        /// <response code="400">The product can't be moved that way.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The product doesn't exist.</response>
        [HttpPost("{productId:int}, {moveUp:bool}")]
        [ProducesResponseType( StatusCodes.Status204NoContent)]
        [ProducesResponseType( StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> MoveProduct(int productId, bool moveUp)
        {
            if (!await _bll.Products.ExistsAsync(productId))
            {
                return NotFound();
            }
            
            var categoryCompanyId = await _bll.Products.GetProductCompanyId(productId);
            if (!await User.DoesUserHaveAuthorityToEdit(categoryCompanyId, _userManager))
            {
                return NotFound();
            }
         
            var result = await _bll.Products.ChangeProductPosition(productId, moveUp);
            if (!result)
            {
                return BadRequest();
            }
        
            return NoContent();
        }
        
        /// <summary>
        /// Copies all other menu items to current menu.
        /// </summary>
        /// <param name="currentMenuId">Id of the menu that is getting the items, int.</param>
        /// <param name="otherMenuId">Id of the menu that the items are being copied from, int.</param>
        /// <returns></returns>
        /// <response code="204">The items were copied.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">One or both of the menus don't exist.</response>
        [HttpPost("{currentMenuId:int}, {otherMenuId:int}")]
        [ProducesResponseType( StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> CopyOtherMenuItems(int currentMenuId, int otherMenuId)
        {
            if (!await _bll.Menus.ExistsAsync(currentMenuId) && !await _bll.Menus.ExistsAsync(otherMenuId))
            {
                return NotFound();
            }
            
            var menuCompanyId = await _bll.Menus.GetMenuCompanyId(currentMenuId);
            var otherMenuCompanyId = await _bll.Menus.GetMenuCompanyId(otherMenuId);
            
            if (menuCompanyId != otherMenuCompanyId || !await User.DoesUserHaveAuthorityToEdit(menuCompanyId, _userManager))
            {
                return Unauthorized();
            }
            
            await _bll.Menus.CopyMenuItemsToOtherMenu(currentMenuId, otherMenuId);

            return NoContent();
        }
    }
}