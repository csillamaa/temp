using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using PublicApi.DTO.v1.Menu;
using PublicApi.DTO.v1.Restaurant;

namespace WebApp.ApiControllers.Admin
{
    /// <summary>
    /// Represent a RESTful admin restaurant service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Admin, Company Admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdminRestaurantsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;

        /// <summary>
        /// Constructor for controller.
        /// </summary>
        /// <param name="bll"></param>
        /// <param name="userManager"></param>
        public AdminRestaurantsController(IAppBLL bll, UserManager<Domain.App.Identity.AppUser> userManager)
        {
            _bll = bll;
            _userManager = userManager;
        }

   
        /// <summary>
        /// Gets all restaurants.
        /// </summary>
        /// <returns>List of restaurants.</returns>
        /// <response code="200">Restaurants were retrieved.</response>
        /// <response code="401">Logged in user doesn't have authority to access this info.</response>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<RestaurantIndex>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IEnumerable<RestaurantIndex>> GetRestaurants()
        {
            var restaurants = await _bll.Restaurants.GetAllIndexAsyncBLL();
            return restaurants.Select(RestaurantMapper.MapToApi);
        }

        /// <summary>
        /// Gets a single restaurant.
        /// </summary>
        /// <param name="id">Id of the restaurant, int.</param>
        /// <returns></returns>
        /// <response code="200">The restaurant was retrieved.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The restaurant doesn't exist.</response>
        [HttpGet("{id:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(RestaurantEdit), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        
        public async Task<ActionResult<RestaurantEdit>> GetRestaurant(int id)
        {
            var restaurant = await _bll.Restaurants.FirstOrDefaultEditAsyncBLL(id);
                
            if (restaurant == null)
            {
                return NotFound();
            }
            
            if (!await User.DoesUserHaveAuthorityToEdit(restaurant.CompanyId, _userManager))
            {
                return Unauthorized();
            }

            return RestaurantMapper.MapToApi(restaurant);
        }
        
        /// <summary>
        /// Updates a restaurant.
        /// </summary>
        /// <param name="id">The id of the restaurant being updated.</param>
        /// <param name="restaurant">The new restaurant's data.</param>
        /// <returns>The updated restaurant.</returns>
        /// <response code="204">The restaurant was updated.</response>
        /// <response code="400">The parameter id doesn't match the restaurant's id.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        /// <response code="404">The restaurant doesn't exist.</response>
        [HttpPut("{id:int}")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        public async Task<IActionResult> PutCompany(int id, RestaurantData restaurant)
        {
            if (id != restaurant.Id)
            {
                return BadRequest();
            }
            
            if (!await User.DoesUserHaveAuthorityToEdit(restaurant.CompanyId, _userManager))
            {
                return Unauthorized();
            }

            var dbRestaurant = await _bll.Restaurants.FirstOrDefaultAsync(id);
            if (dbRestaurant == null)
            {
                return NotFound();
            }
            
            // TODO check if data is valid
            
            dbRestaurant.Name = restaurant.Name;
            dbRestaurant.PhoneNumber = restaurant.PhoneNumber;
            dbRestaurant.Address = restaurant.Address;
            dbRestaurant.Description = restaurant.Description;
            dbRestaurant.Picture = restaurant.Picture;
            dbRestaurant.IsPublic = restaurant.IsPublic;
            dbRestaurant.Email = restaurant.Email;
            if (User.IsInRole("Admin"))
            {
                dbRestaurant.DateJoined = restaurant.DateJoined;
            }
            
            _bll.Restaurants.Update(dbRestaurant);
            await _bll.SaveChangesAsync();
        
            return NoContent();
        }
        
        /// <summary>
        /// Creates a new restaurant.
        /// </summary>
        /// <param name="restaurant">The new restaurant's data</param>
        /// <returns>The created restaurant.</returns>
        /// <response code="201">The restaurant was created.</response>
        /// <response code="400">A restaurant with the same name already exists.</response>
        /// <response code="401">The logged in user doesn't have authority to access this info.</response>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(Restaurant), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Restaurant>> PostRestaurant(RestaurantData restaurant)
        {
            if (!await User.DoesUserHaveAuthorityToEdit(restaurant.CompanyId, _userManager))
            {
                return Unauthorized();
            }
            // TODO: check if data is valid.

            var bllRestaurant = RestaurantMapper.MapToBLL(restaurant);
           
            _bll.Restaurants.Add(bllRestaurant);
            await _bll.SaveChangesAsync();

            restaurant.Id = _bll.Restaurants.GetUpdatedEntityAfterSaveChanges(bllRestaurant).Id;
            
            return CreatedAtAction("GetRestaurant", new { id = restaurant.Id }, restaurant);
        }

        /// <summary>
        /// Deletes a restaurant.
        /// </summary>
        /// <param name="id">The id of the restaurant being deleted.</param>
        /// <returns></returns>
        /// <response code="204">The restaurant was deleted.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The restaurant doesn't exist.</response>
        [HttpDelete("{id:int}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteRestaurant(int id)
        {
            var restaurant = await _bll.Restaurants.FirstOrDefaultAsync(id);
            
            if (restaurant == null)
            {
                return NotFound();
            }

            if (!await User.DoesUserHaveAuthorityToEdit(restaurant.CompanyId, _userManager))
            {
                return Unauthorized();
            }
            
            // TODO: what else needs to be removed
            await _bll.MenusInRestaurant.RemoveAllMenusFromRestaurant(id);
            
            _bll.Restaurants.Remove(restaurant);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Saves a new opening time for a day for a restaurant.
        /// </summary>
        /// <param name="restaurantId">Id of the restaurant that's opening time is being changed, int.</param>
        /// <param name="time">New restaurant opening time.</param>
        /// <returns></returns>
        /// <response code="204">The restaurant's opening time was created/updated.</response>
        /// <response code="400">The opening time wasn't before the closing time.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The restaurant doesn't exist.</response>
        [HttpPost("{restaurantId:int}")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> SaveRestaurantOpeningTime(int restaurantId, PublicApi.DTO.v1.Time time)
        {
            if (!await _bll.Restaurants.ExistsAsync(restaurantId))
            {
                return NotFound();
            }

            if (!await User.DoesUserHaveAuthorityToEdit(await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value),_userManager))
            {
                return Unauthorized();
            }

            if (!_bll.Times.IsOpenTimeBeforeClose(time.StartingTime, time.EndingTime))
            {
                return BadRequest();
            }

            await _bll.Times.SaveNewTime(restaurantId, TimeMapper.Map(time)!);
            
            return NoContent();
        }
        
        /// <summary>
        /// Adds a menu to a restaurant
        /// </summary>
        /// <param name="menuInRestaurant">Object that contains the menu and restaurant Ids, int.</param>
        /// <returns></returns>
        /// <response code="204">The menu was added to the restaurant.</response>
        /// <response code="400">The menu is already added or the menu and restaurant dont belong to the same company.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The restaurant or menu doesn't exist.</response>
        [HttpPost]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AddMenu([Bind("MenuId, RestaurantId")] MenuInRestaurant menuInRestaurant)
        {
            if (!await _bll.Menus.ExistsAsync(menuInRestaurant.MenuId) || !await _bll.Restaurants.ExistsAsync(menuInRestaurant.RestaurantId))
            {
                return NotFound();
            }
            
            var restaurantCompanyId = await _bll.Restaurants.GetRestaurantCompanyId(menuInRestaurant.RestaurantId);
            if (!await User.DoesUserHaveAuthorityToEdit(restaurantCompanyId, _userManager))
            {
                return Unauthorized();
            }
            
            if (!await _bll.MenusInRestaurant.IsMenuAddableToRestaurant(MenuInRestaurantMapper.Map(menuInRestaurant)!))
            {
                return BadRequest();
            }
            
            _bll.MenusInRestaurant.Add(MenuInRestaurantMapper.Map(menuInRestaurant)!);
            await _bll.SaveChangesAsync();
            return NoContent();
        }
        
        /// <summary>
        /// Removes a menu from a restaurant
        /// </summary>
        /// <param name="menuInRestaurant">Object that contains the menu and restaurant Ids, int.</param>
        /// <returns></returns>
        /// <response code="204">The menu was removed from the restaurant.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        /// <response code="404">The restaurant doesn't have the menu.</response>
        [HttpPost]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RemoveMenu([Bind("MenuId, RestaurantId")] MenuInRestaurant menuInRestaurant)
        {
            var restaurantCompanyId = await _bll.Restaurants.GetRestaurantCompanyId(menuInRestaurant.RestaurantId);
            if (!await User.DoesUserHaveAuthorityToEdit(restaurantCompanyId, _userManager))
            {
                return Unauthorized();
            }
            
            if (await _bll.MenusInRestaurant.RemoveMenuFromRestaurant(MenuInRestaurantMapper.Map(menuInRestaurant)!) == null)
            {
                return NotFound();
            }

            await _bll.SaveChangesAsync();
            return NoContent();
        }
    }
}