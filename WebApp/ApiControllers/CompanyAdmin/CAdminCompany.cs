﻿using System.Threading.Tasks;
using Contracts.BLL.App;
using Domain.App.Identity;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Company;
using PublicApi.DTO.v1.Mappers;

namespace WebApp.ApiControllers.CompanyAdmin
{
    /// <summary>
    /// Represent a RESTful Company admin's Company service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Company Admin", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CAdminCompany: Controller
    {
        private readonly IAppBLL _bll;
        private readonly UserManager<AppUser> _userManager;
        

        /// <summary>
        /// Constructor for controller.
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="bll"></param>
        public CAdminCompany(UserManager<AppUser> userManager, IAppBLL bll)
        {
            _userManager = userManager;
            _bll = bll;
        }
        
        
        /// <summary>
        /// Gets the user's company.
        /// </summary>
        /// <returns>Company that was retrieved.</returns>
        /// <response code="200">The user's company was retrieved.</response>
        /// <response code="401">The logged in user doesn't have authority to access this command.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Company>> GetCompany()
        {
            var companyId = await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value);
            var company = await _bll.Companies.FirstOrDefaultAsyncBLL(companyId!.Value);
            return CompanyMapper.MapToPublicApiV1(company!);
        }
    }
    
}