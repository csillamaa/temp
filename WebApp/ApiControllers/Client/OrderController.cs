﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Archive;
using PublicApi.DTO.v1.Mappers;
using PublicApi.DTO.v1.Order;
using PublicApi.DTO.v1.Product;
using PublicApi.DTO.v1.Restaurant;

namespace WebApp.ApiControllers.Client
{
    /// <summary>
    /// Represent a RESTful Client's Order service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OrderController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Constructor for controller.
        /// </summary>
        /// <param name="bll"></param>
        public OrderController(IAppBLL bll)
        {
            _bll = bll;
        }
        
        /// <summary>
        /// Gets user's active orders.
        /// </summary>
        /// <returns>User's active orders.</returns>
        /// <response code="200">The user's active orders were retrieved.</response>
        /// <response code="400">The user isn't a client.</response>
        /// <response code="404">The user doesn't have any active orders.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<OrderClient>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<OrderClient>>> GetAllActiveOrders()
        {
            if (await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value) != null)
            {
                return BadRequest();
            }
            
            var orders = await _bll.Orders.GetAllActiveClientAsyncBLL(User.GetUserId()!.Value);
            if (orders == null)
            {
                return NotFound();
            }

            var apiOrders = orders.Select(OrderMapper.MapToApiOrderClient).ToList();

            return apiOrders!;
        }
        
        /// <summary>
        /// Gets user's completed orders.
        /// </summary>
        /// <returns>User's completed orders.</returns>
        /// <response code="200">The user's completed orders were retrieved.</response>
        /// <response code="400">The user isn't a client.</response>
        /// <response code="404">The user doesn't have any completed orders.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<ArchivedOrderClient>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<ArchivedOrderClient>>> GetAllCompletedOrders()
        {
            if (await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value) != null)
            {
                return BadRequest();
            }
            
            var orders = await _bll.ArchivedOrders.GetAllCompletedOrdersClientAsyncBLL(User.GetUserId()!.Value);
            if (orders == null)
            {
                return NotFound();
            }

            var apiOrders = orders.Select(ArchivedOrderMapper.MapToApiOrderClient).ToList();

            return apiOrders!;
        }
        
        /// <summary>
        /// Adds user's comment to archived/completed order.
        /// </summary>
        /// <param name="orderId">Id of the order, int.</param>
        /// <param name="comment">Comment that is being added to the arhived order.</param>
        /// <returns></returns>
        /// <response code="204">The user's comment was added..</response>
        /// <response code="400">The order doesn't belong to the user.</response>
        /// <response code="404">The order doesn't exist.</response>
        [HttpGet("{orderId:int}, {comment}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> AddCommentToArchivedOrder(int orderId, string comment)
        {
            if (!await _bll.ArchivedOrders.ExistsAsync(orderId))
            {
                return NotFound();
            }
            
            if (!await _bll.Orders.DoesOrderBelongToCustomer(User.GetUserId()!.Value, orderId))
            {
                return BadRequest();
            }

            await _bll.ArchivedOrders.AddCommentToOrder(orderId, comment, true);
            
            return NoContent();
        }
        
        /// <summary>
        /// Adds a rating out of 5 by the client to the completed order.
        /// </summary>
        /// <param name="orderId">Id of the order, int.</param>
        /// <param name="rating">A number 1-5.</param>
        /// <returns></returns>
        /// <response code="204">The user's rating to the order was added..</response>
        /// <response code="400">The order doesn't belong to the user or the rating wasn't in range(1,5).</response>
        /// <response code="404">The order doesn't exist.</response>
        [HttpGet("{orderId:int}, {rating:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> AddRatingToOrder(int orderId, int rating)
        {
            if (rating < 1 || rating > 5)
            {
                return BadRequest();
            }
            
            if (!await _bll.ArchivedOrders.ExistsAsync(orderId))
            {
                return NotFound();
            }
            
            if (!await _bll.Orders.DoesOrderBelongToCustomer(User.GetUserId()!.Value, orderId))
            {
                return BadRequest();
            }

            await _bll.ArchivedOrders.AddRatingToOrder(orderId, rating);

            return NoContent();
        }
    }
}