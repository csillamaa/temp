﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using PublicApi.DTO.v1.Product;
using PublicApi.DTO.v1.Restaurant;

namespace WebApp.ApiControllers.Client
{
    /// <summary>
    /// Represent a RESTful Client's Restaurant service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RestaurantsController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Constructor for controller.
        /// </summary>
        /// <param name="bll"></param>
        public RestaurantsController(IAppBLL bll)
        {
            _bll = bll;
        }
        
        /// <summary>
        /// Gets all client's favourite and all other restaurants.
        /// </summary>
        /// <returns>All favourite and other restaurants.</returns>
        /// <response code="200">The restaurants were retrieved.</response>
        [AllowAnonymous]
        [HttpGet] 
        [Produces("application/json")]
        [ProducesResponseType(typeof(RestaurantClientAllLists), StatusCodes.Status200OK)]
        public async Task<ActionResult<RestaurantClientAllLists>> GetAllRestaurants()
        {
            return RestaurantMapper.MapToApiClientAllLists(await _bll.Restaurants.GetAllRestaurantViewModel(User.GetUserId()));
        }
        
        
        /// <summary>
        /// Gets a single restaurant for client view.
        /// </summary>
        /// <param name="id">Id of the restaurant, int.</param>
        /// <param name="currentCategoryPosition">Position of current menu category that is shown to client (other categories are loaded without products),
        /// starts from 0.</param>
        /// <returns>Restaurant with all data needed for client.</returns>
        /// <response code="200">The restaurant was retrieved.</response>
        /// <response code="404">The restaurant doesn't exist or it's not public.</response>
        [AllowAnonymous]
        [HttpGet("{id:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(RestaurantClient), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<RestaurantClient>> GetRestaurant(int id, int? currentCategoryPosition)
        {
            var restaurant = await _bll.Restaurants.FirstOrDefaultClientAsyncBLL(id, currentCategoryPosition);
            if (restaurant == null)
            {
                return NotFound();
            }
            
            return RestaurantMapper.MapToApiClient(restaurant)!;
        }
        
        /// <summary>
        /// Adds a restaurant to user's favorites.
        /// </summary>
        /// <param name="id">Id of the restaurant, int.</param>
        /// <returns></returns>
        /// <response code="204">The restaurant was added to favorites.</response>
        /// <response code="400">The user isn't a client and can't have favorite restaurants or the user already has
        /// that restaurant added to his/her favorites.</response>
        /// <response code="404">The restaurant doesn't exist.</response>
        [HttpPost("{id:int}")]
        [ProducesResponseType( StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AddRestaurantToUserFavorites(int id)
        {
            if (await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value) != null)
            {
                return BadRequest();
            }

            if (!await _bll.Restaurants.ExistsAsync(id))
            {
                return NotFound();
            }

            if (!await _bll.AppUsers.AddUserFavoriteRestaurant(id, User.GetUserId()!.Value))
            {
                return BadRequest();
            }

            return NoContent();
        }

        /// <summary>
        /// Removes a restaurant from user's favorites.
        /// </summary>
        /// <param name="id">Id of the restaurant, int.</param>
        /// <returns></returns>
        /// <response code="204">The restaurant was removed from favorites.</response>
        /// <response code="400">The user isn't a client and can't have or remove favorite restaurants.</response>
        /// <response code="404">The user doesn't have that restaurant as favorite or the restaurant doesn't exist.</response>
        [HttpPost("{id:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> RemoveUserFavoriteRestaurant(int id)
        {
            if (await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value) != null)
            {
                return BadRequest();
            }

            if (!await _bll.Restaurants.ExistsAsync(id))
            {
                return NotFound();
            }
            
            if (!await _bll.AppUsers.RemoveUserFavoriteRestaurant(id, User.GetUserId()!.Value))
            {
                return NotFound();
            }

            return NoContent();
        }
        
        /// <summary>
        /// Gets a single product for client view.
        /// </summary>
        /// <param name="productId">Id of the product, int.</param>
        /// <returns>The retrieved product.</returns>
        /// <response code="200">The product was retrieved.</response>
        /// <response code="404">The product doesn't exist</response>
        [AllowAnonymous]
        [HttpGet("{productId:int}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ProductClient), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ProductClient>> Product(int productId)
        {
            var product = await _bll.Products.FirstOrDefaultClientAsyncBLL(productId);
            if (product == null)
            {
                return NotFound();
            }
            
            return ProductMapper.MapToApiClient(product);
        }
        
        
        /// <summary>
        /// Adds a product to customers order/basket, if it is from a different restaurant than the other products
        /// already in his basket, then the current order will be deleted and a new one with the added product will be
        /// made.
        /// </summary>
        /// <param name="restaurantId">Id of the restaurant the product is from.</param>
        /// <param name="productId">Id of the product, int.</param>
        /// <param name="amount">Amount of said product added.</param>
        /// <param name="subProductIds">Ids of the subProducts that were chosen by the customer.</param>
        /// <returns></returns>
        /// <response code="204">The product was added to the order/basket.</response>
        /// <response code="400">The user isn't a client or the amount is under 1 or the sub products chosen aren't
        /// from the product or there are too little or too many of them from some sub category or the restaurant isn't open.</response>
        /// <response code="404">The product or restaurant doesn't exist</response>
        [HttpPost("{productId:int}, {amount:int}, {restaurantId:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> AddProductToOrder(int restaurantId, int productId, int amount, int[] subProductIds)
        {
            var userId = User.GetUserId()!.Value;
            if (await _bll.AppUsers.GetUserCompanyId(userId) != null)
            {
                return BadRequest();
            }

            if (!await _bll.Products.ExistsAsync(productId))
            {
                return NotFound();
            }
            
            if (!await _bll.Restaurants.ExistsAsync(restaurantId))
            {
                return NotFound();
            }

            if (amount < 1)
            {
                return BadRequest();
            }

            if (!await _bll.Restaurants.IsRestaurantOpenById(restaurantId))
            {
                return BadRequest();
            }
            
            var errors = await _bll.Orders.AddProductToOrder(userId, productId, restaurantId,amount, subProductIds);
            
            Console.WriteLine(errors ?? "errors on null");

            if (errors != null)
            {
                return BadRequest(errors);
            }

            return NoContent();
        }
    }
}