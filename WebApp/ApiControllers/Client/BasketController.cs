﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using PublicApi.DTO.v1.Order;
using PublicApi.DTO.v1.Product;
using PublicApi.DTO.v1.Restaurant;

namespace WebApp.ApiControllers.Client
{
    /// <summary>
    /// Represent a RESTful Client's Basket service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BasketController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Constructor for controller.
        /// </summary>
        /// <param name="bll"></param>
        public BasketController(IAppBLL bll)
        {
            _bll = bll;
        }
        
        /// <summary>
        /// Gets user's current basket.
        /// </summary>
        /// <returns>User's basket.</returns>
        /// <response code="200">The basket was retrieved.</response>
        /// <response code="400">The user isn't a client.</response>
        /// <response code="404">The user's basket is empty.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(OrderClient), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<OrderClient>> GetBasket()
        {
            var userId = User.GetUserId()!.Value;

            if (await _bll.AppUsers.GetUserCompanyId(userId) != null)
            {
                return BadRequest();
            }
            
            await _bll.Orders.ReCalculateOrderCost(await _bll.Orders.GetUserBasketId(userId));
            var userBasket = await _bll.Orders.FirstOrDefaultClientAsyncBLL(userId);

            var apiUserBasket = OrderMapper.MapToApiOrderClient(userBasket);
            if (apiUserBasket == null)
            {
                return NotFound();
            }
            
            return apiUserBasket;
        }
        
        /// <summary>
        /// Adds a comment from the user to the order. 
        /// </summary>
        /// <param name="orderId">Id of the order, int.</param>
        /// <param name="comment">The comment that is being added.</param>
        /// <returns></returns>
        /// <response code="204">The comment was added</response>
        /// <response code="400">The order doesn't belong to current user.</response>
        /// <response code="404">The order doesn't exist.</response>
        [HttpPost("{orderId:int},{comment}")]
        [ProducesResponseType( StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AddCustomerComment(int orderId, string comment)
        {
            if (!await _bll.Orders.DoesOrderBelongToCustomer(User.GetUserId()!.Value, orderId))
            {
                return BadRequest();
            }
            
            if (!await _bll.Orders.AddCustomerCommentToOrder(User.GetUserId()!.Value, orderId, comment))
            {
                return NotFound();
            }
            
            return NoContent();
        }
        
        /// <summary>
        /// Submits order to the restaurant.
        /// </summary>
        /// <param name="cardNumber">Bankcard number that user is paying with.</param>
        /// <param name="payInRestaurant">True - user pays in restaurant(doesn't have to give card info), false - pays right now with card</param>
        /// <returns></returns>
        /// <response code="204">The order was submitted to the restaurant.</response>
        /// <response code="400">The user doesn't have a basket with products or user chose to pay now and didn't supply card number.</response>
        /// <response code="404">The order doesn't exist.</response>
        [HttpPost("{payInRestaurant:bool}")]
        [ProducesResponseType( StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> SubmitOrder(string? cardNumber, bool payInRestaurant)
        {
            var orderId = await _bll.Orders.GetUserBasketId(User.GetUserId()!.Value);

            if (orderId == default)
            {
                return BadRequest();
            }
            
            if (payInRestaurant)
            {
                if (!await _bll.Orders.SubmitOrder(orderId, null))
                {
                    return NotFound();
                }
                return NoContent();
            }

            if (cardNumber == null)
            {
                return BadRequest();
            }
            
            if (!await _bll.Orders.SubmitOrder(orderId, cardNumber))
            {
                return NotFound();
            }

            return NoContent();
        }
        
        /// <summary>
        /// Changes amount of a product in order.
        /// </summary>
        /// <param name="productInOrderId">Id of the product, int.</param>
        /// <param name="newAmount">New amount of product in order.</param>
        /// <returns></returns>
        /// <response code="204">The product amount was changed.</response>
        /// <response code="400">The order doesn't belong to current user or new amount is under 1.</response>
        /// <response code="404">The order or product in order doesn't exist.</response>
        [HttpPost("{productInOrderId:int}, {newAmount:int}")]
        [ProducesResponseType( StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ChangeProductAmountInOrder(int productInOrderId, int newAmount)
        {
            if (newAmount < 1)
            {
                return BadRequest();
            }
            
            if (!await _bll.ProductsInOrder.ExistsAsync(productInOrderId))
            {
                return NotFound();
            }
            
            var orderId = await _bll.ProductsInOrder.GetOrderId(productInOrderId);
            
            if (!await _bll.Orders.DoesOrderBelongToCustomer(User.GetUserId()!.Value, orderId))
            {
                return BadRequest();
            }
            
            if (!await _bll.ProductsInOrder.ChangeProductInOrderAmount(orderId, productInOrderId, newAmount))
            {
                return NotFound();
            }
            
            await _bll.Orders.ReCalculateOrderCost(orderId);
            
            return NoContent();
        }
        
        /// <summary>
        /// Removes product from order, if order is empty after that will delete order as well.
        /// </summary>
        /// <param name="productInOrderId">Id of the product, int.</param>
        /// <returns></returns>
        /// <response code="204">The product was removed from the order.</response>
        /// <response code="400">The order doesn't belong to current user.</response>
        /// <response code="404">The order or product in order doesn't exist.</response>
        [HttpPost("{productInOrderId:int}")]
        [ProducesResponseType( StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RemoveProductFromOrder(int productInOrderId)
        {
            var productInOrder = await _bll.ProductsInOrder.FirstOrDefaultAsync(productInOrderId);
            if (productInOrder == null)
            {
                return NotFound();
            }
            
            var orderId = await _bll.ProductsInOrder.GetOrderId(productInOrderId);
            
            if (!await _bll.Orders.DoesOrderBelongToCustomer(User.GetUserId()!.Value, orderId))
            {
                return BadRequest();
            }

            _bll.ProductsInOrder.Remove(productInOrder);
            await _bll.SaveChangesAsync();
            
            if (await _bll.Orders.DeleteIfOrderIsEmpty(orderId))
            {
                return NoContent();
            }
            
            await _bll.Orders.ReCalculateOrderCost(orderId);
            
            return NoContent();
        }
    }
}