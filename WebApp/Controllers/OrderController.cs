﻿#pragma warning disable 1591
using System;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Domain.App.Identity;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
      
        private readonly IAppBLL _bll;
        private UserManager<AppUser> _userManager;
        
        public OrderController(IAppBLL bll, UserManager<AppUser> userManager)
        {
            _bll = bll;
            _userManager = userManager;
        }
        
        public async Task<IActionResult> Index()
        {
            var orders = await _bll.Orders.GetAllActiveClientAsyncBLL(User.GetUserId()!.Value);

            return View(orders);
        }
        
        public async Task<IActionResult> Completed()
        {
            var orders = await _bll.ArchivedOrders.GetAllCompletedOrdersClientAsyncBLL(User.GetUserId()!.Value);

            return View(orders);
        }
        
        public async Task<IActionResult> AddCommentToArchivedOrder(int orderId, string comment)
        {
            if (!await _bll.ArchivedOrders.ExistsAsync(orderId))
            {
                return NotFound();
            }
            
            if (!await _bll.Orders.DoesOrderBelongToCustomer(User.GetUserId()!.Value, orderId))
            {
                return BadRequest();
            }

            await _bll.ArchivedOrders.AddCommentToOrder(orderId, comment, true);

            return RedirectToAction(nameof(Completed));
        }
        
        public async Task<IActionResult> AddRatingToOrder(int orderId, int rating)
        {
            if (rating < 1 || rating > 5)
            {
                return BadRequest();
            }
            
            if (!await _bll.ArchivedOrders.ExistsAsync(orderId))
            {
                return NotFound();
            }
            
            if (!await _bll.Orders.DoesOrderBelongToCustomer(User.GetUserId()!.Value, orderId))
            {
                return BadRequest();
            }

            await _bll.ArchivedOrders.AddRatingToOrder(orderId, rating);
            
            return RedirectToAction(nameof(Completed));
        }
    }
}