﻿#pragma warning disable 1591
using System;
using System.Threading.Tasks;
using BLL.App.DTO.Order;
using Contracts.BLL.App;
using Contracts.DAL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace WebApp.Controllers
{
    [Authorize]
    public class BasketController : Controller
    {
        private readonly IAppBLL _bll;
        
        public BasketController(IAppBLL bll)
        {
            _bll = bll;
        }
        
        public async Task<IActionResult> Index()
        {
            var userId = User.GetUserId()!.Value;
            await _bll.Orders.ReCalculateOrderCost(await _bll.Orders.GetUserBasketId(userId));
            var userBasket = await _bll.Orders.FirstOrDefaultClientAsyncBLL(userId);
            
            return View(userBasket);
        }

        [HttpPost]
        public async Task<IActionResult> SubmitOrder(string? cardNumber, bool payInRestaurant)
        {
            var orderId = await _bll.Orders.GetUserBasketId(User.GetUserId()!.Value);

            if (orderId == default)
            {
                return BadRequest();
            }
            
            if (payInRestaurant)
            {
                if (!await _bll.Orders.SubmitOrder(orderId, null))
                {
                    return NotFound();
                }
                return RedirectToAction(nameof(Index), "Order");
            }

            if (cardNumber == null)
            {
                return RedirectToAction(nameof(Index));
            }
            
            if (!await _bll.Orders.SubmitOrder(orderId, cardNumber))
            {
                return NotFound();
            }

            return RedirectToAction(nameof(Index), "Order");
        }

        [HttpPost]
        public async Task<IActionResult> AddCustomerComment(int orderId, string comment)
        {
            if (!await _bll.Orders.DoesOrderBelongToCustomer(User.GetUserId()!.Value, orderId))
            {
                return BadRequest();
            }
            
            if (!await _bll.Orders.AddCustomerCommentToOrder(User.GetUserId()!.Value, orderId, comment))
            {
                return NotFound();
            }
            
            return RedirectToAction(nameof(Index));
        }
        
        [HttpPost]
        public async Task<IActionResult> ChangeProductAmountInOrder(int productInOrderId, int newAmount)
        {
            if (newAmount < 1)
            {
                return BadRequest();
            }
            
            if (!await _bll.ProductsInOrder.ExistsAsync(productInOrderId))
            {
                return NotFound();
            }
            
            var orderId = await _bll.ProductsInOrder.GetOrderId(productInOrderId);
            
            if (!await _bll.Orders.DoesOrderBelongToCustomer(User.GetUserId()!.Value, orderId))
            {
                return BadRequest();
            }
            
            if (!await _bll.ProductsInOrder.ChangeProductInOrderAmount(orderId, productInOrderId, newAmount))
            {
                return NotFound();
            }

            await _bll.Orders.ReCalculateOrderCost(orderId);
            
            return RedirectToAction(nameof(Index));
        }
        
        [HttpPost]
        public async Task<IActionResult> RemoveProductFromOrder(int productInOrderId)
        {
            var productInOrder = await _bll.ProductsInOrder.FirstOrDefaultAsync(productInOrderId);
            if (productInOrder == null)
            {
                return NotFound();
            }
            
            var orderId = await _bll.ProductsInOrder.GetOrderId(productInOrderId);
            
            if (!await _bll.Orders.DoesOrderBelongToCustomer(User.GetUserId()!.Value, orderId))
            {
                return BadRequest();
            }

            _bll.ProductsInOrder.Remove(productInOrder);
            await _bll.SaveChangesAsync();

            if (await _bll.Orders.DeleteIfOrderIsEmpty(orderId))
            {
                return RedirectToAction(nameof(Index));
            }
            
            await _bll.Orders.ReCalculateOrderCost(orderId);

            return RedirectToAction(nameof(Index));
        }
    }
}