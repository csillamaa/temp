﻿#pragma warning disable 1591
using System;
using System.Threading.Tasks;
using BLL.App.DTO.Product;
using Contracts.BLL.App;
using Extensions.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class RestaurantsController : Controller
    {
        private readonly IAppBLL _bll;

        public RestaurantsController(IAppBLL bll)
        {
            _bll = bll;
        }
        
        public async Task<IActionResult> Index()
        {
            var restaurantViewModel = await _bll.Restaurants.GetAllRestaurantViewModel(User.GetUserId());
            
            return View(restaurantViewModel);
        }
        
        public async Task<IActionResult> Restaurant(int id, int? currentCategoryPosition)
        {
            var restaurant = await _bll.Restaurants.FirstOrDefaultClientAsyncBLL(id, currentCategoryPosition);
            if (restaurant == null)
            {
                return NotFound();
            }
            
            return View(restaurant);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddRestaurantToUserFavorites(int id)
        {
            if (await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value) != null)
            {
                return BadRequest();
            }

            if (!await _bll.Restaurants.ExistsAsync(id))
            {
                return NotFound();
            }
            
            if (!await _bll.AppUsers.AddUserFavoriteRestaurant(id, User.GetUserId()!.Value))
            {
                return BadRequest();
            }

            return RedirectToAction(nameof(Restaurant), new {id});
        }
        
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> RemoveRestaurantFromUserFavorites(int id)
        {
            if (await _bll.AppUsers.GetUserCompanyId(User.GetUserId()!.Value) != null)
            {
                return BadRequest();
            }

            if (!await _bll.Restaurants.ExistsAsync(id))
            {
                return NotFound();
            }
            
            if (!await _bll.AppUsers.RemoveUserFavoriteRestaurant(id, User.GetUserId()!.Value))
            {
                return BadRequest();
            }
            
            return RedirectToAction(nameof(Restaurant), new {id});
        }
        
        [HttpGet]
        public async Task<IActionResult> Product(int restaurantId, int productId)
        {
            var product = await _bll.Products.FirstOrDefaultClientAsyncBLL(productId);
            if (product == null)
            {
                return NotFound();
            }

            product.RestaurantId = restaurantId;
            
            return View(product);
        }
        
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddProductToOrder(int restaurantId, int productId, int amount, 
            [Bind("subCategory0")] int[] subCategory0, [Bind("subCategory1")] int[] subCategory1,
            [Bind("subCategory2")] int[] subCategory2, [Bind("subCategory3")] int[] subCategory3,
            [Bind("subCategory4")] int[] subCategory4)
        {
            
            var userId = User.GetUserId()!.Value;
            if (await _bll.AppUsers.GetUserCompanyId(userId) != null)
            {
                return BadRequest();
            }

            if (!await _bll.Products.ExistsAsync(productId))
            {
                return NotFound();
            }
            
            if (!await _bll.Restaurants.ExistsAsync(restaurantId))
            {
                return NotFound();
            }

            if (amount < 1)
            {
                return BadRequest();
            }

            if (!await _bll.Restaurants.IsRestaurantOpenById(restaurantId))
            {
                return BadRequest();
            }
            
            // if (!await _bll.Orders.IsUserBasketEmptyOrFromSameRestaurant(userId, restaurantId))
            // {
            //     return BadRequest();
            // }

            var subProductsIds = _bll.Orders.AddSubProductIdsToOneArray(subCategory0, subCategory1,
                subCategory2, subCategory3, subCategory4);
            var errors = await _bll.Orders.AddProductToOrder(userId, productId, restaurantId,amount,subProductsIds);

            if (errors != null)
            {
                return BadRequest(errors);
            }
            Console.WriteLine(errors ?? "errors on null");
            
            return RedirectToAction(nameof(Product), new {restaurantId, productId});
        }
    }
}