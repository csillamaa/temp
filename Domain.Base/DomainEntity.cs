﻿using System;
using Contracts.Domain.Base;

namespace Domain.Base
{
    public class DomainEntity : DomainEntity<int>, IDomainEntity
    {
        
    }
    public class DomainEntity<TKey> : IDomainEntity<TKey>
    where TKey: IEquatable<TKey>
    {
        public TKey Id { get; set; } = default!;
    }
}