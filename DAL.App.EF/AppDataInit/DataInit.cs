﻿using System;
using System.Threading;
using DAL.App.DTO.Product;
using Domain.App;
using Domain.App.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Product = Domain.App.Product;
using ProductsSubCategory = Domain.App.ProductsSubCategory;
using ProductsSubCategoryProduct = Domain.App.ProductsSubCategoryProduct;

namespace DAL.App.EF.AppDataInit
{
    public class DataInit
    {
        public static void DropDatabase(AppDbContext ctx)
        {
            ctx.Database.EnsureDeleted();
        }

        public static void MigrateDatabase(AppDbContext ctx)
        {
            ctx.Database.Migrate();
        }
        
        public static  void SeedAppData(AppDbContext ctx)
        {
            var company1 = new Company
            {
                Name = "McDonalds",
                DateJoined = DateTime.Now,
                Website = "McDonalds.com",
                PhoneNumber = "51231233",
                Email = "mcdonalds@mcdonalds.com",
            };
            var company2 = new Company
            {
                Name = "Hesburger",
                DateJoined = DateTime.Now,
                Website = "Hesburger.com",
                PhoneNumber = "500000000",
                Email = "hesburger@hesburger.com",
            };

             ctx.Companies.Add(company1);
             ctx.Companies.Add(company2);

             ctx.SaveChanges();

            var restaurant1_1 = new Restaurant
            {
                Name = "McDonalds Viru",
                Email = "Mcdonalds@viru.com",
                PhoneNumber = "555555555",
                Address = "Viru tänav 24",
                Company = company1,
                DateJoined = DateTime.Now,
                Description = "McDonalds restaurant in Tallinn's old town.",
                Picture = "viru.png",
                IsPublic = true,
            };
            var restaurant1_2 = new Restaurant
            {
                Name = "McDonalds lille",
                Email = "mcdonalds@lille.com",
                PhoneNumber = "555555556",
                Address = "Paldiski mnt 44",
                Company = company1,
                DateJoined = DateTime.Now,
                Description = "McDonalds restaurant in Sõle.",
                Picture = "lille.png",
                IsPublic = true,
            };
            var restaurant2_1 = new Restaurant
            {
                Name = "Hesburger Peetri",
                Email = "Hesburger@peetri.com",
                PhoneNumber = "555555557",
                Address = "Veesaare tee 2",
                Company = company2,
                DateJoined = DateTime.Now,
                Description = "Hesburger restaurant in Peetri.",
                Picture = "peetri.png",
                IsPublic = true,

            };
            var restaurant2_2 = new Restaurant
            {
                Name = "Hesburger Rocca-Al-Mare",
                Email = "Hesburger@rocca.com",
                PhoneNumber = "55555558",
                Address = "rocca",
                Company = company2,
                DateJoined = DateTime.Now,
                Description = "Hesburger restaurant in Rocca-Al-Mare.",
                Picture = "rocca.png", 
                IsPublic = true,
            };

             ctx.Restaurants.Add(restaurant1_1);
             ctx.Restaurants.Add(restaurant1_2);
             ctx.Restaurants.Add(restaurant2_1);
             ctx.Restaurants.Add(restaurant2_2);

             ctx.SaveChanges();
          
            
             SeedRestaurantTimes(ctx, restaurant1_1.Id);
             SeedRestaurantTimes(ctx, restaurant1_2.Id);
             
             SeedRestaurantTimes(ctx, restaurant2_1.Id);
             SeedRestaurantTimes(ctx, restaurant2_2.Id);
             
             SeedAppDataMenuMc(ctx);
             SeedAppDataMenuHes(ctx);
        }

        public static void SeedRestaurantTimes(AppDbContext ctx, int restaurantId)
        {
            for (int i = 1; i < 6; i++)
            {
                var restaurant1time1 = new Time
                {
                    RestaurantId = restaurantId,
                    DayOfWeek = i,
                    StartingTime = DateTime.Parse("01:00"),
                    EndingTime = DateTime.Parse("23:00")
                };
                ctx.Times.Add(restaurant1time1);
                ctx.SaveChanges();
            }

            var restaurant1time6 = new Time
            {
                RestaurantId = restaurantId,
                DayOfWeek = 6,
                StartingTime = DateTime.Parse("01:00"),
                EndingTime = DateTime.Parse("23:00")
            };
            var restaurant1time7 = new Time
            {
                RestaurantId = restaurantId,
                DayOfWeek = 0,
                StartingTime = DateTime.Parse("01:00"),
                EndingTime = DateTime.Parse("23:00")
            };
             
            ctx.Times.Add(restaurant1time6);
            ctx.Times.Add(restaurant1time7);

            ctx.SaveChanges();
        }

        public static void SeedAppDataBasket(AppDbContext ctx)
        {   
            
            var order1 = new Order
            {
                RestaurantId = 1,
                FoodCost = 19,
                IsSelfPickup = true,
                IsProcessed = false,
                IsReady = false,
                AmountPaid = 0,
                // CompletedBy = DateTime.Parse("17:00"),
                // SubmitTime = DateTime.Parse("11:00"),
                // InitialCompleteTimeEstimate = DateTime.Parse("16:30")
            };

            ctx.Orders.Add(order1);
            ctx.SaveChanges();

            var orderUser = new AppUserOrder
            {
                AppUserId = 5,
                OrderId = 1
            };

            ctx.AppUserOrders.Add(orderUser);
            ctx.SaveChanges();

            
            var product1Inorder1 = new ProductInOrder
            {
                ProductId = 1,
                Order = order1,
                Amount = 3,
                OneProductPrice = 5
            };
            
            var product2Inorder1 = new ProductInOrder
            {
                ProductId = 2,
                Order = order1,
                Amount = 1,
                OneProductPrice = 4
            };
            ctx.ProductsInOrder.Add(product1Inorder1);
            ctx.ProductsInOrder.Add(product2Inorder1);
            ctx.SaveChanges();

            var order1product1subproduc1 = new SubProductFromProduct
            {
                ProductInOrderId = product1Inorder1.Id,
                Amount = 1,
                ProductsSubCategoryProductId = 1
            };

            ctx.SubProductsFromProduct.Add(order1product1subproduc1);
            ctx.SaveChanges();
        }
        
        public static void SeedAppDataActiveOrder(AppDbContext ctx)
        {
            var order1 = new Order
            {
                RestaurantId = 1,
                FoodCost = 19,
                IsSelfPickup = true,
                IsProcessed = true,
                AmountPaid = 19,
                CompletedBy = DateTime.Parse("17:00"),
                SubmitTime = DateTime.Parse("11:00"),
                InitialCompleteTimeEstimate = DateTime.Parse("16:30")
            };

            ctx.Orders.Add(order1);
            ctx.SaveChanges();

            var orderUser = new AppUserOrder
            {
                AppUserId = 5,
                OrderId = order1.Id
            };

            ctx.AppUserOrders.Add(orderUser);
            ctx.SaveChanges();

            
            var product1Inorder1 = new ProductInOrder
            {
                ProductId = 3,
                Order = order1,
                Amount = 3,
                OneProductPrice = 5
            };
            
            var product2Inorder1 = new ProductInOrder
            {
                ProductId = 2,
                Order = order1,
                Amount = 1,
                OneProductPrice = 4
            };
            ctx.ProductsInOrder.Add(product1Inorder1);
            ctx.ProductsInOrder.Add(product2Inorder1);
            ctx.SaveChanges();

            var order1product1subproduc1 = new SubProductFromProduct
            {
                ProductInOrderId = product1Inorder1.Id,
                Amount = 1,
                ProductsSubCategoryProductId = 2
            };

            ctx.SubProductsFromProduct.Add(order1product1subproduc1);
            ctx.SaveChanges();
            
            // Order2------------
            
            var order2 = new Order
            {
                RestaurantId = 2,
                FoodCost = 25,
                IsSelfPickup = true,
                IsProcessed = true,
                AmountPaid = 25,
                CompletedBy = DateTime.Parse("19:30"),
                SubmitTime = DateTime.Parse("12:00"),
                InitialCompleteTimeEstimate = DateTime.Parse("15:30")
            };

            ctx.Orders.Add(order2);
            ctx.SaveChanges();

            var orderUser2 = new AppUserOrder
            {
                AppUserId = 5,
                OrderId = order2.Id
            };

            ctx.AppUserOrders.Add(orderUser2);
            ctx.SaveChanges();

            
            var product1Inorder2 = new ProductInOrder
            {
                ProductId = 4,
                Order = order2,
                Amount = 2,
                OneProductPrice = 5
            };
            
            var product2Inorder2 = new ProductInOrder
            {
                ProductId = 1,
                Order = order2,
                Amount = 3,
                OneProductPrice = 5
            };
            ctx.ProductsInOrder.Add(product1Inorder2);
            ctx.ProductsInOrder.Add(product2Inorder2);
            ctx.SaveChanges();

            // var order1product1subproduc1 = new SubProductFromProduct
            // {
            //     ProductInOrderId = product1Inorder2.Id,
            //     Amount = 1,
            //     ProductsSubCategoryProductId = 2
            // };
            //
            // ctx.SubProductsFromProduct.Add(order1product1subproduc2);
            // ctx.SaveChanges();
            
            // ready order1
            
            var order3 = new Order
            {
                RestaurantId = 1,
                FoodCost = 22,
                IsSelfPickup = true,
                IsProcessed = true,
                IsReady = true,
                AmountPaid = 19,
                CompletedBy = DateTime.Parse("17:30"),
                SubmitTime = DateTime.Parse("11:15"),
                InitialCompleteTimeEstimate = DateTime.Parse("16:36")
            };

            ctx.Orders.Add(order3);
            ctx.SaveChanges();

            var orderUser3 = new AppUserOrder
            {
                AppUserId = 5,
                OrderId = order3.Id
            };

            ctx.AppUserOrders.Add(orderUser3);
            ctx.SaveChanges();

            
            var product1Inorder3 = new ProductInOrder
            {
                ProductId = 3,
                Order = order3,
                Amount = 3,
                OneProductPrice = 5
            };
            
            var product2Inorder3 = new ProductInOrder
            {
                ProductId = 3,
                Order = order3,
                Amount = 1,
                OneProductPrice = 4
            };
            ctx.ProductsInOrder.Add(product1Inorder3);
            ctx.ProductsInOrder.Add(product2Inorder3);
            ctx.SaveChanges();

            var order3product1subproduc1 = new SubProductFromProduct
            {
                ProductInOrderId = product1Inorder3.Id,
                Amount = 1,
                ProductsSubCategoryProductId = 2
            };

            ctx.SubProductsFromProduct.Add(order3product1subproduc1);
            ctx.SaveChanges();
        }

        public static void SeedAppDataArchivedOrder(AppDbContext ctx)
        {
            var order1 = new Domain.App.Archive.ArchivedOrder
            {
                RestaurantId = 1,
                FoodCost = 19,
                IsSelfPickup = true,
                AmountPaid = 19,
                ActualCompleteTime = DateTime.Parse("17:00"),
                SubmitTime = DateTime.Parse("11:00"),
                EstimatedCompleteTime = DateTime.Parse("16:30"),
                OrderRating = 1,
                CustomerOrderReviewComment = "very good order",
                CustomerComment = "double salad please",
            };

            ctx.ArchivedOrders.Add(order1);
            ctx.SaveChanges();

            var orderUser = new Domain.App.Archive.ArchivedAppUserOrder
            {
                AppUserId = 5,
                ArchivedOrderId = order1.Id
            };

            ctx.ArchivedAppUserOrders.Add(orderUser);
            ctx.SaveChanges();


            var product1Inorder1 = new Domain.App.Archive.ArchivedProductInOrder
            {
                ProductId = 3,
                ArchivedOrder = order1,
                Amount = 3,
                OneProductPrice = 5
            };

            var product2Inorder1 = new Domain.App.Archive.ArchivedProductInOrder
            {
                ProductId = 2,
                ArchivedOrder = order1,
                Amount = 1,
                OneProductPrice = 4
            };
            ctx.ArchivedProductsInOrder.Add(product1Inorder1);
            ctx.ArchivedProductsInOrder.Add(product2Inorder1);
            ctx.SaveChanges();

            var order1product1subproduc1 = new Domain.App.Archive.ArchivedSubProductFromProduct
            {
                ArchivedProductInOrderId = product1Inorder1.Id,
                Amount = 1,
                ProductsSubCategoryProductId = 2
            };

            ctx.ArchivedSubProductsFromProduct.Add(order1product1subproduc1);
            ctx.SaveChanges();
        }

        public static void SeedAppDataMenuMc(AppDbContext ctx)
        {
            var menu1 = new Menu
            {
                Name = "Mcdonald's MainMenu",
                CompanyId = 1
            };
            ctx.Menus.Add(menu1);
            ctx.SaveChanges();
            var menuRestaurant1 = new MenuInRestaurant
            {
                RestaurantId = 1,
                MenuId = 1
            };
            var menuRestaurant2 = new MenuInRestaurant
            {
                RestaurantId = 2,
                MenuId = 1
            };
            ctx.MenusInRestaurant.Add(menuRestaurant1);
            ctx.MenusInRestaurant.Add(menuRestaurant2);

            var menu1Category1 = new MenuCategory
            {
                Name = "Burgers",
                Menu = menu1,
                Position = 0
            };
            
            var menu1Category2 = new MenuCategory
            {
                Name = "Wraps",
                Menu = menu1,
                Position = 1
            };
            
            var menu1Category3 = new MenuCategory
            {
                Name = "Meals",
                Menu = menu1,
                Position = 2
            };
            
            var menu1Category4 = new MenuCategory
            {
                Name = "Desserts",
                Menu = menu1,
                Position = 3
            };
            ctx.MenuCategories.Add(menu1Category1);
            ctx.MenuCategories.Add(menu1Category2);
            ctx.MenuCategories.Add(menu1Category3);
            ctx.MenuCategories.Add(menu1Category4);
            ctx.SaveChanges();

            var menu1Category1Product1 = new Product
            {
                MenuCategory = menu1Category1,
                Name = "McChicken",
                Price = 3.50,
                Description = "Burger with chicken bun",
                IsAlwaysPurchasable = true,
                Position = 1
            };
            
            var menu1Category1Product2 = new Product
            {
                MenuCategory = menu1Category1,
                Name = "BigMac",
                Price = 4.50,
                Description = "Burger with two beef buns",
                IsAlwaysPurchasable = true,
                Position = 0
            };
            
            var menu1Category2Product1 = new Product
            {
                MenuCategory = menu1Category2,
                Name = "Chicken wrap",
                Price = 5,
                Description = "Wrap with chicken",
                IsAlwaysPurchasable = true,
                Position = 0
            };
            
            var menu1Category2Product2 = new Product
            {
                MenuCategory = menu1Category2,
                Name = "Spicy Chicken wrap",
                Price = 5.50,
                Description = "Wrap with spicy chicken",
                IsAlwaysPurchasable = true,
                Position = 1
            };
            
            var menu1Category3Product1 = new Product
            {
                MenuCategory = menu1Category3,
                Name = "McChicken Meal",
                Price = 6,
                Description = "Meal with a burger with chicken bun, fries and a drink.",
                IsAlwaysPurchasable = true,
                Position = 0
            };
            
            var menu1Category3Product2 = new Product
            {
                MenuCategory = menu1Category3,
                Name = "BigMac Meal",
                Price = 6.50,
                Description = "Meal with a burger with two beef buns, fries and a drink.",
                IsAlwaysPurchasable = true,
                Position = 1
            };
            
            var menu1Category3Product3 = new Product
            {
                MenuCategory = menu1Category3,
                Name = "Chicken wrap meal",
                Price = 7,
                Description = "Meal with a chicken wrap, fries and a drink.",
                IsAlwaysPurchasable = true,
                Position = 2
            };
            
            var menu1Category3Product4 = new Product
            {
                MenuCategory = menu1Category3,
                Name = "Spicy chicken wrap meal",
                Price = 7.50,
                Description = "Meal with a spicy chicken wrap, fries and a drink.",
                IsAlwaysPurchasable = true,
                Position = 3
            };
            
            var menu1Category4Product1 = new Product
            {
                MenuCategory = menu1Category4,
                Name = "Sunday",
                Price = 1.25,
                Description = "Vanilla ice-cream in a cone.",
                IsAlwaysPurchasable = true,
                Position = 0
            };
            
            var menu1Category4Product2 = new Product
            {
                MenuCategory = menu1Category4,
                Name = "Apple pie",
                Price = 1.50,
                Description = "Pie with hot apple sauce inside",
                IsAlwaysPurchasable = true,
                Position = 1
            };
            
            var menu1Category4Product3 = new Product
            {
                MenuCategory = menu1Category4,
                Name = "Apple pie",
                Price = 1.50,
                Description = "Pie with hot apple sauce inside",
                IsAlwaysPurchasable = true,
                Position = 2
            };
            ctx.Products.Add(menu1Category1Product1);
            ctx.Products.Add(menu1Category1Product2);
            ctx.Products.Add(menu1Category2Product1);
            ctx.Products.Add(menu1Category2Product2);
            ctx.Products.Add(menu1Category3Product1);
            ctx.Products.Add(menu1Category3Product2);
            ctx.Products.Add(menu1Category3Product3);
            ctx.Products.Add(menu1Category3Product4);
            ctx.Products.Add(menu1Category4Product1);
            ctx.Products.Add(menu1Category4Product2);
            ctx.Products.Add(menu1Category4Product3);
            
            ctx.SaveChanges();
            
            
            // var category3Product1SubCategory1 = new ProductsSubCategory
            // {
            //     Name = "Meal Size",
            //     Product = menu1Category3Product1,
            //     Position = 0,
            //     MaxItemCount = 1,
            //     MandatoryItemCount = 1
            // };
            //
            // var category3Product1SubCategory2 = new ProductsSubCategory
            // {
            //     Name = "Drink",
            //     Product = menu1Category3Product1,
            //     Position = 1,
            //     MaxItemCount = 1,
            //     MandatoryItemCount = 1
            // };
            //
            // var category3Product1SubCategory3 = new ProductsSubCategory
            // {
            //     Name = "Dessert",
            //     Product = menu1Category3Product1,
            //     Position = 2,
            //     MaxItemCount = 3,
            //     MandatoryItemCount = 0
            // };
            //
            // var category3Product2SubCategory1 = new ProductsSubCategory
            // {
            //     Name = "Meal Size",
            //     Product = menu1Category3Product2,
            //     Position = 0,
            //     MaxItemCount = 1,
            //     MandatoryItemCount = 1
            // };
            //
            // var category3Product2SubCategory2 = new ProductsSubCategory
            // {
            //     Name = "Drink",
            //     Product = menu1Category3Product2,
            //     Position = 1,
            //     MaxItemCount = 1,
            //     MandatoryItemCount = 1
            // };
            //
            // var category3Product2SubCategory3 = new ProductsSubCategory
            // {
            //     Name = "Dessert",
            //     Product = menu1Category3Product2,
            //     Position = 2,
            //     MaxItemCount = 3,
            //     MandatoryItemCount = 0
            // };
            //  
            // var category3Product3SubCategory1 = new ProductsSubCategory
            // {
            //     Name = "Meal Size",
            //     Product = menu1Category3Product3,
            //     Position = 0,
            //     MaxItemCount = 1,
            //     MandatoryItemCount = 1
            // };
            //
            // var category3Product3SubCategory2 = new ProductsSubCategory
            // {
            //     Name = "Drink",
            //     Product = menu1Category3Product3,
            //     Position = 1,
            //     MaxItemCount = 1,
            //     MandatoryItemCount = 1
            // };
            //
            // var category3Product3SubCategory3 = new ProductsSubCategory
            // {
            //     Name = "Dessert",
            //     Product = menu1Category3Product3,
            //     Position = 2,
            //     MaxItemCount = 3,
            //     MandatoryItemCount = 0
            // }; 
            //
            // var category3Product4SubCategory1 = new ProductsSubCategory
            // {
            //     Name = "Meal Size",
            //     Product = menu1Category3Product4,
            //     Position = 0,
            //     MaxItemCount = 1,
            //     MandatoryItemCount = 1
            // };
            //
            // var category3Product4SubCategory2 = new ProductsSubCategory
            // {
            //     Name = "Drink",
            //     Product = menu1Category3Product4,
            //     Position = 1,
            //     MaxItemCount = 1,
            //     MandatoryItemCount = 1
            // };
            //
            // var category3Product4SubCategory3 = new ProductsSubCategory
            // {
            //     Name = "Dessert",
            //     Product = menu1Category3Product4,
            //     Position = 2,
            //     MaxItemCount = 3,
            //     MandatoryItemCount = 0
            // };
            //
            // ctx.ProductsSubCategories.Add(category3Product1SubCategory1);
            // ctx.ProductsSubCategories.Add(category3Product1SubCategory2);
            // ctx.ProductsSubCategories.Add(category3Product1SubCategory3);
            // ctx.ProductsSubCategories.Add(category3Product2SubCategory1);
            // ctx.ProductsSubCategories.Add(category3Product2SubCategory2);
            // ctx.ProductsSubCategories.Add(category3Product2SubCategory3);
            // ctx.ProductsSubCategories.Add(category3Product3SubCategory1);
            // ctx.ProductsSubCategories.Add(category3Product3SubCategory2);
            // ctx.ProductsSubCategories.Add(category3Product3SubCategory3);
            // ctx.ProductsSubCategories.Add(category3Product4SubCategory1);
            // ctx.ProductsSubCategories.Add(category3Product4SubCategory2);
            // ctx.ProductsSubCategories.Add(category3Product4SubCategory3);
            //
            // ctx.SaveChanges();

            var category3Product1SubCategories = SeebSubCategoriesMc(menu1Category3Product1, ctx);
            var category3Product2SubCategories = SeebSubCategoriesMc(menu1Category3Product2, ctx);
            var category3Product3SubCategories = SeebSubCategoriesMc(menu1Category3Product3, ctx);
            var category3Product4SubCategories = SeebSubCategoriesMc(menu1Category3Product4, ctx);

            SeedSubProductsMc(category3Product1SubCategories, ctx);
            SeedSubProductsMc(category3Product2SubCategories, ctx);
            SeedSubProductsMc(category3Product3SubCategories, ctx);
            SeedSubProductsMc(category3Product4SubCategories, ctx);
        }

        private static ProductsSubCategory[] SeebSubCategoriesMc(Product product, AppDbContext ctx)
        {
            var subCategories = new ProductsSubCategory[3];
            
            var subCategory1 = new ProductsSubCategory
            {
                Name = "Meal Size",
                Product = product,
                Position = 0,
                MaxItemCount = 1,
                MandatoryItemCount = 1
            };
            
            var subCategory2 = new ProductsSubCategory
            {
                Name = "Drink",
                Product = product,
                Position = 1,
                MaxItemCount = 1,
                MandatoryItemCount = 1
            };
            
            var subCategory3 = new ProductsSubCategory
            {
                Name = "Dessert",
                Product = product,
                Position = 2,
                MaxItemCount = 3,
                MandatoryItemCount = 0
            };

            ctx.ProductsSubCategories.Add(subCategory1);
            ctx.ProductsSubCategories.Add(subCategory1);
            ctx.ProductsSubCategories.Add(subCategory1);
            ctx.SaveChanges();

            subCategories[0] = subCategory1;
            subCategories[1] = subCategory2;
            subCategories[2] = subCategory3;

            return subCategories;
        }

        private static void SeedSubProductsMc(ProductsSubCategory[] categories,AppDbContext ctx)
        {
            var category1Product1SubCategory1Product1 = new ProductsSubCategoryProduct
            {
                Name = "Big",
                Price = 2,
                Position = 0,
                ProductsSubCategory = categories[0],
                IsSelectedOnDefault = false
            };
            
            var category1Product1SubCategory1Product2 = new ProductsSubCategoryProduct
            {
                Name = "Medium",
                Price = 1,
                Position = 1,
                ProductsSubCategory = categories[0],
                IsSelectedOnDefault = false
            };
            
            var category1Product1SubCategory1Product3 = new ProductsSubCategoryProduct
            {
                Name = "Small",
                Price = 0,
                Position = 2,
                ProductsSubCategory = categories[0],
                IsSelectedOnDefault = true
            };
            
            var category1Product1SubCategory2Product1 = new ProductsSubCategoryProduct
            {
                Name = "Coca-Cola",
                Price = 0,
                Position = 0,
                ProductsSubCategory = categories[1],
                IsSelectedOnDefault = true,
            };
            
            var category1Product1SubCategory2Product2 = new ProductsSubCategoryProduct
            {
                Name = "Fanta",
                Price = 0,
                Position = 1,
                ProductsSubCategory = categories[1],
                IsSelectedOnDefault = false,
            };
            
            var category1Product1SubCategory2Product3 = new ProductsSubCategoryProduct
            {
                Name = "Sprite",
                Price = 0,
                Position = 2,
                ProductsSubCategory = categories[1],
                IsSelectedOnDefault = false,
            };
            
            var category1Product1SubCategory3Product1 = new ProductsSubCategoryProduct
            {
                Name = "Sunday",
                Price = 1.25,
                Position = 0,
                ProductsSubCategory = categories[2],
                IsSelectedOnDefault = false,
            };
            
            var category1Product1SubCategory3Product2 = new ProductsSubCategoryProduct
            {
                Name = "Apple Pie",
                Price = 1.75,
                Position = 1,
                ProductsSubCategory = categories[2],
                IsSelectedOnDefault = false,
            };
            
            var category1Product1SubCategory3Product3 = new ProductsSubCategoryProduct
            {
                Name = "Pear Pie",
                Price = 1.75,
                Position = 2,
                ProductsSubCategory = categories[2],
                IsSelectedOnDefault = false,
            };

            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory1Product1);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory1Product2);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory1Product3);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory2Product1);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory2Product2);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory2Product3);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory3Product1);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory3Product2);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory3Product3);

            ctx.SaveChanges();
        }
        
         public static void SeedAppDataMenuHes(AppDbContext ctx)
        {
            var menu1 = new Menu
            {
                Name = "Hesburger's MainMenu",
                CompanyId = 1
            };
            ctx.Menus.Add(menu1);
            ctx.SaveChanges();
            var menuRestaurant1 = new MenuInRestaurant
            {
                RestaurantId = 3,
                MenuId = 2
            };
            var menuRestaurant2 = new MenuInRestaurant
            {
                RestaurantId = 4,
                MenuId = 2
            };
            ctx.MenusInRestaurant.Add(menuRestaurant1);
            ctx.MenusInRestaurant.Add(menuRestaurant2);

            var menu1Category1 = new MenuCategory
            {
                Name = "Burgers",
                Menu = menu1,
                Position = 0
            };
            
            var menu1Category2 = new MenuCategory
            {
                Name = "Tortillas",
                Menu = menu1,
                Position = 1
            };
            
            var menu1Category3 = new MenuCategory
            {
                Name = "Meals",
                Menu = menu1,
                Position = 2
            };
            
            var menu1Category4 = new MenuCategory
            {
                Name = "Desserts",
                Menu = menu1,
                Position = 3
            };
            ctx.MenuCategories.Add(menu1Category1);
            ctx.MenuCategories.Add(menu1Category2);
            ctx.MenuCategories.Add(menu1Category3);
            ctx.MenuCategories.Add(menu1Category4);
            ctx.SaveChanges();

            var menu1Category1Product1 = new Product
            {
                MenuCategory = menu1Category1,
                Name = "BBQ burger",
                Price = 3.30,
                Description = "Burger with two beef buns and BBQ sauce.",
                IsAlwaysPurchasable = true,
                Position = 1
            };
            
            var menu1Category1Product2 = new Product
            {
                MenuCategory = menu1Category1,
                Name = "Hesburger",
                Price = 3.50,
                Description = "Burger with two beef buns",
                IsAlwaysPurchasable = true,
                Position = 0
            };
            
            var menu1Category2Product1 = new Product
            {
                MenuCategory = menu1Category2,
                Name = "Chicken tortilla",
                Price = 4,
                Description = "Tortilla with chicken",
                IsAlwaysPurchasable = true,
                Position = 0
            };
            
            var menu1Category2Product2 = new Product
            {
                MenuCategory = menu1Category2,
                Name = "Soy tortilla",
                Price = 4.10,
                Description = "Tortilla with soy sticks",
                IsAlwaysPurchasable = true,
                Position = 1
            };
            
            var menu1Category3Product1 = new Product
            {
                MenuCategory = menu1Category3,
                Name = "Hesburger Meal",
                Price = 5.50,
                Description = "Meal with a burger with two beef buns, fries and a drink.",
                IsAlwaysPurchasable = true,
                Position = 0
            };
            
            var menu1Category3Product2 = new Product
            {
                MenuCategory = menu1Category3,
                Name = "BBQ burger Meal",
                Price = 5,
                Description = "Meal with a burger with two beef buns and BBQ sauce, fries and a drink.",
                IsAlwaysPurchasable = true,
                Position = 1
            };
            
            var menu1Category3Product3 = new Product
            {
                MenuCategory = menu1Category3,
                Name = "Chicken tortilla meal",
                Price = 7,
                Description = "Meal with a chicken tortilla, fries and a drink.",
                IsAlwaysPurchasable = true,
                Position = 2
            };
            
            var menu1Category3Product4 = new Product
            {
                MenuCategory = menu1Category3,
                Name = "Soy tortilla meal",
                Price = 7.50,
                Description = "Meal with a soy tortilla, fries and a drink.",
                IsAlwaysPurchasable = true,
                Position = 3
            };
            
            var menu1Category4Product1 = new Product
            {
                MenuCategory = menu1Category4,
                Name = "White chocolate cookie",
                Price = 1,
                Description = "Cookie with white chocolate.",
                IsAlwaysPurchasable = true,
                Position = 0
            };
            
            var menu1Category4Product2 = new Product
            {
                MenuCategory = menu1Category4,
                Name = "Chocolate donut",
                Price = 0.75,
                Description = "Donut with chocolate.",
                IsAlwaysPurchasable = true,
                Position = 1
            };
            
            var menu1Category4Product3 = new Product
            {
                MenuCategory = menu1Category4,
                Name = "White chocolate donut",
                Price = 0.75,
                Description = "Donut with white chocolate.",
                IsAlwaysPurchasable = true,
                Position = 2
            };
            ctx.Products.Add(menu1Category1Product1);
            ctx.Products.Add(menu1Category1Product2);
            ctx.Products.Add(menu1Category2Product1);
            ctx.Products.Add(menu1Category2Product2);
            ctx.Products.Add(menu1Category3Product1);
            ctx.Products.Add(menu1Category3Product2);
            ctx.Products.Add(menu1Category3Product3);
            ctx.Products.Add(menu1Category3Product4);
            ctx.Products.Add(menu1Category4Product1);
            ctx.Products.Add(menu1Category4Product2);
            ctx.Products.Add(menu1Category4Product3);
            
            ctx.SaveChanges();

            var category3Product1SubCategories = SeebSubCategoriesHes(menu1Category3Product1, ctx);
            var category3Product2SubCategories = SeebSubCategoriesHes(menu1Category3Product2, ctx);
            var category3Product3SubCategories = SeebSubCategoriesHes(menu1Category3Product3, ctx);
            var category3Product4SubCategories = SeebSubCategoriesHes(menu1Category3Product4, ctx);

            SeedSubProductsHes(category3Product1SubCategories, ctx);
            SeedSubProductsHes(category3Product2SubCategories, ctx);
            SeedSubProductsHes(category3Product3SubCategories, ctx);
            SeedSubProductsHes(category3Product4SubCategories, ctx);
        }

        private static ProductsSubCategory[] SeebSubCategoriesHes(Product product, AppDbContext ctx)
        {
            var subCategories = new ProductsSubCategory[3];
            
            var subCategory1 = new ProductsSubCategory
            {
                Name = "Meal Size",
                Product = product,
                Position = 0,
                MaxItemCount = 1,
                MandatoryItemCount = 1
            };
            
            var subCategory2 = new ProductsSubCategory
            {
                Name = "Drink",
                Product = product,
                Position = 1,
                MaxItemCount = 1,
                MandatoryItemCount = 1
            };
            
            var subCategory3 = new ProductsSubCategory
            {
                Name = "Dessert",
                Product = product,
                Position = 2,
                MaxItemCount = 3,
                MandatoryItemCount = 0
            };

            ctx.ProductsSubCategories.Add(subCategory1);
            ctx.ProductsSubCategories.Add(subCategory1);
            ctx.ProductsSubCategories.Add(subCategory1);
            ctx.SaveChanges();

            subCategories[0] = subCategory1;
            subCategories[1] = subCategory2;
            subCategories[2] = subCategory3;

            return subCategories;
        }

        private static void SeedSubProductsHes(ProductsSubCategory[] categories,AppDbContext ctx)
        {
            var category1Product1SubCategory1Product1 = new ProductsSubCategoryProduct
            {
                Name = "Big",
                Price = 1.50,
                Position = 0,
                ProductsSubCategory = categories[0],
                IsSelectedOnDefault = false
            };
            
            var category1Product1SubCategory1Product3 = new ProductsSubCategoryProduct
            {
                Name = "Small",
                Price = 0,
                Position = 2,
                ProductsSubCategory = categories[0],
                IsSelectedOnDefault = true
            };
            
            var category1Product1SubCategory2Product1 = new ProductsSubCategoryProduct
            {
                Name = "Coca-Cola",
                Price = 0,
                Position = 0,
                ProductsSubCategory = categories[1],
                IsSelectedOnDefault = true,
            };
            
            var category1Product1SubCategory2Product2 = new ProductsSubCategoryProduct
            {
                Name = "Fanta",
                Price = 0,
                Position = 1,
                ProductsSubCategory = categories[1],
                IsSelectedOnDefault = false,
            };
            
            var category1Product1SubCategory2Product3 = new ProductsSubCategoryProduct
            {
                Name = "Sprite",
                Price = 0,
                Position = 2,
                ProductsSubCategory = categories[1],
                IsSelectedOnDefault = false,
            };
            
            var category1Product1SubCategory3Product1 = new ProductsSubCategoryProduct
            {
                Name = "White chocolate cookie",
                Price = 1,
                Position = 0,
                ProductsSubCategory = categories[2],
                IsSelectedOnDefault = false,
            };
            
            var category1Product1SubCategory3Product2 = new ProductsSubCategoryProduct
            {
                Name = "Chocolate donut",
                Price = 0.70,
                Position = 1,
                ProductsSubCategory = categories[2],
                IsSelectedOnDefault = false,
            };
            
            var category1Product1SubCategory3Product3 = new ProductsSubCategoryProduct
            {
                Name = "White chocolate donut",
                Price = 0.70,
                Position = 2,
                ProductsSubCategory = categories[2],
                IsSelectedOnDefault = false,
            };

            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory1Product1);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory1Product3);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory2Product1);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory2Product2);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory2Product3);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory3Product1);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory3Product2);
            ctx.ProductsSubCategoryProducts.Add(category1Product1SubCategory3Product3);

            ctx.SaveChanges();
        }

        public static  void SeedIdentity(AppDbContext ctx, UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            var role = new AppRole();
            role.Name = "Admin";
            var result = roleManager.CreateAsync(role).Result;
            var companyAdminRole = new AppRole();
            companyAdminRole.Name = "Company Admin";
            var companyAdminRoleResult = roleManager.CreateAsync(companyAdminRole).Result;
            var workerRole = new AppRole();
            workerRole.Name = "Restaurant Worker";
            var workerRoleResult = roleManager.CreateAsync(workerRole).Result;

            // if (!result.Succeeded)
            // {
            //     foreach (var identityError in result.Errors)
            //     {
            //         Console.WriteLine("Cant create role! Error: " + identityError.Description);
            //     }
            // }

            var user = new AppUser();
            user.Email = "caspar@gmail.com";
            // user.Address = "Peetri 5";
            user.PhoneNumber = "53058522";
            user.FirstName = "Caspar";
            user.LastName = "Sillamaa";
            user.UserName = user.Email;

            var user2 = new AppUser();
            user2.Email = "caspar2@gmail.com";
            // user2.Address = "Peetri 2";
            user2.PhoneNumber = "53058522kaks";
            user2.FirstName = "Caspar2";
            user2.LastName = "Sillamaa2";
            user2.UserName = user2.Email;
            user2.CompanyId = 1;
            
            var user3 = new AppUser();
            user3.Email = "caspar3@gmail.com";
            // user3.Address = "Peetri 3";
            user3.PhoneNumber = "53058522kolm";
            user3.FirstName = "Caspar3";
            user3.LastName = "Sillamaa3";
            user3.UserName = user3.Email;
            user3.CompanyId = 1;
            
            var user4 = new AppUser();
            user4.Email = "NoRoles@gmail.com";
            // user4.Address = "Peetri 5";
            user4.PhoneNumber = "53058522";
            user4.FirstName = "Caspar";
            user4.LastName = "Sillamaa";
            user4.UserName = user4.Email;
            user4.CompanyId = 1;
            
            
            var user5 = new AppUser();
            user5.Email = "client@gmail.com";
            // user5.Address = "Peetri 5";
            user5.PhoneNumber = "53058522";
            user5.FirstName = "Client";
            user5.LastName = "first";
            user5.UserName = user5.Email;


            result = userManager.CreateAsync(user, "Kool1234!").Result;
            result = userManager.CreateAsync(user2, "Kool1234!").Result;

            result = userManager.CreateAsync(user3, "Kool1234!").Result;
            result = userManager.CreateAsync(user4, "Kool1234!").Result;
            result = userManager.CreateAsync(user5, "Kool1234!").Result;
            // if (!result.Succeeded)
            // {
            //     foreach (var identityError in result.Errors)
            //     {
            //         Console.WriteLine("Cant create user! Error: " + identityError.Description);
            //     }
            // }

            result = userManager.AddToRoleAsync(user, "Admin").Result;
            result = userManager.AddToRoleAsync(user2, "Company Admin").Result;
            // result = userManager.AddToRoleAsync(user2, "Restaurant Worker").Result;
            result = userManager.AddToRoleAsync(user3, "Restaurant Worker").Result;
            
            
            // var companyWorker2 = new AppUserWorkRestaurant
            // {
            //     RestaurantId = 1,
            //     AppUserId = user2.Id,
            //
            // };
            
            var companyWorker3 = new AppUserWorkRestaurant
            {
                RestaurantId = 1,
                AppUserId = user3.Id,
            
            };
            var companyWorker4 = new AppUserWorkRestaurant
            {
                RestaurantId = 2,
                AppUserId = user3.Id,
            
            };
            
             // ctx.CompanyAdminAppusers.Add(companyAdmin);
             // ctx.AppUserWorkRestaurants.Add(companyWorker2);
             ctx.AppUserWorkRestaurants.Add(companyWorker3);
             ctx.AppUserWorkRestaurants.Add(companyWorker4);
            
             ctx.SaveChanges();
             
             SeedAppDataBasket(ctx);
             SeedAppDataActiveOrder(ctx);
             SeedAppDataArchivedOrder(ctx);

             //
             // var userFavRest2 = new AppUserFavoriteRestaurant()
             // {
             //     RestaurantId = 1,
             //     AppUserId = user2.Id,
             //
             // };
             //
             //  
             // var userFavRest1 = new AppUserFavoriteRestaurant()
             // {
             //     RestaurantId = 2,
             //     AppUserId = user2.Id,
             //
             // };

             // ctx.AppUserFavoriteRestaurants.Add(userFavRest1);
             // ctx.AppUserFavoriteRestaurants.Add(userFavRest2);
             //
             // ctx.SaveChanges();

             // if (!result.Succeeded)
             // {
             //     foreach (var identityError in result.Errors)
             //     {
             //         Console.WriteLine("Cant add user to role! Error: " + identityError.Description);
             //     }
             // }
/*
            result = userManager.UpdateAsync(user).Result;
            if (!result.Succeeded)
            {
                foreach (var identityError in result.Errors)
                {
                    Console.WriteLine("Cant update user! Error: " + identityError.Description);
                }
            }
*/
        }
    }
}