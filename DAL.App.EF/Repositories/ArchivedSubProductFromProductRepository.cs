﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Mappers;
using DAL.Base.EF.Repositories;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;

namespace DAL.App.EF.Repositories
{
    public class ArchivedSubProductFromProductRepository : BaseRepository<DALAppDTOOrderArchived.ArchivedSubProductFromProduct,
        Domain.App.Archive.ArchivedSubProductFromProduct, AppDbContext>, IArchivedSubProductFromProductRepository
    {
        public ArchivedSubProductFromProductRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new ArchivedSubProductFromProductMapper(mapper))
        {
        }
        
      
    }
}