﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;

namespace DAL.App.EF.Repositories
{
    public class ArchivedProductInOrderRepository : BaseRepository<DALAppDTOOrderArchived.ArchivedProductInOrder, Domain.App.Archive.ArchivedProductInOrder, AppDbContext>, 
        IArchivedProductInOrderRepository
    {
        public ArchivedProductInOrderRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new ArchivedProductInOrderMapper(mapper))
        {
        }
        
    }
}