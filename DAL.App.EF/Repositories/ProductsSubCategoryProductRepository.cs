﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Mappers;
using DAL.App.DTO.Product;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class ProductsSubCategoryProductRepository: 
        BaseRepository<ProductsSubCategoryProduct, Domain.App.ProductsSubCategoryProduct, AppDbContext>,
        IProductsSubCategoryProductRepository
    {
        public ProductsSubCategoryProductRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new ProductsSubCategoryProductMapper(mapper))
        {
        }
        
        public async Task<int> MoveSubCategoryProductsUpWhenDelete(int subCategoryProductId)
        {
            var query = CreateQuery();

            var subCategoryProduct = await query
                .Where(p => p.Id.Equals(subCategoryProductId))
                .FirstOrDefaultAsync();

            var subCategoryProducts = await query
                .Where(p => p.ProductsSubCategoryId.Equals(subCategoryProduct.ProductsSubCategoryId) 
                            && p.Position > subCategoryProduct.Position)
                .ToListAsync();

            var counter = 0;
            foreach (var item in subCategoryProducts)
            {
                counter++;
                item.Position -= 1;
                RepoDbSet.Update(item);
            }

            // await RepoDbContext.SaveChangesAsync();
            return counter;
        }

        public async Task<int> GetProductsSubCategoryProductCompanyId(int id)
        {
            var query = CreateQuery();
            var res = await query.Where(p => p.Id.Equals(id))
                .Include(p => p.ProductsSubCategory)
                .ThenInclude(c => c.Product)
                .ThenInclude(p => p.MenuCategory)
                .ThenInclude(c => c.Menu)
                .Select(p => p.ProductsSubCategory.Product.MenuCategory.Menu.CompanyId)
                .FirstOrDefaultAsync();
            
            return res;
        }

        public int GetProductSubCategoryProductCountInSubCategory(int subCategoryId)
        {
            var query = CreateQuery();

            var res = query
                .Count(p => p.ProductsSubCategoryId.Equals(subCategoryId));

            return res;
        }

        public async Task<int> GetProductId(int id)
        {
            var query = CreateQuery();
            var res = await query.Where(p => p.Id.Equals(id))
                .Include(p => p.ProductsSubCategory)
                .Select(p => p.ProductsSubCategory.ProductId)
                .FirstOrDefaultAsync();
            
            return res;
        }
        
        public async Task<bool> ChangeSubCategoryProductPosition(int id, bool moveUp)
        {
            var query = CreateQuery();
            
            var subCategoryProduct1 = await query
                .Where(c => c.Id.Equals(id))
                .FirstOrDefaultAsync();
            
            var subCategoryProduct2 = await query
                .Where(c => c.ProductsSubCategoryId.Equals(subCategoryProduct1.ProductsSubCategoryId) && 
                            c.Position.Equals(moveUp ? subCategoryProduct1.Position - 1 : subCategoryProduct1.Position + 1))
                .FirstOrDefaultAsync();
            if (subCategoryProduct2 == null)
            {
                return false;
            }

            subCategoryProduct1.Position = moveUp ? subCategoryProduct1.Position - 1 : subCategoryProduct1.Position + 1;
            subCategoryProduct2.Position = moveUp ? subCategoryProduct1.Position + 1 : subCategoryProduct1.Position - 1;

            RepoDbContext.Update(subCategoryProduct1);
            RepoDbContext.Update(subCategoryProduct2);
            await RepoDbContext.SaveChangesAsync();
            return true;
        }

        public async Task<ProductsSubCategoryProductData?> FirstOrDefaultEditAsync(int id, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(p => p.Id.Equals(id))
                .FirstOrDefaultAsync();

            return ProductsSubCategoryProductMapper.MapToData(res);
        }

        public async Task<BLL.App.DTO.Product.ProductsSubCategoryProductData?> FirstOrDefaultEditAsyncBLL(int id, bool noTracking = true)
        {
            var dalSubProduct = await FirstOrDefaultEditAsync(id, noTracking);


            return BLL.App.DTO.Mappers.ProductsSubCategoryProductMapper.Map(dalSubProduct);
        }

        public bool AreSubProductsFromTheProduct(int productId, int[] subCategoryProducts, bool noTracking = true)
        {
            if (subCategoryProducts.Length == 0)
            {
                return true;
            }
            
            var query = CreateQuery(default, noTracking);

            var count = query
                .Count(p => subCategoryProducts.Contains(p.Id) && p.ProductsSubCategory.ProductId.Equals(productId));

            return count == subCategoryProducts.Length;
            //
            // int? lastSubProductCategoryId = null;
            // foreach (var subProductId in subCategoryProducts)
            // {
            //     var currentSubProductCategoryId = await query
            //         .Include(p => p.ProductsSubCategory)
            //         .Where(p => p.Id.Equals(subProductId) && p.ProductsSubCategory.ProductId.Equals(productId))
            //         .Select(p => p.ProductsSubCategoryId)
            //         .FirstOrDefaultAsync();
            //
            //     if (currentSubProductCategoryId == default || lastSubProductCategoryId != null && currentSubProductCategoryId != lastSubProductCategoryId)
            //     {
            //         return false;
            //     }
            //
            //     lastSubProductCategoryId = currentSubProductCategoryId;
            // }
            //
            // return true;
        }

        public async Task<IEnumerable<ProductsSubCategoryProductOnlyPrice>> GetAllPricesById(int[] subProductIds, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(p => subProductIds.Contains(p.Id))
                .Select(p => ProductsSubCategoryProductMapper.MapToDalOnlyPrice(p))
                .ToListAsync();

            return res;
        }

        public async Task<bool> IsThereTooManySubProductsFromACategory(int[] allSubProductIds, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(p => allSubProductIds.Contains(p.Id))
                .Include(p => p.ProductsSubCategory)
                .Select(p => ProductsSubCategoryMapper.MapToDalItemCount(p))
                .ToListAsync();

            foreach (var subCategory in res)
            {
                var categoryItemCount = res.Count(x => x.Id.Equals(subCategory.Id));
                if (categoryItemCount > subCategory.MaxItemCount || categoryItemCount < subCategory.MandatoryItemCount)
                {
                    return false;
                }
            }
            
            return true;
        }
    }
}