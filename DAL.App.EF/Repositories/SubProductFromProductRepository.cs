﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Mappers;
using DAL.Base.EF.Repositories;
using DALAppDTOOrder = DAL.App.DTO.Order;
using BLLAppDTOOrder = BLL.App.DTO.Order;

namespace DAL.App.EF.Repositories
{
    public class SubProductFromProductRepository : BaseRepository<DALAppDTOOrder.SubProductFromProduct, Domain.App.SubProductFromProduct, AppDbContext>, ISubProductFromProductRepository
    {
        public SubProductFromProductRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new SubProductFromProductMapper(mapper))
        {
        }
        
      
    }
}