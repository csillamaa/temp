﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Mappers;
using DAL.App.DTO.Order;
using DAL.Base.EF.Repositories;
using Domain.App;
using Microsoft.EntityFrameworkCore;
using Order = DAL.App.DTO.Order.Order;
using ProductInOrder = Domain.App.ProductInOrder;
using DALAppDTO = DAL.App.DTO;

namespace DAL.App.EF.Repositories
{
    public class OrderRepository : BaseRepository<Order, Domain.App.Order, AppDbContext>, IOrderRepository
    {
        public OrderRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new OrderMapper(mapper))
        {
        }

        public async Task<ICollection<OrderWorker>?> GetAllRestaurantActiveOrdersAsync(int restaurantId, bool isReady, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.RestaurantId.Equals(restaurantId) && o.IsProcessed && o.IsReady.Equals(isReady)
                            && !o.ProductsInOrder
                                .Any(p => p.SubProducts != null && p.SubProducts
                                    .Any(x => x.ProductsSubCategoryProduct == null)))
                .Include(o => o.OrderAppUsers)
                .ThenInclude(u => u.AppUser)
                .Include(o => o.ProductsInOrder)
                .ThenInclude(p => p.Product)
                .Include(o => o.ProductsInOrder)
                .ThenInclude(p => p.SubProducts)
                .ThenInclude(s => s.ProductsSubCategoryProduct)
                .ThenInclude(p => p!.ProductsSubCategory)
                .Select(o => OrderMapper.MapToDalOrderWorker(o))
                .ToListAsync();

            return res;
        }

        public async Task<ICollection<BLL.App.DTO.Order.OrderWorker>?> GetAllRestaurantActiveOrdersAsyncBLL(int restaurantId, bool isReady, bool noTracking = true)
        {
            var dalOrders = await GetAllRestaurantActiveOrdersAsync(restaurantId, isReady, noTracking);

            var bllOrders = dalOrders?.Select(BLL.App.DTO.Mappers.OrderMapper.MapToBLLOrderWorker).ToList();

            return bllOrders;
        }

        
        public async Task<Domain.App.Order?> FirstOrDefaultAsync(int orderId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);
            
            var res = await query
                .Where(o => o.Id.Equals(orderId) && o.IsProcessed && o.IsReady
                            && !o.ProductsInOrder
                                .Any(p => p.SubProducts != null && p.SubProducts
                                    .Any(x => x.ProductsSubCategoryProduct == null)))
                .Include(o => o.OrderAppUsers)
                .ThenInclude(u => u.AppUser)
                .Include(o => o.ProductsInOrder)
                .ThenInclude(p => p.SubProducts)
                // .ThenInclude(s => s.ProductsSubCategoryProduct)
                // .ThenInclude(p => p!.ProductsSubCategory)
                .FirstOrDefaultAsync();
            
            return res;
        }

        public async Task<OrderClient?> FirstOrDefaultClientAsync(int userId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => !o.IsProcessed && o.OrderAppUsers.Any(u => u.AppUserId.Equals(userId))
                                           && !o.ProductsInOrder
                    .Any(p => p.SubProducts != null && p.SubProducts
                        .Any(x => x.ProductsSubCategoryProduct == null)))
                .Include(o => o.Restaurant)
                .Include(o => o.ProductsInOrder)
                .ThenInclude(p => p.Product)
                .Include(o => o.ProductsInOrder)
                .ThenInclude(p => p.SubProducts)
                .ThenInclude(s => s.ProductsSubCategoryProduct)
                .ThenInclude(p => p!.ProductsSubCategory)
                .Select(o => OrderMapper.MapToDalOrderClient(o))
                .FirstOrDefaultAsync();

            return res;
        }
        

        public async Task<BLL.App.DTO.Order.OrderClient?> FirstOrDefaultClientAsyncBLL(int userId, bool noTracking = true)
        {
            var dalOrder = await FirstOrDefaultClientAsync(userId, noTracking);

            var bllOrder = BLL.App.DTO.Mappers.OrderMapper.MapToBLLOrderClient(dalOrder);
            
            return bllOrder;
        }
        
        public async Task<List<OrderClient>?> GetAllActiveClientAsync(int userId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.IsProcessed && o.OrderAppUsers.Any(u => u.AppUserId.Equals(userId)) && !o.ProductsInOrder
                    .Any(p => p.SubProducts != null && p.SubProducts
                        .Any(x => x.ProductsSubCategoryProduct == null)))
                .Include(o => o.Restaurant)
                .Include(o => o.ProductsInOrder)
                .ThenInclude(p => p.Product)
                .Include(o => o.ProductsInOrder)
                .ThenInclude(p => p.SubProducts)
                .ThenInclude(s => s.ProductsSubCategoryProduct)
                .ThenInclude(p => p!.ProductsSubCategory)
                .Select(o => OrderMapper.MapToDalOrderClient(o))
                .ToListAsync();

            return res!;
        }
        
        public async Task<List<BLL.App.DTO.Order.OrderClient>?> GetAllActiveClientAsyncBLL(int userId, bool noTracking = true)
        {
            var dalOrders = await GetAllActiveClientAsync(userId, noTracking);

            var bllOrders = dalOrders?.Select(BLL.App.DTO.Mappers.OrderMapper.MapToBLLOrderClient).ToList();
            
            return bllOrders!;
        }

        public async Task<bool> AddCustomerCommentToOrder(int userId, int orderId, string comment, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(orderId) && o.OrderAppUsers.Any(u => u.AppUserId.Equals(userId)))
                .FirstOrDefaultAsync();

            if (res == null)
            {
                return false;
            }
            
            res.CustomerComment = comment;
            RepoDbSet.Update(res);
            await RepoDbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DoesOrderBelongToCustomer(int userId, int orderId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(orderId) && o.OrderAppUsers.Any(u => u.AppUserId.Equals(userId)))
                .FirstOrDefaultAsync();
            
            return res != null;
        }

        public async Task<bool> SubmitOrder(int orderId, string? cardNumber, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(orderId))
                // .Include(o => o.Restaurant)
                // .Where(o => o.Restaurant.IsPublic)
                .FirstOrDefaultAsync();

            if (res == null)
            {
                return false;
            }

            if (cardNumber != null)
            {
                res.AmountPaid = res.FoodCost;
            }
            res.IsProcessed = true;
            res.SubmitTime = DateTime.Now;
            var rnd = new Random();
            var rndTime = rnd.Next(0, 15);
            
            res.InitialCompleteTimeEstimate = DateTime.Now.AddMinutes(30 + rndTime);
            res.CompletedBy = res.InitialCompleteTimeEstimate;

            RepoDbSet.Update(res);
            await RepoDbContext.SaveChangesAsync();
            
            return true;
        }

        public async Task<bool> DeleteIfOrderIsEmpty(int orderId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(orderId) && o.ProductsInOrder.Count == 0)
                .FirstOrDefaultAsync();

            if (res == null)
            {
                return false;
            }

            RepoDbSet.Remove(res);
            await RepoDbContext.SaveChangesAsync();
            
            return true;
        }

        public async Task<bool> ReCalculateOrderCost(int orderId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(orderId))
                .Include(o => o.ProductsInOrder)
                .FirstOrDefaultAsync();

            if (res == null)
            {
                return false;
            }
            double orderCost = 0;
            
            foreach (var productInOrder in res.ProductsInOrder)
            {
                orderCost += productInOrder.Amount * productInOrder.OneProductPrice;
            }

            res.ProductsInOrder = new List<ProductInOrder>();
            res.FoodCost = orderCost;
            RepoDbSet.Update(res);
            await RepoDbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ChangeOrderCompleteTime(int orderId, DateTime newCompleteTime, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(orderId) && !o.IsReady && o.IsProcessed)
                .FirstOrDefaultAsync();

            if (res == null)
            {
                return false;
            }
            res.CompletedBy = newCompleteTime;
            RepoDbSet.Update(res);
            await RepoDbContext.SaveChangesAsync();

            return true;
        }

        public async Task<int?> GetRestaurantId(int orderId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(orderId))
                .Select(o => o.RestaurantId)
                .FirstOrDefaultAsync();

            return res;
        }

        public async Task<bool> ChangeOrderToReady(int orderId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(orderId) && !o.IsReady && o.IsProcessed)
                .FirstOrDefaultAsync();

            if (res == null)
            {
                return false;
            }
            res.IsReady = true;
            res.CompletedBy = DateTime.Now;
            RepoDbSet.Update(res);
            await RepoDbContext.SaveChangesAsync();

            return true;
        }

        public async Task<int?> GetCompanyId(int orderId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(orderId))
                .Include(o => o.Restaurant)
                .Select(o => o.Restaurant.CompanyId)
                .FirstOrDefaultAsync();

            return res;
        }

        public async Task<bool> ArchiveOrder(int orderId, bool noTracking = true)
        {
            var order = await FirstOrDefaultAsync(orderId, noTracking);
            if (order == null)
            {
                return false;
            }
     
            //TODO: comment, should the datetime be generated here?
            var archivedOrder = new Domain.App.Archive.ArchivedOrder
            {
                SubmitTime = order.SubmitTime,
                EstimatedCompleteTime = order.InitialCompleteTimeEstimate,
                ActualCompleteTime = order.CompletedBy,
                // OrderHandedOver = order.OrderHandedOverBy,
                OrderHandedOver = DateTime.Now,
                IsSelfPickup = order.IsSelfPickup,
                DeliveryCost = order.DeliveryCost,
                FoodCost = order.FoodCost,
                AmountPaid = order.AmountPaid,
                CustomerComment = order.CustomerComment,
                RestaurantId = order.RestaurantId
            };
           
            RepoDbContext.ArchivedOrders.Add(archivedOrder);
            await RepoDbContext.SaveChangesAsync();
            
            foreach (var user in order.OrderAppUsers)
            {
                var archivedUser = new Domain.App.Archive.ArchivedAppUserOrder
                {
                    AppUserId = user.AppUserId,
                    ArchivedOrderId = archivedOrder.Id
                };
                RepoDbContext.ArchivedAppUserOrders.Add(archivedUser);
                await RepoDbContext.SaveChangesAsync();
            }
         
            foreach (var productInOrder in order.ProductsInOrder)
            {
                var archivedProductInorder = new Domain.App.Archive.ArchivedProductInOrder
                {
                    Amount = productInOrder.Amount,
                    OneProductPrice = productInOrder.OneProductPrice,
                    ProductId = productInOrder.ProductId!.Value,
                    ArchivedOrderId = archivedOrder.Id
                };
                RepoDbContext.ArchivedProductsInOrder.Add(archivedProductInorder);
                await RepoDbContext.SaveChangesAsync();
                
                var subProducts = productInOrder.SubProducts;
                if (subProducts == null) continue;
                foreach (var subProduct in subProducts)
                {
                    var archivedSubProductFromProduct = new Domain.App.Archive.ArchivedSubProductFromProduct
                    {
                        ProductsSubCategoryProductId = subProduct.ProductsSubCategoryProductId,
                        ArchivedProductInOrderId = archivedProductInorder.Id,
                        Amount = subProduct.Amount
                    };
                    RepoDbContext.ArchivedSubProductsFromProduct.Add(archivedSubProductFromProduct);
                }
                        
            } 
            await RepoDbContext.SaveChangesAsync();
            RepoDbSet.Remove(order);
            await RepoDbContext.SaveChangesAsync();
            
            return true;
        }

        public async Task<bool> IsUserBasketEmptyOrFromSameRestaurant(int userId, int restaurantId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);
            
            
            // Does user have a empty basket?
            var res = await query
                .Where(o => !o.IsProcessed && o.OrderAppUsers.Any(u => u.AppUserId.Equals(userId)))
                .FirstOrDefaultAsync();

            if (res == null)
            {
                return true;
            }
            
            // If user's basket isn't empty, is the basket from the same restaurant as the currently added product is.
            res = await query
                .Where(o => !o.IsProcessed && o.RestaurantId.Equals(restaurantId))
                .FirstOrDefaultAsync();

            return res != null;
        }

       

        public async Task<bool> AddProductToOrder(int userBasketId, int userId, DALAppDTO.Product.ProductOnlyPrice product, int restaurantId, int amount, 
            IEnumerable<DAL.App.DTO.Product.ProductsSubCategoryProductOnlyPrice> allSubProductsWithIdAndPrice, bool noTracking = true)
        {
            Domain.App.Order order;
            if (userBasketId == default)
            {
                order = new Domain.App.Order
                {
                    IsReady = false,
                    AmountPaid = 0,
                    FoodCost = 0,
                    DeliveryCost = 0,
                    IsProcessed = false,
                    IsSelfPickup = true,
                    RestaurantId = restaurantId
                };
                
                RepoDbSet.Add(order);
                await RepoDbContext.SaveChangesAsync();
                userBasketId = order.Id;
                
                var orderUser = new AppUserOrder
                {
                    AppUserId = userId,
                    OrderId = userBasketId
                };

                RepoDbContext.AppUserOrders.Add(orderUser);
            }

            var productInOrder = new ProductInOrder
            {
                OrderId = userBasketId,
                ProductId = product.Id,
                Amount = amount,
                OneProductPrice = product.Price
            };

            RepoDbContext.ProductsInOrder.Add(productInOrder);
            await RepoDbContext.SaveChangesAsync();


            foreach (var subProduct in allSubProductsWithIdAndPrice)
            {
                var subProductFromProduct = new Domain.App.SubProductFromProduct
                {
                    ProductInOrderId = productInOrder.Id,
                    ProductsSubCategoryProductId = subProduct.Id,
                    Amount = 1,
                };
                RepoDbContext.SubProductsFromProduct.Add(subProductFromProduct);

                productInOrder.OneProductPrice += subProduct.Price;

            }
            
            RepoDbContext.ProductsInOrder.Update(productInOrder);
            await RepoDbContext.SaveChangesAsync();
            return true;
        }

        public async Task<int> GetUserBasketId(int userId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => !o.IsProcessed && o.OrderAppUsers.Any(u => u.AppUserId.Equals(userId)))
                .Select(o => o.Id)
                .FirstOrDefaultAsync();

            return res;
        }

        public async Task<bool> DeleteOrderIfNewProductFromDifferentRestaurant(int userBasketId, int restaurantId, bool noTracking = true)
        {
            if (userBasketId == default)
            {
                return false;
            }
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(userBasketId))
                .Select(o => o.RestaurantId)
                .FirstOrDefaultAsync();
            if (res == restaurantId)
            {
                return false;
            }
            
            var oldOrder = await query
                .Where(o => o.Id.Equals(userBasketId))
                .FirstOrDefaultAsync();
            
            RepoDbSet.Remove(oldOrder);
            await RepoDbContext.SaveChangesAsync();

            return true;
        }
    }
}