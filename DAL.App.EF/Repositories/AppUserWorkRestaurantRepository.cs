﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class AppUserWorkRestaurantRepository: BaseRepository<DAL.App.DTO.AppUserWorkRestaurant,
        Domain.App.AppUserWorkRestaurant, AppDbContext>, IAppUserWorkRestaurantRepository
    {
        public AppUserWorkRestaurantRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new AppUserWorkRestaurantMapper(mapper))
        {
        }

        public override async Task<DAL.App.DTO.AppUserWorkRestaurant?> FirstOrDefaultAsync(int id, int userId = default,
            bool noTracking = true)
        {
            var query = CreateQuery(userId, noTracking);

            var res = await query.FirstOrDefaultAsync(r => r.RestaurantId.Equals(id));
            

            return Mapper.Map(res);
        }

        public async Task<IList<DAL.App.DTO.AppUserWorkRestaurant>?> RemoveFromAllRestaurants(int id, bool noTracking = true)
        {
            
            // TODO can i just ignore the nullables?

            var query = await CreateQuery(id, noTracking).ToListAsync();

            foreach (var restaurant in query)
            {
                RepoDbSet.Remove(restaurant);
            }
            
            await RepoDbContext.SaveChangesAsync();

            //TODO: What to return here, should loop through list and map them and then return or just return nothing?
            return null;
        }

        public int[]? GetAppUserWorkRestaurantIds(int userId, bool noTracking = true)
        {
            var query = CreateQuery(userId, noTracking);

            var resQuery = query
                // .Where(r => r.AppUserId.Equals(userId))
                .Select(r => r.RestaurantId);
             

            return resQuery.ToArray();

        }
    }
}