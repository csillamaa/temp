﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Identity;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.DTO.Mappers;
using DAL.App.DTO.Restaurant;
using DAL.Base.EF.Repositories;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using AppUserIndex = DAL.App.DTO.Identity.AppUserIndex;

namespace DAL.App.EF.Repositories
{
    public class CompanyRepository: BaseRepository<DAL.App.DTO.Company, Domain.App.Company, AppDbContext>, ICompanyRepository
    {
        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;
        public CompanyRepository(AppDbContext dbContext, IMapper mapper, UserManager<Domain.App.Identity.AppUser> userManager) : base(dbContext, new CompanyMapper(mapper))
        {
            _userManager = userManager;
        }

        
        public async Task<IEnumerable<DAL.App.DTO.CompanyOnlyName>> GetAllJustNameAsync(bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var resQuery = query.Select(c => CompanyMapper.MapToDalJustName(c));
           
            return await resQuery.ToListAsync();
        }
        
        public async Task<IEnumerable<BLL.App.DTO.CompanyOnlyName>> GetAllJustNameAsyncBLL(bool noTracking = true)
        {
            var dalCompanies = await GetAllJustNameAsync();
            var bllCompanies = new Collection<BLL.App.DTO.CompanyOnlyName>();

            
            foreach (var dalCompany in dalCompanies)
            {
                bllCompanies.Add(BLL.App.DTO.Mappers.CompanyMapper.MapToBLL(dalCompany));
            }
           
            return bllCompanies;
        }

        public async Task<Company?> FirstOrDefaultDeleteAsync(int id, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(c => c.Id.Equals(id))
                .FirstOrDefaultAsync();

            return Mapper.Map(res);
        }

        public override async Task<DAL.App.DTO.Company?> FirstOrDefaultAsync(int id, int userId = default, bool noTracking = true)
        { 
            var query = CreateQuery(default, noTracking);
            
            query = query
                .Where(c => c.Id.Equals(id))
                .Include(c => c.Restaurants)
                .ThenInclude(r => r.Workers)
                .ThenInclude(rw => rw.AppUser)
                .Include(c => c.CompanyUsers);

            return Mapper.Map(await query.FirstOrDefaultAsync());
        }


     
        public override async Task<DAL.App.DTO.Company> RemoveAsync(int id, int userId)
        {
            var company = await FirstOrDefaultAsync(id);
            if (company == null)
            {
                throw new NullReferenceException($"Company with id {id} not found.");
            }
            var restaurants = company.Restaurants?.ToList();
            var companyAdmins = company.CompanyUsers?.ToList();

            if (restaurants != null){ 
                foreach (var restaurant in restaurants)
                {
                    // HOW TO REMOVE HERE, MAP AND THEN REMOVE OR REMOVE OUTSIDE THIS CLASS
                    // RepoDbContext.Restaurants.Remove(restaurant);
                }
            }

            // if (companyAdmins != null)
            // {
            //     foreach (var admin in companyAdmins)
            //     {
            //         admin.Company = null;
            //     }
            // }

            await RepoDbContext.SaveChangesAsync();

            return base.Remove(company);
        }

        // public async Task<(List<DAL.App.DTO.Identity.AppUser>, List<DAL.App.DTO.Identity.AppUser>)> GetAllCompanyAdminsAndWorkers(int companyId, bool noTracking = true)
        // {
        //     var companyUsers = await _userManager.Users
        //         .Where(u => u.CompanyId.Equals(companyId))
        //         .Include(u => u.WorkRestaurants)
        //         .ThenInclude(u => u.Restaurant)
        //         .ToListAsync();
        //     var companyAdmins = new List<DAL.App.DTO.Identity.AppUser>();
        //     var companyWorkers = new List<DAL.App.DTO.Identity.AppUser>();
        //
        //     foreach (var companyUser in companyUsers)
        //     {
        //         var userRoles = await _userManager.GetRolesAsync(companyUser);
        //         if (userRoles.Contains("Company Admin"))
        //         {
        //             companyAdmins.Add(companyUser);
        //         }
        //         
        //         if (userRoles.Contains("Restaurant Worker"))
        //         {
        //             companyWorkers.Add(companyUser);
        //         }
        //     }
        //     return (companyAdmins, companyWorkers);
        // }
        
    }
}