﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Archive;
using DAL.App.DTO.Mappers;
using DAL.Base.EF.Repositories;
using Domain.App;
using Microsoft.EntityFrameworkCore;
using ArchivedOrder = Domain.App.Archive.ArchivedOrder;
using Order = DAL.App.DTO.Order.Order;

namespace DAL.App.EF.Repositories
{
    public class ArchivedOrderRepository : BaseRepository<DAL.App.DTO.Archive.ArchivedOrder, Domain.App.Archive.ArchivedOrder, AppDbContext>, IArchivedOrderRepository
    {
        public ArchivedOrderRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new ArchivedOrderMapper(mapper))
        {
        }
        
        public async Task<ICollection<ArchivedOrderWorker>?> GetAllRestaurantCompletedOrdersAsync(int restaurantId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.RestaurantId.Equals(restaurantId) && !o.ArchivedProductsInOrder
                                .Any(p => p.ArchivedSubProducts != null && p.ArchivedSubProducts
                                    .Any(x => x.ProductsSubCategoryProduct == null)))
                .Include(o => o.AppUsers)
                .ThenInclude(u => u.AppUser)
                .Include(o => o.ArchivedProductsInOrder)
                .ThenInclude(p => p.Product)
                .Include(o => o.ArchivedProductsInOrder)
                .ThenInclude(p => p.ArchivedSubProducts)
                .ThenInclude(s => s.ProductsSubCategoryProduct)
                .ThenInclude(p => p!.ProductsSubCategory)
                .Select(o => ArchivedOrderMapper.MapToDalOrderWorker(o))
                .ToListAsync();

            return res;
        }

        public async Task<ICollection<BLL.App.DTO.Archive.ArchivedOrderWorker>?> GetAllRestaurantCompletedOrdersAsyncBLL(int restaurantId, bool noTracking = true)
        {
            var dalOrders = await GetAllRestaurantCompletedOrdersAsync(restaurantId, noTracking);

            var bllOrders = dalOrders?.Select(BLL.App.DTO.Mappers.ArchivedOrderMapper.MapToBLLOrderWorker).ToList();

            return bllOrders;
        }

        public async Task<List<ArchivedOrderClient>?> GetAllCompletedOrdersClientAsync(int userId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o =>  o.AppUsers.Any(u => u.AppUserId.Equals(userId)) && !o.ArchivedProductsInOrder
                    .Any(p => p.ArchivedSubProducts != null && p.ArchivedSubProducts
                        .Any(x => x.ProductsSubCategoryProduct == null)))
                .Include(o => o.Restaurant)
                .Include(o => o.ArchivedProductsInOrder)
                .ThenInclude(p => p.Product)
                .Include(o => o.ArchivedProductsInOrder)
                .ThenInclude(p => p.ArchivedSubProducts)
                .ThenInclude(s => s.ProductsSubCategoryProduct)
                .ThenInclude(p => p!.ProductsSubCategory)
                .Select(o => ArchivedOrderMapper.MapToDalOrderClient(o))
                .ToListAsync();

            return res!;
        }

        public async Task<List<BLL.App.DTO.Archive.ArchivedOrderClient>?> GetAllCompletedOrdersClientAsyncBLL(int userId, bool noTracking = true)
        {
            var dalOrders = await GetAllCompletedOrdersClientAsync(userId, noTracking);

            var bllOrders = dalOrders?.Select(BLL.App.DTO.Mappers.ArchivedOrderMapper.MapToBLLOrderClient).ToList();
            
            return bllOrders!;
        }

        public async Task<bool> AddCommentToOrder(int orderId, string comment, bool isCustomerComment, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(orderId))
                .FirstOrDefaultAsync();

            if (isCustomerComment)
            {
                res.CustomerOrderReviewComment = comment;
            }
            else
            {
                res.Comment = comment;
            }

            RepoDbSet.Update(res);
            await RepoDbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> AddRatingToOrder(int orderId, int rating, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(orderId))
                .FirstOrDefaultAsync();

            res.OrderRating = rating;
            
            RepoDbSet.Update(res);
            await RepoDbContext.SaveChangesAsync();

            return true;
        }
        
        public async Task<int?> GetCompanyId(int orderId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(orderId))
                .Include(o => o.Restaurant)
                .Select(o => o.Restaurant.CompanyId)
                .FirstOrDefaultAsync();

            return res;
        }
        
        public async Task<int?> GetRestaurantId(int orderId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(o => o.Id.Equals(orderId))
                .Select(o => o.RestaurantId)
                .FirstOrDefaultAsync();

            return res;
        }
    }
}