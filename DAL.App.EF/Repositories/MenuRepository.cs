﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Mappers;
using DALAppDTOMenu = DAL.App.DTO.Menu;
using BLLAppDTOMenu = BLL.App.DTO.Menu;
using DAL.Base.EF.Repositories;
using Domain.App;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class MenuRepository: BaseRepository<DALAppDTOMenu.Menu, Domain.App.Menu, AppDbContext>, IMenuRepository
    {
        public MenuRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new MenuMapper(mapper))
        {
        }

        public async Task<DALAppDTOMenu.MenuEdit?> FirstOrDefaultEditAsync(int menuId, int? categoryPicked,
            bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);
            
            var menu = await query
                .Where(m => m.Id.Equals(menuId))
                .Include(m => m.Company)
                .Include(m => m.RestaurantsWithTheMenu)
                    .ThenInclude(r => r.Restaurant)
                .Include(m => m.MenuCategories)
                    .ThenInclude(c => c.Products)
                        .ThenInclude(p => p.ProductsSubCategories)
                            .ThenInclude(sc => sc.SubCategoryProducts)
                .Select(m => MenuMapper.MapToDalEdit(m))
                .FirstOrDefaultAsync();

            return menu;
        }


        public async Task<BLLAppDTOMenu.MenuEdit?> FirstOrDefaultEditAsyncBLL(int menuId, int? categoryPicked,
            bool noTracking = true)
        {
            var dalMenu = await FirstOrDefaultEditAsync(menuId, categoryPicked, noTracking);
            if (dalMenu == null)
            {
                return null;
            }

            var bllMenu = BLL.App.DTO.Mappers.MenuMapper.Map(dalMenu);
            return bllMenu;
        }

            public async Task<IEnumerable<DALAppDTOMenu.MenuIndex>> GetAllIndexASync(int? companyId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);
            if (companyId != null)
            {
                query = query.Where(m => m.CompanyId.Equals(companyId));
            }

            var resQuery = query
                .Include(m => m.Company)
                .Include(m => m.RestaurantsWithTheMenu)
                    .ThenInclude(r => r.Restaurant)
                // .Include(m => m.MenuCategories)
                //     .ThenInclude(c => c.Products)
                //         .ThenInclude(p => p.ProductSubCategories)
                //             .ThenInclude(sc => sc.SubCategoryProducts)
                .Select(m => MenuMapper.MapToDalIndex(m));

            return await resQuery.ToListAsync();
        }

        public async Task<IEnumerable<BLLAppDTOMenu.MenuIndex>> GetAllIndexASyncBLL(int? companyId, bool noTracking = true)
        {
            var dalMenus = await GetAllIndexASync(companyId, noTracking);
            var bllMenus = dalMenus.Select(BLL.App.DTO.Mappers.MenuMapper.Map);

            return bllMenus;
        }

        public async Task<List<DALAppDTOMenu.MenuOnlyName>?> GetAllUnPickedCompanyMenus(int companyId, int restaurantId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var resQuery = query
                .Where(m => m.CompanyId.Equals(companyId))
                .Where(m => m.RestaurantsWithTheMenu == null ||
                            !m.RestaurantsWithTheMenu.Any(r => r.RestaurantId.Equals(restaurantId)))
                .Select(m => MenuMapper.MapToOnlyName(m));
            
            return await resQuery.ToListAsync();
        }

        public async Task<List<BLLAppDTOMenu.MenuOnlyName>?> GetAllUnpickedCompanyMenusBLL(int companyId, int restaurantId, bool noTracking = true)
        {
            var dalMenus = await GetAllUnPickedCompanyMenus(companyId, restaurantId, noTracking);

            var bllMenus = dalMenus?.Select(BLL.App.DTO.Mappers.MenuMapper.Map).ToList();
            return bllMenus;
        }

        public async Task<List<DALAppDTOMenu.MenuOnlyName>?> GetAllOtherCompanyMenus(int companyId, int menuId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(m => !m.Id.Equals(menuId) && m.CompanyId.Equals(companyId))
                .Select(m => MenuMapper.MapToOnlyName(m))
                .ToListAsync();

            return res;
        }

        public async Task<List<BLLAppDTOMenu.MenuOnlyName>?> GetAllOtherCompanyMenusBLL(int companyId, int menuId, bool noTracking = true)
        {
            var dalMenus = await GetAllOtherCompanyMenus(companyId, menuId, noTracking);

            var bllMenus = dalMenus?.Select(BLL.App.DTO.Mappers.MenuMapper.Map).ToList();
            return bllMenus;
        }


        public Task<int> GetMenuCompanyId(int menuId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var companyId = query
                .Where(m => m.Id.Equals(menuId))
                .Select(m=> m.CompanyId)
                .FirstOrDefaultAsync();

            return companyId;
        }

        public async Task<int> CopyMenuItemsToOtherMenu(int currentMenuId, int otherMenuId)
        {
            var query = CreateQuery();

            var otherMenu = await query
                .Where(m => m.Id.Equals(otherMenuId))
                .Include(m => m.MenuCategories)
                .ThenInclude(c => c.Products)
                .ThenInclude(p => p.ProductsSubCategories)
                .ThenInclude(sc => sc.SubCategoryProducts)
                .FirstOrDefaultAsync();
            
            var currentMenu = await query
                .Where(m => m.Id.Equals(currentMenuId))
                .Include(m => m.MenuCategories)
                .FirstOrDefaultAsync();

     
            var counter = 0;
            foreach (var menuCategory in otherMenu.MenuCategories ?? new List<MenuCategory>())
            {
                var newCategory = new MenuCategory
                {
                    MenuId = currentMenu.Id,
                    Name = menuCategory.Name,
                    Position = currentMenu.MenuCategories?.Count + counter ?? counter
                };
                counter++;
                
                RepoDbContext.MenuCategories.Add(newCategory);
                await RepoDbContext.SaveChangesAsync();
                foreach (var product in menuCategory.Products ?? new List<Product>())
                {
                    var newProduct = new Product
                    {
                        Name = product.Name,
                        Price = product.Price,
                        Picture = product.Picture,
                        MenuCategoryId = newCategory.Id,
                        Description = product.Description,
                        IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                        Position = product.Position
                    };
                    RepoDbContext.Products.Add(newProduct);
                    await RepoDbContext.SaveChangesAsync();
                    
                    foreach (var sellingTime in product.SellingTimes ?? new List<Time>())
                    {
                        var newTime = new Time
                        {
                            ProductId = newProduct.Id,
                            DayOfWeek = sellingTime.DayOfWeek,
                            EndingTime = sellingTime.EndingTime,
                            StartingTime = sellingTime.StartingTime,
                        };
                        RepoDbContext.Times.Add(newTime);
                    }

                    foreach (var subCategory in product.ProductsSubCategories ?? new List<ProductsSubCategory>())
                    {
                        var newSubCategory = new ProductsSubCategory
                        {
                            ProductId = newProduct.Id,
                            Position = subCategory.Position,
                            Name = subCategory.Name,
                            MandatoryItemCount = subCategory.MandatoryItemCount,
                            MaxItemCount = subCategory.MaxItemCount
                        };
                        RepoDbContext.ProductsSubCategories.Add(newSubCategory);
                        await RepoDbContext.SaveChangesAsync();
                        
                        foreach (var subProduct in subCategory.SubCategoryProducts ?? new List<ProductsSubCategoryProduct>())
                        {
                            var newSubProduct = new ProductsSubCategoryProduct
                            {
                                ProductsSubCategoryId = newSubCategory.Id,
                                IsSelectedOnDefault = subProduct.IsSelectedOnDefault,
                                Description = subProduct.Description,
                                Price = subProduct.Price,
                                Position = subProduct.Position,
                                Name = subProduct.Name
                            };
                            RepoDbContext.ProductsSubCategoryProducts.Add(newSubProduct);
                        }
                    }
                }
            }

            await RepoDbContext.SaveChangesAsync();

            return 0;
        }
        
    }
}