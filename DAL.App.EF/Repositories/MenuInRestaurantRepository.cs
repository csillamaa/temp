﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Mappers;
using DAL.App.DTO.Menu;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class MenuInRestaurantRepository: BaseRepository<DAL.App.DTO.Menu.MenuInRestaurant, Domain.App.MenuInRestaurant, AppDbContext>, IMenuInRestaurantRepository
    {
        public MenuInRestaurantRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new MenuInRestaurantMapper(mapper))
        {
        }

        public async Task<List<MenuInRestaurant>> RemoveMenuFromAllRestaurants(int menuId)
        {
            var query = CreateQuery();

            var resQuery = query.Where(m => m.MenuId.Equals(menuId));

            var menusInRestaurants = await resQuery.ToListAsync();

            foreach (var menuInRestaurant in menusInRestaurants)
            {
                RepoDbSet.Remove(menuInRestaurant);
            }

            await RepoDbContext.SaveChangesAsync();

            return menusInRestaurants.Select(Mapper.Map).ToList()!;
        }

        public async Task<MenuInRestaurant?> RemoveMenuFromRestaurant(MenuInRestaurant menuInRestaurant, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(m => m.MenuId.Equals(menuInRestaurant.MenuId) 
                            && m.RestaurantId.Equals(menuInRestaurant.RestaurantId))
                .FirstOrDefaultAsync();

            if (res == null)
            {
                return null;
            }
            
            RepoDbSet.Remove(res);

            return Mapper.Map(res)!;
        }

        public async Task<bool> IsMenuAddableToRestaurant(MenuInRestaurant menuInRestaurant, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var resQuery = await query
                .Where(m => m.MenuId.Equals(menuInRestaurant.MenuId)
                            && m.RestaurantId.Equals(menuInRestaurant.RestaurantId))
                .FirstOrDefaultAsync();

            if (resQuery != null)
            {
                return false;
            }
            
            return true;
        }
        
        public async Task<int> RemoveAllMenusFromRestaurant(int id, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(m => m.RestaurantId.Equals(id))
                .ToListAsync();
            
            var counter = 0;

            foreach (var menuInRestaurant in res)
            {
                RepoDbContext.MenusInRestaurant.Remove(menuInRestaurant);
                counter++;
            }

            await RepoDbContext.SaveChangesAsync();

            return counter;
        }
    }
}