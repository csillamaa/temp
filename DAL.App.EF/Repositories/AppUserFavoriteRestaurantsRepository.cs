﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.DTO.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class AppUserFavoriteRestaurantsRepository: BaseRepository<AppUserFavoriteRestaurant, 
        Domain.App.AppUserFavoriteRestaurant, AppDbContext>, IAppUserFavoriteRestaurantsRepository
    {
        public AppUserFavoriteRestaurantsRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new AppUserFavoriteRestaurantMapper(mapper))
        {
        }

        public async Task<int> RemoveAllUserFavoriteRestaurants(int userId, bool noTracking = true)
        {
            var query = CreateQuery(userId, noTracking);

            var res = await query.ToListAsync();

            var counter = 0;

            foreach (var favRestaurants in res)
            {
                counter++;
                RepoDbSet.Remove(favRestaurants);
            }

            await RepoDbContext.SaveChangesAsync();
            return counter;
        }

        public async Task<int[]?> GetAllAppUserFavoriteRestaurants(int? userId, bool noTracking = true)
        {
            if (userId == null)
            {
                return null;
            }
            var query = CreateQuery(userId.Value, noTracking);

            var res = await query
                .Select(r => r.RestaurantId)
                .ToArrayAsync();
            
            return res;
        }
    }
}