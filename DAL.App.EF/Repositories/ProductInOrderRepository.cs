﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Mappers;
using DAL.Base.EF.Repositories;
using Domain.App;
using Microsoft.EntityFrameworkCore;
using DALAppDTOOrder = DAL.App.DTO.Order;
using BLLAppDTOOrder = BLL.App.DTO.Order;

namespace DAL.App.EF.Repositories
{
    public class ProductInOrderRepository : BaseRepository<DALAppDTOOrder.ProductInOrder, Domain.App.ProductInOrder, AppDbContext>, IProductInOrderRepository
    {
        public ProductInOrderRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new ProductInOrderMapper(mapper))
        {
        }


        public async Task<bool> ChangeProductInOrderAmount(int orderId, int productInOrderId, int newAmount, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(p => p.Id.Equals(productInOrderId) && p.OrderId.Equals(orderId))
                .FirstOrDefaultAsync();

            if (res == null)
            {
                return false;
            }
            
            res.Amount = newAmount;
            RepoDbSet.Update(res);
            await RepoDbContext.SaveChangesAsync();

            return true;
        }

        public async Task<int> GetOrderId(int id, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(p => p.Id.Equals(id))
                .Select(p => p.OrderId)
                .FirstOrDefaultAsync();
            
            return res;
        }
        
        public async Task<bool> AddAmountToProductIfSameOneAlreadyInOrder(int userBasketId, int productId, int amount, int[] allSubProductIds, bool noTracking = true)
        {
            if (userBasketId == default)
            {
                return false;
            }
            
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(p => p.OrderId.Equals(userBasketId)
                            && p.ProductId.Equals(productId))
                .Include(p => p.SubProducts)
                // .Select(p => new ProductInOrder
                // {
                //     Id = p.Id,
                //     Amount = p.Amount,
                //     SubProducts = p.SubProducts,
                //     OrderId = p.OrderId
                // })
                .ToListAsync();

            if (res == null)
            {
                return false;
            }

            foreach (var product in res)
            {
                if (product.SubProducts == null && allSubProductIds.Length == 0
                    || (product.SubProducts != null && AreSubProductsTheSame(allSubProductIds, 
                    product.SubProducts.Select(sp => 
                        sp.ProductsSubCategoryProductId!.Value).ToArray())))
                {
                    product.Amount += amount;
                    RepoDbSet.Update(product);
                    await RepoDbContext.SaveChangesAsync();

                    return true;
                }
               
            }
            return false;
        }
        
        public bool AreSubProductsTheSame(int[] newSubProductIds, int[] orderProductSubProductIds)
        {
            foreach (var newSubProductId in newSubProductIds)
            {
                var foundMatch = false;
                foreach (var productSubProductId in orderProductSubProductIds)
                {
                    if (productSubProductId.Equals(newSubProductId))
                    {
                        foundMatch = true;
                        break;
                    }
                }

                if (!foundMatch)
                {
                    return false;
                }
            }
            return true;
        }
    }
}