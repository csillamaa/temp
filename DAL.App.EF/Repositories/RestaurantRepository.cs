﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.DTO.Mappers;
using DAL.App.DTO.Menu;
using DAL.App.DTO.Restaurant;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class RestaurantRepository : BaseRepository<Restaurant, Domain.App.Restaurant, AppDbContext>, IRestaurantRepository
    {
        public RestaurantRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new RestaurantMapper(mapper))
        {
        }

        public async Task<IEnumerable<RestaurantIndex>> GetAllIndexAsync(bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = query
                .Include(r => r.Company)
                .Include(r => r.Workers)
                .Select(r => DAL.App.DTO.Mappers.RestaurantMapper.MapToDalIndex(r));
            
            return await res.ToListAsync();
        }

        public async Task<IEnumerable<BLL.App.DTO.Restaurant.RestaurantIndex>> GetAllIndexAsyncBLL(
            bool noTracking = true)
        {
            var dalRestaurants = await GetAllIndexAsync();
            var bllRestaurants = dalRestaurants
                .Select(BLL.App.DTO.Mappers.RestaurantMapper.MapToDal);

            return bllRestaurants;
        }


        public async Task<RestaurantClient?> FirstOrDefaultClientAsync(int id, int? currentCategoryPosition = 0, bool noTracking = true)
        {
            if (currentCategoryPosition == null)
            {
                currentCategoryPosition = 0;
            }
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Include(r => r.OpeningTimes)
                .Include(r => r.Menus)
                    .ThenInclude(m => m.Menu)
                        .ThenInclude(m => m.MenuCategories)
                            .ThenInclude(mc => mc.Products)
                                .ThenInclude(p => p.ProductsSubCategories)
                                    .ThenInclude(psc => psc.SubCategoryProducts)
                .FirstOrDefaultAsync(r => r.Id.Equals(id));
                // .Include(r => r.Menus)
                //     .ThenInclude(m => m.Menu)
                //         .ThenInclude(m => m.MenuCategories)
                //             .ThenInclude(mc => mc.Products)
                //                 .ThenInclude(p => p.SellingTimes);
                var restaurant =  RestaurantMapper.MapToDalClient(res);
                if (restaurant == null)
                {
                    return null;
                }

                restaurant.Menus = res.Menus?
                    .Select(m => new Menu
                    {
                        CompanyId = m.Menu.CompanyId,
                        MenuCategories = m.Menu.MenuCategories?
                            .Select(MenuCategoryMapper.MapToOnlyName)
                            .ToList(),
                        CurrentCategory = m.Menu.MenuCategories?
                            .Where(c => c.Position.Equals(currentCategoryPosition))
                            .Select(MenuCategoryMapper.Map)
                            .FirstOrDefault()
                    }).ToList();
                        
            return restaurant;
        }

        public async Task<BLL.App.DTO.Restaurant.RestaurantClient?> FirstOrDefaultClientAsyncBLL(int id, int? currentCategoryPosition = 0, bool noTracking = true)
        {
            var dalRestaurant = await FirstOrDefaultClientAsync(id, currentCategoryPosition, noTracking);

            var bllRestaurant = BLL.App.DTO.Mappers.RestaurantMapper.MapToBLLClient(dalRestaurant);

            return bllRestaurant;
        }

        public async Task<string?> GetRestaurantName(int id, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(r => r.Id.Equals(id))
                .Select(r => r.Name)
                .FirstOrDefaultAsync();

            return res;
        }

        public async Task<bool> IsRestaurantOpenById(int restaurantId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var currentTime = DateTime.Now;
            
            var restaurantOpenTime = await query
                .Where(r => r.Id.Equals(restaurantId) && r.IsPublic && r.OpeningTimes != null)
                .Include(r => r.OpeningTimes)
                .Select(r => r.OpeningTimes!.FirstOrDefault(t => t.DayOfWeek.Equals((int)currentTime.DayOfWeek)))
                .FirstOrDefaultAsync();
            
            return restaurantOpenTime != null && TimeSpan.Compare(restaurantOpenTime.StartingTime.TimeOfDay, currentTime.TimeOfDay) == -1 && 
                   TimeSpan.Compare(restaurantOpenTime.EndingTime.TimeOfDay, currentTime.TimeOfDay) == 1;
        }

        public async Task<bool> IsProductInRestaurantMenu(int productId, int restaurantId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(r => r.Id.Equals(restaurantId) && r.Menus!
                    .Any(m =>  m.Menu.MenuCategories!
                        .Any(c => c.Products!.Any(p => p.Id.Equals(productId)))))
                .Select(r => r.Name)
                .FirstOrDefaultAsync();

            return res != null;
        }

        public async Task<bool> IsRestaurantOwnedByCompany(int restaurantId, int companyId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(r => r.Id.Equals(restaurantId))
                .FirstOrDefaultAsync();

            return res.CompanyId.Equals(companyId);
        }

        public async Task<RestaurantEdit?> FirstOrDefaultEditAsync(int id, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            query = query
                .Include(r => r.OpeningTimes)
                .Include(r => r.Menus)
                    .ThenInclude(m => m.Menu)
                .Include(r => r.Workers)
                    .ThenInclude(w => w.AppUser)
                .Include(r => r.Company);

            return DAL.App.DTO.Mappers.RestaurantMapper.MapToDal(await query.FirstOrDefaultAsync(r => r.Id.Equals(id)));
        }
        public async Task<BLL.App.DTO.Restaurant.RestaurantEdit?> FirstOrDefaultEditAsyncBLL(int id, bool noTracking = true)
        {
            var dalRestaurant = await FirstOrDefaultEditAsync(id, noTracking);
            if (dalRestaurant == null)
            {
                return null;
            }

            var bllRestaurant = BLL.App.DTO.Mappers.RestaurantMapper.MapToBLL(dalRestaurant);
            
            return bllRestaurant;
        }

        public async Task<int> GetRestaurantCompanyId(int restaurantId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);
            
            var companyId = await query
                .Where(r => r.Id.Equals(restaurantId))
                .Select(r => r.CompanyId)
                .FirstOrDefaultAsync();
            
            return companyId;

        }

        public bool IsRestaurantOpen(Time? restaurantOpenTime)
        {
            var currentTime = DateTime.Now;
      
            return restaurantOpenTime != null && TimeSpan.Compare(restaurantOpenTime.StartingTime.TimeOfDay, currentTime.TimeOfDay) == -1 && 
                   TimeSpan.Compare(restaurantOpenTime.EndingTime.TimeOfDay, currentTime.TimeOfDay) == 1;
        }

        public async Task<Restaurant?> FirstOrDefaultDeleteAsync(int id, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(r => r.Id.Equals(id))
                .FirstOrDefaultAsync();

            return Mapper.Map(res);
        }
        
        public async Task<IList<RestaurantOnlyName>?> GetAllRestaurantsAppUserWorksInAsync(int userId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = query
                .Where(r => r.Workers != null &&
                            r.Workers.Any(w => w.AppUserId.Equals(userId)))
                .Include(r => r.Workers)
                .Select(r => RestaurantMapper.MapToDalJustName(r));

            return await res.ToListAsync();
        }
        
        public async Task<IList<BLL.App.DTO.Restaurant.RestaurantOnlyName>?> GetAllRestaurantsAppUserWorksInAsyncBLL
            (int userId, bool noTracking = true)
        {
            var dalRestaurants = await GetAllRestaurantsAppUserWorksInAsync(userId, noTracking);
            var bllRestaurants = dalRestaurants?
                .Select(BLL.App.DTO.Mappers.RestaurantMapper.MapToBLL);
           
            return bllRestaurants?.ToList();
        }

        public async Task<ICollection<DAL.App.DTO.Restaurant.RestaurantOnlyName>?> GetAllRestaurantsAddableToUser(int companyId, int userId,bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);
            var res = query
                .Where(r => r.CompanyId.Equals(companyId))
                .Where(r => r.Workers == null ||
                            !r.Workers.Any(rw => rw.AppUserId.Equals(userId)))
                .Select(r => RestaurantMapper.MapToDalJustName(r));
          
            return await res.ToListAsync();
        }

        public async Task<ICollection<BLL.App.DTO.Restaurant.RestaurantOnlyName>?> GetAllRestaurantsAddableToUserBLL(int companyId, int userId, bool noTracking = true)
        {
            var dalRestaurants = await GetAllRestaurantsAddableToUser(companyId, userId, noTracking);

            var bllRestaurants = dalRestaurants?
                .Select(BLL.App.DTO.Mappers.RestaurantMapper.MapToBLL);
            
            return bllRestaurants?.ToList();
        }

        public async Task<IEnumerable<RestaurantIndex>?> GetAllCompanyRestaurants(int companyId, bool noTracking)
        {
            var query = CreateQuery(default, noTracking);

            var res = query
                .Where(r => r.CompanyId.Equals(companyId))
                .Include(r => r.Company)
                .Include(r => r.Workers)
                .Select(r => DAL.App.DTO.Mappers.RestaurantMapper.MapToDalIndex(r));
            
            return await res.ToListAsync();
        }
        
        public async Task<IEnumerable<BLL.App.DTO.Restaurant.RestaurantIndex>?> GetAllCompanyRestaurantsBLL(int companyId, bool noTracking = true)
        {
            var dalRestaurants = await GetAllCompanyRestaurants(companyId, noTracking);
            var bllRestaurants = dalRestaurants?
                .Select(BLL.App.DTO.Mappers.RestaurantMapper.MapToDal);

            return bllRestaurants;
        }
        
        public async Task<List<RestaurantClientAll>> GetAllClientAsync(bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(r => r.IsPublic)
                .Include(r => r.OpeningTimes)
                .Select(r => RestaurantMapper.MapToDalClientAll(r))
                .ToListAsync();
            
            // TODO: Make it inside query

            var restaurants = new List<RestaurantClientAll>();
            foreach (var restaurant in res)
            {
                if (IsRestaurantOpen(restaurant.OpeningTimes))
                {
                    restaurants.Add(restaurant);
                }
            }

            return restaurants;
        }

        public async Task<List<BLL.App.DTO.Restaurant.RestaurantClientAll>> GetAllClientAsyncBLL(bool noTracking = true)
        {
            var dalRestaurants = await GetAllClientAsync(noTracking);

            var bllRestaurants = dalRestaurants
                .Select(BLL.App.DTO.Mappers.RestaurantMapper.MapToBLLClientAll)
                .ToList();

            return bllRestaurants;
        }

        public async Task<List<RestaurantClientAll>?> GetAllAppUserFavouriteRestaurants(int[]? userFavoriteRestaurantIds, bool noTracking = true)
        {
            if (userFavoriteRestaurantIds == null)
            {
                return null;
            }
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(r => r.IsPublic && userFavoriteRestaurantIds.Contains(r.Id))
                .Include(r => r.OpeningTimes)
                .Select(r => RestaurantMapper.MapToDalClientAll(r))
                .ToListAsync();

            return res;
        }

        public async Task<List<BLL.App.DTO.Restaurant.RestaurantClientAll>?> GetAllAppUserFavouriteRestaurantsBLL(int[]? userFavoriteRestaurantIds, bool noTracking = true)
        {
            var dalRestaurants = await GetAllAppUserFavouriteRestaurants(userFavoriteRestaurantIds, noTracking);

            var bllRestaurants = dalRestaurants?
                .Select(BLL.App.DTO.Mappers.RestaurantMapper.MapToBLLClientAll)
                .ToList();

            return bllRestaurants;
        }
    }
}