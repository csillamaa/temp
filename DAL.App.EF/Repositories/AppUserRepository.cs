﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Mappers;
using DAL.Base.EF.Repositories;
using Domain.App;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using AppRole = Domain.App.Identity.AppRole;
using AppUser = Domain.App.Identity.AppUser;
using AppUserEdit = DAL.App.DTO.Identity.AppUserEdit;
using AppUserOnlyName = DAL.App.DTO.Identity.AppUserOnlyName;

namespace DAL.App.EF.Repositories
{
    public class AppUserRepository:  BaseRepository<DAL.App.DTO.Identity.AppUser,
        Domain.App.Identity.AppUser, AppDbContext>, IAppUserRepository
    {
        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;

        public AppUserRepository(UserManager<AppUser> userManager, AppDbContext dbContext,
            IMapper mapper) : base(dbContext, new AppUserMapper(mapper))
        {
            _userManager = userManager;
        }
        
        public async Task<IEnumerable<string>> GetUserRoles(int userId)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
            var userRoles = await _userManager.GetRolesAsync(user);
            return userRoles;
        }

        public async Task<IList<AppUserOnlyName>> GetAllUsers()
        {
            var users = await _userManager.Users
                .Select(u => AppUserMapper.MapToDalOnlyName(u))
                .ToListAsync();

            return users;
        }

        public async Task<IEnumerable<BLL.App.DTO.Identity.AppUserOnlyName>> GetAllUsersBLL()
        {
            var users = (await GetAllUsers())
                .Select(BLL.App.DTO.Mappers.AppUserMapper.MapToBLL);

            return users;
        }

        public async Task<bool> IsUserCompanyAdmin(int userId)
        { 
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Id.Equals(userId));
            return await _userManager.IsInRoleAsync(user!, "Company Admin");
        }

        public async Task<AppUserEdit?> FirstOrDefaultEditAsync(int userId, bool noTracking = true)
        {
            var user = await _userManager.Users
                .Include(u => u.Company)
                .FirstOrDefaultAsync(u => u.Id.Equals(userId));
            if (user == null)
            {
                return null;
            }

            var editUser = AppUserMapper.MapToDalEdit(user);
            

            return editUser;
        }

        public async Task<BLL.App.DTO.Identity.AppUserEdit?> FirstOrDefaultEditAsyncBLL(int userId, bool noTracking = true)
        {
            var dalUser = await FirstOrDefaultEditAsync(userId, noTracking);
            if (dalUser == null)
            {
                return null;
            }

            var bllUser = BLL.App.DTO.Mappers.AppUserMapper.MapToBLL(dalUser);

            return bllUser;
        }

        public async Task<int?> AddUser(DAL.App.DTO.Identity.AppUserData appUser)
        {
            var domainUser = AppUserMapper.MapToDomain(appUser);

            var result = await _userManager.CreateAsync(domainUser, "Kool1234!");

            if (result.Succeeded)
            {
                return domainUser.Id;
            }

            return null;
        }

        public async Task<int?> GetUserCompanyId(int userId)
        {
            var query = CreateQuery(userId);

            var user = await query.FirstOrDefaultAsync(u => u.Id.Equals(userId));

            return user?.CompanyId;
        }

        public async Task<int?> AddUserBLL(BLL.App.DTO.Identity.AppUserData appUser)
        {
            
            var dalAppuser = BLL.App.DTO.Mappers.AppUserMapper.MapToDal(appUser);

            return await AddUser(dalAppuser);
        }

        public async Task<List<DAL.App.DTO.Identity.AppUserIndex>> GetAllIndexAsync(int userId, bool noTracking = true)
        {
            var userCompanyId = await GetUserCompanyId(userId);
            
            var query = CreateQuery(default, noTracking);
            
            if (userCompanyId != null)
            {
                query = query.Where(u => u.CompanyId.Equals(userCompanyId));
            }
                
            var users = query
                .Include(u => u.Company)
                .Include(u => u.WorkRestaurants)
                .ThenInclude(r => r.Restaurant)
                .Select(AppUserMapper.MapToDalIndex);

            return users.ToList();
        }
        
        public async Task<ICollection<BLL.App.DTO.Identity.AppUserIndex>> GetAllIndexAsyncBLL(int userId, bool noTracking = true)
        {
            var dalUsers = await GetAllIndexAsync(userId, noTracking);
            var bllUsers = dalUsers.Select(BLL.App.DTO.Mappers.AppUserMapper.MapToBLL).ToList();
           
            return bllUsers;
        }

        public async Task<bool> RemoveAllUserOrders(int id, bool noTracking = true)
        {
            var user = await _userManager

                .Users
                .Include(u => u.Orders)
                .ThenInclude(o => o.Order)
                .Include(u => u.PastOrders)
                .ThenInclude(o => o.ArchivedOrder)
                .FirstOrDefaultAsync(u => u.Id.Equals(id));

            if (user.Orders != null)
            {
                foreach (var order in user.Orders)
                {
                    RepoDbContext.Orders.Remove(order.Order);
                }
            }
            if (user.PastOrders != null)
            {
                foreach (var order in user.PastOrders)
                {
                    RepoDbContext.ArchivedOrders.Remove(order.ArchivedOrder);
                }
            }

            return true;
        }

        // public async Task<bool> RemoveAllCompanyUserRoles(int companyId)
        // {
        //     var res = await _userManager.Users
        //         .Where(u => u.CompanyId.Equals(companyId))
        //         .ToListAsync();
        //
        //     foreach (var user in res)
        //     {
        //         var userRoles = await _userManager.GetRolesAsync(user);
        //         await _userManager.RemoveFromRolesAsync(user, userRoles);
        //     }
        //
        //     return true;
        // }

        public async Task<int[]> GetAllCompanyUserIds(int companyId, bool noTracking = true)
        {
            var res = await _userManager.Users
                .Where(u => u.CompanyId.Equals(companyId))
                .ToListAsync();

            var userIds = new int[res.Count];
            for (int i = 0; i < res.Count; i++)
            {
                var user = res[i];
                user.CompanyId = null;
                userIds[i] = user.Id;
            }

            return userIds;
        }
        public async Task<int> RemoveAllCompanyUsers(int[] userIds, bool noTracking = true)
        {
            var res = await _userManager.Users
                .Where(u => userIds.Contains(u.Id))
                .ToListAsync();

            var counter = 0;
            foreach (var user in res)
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                await _userManager.RemoveFromRolesAsync(user, userRoles);
                await _userManager.DeleteAsync(user);
                
                counter++;
            }

            return counter;
        }

        public async Task<bool> AddUserFavoriteRestaurant(int restaurantId, int userId)
        {
            var userFavoriteRestaurant = new AppUserFavoriteRestaurant
            {
                RestaurantId = restaurantId,
                AppUserId = userId
            };

            var res = await _userManager.Users
                .Where(u => u.Id.Equals(userId))
                .Where(u => u.FavoriteRestaurants!.Any(r => r.RestaurantId.Equals(restaurantId)))
                .Select(u => u.FirstName)
                .FirstOrDefaultAsync();
            
            if (res != null)
            {
                return false;
            }
            
            RepoDbContext.AppUserFavoriteRestaurants.Add(userFavoriteRestaurant);
            await RepoDbContext.SaveChangesAsync();
            
            return true;
        }
        
        public async Task<bool> RemoveUserFavoriteRestaurant(int restaurantId, int userId)
        {
            var userFavoriteRestaurant = await _userManager.Users
                .Where(u => u.Id.Equals(userId) && u.FavoriteRestaurants != null)
                .Select(u => u.FavoriteRestaurants!
                    .FirstOrDefault(r => r.RestaurantId.Equals(restaurantId)))
                .FirstOrDefaultAsync();

            if (userFavoriteRestaurant == null)
            {
                return false;
            }
            
            RepoDbContext.AppUserFavoriteRestaurants.Remove(userFavoriteRestaurant);
            await RepoDbContext.SaveChangesAsync();
            
            return true;
        }
    }
}