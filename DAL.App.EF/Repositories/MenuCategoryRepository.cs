﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Mappers;
using DAL.App.DTO.Menu;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class MenuCategoryRepository: BaseRepository<DAL.App.DTO.Menu.MenuCategory, Domain.App.MenuCategory, AppDbContext>, IMenuCategoryRepository
    {
        public MenuCategoryRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new MenuCategoryMapper(mapper))
        {
        }

       

        // public MenuCategory Add(MenuCategoryData category)
        // {
        //     var query = CreateQuery();
        //
        //     
        //     var categoryCount = query
        //         .Count(c => c.MenuId.Equals(category.MenuId));
        //
        //     var categoryDal = MenuCategoryMapper.MapToRegDal(category);
        //     categoryDal.Position = categoryCount;
        //    
        //     
        //     var created = base.Add(categoryDal);
        //     return categoryDal;
        // }
        //
        // public BLL.App.DTO.Menu.MenuCategory Add(BLL.App.DTO.Menu.MenuCategoryData category)
        // {
        //     return BLL.App.DTO.Mappers.MenuCategoryMapper.Map(
        //         Add(BLL.App.DTO.Mappers.MenuCategoryMapper.Map(category)))!;
        // }

        public int GetCategoryCountInMenu(int menuId)
        {
            var query = CreateQuery();
            
            var categoryCount = query
            .Count(c => c.MenuId.Equals(menuId));

            return categoryCount;
        }

        public async Task<bool> ChangeCategoryPosition(int id, bool moveUp)
        {
            var query = CreateQuery();
            
            var category1 = await query.Where(c => c.Id.Equals(id)).FirstOrDefaultAsync();
            var category2 = await query
                .Where(c => c.MenuId.Equals(category1.MenuId) && 
                            c.Position.Equals(moveUp ? category1.Position - 1 : category1.Position + 1))
                .FirstOrDefaultAsync();
            if (category2 == null)
            {
                return false;
            }

            category1.Position = moveUp ? category1.Position - 1 : category1.Position + 1;
            category2.Position = moveUp ? category1.Position + 1 : category1.Position - 1;

            RepoDbContext.Update(category1);
            RepoDbContext.Update(category2);
            await RepoDbContext.SaveChangesAsync();
            return true;
        }

        public async Task<int> GetCategoryCompanyId(int id)
        {
            var query = CreateQuery();
            var res = await query.Where(c => c.Id.Equals(id))
                .Include(c => c.Menu)
                .Select(c => c.Menu.CompanyId)
                .FirstOrDefaultAsync();
            
            return res;
        }
        
        public async Task<int> GetCategoryMenuId(int id)
        {
            var query = CreateQuery();
            var res = await query
                .Where(c => c.Id.Equals(id))
                .Select(c => c.MenuId)
                .FirstOrDefaultAsync();

            return res;
        }

        public async Task<int> MoveCategoriesUpWhenDelete(int menuCategoryId)
        {
            var query = CreateQuery();

            var menuCategory = await query
                .Where(c => c.Id.Equals(menuCategoryId))
                .FirstOrDefaultAsync();

            var categories = await query
                .Where(c => c.MenuId.Equals(menuCategory.MenuId) && c.Position > menuCategory.Position)
                .ToListAsync();

            var counter = 0;
            foreach (var category in categories)
            {
                counter ++;
                category.Position -= 1;
                RepoDbSet.Update(category);
            }

            // await RepoDbContext.SaveChangesAsync();
            
            return counter;
        }

        public async Task<List<MenuCategoryOnlyName>> GetAllMenuCategoriesOnlyName(int menuId, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(c => c.MenuId.Equals(menuId))
                .Select(c => MenuCategoryMapper.MapToOnlyName(c))
                .ToListAsync();

            return res.OrderBy(c => c.Position).ToList();
        }

        public async Task<List<BLL.App.DTO.Menu.MenuCategoryOnlyName>> GetAllMenuCategoriesOnlyNameBLL(int menuId, bool noTracking = true)
        {
            var dalCategories = await GetAllMenuCategoriesOnlyName(menuId, noTracking);

            var bllCategories = dalCategories
                .Select(BLL.App.DTO.Mappers.MenuCategoryMapper.MapToOnlyName)
                .ToList();
            
            return bllCategories;
        }

        public override async Task<MenuCategory?> FirstOrDefaultAsync(int id, int userId = default, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);
            
            var resQuery = query
                .Where(c => c.Id.Equals(id))
                .Include(c => c.Products)
                    .ThenInclude(p => p.ProductsSubCategories)
                        .ThenInclude(sc => sc.SubCategoryProducts)
                .Select(m => MenuCategoryMapper.Map(m));
            
            return await resQuery.FirstOrDefaultAsync();
        }

        public override async Task<MenuCategory> RemoveAsync(int id, int userId)
        {
            var query = CreateQuery();
        
            var menuCategory = await query
                .Where(c => c.Id.Equals(id))
                .Include(c => c.Products)
                    .ThenInclude(p => p.SellingTimes)
                .Include(c => c.Products)
                    .ThenInclude(p => p.ProductsSubCategories)
                        .ThenInclude(sc => sc.SubCategoryProducts)
                .FirstOrDefaultAsync();
            
            if (menuCategory.Products != null)
            {
                foreach (var product in menuCategory.Products)
                {
                    if (product.ProductsSubCategories != null)
                    {
                        foreach (var productsSubCategory in product.ProductsSubCategories)
                        {
                            if (productsSubCategory.SubCategoryProducts != null)
                            {
                                foreach (var subCategoryProduct in productsSubCategory.SubCategoryProducts)
                                {
                                    RepoDbContext.ProductsSubCategoryProducts.Remove(subCategoryProduct);
                                }
                            }
                            RepoDbContext.ProductsSubCategories.Remove(productsSubCategory);
                        }
                    }

                    if (product.SellingTimes != null)
                    {
                        foreach (var sellingTime in product.SellingTimes)
                        {
                            RepoDbContext.Times.Remove(sellingTime);
                        }
                    }
                    RepoDbContext.Products.Remove(product);
                }
            }
        
            RepoDbSet.Remove(menuCategory);
            await RepoDbContext.SaveChangesAsync();
            return Mapper.Map(menuCategory)!;
        }

        public async Task<List<MenuCategory>?> RemoveAllMenuCategories(int menuId)
        {
            var query = CreateQuery();

            var resQuery = query.Where(p => p.MenuId.Equals(menuId));

            var categories = await resQuery.ToListAsync();
            if (categories == null)
            {
                return null;
            }

            foreach (var menuCategory in categories)
            {
                await RemoveAsync(menuCategory.Id, 0);
            }

            return categories.Select(Mapper.Map).ToList()!;
        }

        public async Task<MenuCategoryData?> FirstOrDefaultEditAsync(int id)
        {
            var query = CreateQuery();

            var resQuery = query.Where(c => c.Id.Equals(id));

            return MenuCategoryMapper.MapToDalData(await resQuery.FirstOrDefaultAsync());
        }
        
        public async Task<BLL.App.DTO.Menu.MenuCategoryData?> FirstOrDefaultEditAsyncBLL(int id)
        {
            var dalCategory = await FirstOrDefaultEditAsync(id);

            return BLL.App.DTO.Mappers.MenuCategoryMapper.Map(dalCategory);
        }
    }
}