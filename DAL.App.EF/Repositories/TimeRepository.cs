﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.DTO.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class TimeRepository: BaseRepository<Time, Domain.App.Time, AppDbContext>, ITimeRepository
    {
        public TimeRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new TimeMapper(mapper))
        {
        }

        public async Task<List<Time>?> GetAllRestaurantOpenTimes(int restaurantId, bool noTracking = true)
        {
            var query = CreateQuery();

            var resQuery = await query
                .Where(t => t.RestaurantId.Equals(restaurantId))
                .ToListAsync();
                
            return resQuery.Select(t => Mapper.Map(t)).ToList()!;
        }

        public async Task<bool> SaveNewTime(int restaurantId, DAL.App.DTO.Time time)
        {
            var query = CreateQuery();
            var dbTime = await query
                .Where(t => t.RestaurantId.Equals(restaurantId) && t.DayOfWeek.Equals(time.DayOfWeek))
                .FirstOrDefaultAsync();
            if (dbTime != null)
            {
                dbTime.StartingTime = time.StartingTime;
                dbTime.EndingTime = time.EndingTime;
                RepoDbSet.Update(dbTime);
                await RepoDbContext.SaveChangesAsync();
                return false;
            }

            time.RestaurantId = restaurantId;
            RepoDbSet.Add(Mapper.Map(time)!);
            await RepoDbContext.SaveChangesAsync();
            return true;
        }
        
        public async Task<bool> IsRestaurantOpen(int restaurantId)
        {
            var query = CreateQuery();

            var datetimeNow = DateTime.Now;
            var resQuery = await query
                .Where(t => t.RestaurantId.Equals(restaurantId) && t.DayOfWeek.Equals((int)datetimeNow.DayOfWeek))
                .FirstOrDefaultAsync();
            
            return resQuery != null && TimeSpan.Compare(resQuery.StartingTime.TimeOfDay, datetimeNow.TimeOfDay) == -1 && 
                   TimeSpan.Compare(resQuery.EndingTime.TimeOfDay, datetimeNow.TimeOfDay) == 1;
        }
    }
}