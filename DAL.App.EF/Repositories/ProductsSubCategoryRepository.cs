﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;
using DALAppDTOProduct = DAL.App.DTO.Product;
using BLLAppDTOProduct = BLL.App.DTO.Product;
namespace DAL.App.EF.Repositories
{
    public class ProductsSubCategoryRepository : BaseRepository<DALAppDTOProduct.ProductsSubCategory,
        Domain.App.ProductsSubCategory, AppDbContext>, IProductsSubCategoryRepository 
    {
        public ProductsSubCategoryRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new ProductsSubCategoryMapper(mapper))
        {
        }

        public async Task<DALAppDTOProduct.ProductsSubCategoryData?> FirstOrDefaultEditAsync(int id, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(c => c.Id.Equals(id))
                .FirstOrDefaultAsync();

            return ProductsSubCategoryMapper.MapToData(res);
        }

        public async Task<BLLAppDTOProduct.ProductsSubCategoryData?> FirstOrDefaultEditAsyncBLL(int id, bool noTracking = true)
        {
            var dalSubCategory = await FirstOrDefaultEditAsync(id, noTracking);

            return BLL.App.DTO.Mappers.ProductsSubCategoryMapper.Map(dalSubCategory);
        }

        public int GetProductsSubCategoryCountInProduct(int productId)
        {
            var query = CreateQuery();

            var res = query.Count(psc => psc.ProductId.Equals(productId));

            return res;
        }
        
        public async Task<int> MoveSubCategoriesUpWhenDelete(int subCategoryId)
        {
            var query = CreateQuery();

            var subCategory = await query
                .Where(p => p.Id.Equals(subCategoryId))
                .FirstOrDefaultAsync();

            var subCategories = await query
                .Where(p => p.ProductId.Equals(subCategory.ProductId) 
                            && p.Position > subCategory.Position)
                .ToListAsync();

            var counter = 0;
            foreach (var item in subCategories)
            {
                counter++;
                item.Position -= 1;
                RepoDbSet.Update(item);
            }

            // await RepoDbContext.SaveChangesAsync();
            return  counter;
        }

        public async Task<int> GetSubCategoryCompanyId(int id)
        {
            var query = CreateQuery();
            var res = await query.Where(p => p.Id.Equals(id))
                .Include(c => c.Product)
                .ThenInclude(p => p.MenuCategory)
                .ThenInclude(c => c.Menu)
                .Select(c => c.Product.MenuCategory.Menu.CompanyId)
                .FirstOrDefaultAsync();
            
            return res;
        }

        public int GetSubCategoryCountInProduct(int productId)
        {
            var query = CreateQuery();

            var res = query
                .Count(p => p.ProductId.Equals(productId));

            return res;
        }

        public async Task<int> GetProductId(int id)
        {
            var query = CreateQuery();
            var res = await query.Where(p => p.Id.Equals(id))
                .Select(p => p.ProductId)
                .FirstOrDefaultAsync();
            
            return res;
        }
        public async Task<bool> ChangeSubCategoryPosition(int id, bool moveUp)
        {
            var query = CreateQuery();
            
            var subCategory1 = await query
                .Where(c => c.Id.Equals(id))
                .FirstOrDefaultAsync();
            
            var subCategory2 = await query
                .Where(c => c.ProductId.Equals(subCategory1.ProductId) && 
                            c.Position.Equals(moveUp ? subCategory1.Position - 1 : subCategory1.Position + 1))
                .FirstOrDefaultAsync();
            if (subCategory2 == null)
            {
                return false;
            }

            subCategory1.Position = moveUp ? subCategory1.Position - 1 : subCategory1.Position + 1;
            subCategory2.Position = moveUp ? subCategory1.Position + 1 : subCategory1.Position - 1;

            RepoDbContext.Update(subCategory1);
            RepoDbContext.Update(subCategory2);
            await RepoDbContext.SaveChangesAsync();
            return true;
        }
    }
}