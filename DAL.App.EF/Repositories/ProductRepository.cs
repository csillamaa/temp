﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Mappers;
using DAL.App.DTO.Product;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;
using BLLAppDTOProduct = BLL.App.DTO.Product;

namespace DAL.App.EF.Repositories
{
    public class ProductRepository: BaseRepository<Product, Domain.App.Product, AppDbContext>, IProductRepository
    {
        public ProductRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new ProductMapper(mapper))
        {
        }
        public async Task<int> GetProductCompanyId(int id)
        {
            var query = CreateQuery();
            var res = await query.Where(p => p.Id.Equals(id))
                .Include(c => c.MenuCategory)
                .ThenInclude(c => c.Menu)
                .Select(p => p.MenuCategory.Menu.CompanyId)
                .FirstOrDefaultAsync();
            
            return res;
        }
        

        public async Task<int> GetProductMenuId(int id)
        {
            var query = CreateQuery();
            var res = await query.Where(p => p.Id.Equals(id))
                .Include(c => c.MenuCategory)
                .Select(p => p.MenuCategory.MenuId)
                .FirstOrDefaultAsync();
            
            return res;
        }

        public int GetProductCountInCategory(int categoryId)
        {
            var query = CreateQuery();

            var res = query
                .Count(p => p.MenuCategoryId.Equals(categoryId));

            return res;
        }

        public async Task<bool> ChangeProductPosition(int id, bool moveUp)
        {
            var query = CreateQuery();
            
            var product1 = await query.Where(c => c.Id.Equals(id)).FirstOrDefaultAsync();
            var product2 = await query
                .Where(c => c.MenuCategoryId.Equals(product1.MenuCategoryId) && 
                            c.Position.Equals(moveUp ? product1.Position - 1 : product1.Position + 1))
                .FirstOrDefaultAsync();
            if (product2 == null)
            {
                return false;
            }

            product1.Position = moveUp ? product1.Position - 1 : product1.Position + 1;
            product2.Position = moveUp ? product1.Position + 1 : product1.Position - 1;

            RepoDbContext.Update(product1);
            RepoDbContext.Update(product2);
            await RepoDbContext.SaveChangesAsync();
            return true;
        }

        public async Task<ProductEdit?> FirstOrDefaultEditAsync(int id, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(p => p.Id.Equals(id))
                .Include(p => p.ProductsSubCategories)
                .ThenInclude(c => c.SubCategoryProducts)
                .FirstOrDefaultAsync();
            
            return ProductMapper.MapToEdit(res);
        }

        public async Task<BLLAppDTOProduct.ProductEdit?> FirstOrDefaultEditAsyncBLL(int id, bool noTracking = true)
        {
            var dalProduct = await FirstOrDefaultEditAsync(id, noTracking);
            
            var bllProduct = BLL.App.DTO.Mappers.ProductMapper.MapToEdit(dalProduct);

            return bllProduct;
        }

        public async Task<ProductClient?> FirstOrDefaultClientAsync(int id, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(p => p.Id.Equals(id))
                .Include(p => p.ProductsSubCategories)
                    .ThenInclude(c => c.SubCategoryProducts)
                .FirstOrDefaultAsync();
            
            return ProductMapper.MapToDalClient(res);
        }

        public async Task<BLLAppDTOProduct.ProductClient?> FirstOrDefaultClientAsyncBLL(int id, bool noTracking = true)
        {
            var dalProduct = await FirstOrDefaultClientAsync(id, noTracking);
            
            var bllProduct = BLL.App.DTO.Mappers.ProductMapper.MapToBLLClient(dalProduct);

            return bllProduct;
        }

        public async Task<int> MoveProductsUpWhenDelete(int productId)
        {
            var query = CreateQuery();

            var product = await query
                .Where(p => p.Id.Equals(productId))
                .FirstOrDefaultAsync();

            var products = await query
                .Where(p => p.MenuCategoryId.Equals(product.MenuCategoryId) && p.Position > product.Position)
                .ToListAsync();

            var counter = 0;
            foreach (var item in products)
            {
                counter++;
                item.Position -= 1;
                RepoDbSet.Update(item);
            }

            // await RepoDbContext.SaveChangesAsync();

            return counter;
        }

        public async Task<ProductOnlyPrice> GetProductOnlyPriceById(int id, bool noTracking = true)
        {
            var query = CreateQuery(default, noTracking);

            var res = await query
                .Where(p => p.Id.Equals(id))
                .Select(p => ProductMapper.MapToDalOnlyPrice(p))
                .FirstOrDefaultAsync();

            return res;
        }
    }
}