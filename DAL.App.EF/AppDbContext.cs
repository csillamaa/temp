﻿using System;
using System.Linq;
using Domain.App;
using Domain.App.Archive;
using Domain.App.Identity;
using Domain.Base;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF
{
    public class AppDbContext: IdentityDbContext<AppUser, AppRole, int>
    {
        // public DbSet<AppUser> AppUsers { get; set; } = default!;

        public DbSet<AppUserOrder> AppUserOrders { get; set; } = default!;
        public DbSet<AppUserFavoriteRestaurant> AppUserFavoriteRestaurants { get; set; } = default!;
        public DbSet<AppUserWorkRestaurant> AppUserWorkRestaurants { get; set; } = default!;
        public DbSet<ArchivedAppUserOrder> ArchivedAppUserOrders { get; set; } = default!;
        public DbSet<ArchivedOrder> ArchivedOrders { get; set; } = default!;
        public DbSet<ArchivedProductInOrder> ArchivedProductsInOrder { get; set; } = default!;
        public DbSet<ArchivedSubProductFromProduct> ArchivedSubProductsFromProduct { get; set; } = default!;

        public DbSet<BankCard> BankCards { get; set; } = default!;
        public DbSet<Company> Companies { get; set; } = default!;
        
        // public DbSet<CompanyAdminAppUser> CompanyAdminAppusers { get; set; } = default!;

        public DbSet<Menu> Menus { get; set; } = default!;
        public DbSet<MenuCategory> MenuCategories { get; set; } = default!;
        public DbSet<MenuInRestaurant> MenusInRestaurant { get; set; } = default!;
        public DbSet<Order> Orders { get; set; } = default!;
        public DbSet<Product> Products { get; set; } = default!;
        public DbSet<ProductInOrder> ProductsInOrder { get; set; } = default!;
        public DbSet<ProductsSubCategory> ProductsSubCategories { get; set; } = default!;
        public DbSet<Restaurant> Restaurants { get; set; } = default!;
        public DbSet<ProductsSubCategoryProduct> ProductsSubCategoryProducts { get; set; } = default!;
        public DbSet<SubProductFromProduct> SubProductsFromProduct { get; set; } = default!;
        public DbSet<Time> Times { get; set; } = default!;
        
        public DbSet<LangString> LangStrings { get; set; } = default!;
        public DbSet<Translation> Translations { get; set; } = default!;

        
        public AppDbContext(DbContextOptions<AppDbContext> options)
            :base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Translation>().HasKey(k => new {k.Culture, k.LangStringId});

            foreach (var relationship in builder.Model
                .GetEntityTypes()
                .SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            // Menu cascade delete
            
            builder.Entity<Menu>()
                .HasMany(c => c.RestaurantsWithTheMenu)
                .WithOne(p => p.Menu)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<Menu>()
                .HasMany(c => c.MenuCategories)
                .WithOne(p => p.Menu)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<MenuCategory>()
                .HasMany(c => c.Products)
                .WithOne(p => p.MenuCategory)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<Product>()
                .HasMany(c => c.ProductsSubCategories)
                .WithOne(p => p.Product)
                .OnDelete(DeleteBehavior.Cascade);
       
            //TODO: notes--
            // builder.Entity<Product>()
            //     .HasMany(c => c.SellingTimes)
            //     .WithOne(p => p.Product!)
            //     .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<ProductsSubCategory>()
                .HasMany(c => c.SubCategoryProducts)
                .WithOne(p => p.ProductsSubCategory)
                .OnDelete(DeleteBehavior.Cascade);

            // On Order delete

       
            builder.Entity<Order>()
                .HasMany(c => c.ProductsInOrder)
                .WithOne(p => p.Order)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<Order>()
                .HasMany(c => c.OrderAppUsers)
                .WithOne(p => p.Order)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<ProductInOrder>()
                .HasMany(c => c.SubProducts)
                .WithOne(p => p.ProductInOrder)
                .OnDelete(DeleteBehavior.Cascade);
            
            // On Archieved Order delete
            
            builder.Entity<ArchivedOrder>()
                .HasMany(a => a.AppUsers)
                .WithOne(o => o.ArchivedOrder)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<ArchivedOrder>()
                .HasMany(c => c.ArchivedProductsInOrder)
                .WithOne(p => p.ArchivedOrder)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<ArchivedProductInOrder>()
                .HasMany(c => c.ArchivedSubProducts)
                .WithOne(p => p.ArchivedProductInOrder)
                .OnDelete(DeleteBehavior.Cascade);
            
            // On User delete
            
            builder.Entity<AppUser>()
                .HasMany(c => c.Cards)
                .WithOne(p => p.AppUser)
                .OnDelete(DeleteBehavior.Cascade);
            
            // On Company delete
            
            builder.Entity<Company>()
                .HasMany(c => c.Restaurants)
                .WithOne(p => p.Company)
                .OnDelete(DeleteBehavior.Cascade);
            
            
            builder.Entity<Company>()
                .HasMany(c => c.Menus)
                .WithOne(p => p.Company)
                .OnDelete(DeleteBehavior.Cascade);
            
            // On Restaurant delete
            
            builder.Entity<Restaurant>()
                .HasMany(c => c.Workers)
                .WithOne(p => p.Restaurant)
                .OnDelete(DeleteBehavior.Cascade);
            //
            // builder.Entity<Restaurant>()
            //     .HasMany(c => c.Menus)
            //     .WithOne(p => p.Restaurant)
            //     .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<Restaurant>()
                .HasMany(c => c.OpeningTimes)
                .WithOne(p => p.Restaurant!)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<Restaurant>()
                .HasMany(c => c.UsersWithFavoriteRestaurant)
                .WithOne(p => p.Restaurant)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<Restaurant>()
                .HasMany(c => c.Orders)
                .WithOne(p => p.Restaurant)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<Restaurant>()
                .HasMany(c => c.CompletedOrders)
                .WithOne(p => p.Restaurant)
                .OnDelete(DeleteBehavior.Cascade);
            
            // No idea why this cascade doesn't work, did it manually
            
            builder.Entity<Company>()
            .HasMany(c => c.CompanyUsers)
            .WithOne(p => p.Company!)
            .OnDelete(DeleteBehavior.NoAction);
        }
    }
}