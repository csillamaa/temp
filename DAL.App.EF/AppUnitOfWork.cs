﻿using AutoMapper;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base.Repositories;
using DAL.App.EF.Repositories;
using DAL.Base.EF;
using DAL.Base.EF.Repositories;
using DAL.App.DTO;
using DAL.App.DTO.Archive;
using DAL.App.DTO.Mappers;
using DAL.App.DTO.Menu;
using DAL.App.DTO.Order;
using Domain.App.Identity;
using Microsoft.AspNetCore.Identity;

namespace DAL.App.EF
{
    public class AppUnitOfWork : BaseUnitOfWork<AppDbContext>, IAppUnitOfWork
    {
        protected readonly IMapper Mapper;
        protected readonly UserManager<Domain.App.Identity.AppUser> UserManager;
      
        public AppUnitOfWork(AppDbContext uowDbContext, IMapper mapper, UserManager<AppUser> userManager) : base(uowDbContext)
        {
            Mapper = mapper;
            UserManager = userManager;
        }
        
        public IAppUserRepository AppUsers =>
            GetRepository(() => new AppUserRepository(UserManager,UowDbContext, Mapper));
        public IBaseRepository<AppUserOrder> AppUserOrders =>
            GetRepository(() => new BaseRepository<DAL.App.DTO.AppUserOrder, Domain.App.AppUserOrder, 
                AppDbContext>(UowDbContext, new BaseMapper<DAL.App.DTO.AppUserOrder, Domain.App.AppUserOrder>(Mapper)));
        public IBaseRepository<ArchivedAppUserOrder> ArchivedAppUserOrders =>
            GetRepository(() => 
                new BaseRepository<DAL.App.DTO.Archive.ArchivedAppUserOrder, Domain.App.Archive.ArchivedAppUserOrder, 
                    AppDbContext>(UowDbContext, new BaseMapper<DAL.App.DTO.Archive.ArchivedAppUserOrder, Domain.App.Archive.ArchivedAppUserOrder>(Mapper)));
        public IAppUserFavoriteRestaurantsRepository AppUserFavoriteRestaurants =>
            GetRepository(() => new AppUserFavoriteRestaurantsRepository(UowDbContext, Mapper));
        public  IAppUserWorkRestaurantRepository AppUserWorkRestaurants =>
            GetRepository(() => new  AppUserWorkRestaurantRepository(UowDbContext, Mapper));
        public IBaseRepository<BankCard> BankCards =>
            GetRepository(() => new BaseRepository<DAL.App.DTO.BankCard, Domain.App.BankCard, 
                AppDbContext>(UowDbContext, new BaseMapper<DAL.App.DTO.BankCard, Domain.App.BankCard>(Mapper)));
       
        public  IArchivedProductInOrderRepository ArchivedProductsInOrder =>
            GetRepository(() => new  ArchivedProductInOrderRepository(UowDbContext, Mapper));
        
        public  IArchivedSubProductFromProductRepository ArchivedSubProductsFromProduct =>
            GetRepository(() => new  ArchivedSubProductFromProductRepository(UowDbContext, Mapper));
        public IArchivedOrderRepository ArchivedOrders =>
            GetRepository(() => new ArchivedOrderRepository(UowDbContext, Mapper));
        public ICompanyRepository Companies =>
            GetRepository(() => new CompanyRepository(UowDbContext, Mapper, UserManager));
        public IMenuRepository Menus =>
            GetRepository(() => new MenuRepository(UowDbContext, Mapper));
        public IMenuCategoryRepository MenuCategories =>
            GetRepository(() => new MenuCategoryRepository(UowDbContext, Mapper));
      
        public IMenuInRestaurantRepository MenusInRestaurant =>
            GetRepository(() => new MenuInRestaurantRepository(UowDbContext, Mapper));
        public IOrderRepository Orders =>
            GetRepository(() => new OrderRepository(UowDbContext, Mapper));
        public IProductRepository Products =>
            GetRepository(() => new ProductRepository(UowDbContext, Mapper));
       
        public IProductInOrderRepository ProductsInOrder =>
            GetRepository(() => new ProductInOrderRepository(UowDbContext, Mapper));

        public IProductsSubCategoryRepository ProductsSubCategories =>
            GetRepository(() => new ProductsSubCategoryRepository(UowDbContext, Mapper));
        public IRestaurantRepository Restaurants =>
            GetRepository(() => new RestaurantRepository(UowDbContext, Mapper));
       
        public IProductsSubCategoryProductRepository ProductsSubCategoryProducts =>
            GetRepository(() => new ProductsSubCategoryProductRepository(UowDbContext, Mapper));
      
        public ISubProductFromProductRepository SubProductsFromProduct =>
            GetRepository(() => new SubProductFromProductRepository(UowDbContext, Mapper));
        public ITimeRepository Times =>
            GetRepository(() => new TimeRepository(UowDbContext, Mapper));
    }
}