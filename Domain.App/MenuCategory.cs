﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace Domain.App
{
    public class MenuCategory : DomainEntity
    {
        public int MenuId { get; set; }
        public Menu Menu { get; set; } = default!;

        [MaxLength(35)]
        public string Name { get; set; } = default!;

        public int Position { get; set; }

        public ICollection<Product>? Products { get; set; }
    }
}