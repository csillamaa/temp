﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace Domain.App
{
    public class Menu : DomainEntity
    {
        [MaxLength(35)]
        public string Name { get; set; } = default!;
        
        public bool IsTemporary { get; set; }

        public DateTime? TemporaryMenuStart { get; set; }
        public DateTime? TemporaryMenuEnd { get; set; }

        public int CompanyId { get; set; }
        public Company Company { get; set; } = default!;

        public ICollection<MenuCategory>? MenuCategories { get; set; }

        public ICollection<MenuInRestaurant>? RestaurantsWithTheMenu { get; set; }
    }
}