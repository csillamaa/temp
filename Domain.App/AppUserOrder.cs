﻿using Contracts.Domain.Base;
using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class AppUserOrder: DomainEntity, IDomainAppUserId, IDomainAppUser<AppUser>
    {
        public int OrderId { get; set; }
        public Order Order { get; set; } = default!;

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; } = default!;
    }
}