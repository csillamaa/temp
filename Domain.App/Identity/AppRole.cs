﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Domain.App.Identity
{
    public class AppRole: IdentityRole<int>
    {
        [MaxLength(32)]
        public override string Name { get; set; } = default!;
    }
}