﻿using System.Collections.Generic;
using Domain.Base;

namespace Domain.App
{
    public class ProductInOrder : DomainEntity
    {
        public int Amount { get; set; }

        public double OneProductPrice { get; set; }

        public int? ProductId { get; set; }
        public Product? Product { get; set; } = default!;

        public int OrderId { get; set; }
        public Order Order { get; set; } = default!;
        public ICollection<SubProductFromProduct>? SubProducts { get; set; }
    }
}