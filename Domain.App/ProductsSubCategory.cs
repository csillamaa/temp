﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;
using Microsoft.VisualBasic;

namespace Domain.App
{
    public class ProductsSubCategory : DomainEntity
    {
        public int ProductId { get; set; }
        public Product Product{ get; set; } = default!;

        [MaxLength(60)]
        public string Name { get; set; } = default!;

        public int Position { get; set; }

        public int MandatoryItemCount { get; set; }
        
        public int MaxItemCount { get; set; }
        
        // public int[]? DefaultSelectedSubProductsIds { get; set; }

        public ICollection<ProductsSubCategoryProduct>? SubCategoryProducts { get; set; }
    }
}