﻿using Domain.Base;

namespace Domain.App
{
    public class MenuInRestaurant : DomainEntity
    {
        public int RestaurantId { get; set; }
        public Restaurant Restaurant { get; set; } = default!;

        public int MenuId { get; set; }
        public Menu Menu { get; set; } = default!;
    }
}