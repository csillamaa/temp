﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class Company : DomainEntity
    {
        [MaxLength(60)]
        public string Name { get; set; } = default!;

        public DateTime DateJoined { get; set; }
        
        [MaxLength(60)]
        public string? Website { get; set; }

        [MaxLength(20)]
        public string PhoneNumber { get; set; } = default!;

        [MaxLength(60)]
        public string Email { get; set; } = default!;
        
        public ICollection<Restaurant>? Restaurants { get; set; }
        public ICollection<AppUser>? CompanyUsers { get; set; }
        public ICollection<Menu>? Menus { get; set; }
        
    }
}