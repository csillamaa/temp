﻿using System;
using Domain.Base;

namespace Domain.App
{
    public class Time : DomainEntity
    {
        // public int StartingDayOfWeek { get; set; }
        // public DateTime StartingTime { get; set; }
        //
        // public int EndingDayOfWeek { get; set; }
        // public DateTime EndingTime { get; set; }
        
       
        
        public int DayOfWeek { get; set; }
        
        public DateTime StartingTime { get; set; }

        public DateTime EndingTime { get; set; }

        public int? RestaurantId { get; set; }
        public Restaurant? Restaurant { get; set; }
        
        public int? ProductId { get; set; }
        public Product? Product { get; set; }
          
    }
}