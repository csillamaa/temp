﻿using Contracts.Domain.Base;
using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class AppUserWorkRestaurant: DomainEntity, IDomainAppUserId, IDomainAppUser<AppUser>
    {
        public int RestaurantId { get; set; }
        public Restaurant Restaurant { get; set; } = default!;

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; } = default!;
    }
}