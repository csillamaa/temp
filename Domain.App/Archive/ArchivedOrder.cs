﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace Domain.App.Archive
{
    public class ArchivedOrder : DomainEntity 
    {
        public DateTime SubmitTime { get; set; }

        public DateTime EstimatedCompleteTime { get; set; }
        public DateTime ActualCompleteTime { get; set; }

        public DateTime OrderHandedOver { get; set; }

        public int State { get; set; }

        public bool IsSelfPickup { get; set; }

        public double DeliveryCost { get; set; }

        public double FoodCost { get; set; }

        public double AmountPaid { get; set; }
       

        [MaxLength(500)]
        public string? CustomerComment { get; set; }

        public int? OrderRating { get; set; }
        [MaxLength(350)]
        public string? CustomerOrderReviewComment { get; set; } = default!;

        public int RestaurantId { get; set; }
        public Restaurant Restaurant { get; set; } = default!;

        [MaxLength(250)]
        public string? Comment { get; set; } = default!;
        
        public ICollection<ArchivedProductInOrder> ArchivedProductsInOrder { get; set; } = default!;
        
        public ICollection<ArchivedAppUserOrder> AppUsers { get; set; } = default!;

    }
}