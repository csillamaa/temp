﻿using System.Collections.Generic;
using Domain.Base;

namespace Domain.App.Archive
{
    public class ArchivedProductInOrder : DomainEntity
    {
        public int Amount { get; set; }

        public double OneProductPrice { get; set; }

        public int? ProductId { get; set; }
        public Product? Product { get; set; } = default!;

        public int ArchivedOrderId { get; set; }
        public ArchivedOrder ArchivedOrder { get; set; } = default!;
        
        public ICollection<ArchivedSubProductFromProduct>? ArchivedSubProducts { get; set; }
    }
}