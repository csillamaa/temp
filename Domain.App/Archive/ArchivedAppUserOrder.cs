﻿using Contracts.Domain.Base;
using Domain.App.Identity;
using Domain.Base;

namespace Domain.App.Archive
{
    public class ArchivedAppUserOrder: DomainEntity, IDomainAppUserId, IDomainAppUser<AppUser>
    {
        public int ArchivedOrderId { get; set; }
        public ArchivedOrder ArchivedOrder { get; set; } = default!;

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; } = default!;
    }
}