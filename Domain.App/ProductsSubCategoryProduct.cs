﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace Domain.App
{
    public class ProductsSubCategoryProduct : DomainEntity
    {
        public int ProductsSubCategoryId { get; set; }
        public ProductsSubCategory ProductsSubCategory { get; set; } = default!;

        [MaxLength(50)]
        public string Name { get; set; } = default!;

        public double Price { get; set; }

        public bool IsSelectedOnDefault { get; set; }
        
        [MaxLength(250)]
        public string? Description { get; set; }
        public int Position { get; set; }
    }
}