using System;

namespace Contracts.Domain.Base
{
    public interface IDomainEntity : IDomainEntity<int>
    {
        
    }

    public interface IDomainEntity<TKey> 
    where TKey : IEquatable<TKey>
    {
        TKey Id { get; set; }
    }
}