﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Domain.App.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace Extensions.Base
{
    public static class IdentityExtensions
    {
        public static int? GetUserId(this ClaimsPrincipal user)
        {
            var userId = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
            if (userId != null)
            {
                return int.Parse(userId);
            }

            return null;
        }
        public static async Task<bool> DoesUserHaveAuthorityToEdit(this ClaimsPrincipal user,int? companyId, UserManager<AppUser> userManager)
        {
            if (user.IsInRole("Company Admin"))
            {
                var userCompanyId = await userManager.Users
                    .Where(u => u.Id.Equals(user.GetUserId()!.Value))
                    .Select(u => u.CompanyId)
                    .FirstOrDefaultAsync();
                
                if (!companyId.Equals(userCompanyId))
                {
                    return false;
                }
            }
            return true;
        }
        
        public static bool DoesUserHaveRoles(this ClaimsPrincipal user)
        {
            return ((ClaimsIdentity) user.Identity!).Claims.Any(c => c.Type == ClaimTypes.Role);
        }
        public static async Task<bool> IsRestaurantAddedToUserFavorites(this ClaimsPrincipal user, UserManager<AppUser> userManager, int restaurantId)
        {
            var res = await userManager.Users
                .Where(u => u.Id.Equals(user.GetUserId()))
                .Where(u => u.FavoriteRestaurants!
                    .Any(r => r.RestaurantId.Equals(restaurantId)))
                .Select(u => u.FirstName)
                .FirstOrDefaultAsync();

            return res != null;
        }
        
        public static string GenerateJwt(IEnumerable<Claim> claims, string key, string issuer, string audience,
            DateTime expirationDateTime)
        {
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer,
                audience, 
                claims, 
                expires: expirationDateTime, 
                signingCredentials: signingCredentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}