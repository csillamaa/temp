﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;

namespace BLL.App.Services
{
    public class ArchivedProductInOrderService : BaseEntityService<IAppUnitOfWork, IArchivedProductInOrderRepository, 
            BLLAppDTOOrderArchived.ArchivedProductInOrder, DALAppDTOOrderArchived.ArchivedProductInOrder>, 
        IArchivedProductInOrderService
    {
        public ArchivedProductInOrderService(IAppUnitOfWork serviceUow, IArchivedProductInOrderRepository serviceRepository, IMapper mapper)
            : base(serviceUow, serviceRepository, new ArchivedProductInOrderMapper(mapper))
        {
        }
    }
}