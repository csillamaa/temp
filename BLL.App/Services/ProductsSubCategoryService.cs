﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Product;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace BLL.App.Services
{
    public class ProductsSubCategoryService : BaseEntityService<IAppUnitOfWork, IProductsSubCategoryRepository, 
        DTO.Product.ProductsSubCategory, ProductsSubCategory>, IProductsSubCategoryService
    {
        public ProductsSubCategoryService(IAppUnitOfWork serviceUow, IProductsSubCategoryRepository serviceRepository, IMapper mapper)
            : base(serviceUow, serviceRepository, new ProductsSubCategoryMapper(mapper))
        {
        }

        public async Task<ProductsSubCategoryData?> FirstOrDefaultEditAsync(int id, bool noTracking = true)
        {
            return await ServiceRepository.FirstOrDefaultEditAsync(id, noTracking);
        }

        public async Task<DTO.Product.ProductsSubCategoryData?> FirstOrDefaultEditAsyncBLL(int id, bool noTracking = true)
        {
            return await ServiceRepository.FirstOrDefaultEditAsyncBLL(id, noTracking);
        }

        public int GetProductsSubCategoryCountInProduct(int productId)
        {
            return ServiceRepository.GetProductsSubCategoryCountInProduct(productId);
        }

        public async Task<int> MoveSubCategoriesUpWhenDelete(int productId)
        {
            return await ServiceRepository.MoveSubCategoriesUpWhenDelete(productId);
        }

        public async Task<int> GetSubCategoryCompanyId(int id)
        {
            return await ServiceRepository.GetSubCategoryCompanyId(id);
        }

        public int GetSubCategoryCountInProduct(int productId)
        {
            return ServiceRepository.GetSubCategoryCountInProduct(productId);
        }

        public async Task<int> GetProductId(int id)
        {
            return await ServiceRepository.GetProductId(id);
        }

        public async Task<bool> ChangeSubCategoryPosition(int id, bool moveUp)
        {
            return await ServiceRepository.ChangeSubCategoryPosition(id, moveUp);
        }
    }
}