﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

using BLLAppDTOOrder = BLL.App.DTO.Order;
using DALAppDTOOrder = DAL.App.DTO.Order;

namespace BLL.App.Services
{
    public class ProductInOrderService : BaseEntityService<IAppUnitOfWork, IProductInOrderRepository, BLLAppDTOOrder.ProductInOrder, DALAppDTOOrder.ProductInOrder>, 
        IProductInOrderService
    {
        public ProductInOrderService(IAppUnitOfWork serviceUow, IProductInOrderRepository serviceRepository, IMapper mapper)
            : base(serviceUow, serviceRepository, new ProductInOrderMapper(mapper))
        {
        }
        
        public async Task<bool> ChangeProductInOrderAmount(int orderId, int productInOrderId, int newAmount, bool noTracking = true)
        {
            return await ServiceRepository.ChangeProductInOrderAmount(orderId, productInOrderId, newAmount, noTracking);
        }

        public async Task<int> GetOrderId(int id, bool noTracking = true)
        {
            return await ServiceRepository.GetOrderId(id, noTracking);
        }
        
        public async Task<bool> AddAmountToProductIfSameOneAlreadyInOrder(int userBasketId, int productId, int amount, int[] allSubProductIds, bool noTracking = true)
        {
            return await ServiceRepository.AddAmountToProductIfSameOneAlreadyInOrder(userBasketId, productId, amount,
                allSubProductIds);
        }

    }
}