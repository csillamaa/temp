﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace BLL.App.Services
{
    public class TimeService : BaseEntityService<IAppUnitOfWork, ITimeRepository, BLLAppDTO.Time, DALAppDTO.Time>, ITimeService
    {
        public TimeService(IAppUnitOfWork serviceUow, ITimeRepository serviceRepository, IMapper mapper)
            : base(serviceUow, serviceRepository, new TimeMapper(mapper))
        {
        }

        public async Task<List<BLLAppDTO.Time>?> GetAllRestaurantOpenTimes(int restaurantId, bool noTracking = true)
        {
            return (await ServiceRepository.GetAllRestaurantOpenTimes(restaurantId, noTracking))?
                .Select(time => Mapper.Map(time)!)
                .ToList();
        }

        public async Task<bool> SaveNewTime(int restaurantId, BLL.App.DTO.Time time)
        {
            return await ServiceRepository.SaveNewTime(restaurantId, TimeMapper.MapToDal(time)!);
        }

        public async Task<bool> IsRestaurantOpen(int restaurantId)
        {
            return await ServiceRepository.IsRestaurantOpen(restaurantId);
        }

        public async Task<string?> ValidateAndSaveOpeningTime(int restaurantId, string dayString, int dayOfWeek, 
            string? timeOpen, string? timeClose)
        {
            if (string.IsNullOrEmpty(timeOpen) && string.IsNullOrEmpty(timeClose))
            {
                return null;
            }
            
            if (!DateTime.TryParse(timeOpen, out var openTime) || !DateTime.TryParse(timeClose, out var closeTime))
            {
                return dayString + " ";
            }

            if (!IsOpenTimeBeforeClose(openTime, closeTime))
            {
                return dayString + " ";
            }

            await SaveNewTime(restaurantId, new BLLAppDTO.Time
            {
                DayOfWeek = dayOfWeek,
                StartingTime = openTime,
                EndingTime = closeTime
            });
            return null;
        }

        public bool IsOpenTimeBeforeClose(DateTime openTime, DateTime closeTime)
        {
            return TimeSpan.Compare(openTime.TimeOfDay, closeTime.TimeOfDay) == -1;
        }
    }
}