﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.App.DTO.Menu;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace BLL.App.Services
{
    public class MenuInRestaurantService : BaseEntityService<IAppUnitOfWork, IMenuInRestaurantRepository,
        BLLAppDTO.Menu.MenuInRestaurant, DALAppDTO.Menu.MenuInRestaurant>, IMenuInRestaurantService
    {
        public MenuInRestaurantService(IAppUnitOfWork serviceUow, IMenuInRestaurantRepository serviceRepository, IMapper mapper)
            : base(serviceUow, serviceRepository, new MenuInRestaurantMapper(mapper))
        {
        }

        public async Task<List<BLLAppDTO.Menu.MenuInRestaurant>> RemoveMenuFromAllRestaurants(int menuId)
        {
            return (await ServiceRepository.RemoveMenuFromAllRestaurants(menuId)).Select(Mapper.Map).ToList()!;
        }

        public async Task<MenuInRestaurant?> RemoveMenuFromRestaurant(MenuInRestaurant menuInRestaurant, bool noTracking = true)
        {
            return Mapper.Map(await ServiceRepository.RemoveMenuFromRestaurant(Mapper.Map(menuInRestaurant)!, noTracking))!;
        }

        public async Task<bool> IsMenuAddableToRestaurant(MenuInRestaurant menuInRestaurant, bool noTracking = true)
        {
            var menuExists = await ServiceUow.Menus.ExistsAsync(menuInRestaurant.MenuId);
            var restaurantExists = await ServiceUow.Restaurants.ExistsAsync(menuInRestaurant.RestaurantId);
            
            if (!menuExists || !restaurantExists)
            {
                return false;
            }
            
            var menuCompanyId = await ServiceUow.Menus.GetMenuCompanyId(menuInRestaurant.MenuId);
            var restaurantCompanyId = await ServiceUow.Restaurants.GetRestaurantCompanyId(menuInRestaurant.RestaurantId);

            if (menuCompanyId != restaurantCompanyId)
            {
                return false;
            }

            return await ServiceRepository.IsMenuAddableToRestaurant(Mapper.Map(menuInRestaurant)!, noTracking);
        }

        public async Task<int> RemoveAllMenusFromRestaurant(int id, bool noTracking = true)
        {
            return await ServiceRepository.RemoveAllMenusFromRestaurant(id, noTracking);
        }
    }
}