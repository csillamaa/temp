﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Menu;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;
using Menu = BLL.App.DTO.Menu.Menu;
using MenuData = BLL.App.DTO.Menu.MenuData;

namespace BLL.App.Services
{
    public class MenuService : BaseEntityService<IAppUnitOfWork, IMenuRepository, BLLAppDTO.Menu.Menu, DALAppDTO.Menu.Menu>, IMenuService
    {
        public MenuService(IAppUnitOfWork serviceUow, IMenuRepository serviceRepository, IMapper mapper)
            : base(serviceUow, serviceRepository, new MenuMapper(mapper))
        {
        }

        // public async Task<BLLAppDTO.Menu.Menu> RemoveMenu(BLLAppDTO.Menu.Menu menu, int userId = default)
        // {
        //     if (menu.MenuCategories != null)
        //     {
        //         await ServiceUow.MenuCategories.RemoveAllMenuCategories(menu.Id);
        //     }
        //
        //     await ServiceUow.MenusInRestaurant.RemoveMenuFromAllRestaurants(menu.Id);
        //
        //
        //     return Mapper.Map(ServiceRepository.Remove(Mapper.Map(menu)!))!;
        // }

        public async Task<int> GetMenuCompanyId(int menuId, bool noTracking = true)
        {
            return await ServiceRepository.GetMenuCompanyId(menuId, noTracking);
        }

        public async Task<int> CopyMenuItemsToOtherMenu(int currentMenuId, int otherMenuId)
        {
            return await ServiceRepository.CopyMenuItemsToOtherMenu(currentMenuId, otherMenuId);
        }
        

        public async Task<MenuEdit?> FirstOrDefaultEditAsync(int menuId,int? currentCategory, bool noTracking = true)
        {
            return await ServiceRepository.FirstOrDefaultEditAsync(menuId, currentCategory, noTracking);
        }

        public async Task<DTO.Menu.MenuEdit?> FirstOrDefaultEditAsyncBLL(int menuId, int? currentCategoryId, bool noTracking = true)
        {
            var menu = await ServiceRepository.FirstOrDefaultEditAsyncBLL(menuId, currentCategoryId, noTracking);
            if (menu == null)
            {
                return null;
            }

            menu.CompanyMenus = await ServiceRepository.GetAllOtherCompanyMenusBLL(menu.CompanyId, menuId, noTracking);

            currentCategoryId ??= menu.MenuCategories?.FirstOrDefault(c => c.Position.Equals(0))?.Id;
        
            if (currentCategoryId == null)
            {
                return menu;
            }
            menu.CurrentCategory = MenuCategoryMapper.Map(await ServiceUow.MenuCategories.FirstOrDefaultAsync(currentCategoryId.Value));
        
            return menu;
        }

        public async Task<IEnumerable<MenuIndex>> GetAllIndexASync(int? companyId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllIndexASync(companyId, noTracking);
        }

        public async Task<IEnumerable<DTO.Menu.MenuIndex>> GetAllIndexASyncBLL(int? companyId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllIndexASyncBLL(companyId, noTracking);
        }

        public async Task<List<MenuOnlyName>?> GetAllUnPickedCompanyMenus(int companyId, int restaurantId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllUnPickedCompanyMenus(companyId, restaurantId, noTracking);
        }

        public async Task<List<DTO.Menu.MenuOnlyName>?> GetAllUnpickedCompanyMenusBLL(int companyId, int restaurantId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllUnpickedCompanyMenusBLL(companyId, restaurantId, noTracking);
        }

        public async Task<List<MenuOnlyName>?> GetAllOtherCompanyMenus(int companyId, int menuId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllOtherCompanyMenus(companyId, menuId, noTracking);
        }
        
        public async Task<List<DTO.Menu.MenuOnlyName>?> GetAllOtherCompanyMenusBLL(int companyId, int menuId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllOtherCompanyMenusBLL(companyId, menuId, noTracking);
        }
    }
}