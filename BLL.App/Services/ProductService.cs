﻿using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using BLLAppDTOProduct = BLL.App.DTO.Product;
using DALAppDTOProduct = DAL.App.DTO.Product;

namespace BLL.App.Services
{
    public class ProductService : BaseEntityService<IAppUnitOfWork, IProductRepository, BLLAppDTOProduct.Product, 
        DALAppDTOProduct.Product>, IProductService
    {
        public ProductService(IAppUnitOfWork serviceUow, IProductRepository serviceRepository, IMapper mapper)
            : base(serviceUow, serviceRepository, new ProductMapper(mapper))
        {
        }
        
        public async Task<int> GetProductCompanyId(int id)
        {
            return await ServiceRepository.GetProductCompanyId(id);
        }

        public async Task<int> GetProductMenuId(int id)
        {
            return await ServiceRepository.GetProductMenuId(id);
        }

        public int GetProductCountInCategory(int categoryId)
        {
            return ServiceRepository.GetProductCountInCategory(categoryId);
        }

        public async Task<bool> ChangeProductPosition(int id, bool moveUp)
        {
            return await ServiceRepository.ChangeProductPosition(id, moveUp);
        }

        public async Task<DALAppDTOProduct.ProductEdit?> FirstOrDefaultEditAsync(int id, bool noTracking = true)
        {
            return await ServiceRepository.FirstOrDefaultEditAsync(id, noTracking);
        }

        public async Task<BLLAppDTOProduct.ProductEdit?> FirstOrDefaultEditAsyncBLL(int id, bool noTracking = true)
        {
            var product = await ServiceRepository.FirstOrDefaultEditAsyncBLL(id, noTracking);


            if (product == null)
            {
                return null;
            }
            
            product.MenuId = await ServiceRepository.GetProductMenuId(id);
            product.MenuCategories =
                await ServiceUow.MenuCategories.GetAllMenuCategoriesOnlyNameBLL(
                    await ServiceRepository.GetProductMenuId(id));

            return product;
        }

        public async Task<DALAppDTOProduct.ProductClient?> FirstOrDefaultClientAsync(int id, bool noTracking = true)
        {
            return await ServiceRepository.FirstOrDefaultClientAsync(id, noTracking);
        }

        public async Task<BLLAppDTOProduct.ProductClient?> FirstOrDefaultClientAsyncBLL(int id, bool noTracking = true)
        {
            return await ServiceRepository.FirstOrDefaultClientAsyncBLL(id, noTracking);
        }

        public async Task<int> MoveProductsUpWhenDelete(int productId)
        {
            return await ServiceRepository.MoveProductsUpWhenDelete(productId);
        }

        public async Task<DALAppDTOProduct.ProductOnlyPrice> GetProductOnlyPriceById(int id, bool noTracking = true)
        {
            return await ServiceRepository.GetProductOnlyPriceById(id, noTracking);
        }
    }
}