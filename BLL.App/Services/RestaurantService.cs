﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.App.DTO.Restaurant;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;
using Restaurant = DAL.App.DTO.Restaurant.Restaurant;
using RestaurantClient = DAL.App.DTO.Restaurant.RestaurantClient;
using RestaurantClientAll = DAL.App.DTO.Restaurant.RestaurantClientAll;
using RestaurantEdit = DAL.App.DTO.Restaurant.RestaurantEdit;
using RestaurantIndex = DAL.App.DTO.Restaurant.RestaurantIndex;
using RestaurantOnlyName = DAL.App.DTO.Restaurant.RestaurantOnlyName;

namespace BLL.App.Services
{
    public class RestaurantService
        : BaseEntityService<IAppUnitOfWork, IRestaurantRepository, DTO.Restaurant.Restaurant, Restaurant>, IRestaurantService
    {
        public RestaurantService(IAppUnitOfWork serviceUow, IRestaurantRepository serviceRepository, IMapper mapper)
            : base(serviceUow, serviceRepository, new RestaurantMapper(mapper))
        {
        }

        public async Task<IEnumerable<RestaurantIndex>> GetAllIndexAsync(bool noTracking = true)
        {
            return await ServiceRepository.GetAllIndexAsync();
        }

        public async Task<IEnumerable<DTO.Restaurant.RestaurantIndex>> GetAllIndexAsyncBLL(bool noTracking = true)
        {
            return await ServiceRepository.GetAllIndexAsyncBLL();
        }
        

        // public async Task<IEnumerable<DTO.Restaurant.Restaurant>> GetAllAppUserAvailableAsync(int userId = default, bool noTracking = true)
        // {
        //     return (await ServiceRepository.GetAllAppUserAvailableAsync(userId, noTracking))
        //         .Select(x => Mapper.Map(x))!;
        // }

        public async Task<IList<DALAppDTO.Restaurant.RestaurantOnlyName>?> GetAllRestaurantsAppUserWorksInAsync(int userId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllRestaurantsAppUserWorksInAsync(userId, noTracking);

        }
        public async Task<IList<BLLAppDTO.Restaurant.RestaurantOnlyName>?> GetAllRestaurantsAppUserWorksInAsyncBLL(int userId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllRestaurantsAppUserWorksInAsyncBLL(userId, noTracking);

        }

        public async Task<ICollection<RestaurantOnlyName>?> GetAllRestaurantsAddableToUser(int companyId, int userId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllRestaurantsAddableToUser(companyId, userId, noTracking);
        }

        public async Task<ICollection<DTO.Restaurant.RestaurantOnlyName>?> GetAllRestaurantsAddableToUserBLL(int companyId, int userId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllRestaurantsAddableToUserBLL(companyId, userId, noTracking);
        }

        public async Task<IEnumerable<RestaurantIndex>?> GetAllCompanyRestaurants(int companyId, bool noTracking)
        {
            return await ServiceRepository.GetAllCompanyRestaurants(companyId, noTracking);
        }

        public async Task<IEnumerable<DTO.Restaurant.RestaurantIndex>?> GetAllCompanyRestaurantsBLL(int companyId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllCompanyRestaurantsBLL(companyId, noTracking);
        }

        public async Task<bool> IsRestaurantOwnedByCompany(int restaurantId, int companyId, bool noTracking = true)
        {
            return await ServiceRepository.IsRestaurantOwnedByCompany(restaurantId, companyId, noTracking);
        }

        public async Task<RestaurantEdit?> FirstOrDefaultEditAsync(int id, bool noTracking)
        {
            return await ServiceRepository.FirstOrDefaultEditAsync(id, noTracking);
        }

        public async Task<DTO.Restaurant.RestaurantEdit?> FirstOrDefaultEditAsyncBLL(int id, bool noTracking = true)
        {
            var restaurant = await ServiceRepository.FirstOrDefaultEditAsyncBLL(id, noTracking);
            if (restaurant == null)
            {
                return null;
            }
            restaurant.CompanyMenus = await ServiceUow.Menus.GetAllUnpickedCompanyMenusBLL(restaurant.CompanyId, restaurant.Id, noTracking);
            return restaurant;
        }

        public async Task<int> GetRestaurantCompanyId(int restaurantId, bool noTracking = true)
        {
            return await ServiceRepository.GetRestaurantCompanyId(restaurantId, noTracking);
        }

        public bool IsRestaurantOpen(DALAppDTO.Time? restaurantOpenTime)
        {
            return ServiceRepository.IsRestaurantOpen(restaurantOpenTime);
        }

        public async Task<DTO.Restaurant.Restaurant?> FirstOrDefaultDeleteAsync(int id, bool noTracking = true)
        {
            return Mapper.Map(await ServiceRepository.FirstOrDefaultDeleteAsync(id, noTracking));
        }

        public async Task<List<RestaurantClientAll>> GetAllClientAsync(bool noTracking = true)
        {
            return await ServiceRepository.GetAllClientAsync(noTracking);
        }

        public async Task<List<DTO.Restaurant.RestaurantClientAll>> GetAllClientAsyncBLL(bool noTracking = true)
        {
            return await ServiceRepository.GetAllClientAsyncBLL(noTracking);

        }

        public async Task<List<RestaurantClientAll>?> GetAllAppUserFavouriteRestaurants(int[]? userFavoriteRestaurantIds, bool noTracking = true)
        {
            return await ServiceRepository.GetAllAppUserFavouriteRestaurants(userFavoriteRestaurantIds, noTracking);
        }

        public async Task<List<DTO.Restaurant.RestaurantClientAll>?> GetAllAppUserFavouriteRestaurantsBLL(int[]? userFavoriteRestaurantIds, bool noTracking = true)
        {
            return await ServiceRepository.GetAllAppUserFavouriteRestaurantsBLL(userFavoriteRestaurantIds, noTracking);
        }

        public async Task<RestaurantClient?> FirstOrDefaultClientAsync(int id, int? currentCategoryPosition = 0, bool noTracking = true)
        {
            return await ServiceRepository.FirstOrDefaultClientAsync(id, currentCategoryPosition, noTracking);
        }
        
        public async Task<BLL.App.DTO.Restaurant.RestaurantClient?> FirstOrDefaultClientAsyncBLL(int id, int? currentCategoryPosition = 0, bool noTracking = true)
        {
            return await ServiceRepository.FirstOrDefaultClientAsyncBLL(id, currentCategoryPosition, noTracking);
        }

        public async Task<string?> GetRestaurantName(int id, bool noTracking = true)
        {
            return await ServiceRepository.GetRestaurantName(id, noTracking);
        }

        public async Task<bool> IsRestaurantOpenById(int restaurantId, bool noTracking = true)
        {
            return await ServiceRepository.IsRestaurantOpenById(restaurantId, noTracking);
        }

        public async Task<bool> IsProductInRestaurantMenu(int productId, int restaurantId, bool noTracking = true)
        {
            return await ServiceRepository.IsProductInRestaurantMenu(productId, restaurantId);
        }

        public async Task<RestaurantClientAllLists> GetAllRestaurantViewModel(int? userId, bool noTracking = true)
        {
            var allRestaurants = await ServiceRepository.GetAllClientAsyncBLL();
            
            var userFavoriteRestaurantIds = await ServiceUow.AppUserFavoriteRestaurants.GetAllAppUserFavoriteRestaurants(userId);
            
            var allFavouriteRestaurants = await ServiceRepository.GetAllAppUserFavouriteRestaurantsBLL(userFavoriteRestaurantIds);

            var restaurants = new RestaurantClientAllLists
            {
                AllRestaurants = allRestaurants,
                UserFavoriteRestaurants = allFavouriteRestaurants
            };

            return restaurants;
        }
    }
}