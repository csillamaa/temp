﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.App.DTO.Order;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;
using Order = DAL.App.DTO.Order.Order;
using OrderClient = DAL.App.DTO.Order.OrderClient;
using OrderWorker = DAL.App.DTO.Order.OrderWorker;

namespace BLL.App.Services
{
    public class OrderService
        : BaseEntityService<IAppUnitOfWork, IOrderRepository, DTO.Order.Order, Order>, IOrderService
    {
        public OrderService(IAppUnitOfWork serviceUow, IOrderRepository serviceRepository, IMapper mapper)
            : base(serviceUow, serviceRepository, new OrderMapper(mapper))
        {
        }

        public async Task<ICollection<OrderWorker>?> GetAllRestaurantActiveOrdersAsync(int restaurantId, bool isReady, bool noTracking = true)
        {
            return await ServiceRepository.GetAllRestaurantActiveOrdersAsync(restaurantId, isReady, noTracking);
        }

        public async Task<ICollection<DTO.Order.OrderWorker>?> GetAllRestaurantActiveOrdersAsyncBLL(int restaurantId, bool isReady, bool noTracking = true)
        {
            return await ServiceRepository.GetAllRestaurantActiveOrdersAsyncBLL(restaurantId, isReady, noTracking);
        }
        
        public async Task<OrderClient?> FirstOrDefaultClientAsync(int userId, bool noTracking = true)
        {
            return await ServiceRepository.FirstOrDefaultClientAsync(userId, noTracking);
        }

        public async Task<DTO.Order.OrderClient?> FirstOrDefaultClientAsyncBLL(int userId, bool noTracking = true)
        {
            return await ServiceRepository.FirstOrDefaultClientAsyncBLL(userId, noTracking);
        }

        public async Task<List<OrderClient>?> GetAllActiveClientAsync(int userId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllActiveClientAsync(userId, noTracking);
        }

        public async Task<List<DTO.Order.OrderClient>?> GetAllActiveClientAsyncBLL(int userId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllActiveClientAsyncBLL(userId, noTracking);
        }

        public async Task<bool> AddCustomerCommentToOrder(int userId, int orderId, string comment, bool noTracking = true)
        {
            return await ServiceRepository.AddCustomerCommentToOrder(userId, orderId, comment, noTracking);
        }

        public async Task<bool> DoesOrderBelongToCustomer(int userId, int orderId, bool noTracking = true)
        {
            return await ServiceRepository.DoesOrderBelongToCustomer(userId, orderId, noTracking);
        }

        public async Task<bool> SubmitOrder(int orderId, string? cardNumber, bool noTracking = true)
        {
            var restaurantId = await ServiceRepository.GetRestaurantId(orderId);

            if (!await ServiceUow.Restaurants.IsRestaurantOpenById(restaurantId!.Value))
            {
                return false;
            }
            
            return await ServiceRepository.SubmitOrder(orderId, cardNumber, noTracking);
        }

        public async Task<bool> DeleteIfOrderIsEmpty(int orderId, bool noTracking = true)
        {
            return await ServiceRepository.DeleteIfOrderIsEmpty(orderId, noTracking);
        }

        public async Task<bool> ReCalculateOrderCost(int orderId, bool noTracking = true)
        {
            return await ServiceRepository.ReCalculateOrderCost(orderId, noTracking);
        }

        public async Task<bool> ChangeOrderCompleteTime(int orderId, DateTime newCompleteTime, bool noTracking = true)
        {
            return await ServiceRepository.ChangeOrderCompleteTime(orderId, newCompleteTime, noTracking);
        }

        public async Task<int?> GetRestaurantId(int orderId, bool noTracking = true)
        {
            return await ServiceRepository.GetRestaurantId(orderId, noTracking);
        }

        public async Task<bool> ChangeOrderToReady(int orderId, bool noTracking = true)
        {
            return await ServiceRepository.ChangeOrderToReady(orderId, noTracking);
        }

        public async Task<int?> GetCompanyId(int orderId, bool noTracking = true)
        {
            return await ServiceRepository.GetCompanyId(orderId, noTracking);
        }

        public async Task<bool> ArchiveOrder(int orderId, bool noTracking = true)
        {
            return await ServiceRepository.ArchiveOrder(orderId, noTracking);
        }

        public async Task<bool> IsUserBasketEmptyOrFromSameRestaurant(int userId, int restaurantId, bool noTracking = true)
        {
            return await ServiceRepository.IsUserBasketEmptyOrFromSameRestaurant(userId, restaurantId, noTracking);
        }

      
        public async Task<bool> AddProductToOrder(int userBasketId, int userId, DALAppDTO.Product.ProductOnlyPrice product,int restaurantId,
            int amount,  IEnumerable<DALAppDTO.Product.ProductsSubCategoryProductOnlyPrice> subCategoryProductOnlyPrice, bool noTracking = true)
        {
            return await ServiceRepository.AddProductToOrder(userBasketId, userId, product, restaurantId,  amount,
                subCategoryProductOnlyPrice);
        }

        public async Task<int> GetUserBasketId(int userId, bool noTracking = true)
        {
            return await ServiceRepository.GetUserBasketId(userId, noTracking);
        }

        public async Task<bool> DeleteOrderIfNewProductFromDifferentRestaurant(int userBasketId, int restaurantId, bool noTracking = true)
        {
            return await ServiceRepository.DeleteOrderIfNewProductFromDifferentRestaurant(userBasketId, restaurantId,
                noTracking);
        }

        public async Task<OrderWorkerView> GetWorkerOrderView(int restaurantId, bool isReady, bool noTracking = true)
        {
            var orders = await ServiceRepository.GetAllRestaurantActiveOrdersAsyncBLL(restaurantId, isReady, noTracking);

            var orderView = new OrderWorkerView
            {
                Orders = orders,
                RestaurantId = restaurantId,
                RestaurantName = (await ServiceUow.Restaurants.GetRestaurantName(restaurantId))!
            };

            return orderView;
        }

        public async Task<string?> AddProductToOrder(int userId, int productId, int restaurantId, int amount, int[] subProductIds)
        {
            if (!await ServiceUow.Restaurants.IsProductInRestaurantMenu(productId, restaurantId))
            {
                return "Product isn't in restaurant's menu";
            }
            
            if (!ServiceUow.ProductsSubCategoryProducts.AreSubProductsFromTheProduct(productId, subProductIds))
            {
                return "Some sub products aren't from the product or they don't exist.";
            }

            if (!await ServiceUow.ProductsSubCategoryProducts.IsThereTooManySubProductsFromACategory(subProductIds))
            {
                return "There are too many or too little of some sub products";
            }

            var userBasketId = await ServiceRepository.GetUserBasketId(userId);
            
            if (await ServiceUow.Orders.DeleteOrderIfNewProductFromDifferentRestaurant(userBasketId, restaurantId))
            {
                userBasketId = default;
            }
            
            if (await ServiceUow.ProductsInOrder.AddAmountToProductIfSameOneAlreadyInOrder(userBasketId, productId, amount, subProductIds))
            {
                return null;
            }

            var allSubProductsWithIdAndPrice =
                await ServiceUow.ProductsSubCategoryProducts.GetAllPricesById(subProductIds);

            var productWithPrice = await ServiceUow.Products.GetProductOnlyPriceById(productId);

            await ServiceRepository.AddProductToOrder(userBasketId, userId, productWithPrice, restaurantId, amount, allSubProductsWithIdAndPrice);

            return null;
        }

        public int[] AddSubProductIdsToOneArray(int[] subCategory0, int[] subCategory1, int[] subCategory2, int[] subCategory3, int[] subCategory4)
        {
            var allSubProductIds = new int[subCategory0.Length + subCategory1.Length + subCategory2.Length 
                                           + subCategory3.Length + subCategory4.Length];

            subCategory0.CopyTo(allSubProductIds, 0);
            subCategory1.CopyTo(allSubProductIds, subCategory0.Length);
            subCategory2.CopyTo(allSubProductIds,  subCategory0.Length + subCategory1.Length);
            subCategory3.CopyTo(allSubProductIds, subCategory0.Length + subCategory1.Length + subCategory2.Length);
            subCategory4.CopyTo(allSubProductIds, subCategory0.Length + subCategory1.Length + subCategory2.Length + subCategory3.Length);
            
            return allSubProductIds;
        }
    }
}