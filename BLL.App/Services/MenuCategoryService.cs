﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Identity;
using BLL.App.DTO.Mappers;
using BLL.App.DTO.Menu;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;
using MenuCategoryData = DAL.App.DTO.Menu.MenuCategoryData;
using MenuCategoryOnlyName = DAL.App.DTO.Menu.MenuCategoryOnlyName;

namespace BLL.App.Services
{
    public class MenuCategoryService
        : BaseEntityService<IAppUnitOfWork, IMenuCategoryRepository, BLLAppDTO.Menu.MenuCategory, DALAppDTO.Menu.MenuCategory>,
            IMenuCategoryService
    {
        public MenuCategoryService(IAppUnitOfWork serviceUow, IMenuCategoryRepository serviceRepository, IMapper mapper)
            : base(serviceUow, serviceRepository, new MenuCategoryMapper(mapper))
        {
        }

        public new async Task<MenuCategory> RemoveAsync(int id, int userId)
        {
            return Mapper.Map(await ServiceRepository.RemoveAsync(id, userId))!;
        }

        public async Task<List<MenuCategory>?> RemoveAllMenuCategories(int menuId)
        {
            return (await ServiceRepository.RemoveAllMenuCategories(menuId))?.Select(Mapper.Map).ToList()!;
        }

        public async Task<MenuCategoryData?> FirstOrDefaultEditAsync(int id)
        {
            return await ServiceRepository.FirstOrDefaultEditAsync(id);
        }

        public async Task<DTO.Menu.MenuCategoryData?> FirstOrDefaultEditAsyncBLL(int id)
        {
            return await ServiceRepository.FirstOrDefaultEditAsyncBLL(id);
        }

        public int GetCategoryCountInMenu(int menuId)
        {
            return ServiceRepository.GetCategoryCountInMenu(menuId);
        }
        
        public async Task<bool> ChangeCategoryPosition(int id, bool moveUp)
        {
            return await ServiceRepository.ChangeCategoryPosition(id, moveUp);
        }

        public async Task<int> GetCategoryCompanyId(int id)
        {
            return await ServiceRepository.GetCategoryCompanyId(id);
        }

        public async Task<int> GetCategoryMenuId(int id)
        {
            return await ServiceRepository.GetCategoryMenuId(id);
        }

        public async Task<int> MoveCategoriesUpWhenDelete(int menuCategoryId)
        {
            return await ServiceRepository.MoveCategoriesUpWhenDelete(menuCategoryId);
        }

        public async Task<List<MenuCategoryOnlyName>> GetAllMenuCategoriesOnlyName(int menuId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllMenuCategoriesOnlyName(menuId, noTracking);
        }

        public async Task<List<DTO.Menu.MenuCategoryOnlyName>> GetAllMenuCategoriesOnlyNameBLL(int menuId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllMenuCategoriesOnlyNameBLL(menuId, noTracking);
        }
    }
}