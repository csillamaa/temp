﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Identity;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace BLL.App.Services
{
    public class AppUserService: BaseEntityService<IAppUnitOfWork, IAppUserRepository, BLLAppDTO.Identity.AppUser,
        DALAppDTO.Identity.AppUser>, IAppUserService
    {
        
        public AppUserService(IAppUnitOfWork serviceUow, IAppUserRepository serviceRepository, 
            IMapper mapper) : base(serviceUow, serviceRepository, new AppUserMapper(mapper))
        {
        }
        
        public async Task<IEnumerable<string>> GetUserRoles(int userId)
        {
            return await ServiceRepository.GetUserRoles(userId);
        }

        public async Task<IList<DALAppDTO.Identity.AppUserOnlyName>> GetAllUsers()
        {
            return await ServiceRepository.GetAllUsers();
        }

        public async Task<IEnumerable<DTO.Identity.AppUserOnlyName>> GetAllUsersBLL()
        {
            return await ServiceRepository.GetAllUsersBLL();
        }

        public async Task<bool> IsUserCompanyAdmin(int userId)
        {
            return await ServiceRepository.IsUserCompanyAdmin(userId);
        }

        public async Task<DALAppDTO.Identity.AppUserEdit?> FirstOrDefaultEditAsync(int userId, bool noTracking = true)
        {
            return await ServiceRepository.FirstOrDefaultEditAsync(userId, noTracking);
        }

        public async Task<DTO.Identity.AppUserEdit?> FirstOrDefaultEditAsyncBLL(int userId, bool noTracking = true)
        {
            var user = await ServiceRepository.FirstOrDefaultEditAsyncBLL(userId, noTracking);
            if (user == null)
            {
                return null;
            }

            user.Roles = await GetUserRoles(userId);
            user.WorkRestaurants = await ServiceUow.Restaurants.GetAllRestaurantsAppUserWorksInAsyncBLL(userId, noTracking);
            if (user.CompanyId == null)
            {
                if (user.Roles == null || !user.Roles.Contains("Admin"))
                {
                    user.AllCompanies = await ServiceUow.Companies.GetAllJustNameAsyncBLL();
                }
            }
            else
            {
                user.AddableRestaurants =
                    await ServiceUow.Restaurants.GetAllRestaurantsAddableToUserBLL(
                        user.CompanyId.Value, userId, noTracking);
            }
            
            return user;
        }

        public async Task<int?> AddUser(AppUserData appUser)
        {
            return await ServiceRepository.AddUser(appUser);
        }

        public async Task<int?> GetUserCompanyId(int userId)
        {
            return await ServiceRepository.GetUserCompanyId(userId);
        }

        public async Task<int?> AddUserBLL(BLL.App.DTO.Identity.AppUserData appUser)
        {
            if (appUser.CompanyId != null && !await ServiceUow.Companies.ExistsAsync(appUser.CompanyId.Value))
            {
                return null;
            }
            return await ServiceRepository.AddUserBLL(appUser);
        }

        public async Task<List<DALAppDTO.Identity.AppUserIndex>> GetAllIndexAsync(int userId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllIndexAsync(userId, noTracking);
        }

        public async Task<ICollection<DTO.Identity.AppUserIndex>> GetAllIndexAsyncBLL(int userId, bool noTracking = true)
        {
            var users = await ServiceRepository.GetAllIndexAsyncBLL(userId, noTracking);
            
            foreach (var user in users)
            {
                user.Roles = await GetUserRoles(user.Id);
            }
            
            return users;
        }

        public async Task<bool> RemoveAllUserOrders(int id, bool noTracking = true)
        {
            return await ServiceRepository.RemoveAllUserOrders(id, noTracking);
        }

        public async Task<int[]> GetAllCompanyUserIds(int companyId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllCompanyUserIds(companyId, noTracking);
        }

        public async Task<int> RemoveAllCompanyUsers(int[] userIds, bool noTracking = true)
        {
            return await ServiceRepository.RemoveAllCompanyUsers(userIds, noTracking);
        }

        public async Task<bool> AddUserFavoriteRestaurant(int restaurantId, int userId)
        {
            return await ServiceRepository.AddUserFavoriteRestaurant(restaurantId, userId);
        }

        public async Task<bool> RemoveUserFavoriteRestaurant(int restaurantId, int userId)
        {
            return await ServiceRepository.RemoveUserFavoriteRestaurant(restaurantId, userId);

        }
    }
}