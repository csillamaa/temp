﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Order;
using BLLAppDTOArchived = BLL.App.DTO.Archive;
using DALAppDTOArchived = DAL.App.DTO.Archive;

namespace BLL.App.Services
{
    public class ArchivedOrderService
        : BaseEntityService<IAppUnitOfWork, IArchivedOrderRepository, BLLAppDTOArchived.ArchivedOrder, 
            DALAppDTOArchived.ArchivedOrder>, IArchivedOrderService
    {
        public ArchivedOrderService(IAppUnitOfWork serviceUow, IArchivedOrderRepository serviceRepository, IMapper mapper) 
            : base(serviceUow, serviceRepository, new ArchivedOrderMapper(mapper))
        {
        }
        
        public async Task<ICollection<DALAppDTOArchived.ArchivedOrderWorker>?> GetAllRestaurantCompletedOrdersAsync(int restaurantId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllRestaurantCompletedOrdersAsync(restaurantId, noTracking);
        }

        public async Task<ICollection<BLLAppDTOArchived.ArchivedOrderWorker>?> GetAllRestaurantCompletedOrdersAsyncBLL(int restaurantId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllRestaurantCompletedOrdersAsyncBLL(restaurantId, noTracking);

        }

        public async Task<List<DALAppDTOArchived.ArchivedOrderClient>?> GetAllCompletedOrdersClientAsync(int userId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllCompletedOrdersClientAsync(userId, noTracking);
        }

        public async Task<List<BLLAppDTOArchived.ArchivedOrderClient>?> GetAllCompletedOrdersClientAsyncBLL(int userId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllCompletedOrdersClientAsyncBLL(userId, noTracking);
        }

        public async Task<bool> AddCommentToOrder(int orderId, string comment, bool isCustomerComment, bool noTracking = true)
        {
            return await ServiceRepository.AddCommentToOrder(orderId, comment, isCustomerComment, noTracking);
        }

        public async Task<bool> AddRatingToOrder(int orderId, int rating, bool noTracking = true)
        {
            return await ServiceRepository.AddRatingToOrder(orderId, rating, noTracking);

        }

        public async Task<int?> GetCompanyId(int orderId, bool noTracking = true)
        {
            return await ServiceRepository.GetCompanyId(orderId, noTracking);
        }

        public async Task<int?> GetRestaurantId(int orderId, bool noTracking = true)
        {
            return await ServiceRepository.GetRestaurantId(orderId, noTracking);
        }

        public async Task<BLLAppDTOArchived.ArchivedOrderWorkerView> GetWorkerOrderView(int restaurantId, bool noTracking = true)
        {
            var orders = await ServiceRepository.GetAllRestaurantCompletedOrdersAsyncBLL(restaurantId, noTracking);

            var orderView = new BLLAppDTOArchived.ArchivedOrderWorkerView
            {
                Orders = orders,
                RestaurantId = restaurantId,
                RestaurantName = (await ServiceUow.Restaurants.GetRestaurantName(restaurantId, noTracking))!
            };

            return orderView;
        }
    }
}