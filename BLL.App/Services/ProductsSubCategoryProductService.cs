﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Product;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace BLL.App.Services
{
    public class ProductsSubCategoryProductService : BaseEntityService<IAppUnitOfWork, IProductsSubCategoryProductRepository, 
        BLLAppDTO.Product.ProductsSubCategoryProduct, ProductsSubCategoryProduct>, IProductsSubCategoryProductService
    {
        public ProductsSubCategoryProductService(IAppUnitOfWork serviceUow, IProductsSubCategoryProductRepository serviceRepository, IMapper mapper)
            : base(serviceUow, serviceRepository, new ProductsSubCategoryProductMapper(mapper))
        {
        }
        public async Task<int> MoveSubCategoryProductsUpWhenDelete(int subCategoryProductId)
        {
            return await ServiceRepository.MoveSubCategoryProductsUpWhenDelete(subCategoryProductId);
        }

        public async Task<int> GetProductsSubCategoryProductCompanyId(int id)
        {
            return await ServiceRepository.GetProductsSubCategoryProductCompanyId(id);
        }

        public int GetProductSubCategoryProductCountInSubCategory(int subCategoryId)
        {
            return ServiceRepository.GetProductSubCategoryProductCountInSubCategory(subCategoryId);
        }

        public async Task<int> GetProductId(int id)
        {
            return await ServiceRepository.GetProductId(id);
        }

        public async Task<bool> ChangeSubCategoryProductPosition(int id, bool moveUp)
        {
            return await ServiceRepository.ChangeSubCategoryProductPosition(id, moveUp);
        }

        public async Task<ProductsSubCategoryProductData?> FirstOrDefaultEditAsync(int id, bool noTracking = true)
        {
            return await ServiceRepository.FirstOrDefaultEditAsync(id, noTracking);
        }

        public async Task<DTO.Product.ProductsSubCategoryProductData?> FirstOrDefaultEditAsyncBLL(int id, bool noTracking = true)
        {
            return await ServiceRepository.FirstOrDefaultEditAsyncBLL(id, noTracking);
        }

        public bool AreSubProductsFromTheProduct(int productId, int[] subCategoryProducts, bool noTracking = true)
        {
            return ServiceRepository.AreSubProductsFromTheProduct(productId, subCategoryProducts, noTracking);
        }

        public async Task<IEnumerable<ProductsSubCategoryProductOnlyPrice>> GetAllPricesById(int[] subProductIds, bool noTracking = true)
        {
            return await ServiceRepository.GetAllPricesById(subProductIds, noTracking);
        }

        public async Task<bool> IsThereTooManySubProductsFromACategory(int[] allSubProductIds, bool noTracking = true)
        {
            return await ServiceRepository.IsThereTooManySubProductsFromACategory(allSubProductIds, noTracking);
        }
    }
}