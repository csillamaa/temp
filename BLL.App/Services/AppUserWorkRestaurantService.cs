﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace BLL.App.Services
{
    public class AppUserWorkRestaurantService
        : BaseEntityService<IAppUnitOfWork, IAppUserWorkRestaurantRepository, BLLAppDTO.AppUserWorkRestaurant,
            DALAppDTO.AppUserWorkRestaurant>, IAppUserWorkRestaurantService
    {
        public AppUserWorkRestaurantService(IAppUnitOfWork serviceUow, IAppUserWorkRestaurantRepository serviceRepository, IMapper mapper) 
            : base(serviceUow, serviceRepository, new AppUserWorkRestaurantMapper(mapper))
        {
        }

        public async Task<IList<BLLAppDTO.AppUserWorkRestaurant>?> RemoveFromAllRestaurants(int id, bool noTracking = true)
        {
            var dalWorkRestaurants = await ServiceRepository.RemoveFromAllRestaurants(id, noTracking);
            var bllWorkRestaurants = 
                dalWorkRestaurants?.Select(AppUserWorkRestaurantMapper.MapToBLL);
            return bllWorkRestaurants?.ToList();
        }

        public int[]? GetAppUserWorkRestaurantIds(int userId, bool noTracking = true)
        {
            return ServiceRepository.GetAppUserWorkRestaurantIds(userId, noTracking);
        }
    }
}