﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace BLL.App.Services
{
    public class AppUserFavoriteRestaurantService
        : BaseEntityService<IAppUnitOfWork, IAppUserFavoriteRestaurantsRepository, BLLAppDTO.AppUserFavoriteRestaurant, DALAppDTO.AppUserFavoriteRestaurant>,
            IAppUserFavoriteRestaurantService
    {
        public AppUserFavoriteRestaurantService(IAppUnitOfWork serviceUow, 
            IAppUserFavoriteRestaurantsRepository serviceRepository,IMapper mapper) 
            : base(serviceUow, serviceRepository, new AppUserFavoriteRestaurantMapper(mapper))
        {
        }

        public async Task<int> RemoveAllUserFavoriteRestaurants(int userId, bool noTracking = true)
        {
            return await ServiceRepository.RemoveAllUserFavoriteRestaurants(userId, noTracking);
        }

        public async Task<int[]?> GetAllAppUserFavoriteRestaurants(int? userId, bool noTracking = true)
        {
            return await ServiceRepository.GetAllAppUserFavoriteRestaurants(userId, noTracking);
        }
    }
}