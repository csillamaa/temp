﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using BLLAppDTOOrder = BLL.App.DTO.Order;
using DALAppDTOOrder = DAL.App.DTO.Order;

namespace BLL.App.Services
{
    public class SubProductFromProductService : BaseEntityService<IAppUnitOfWork, ISubProductFromProductRepository, 
            BLLAppDTOOrder.SubProductFromProduct, DALAppDTOOrder.SubProductFromProduct>, ISubProductFromProductService
    {
        public SubProductFromProductService(IAppUnitOfWork serviceUow, ISubProductFromProductRepository serviceRepository, IMapper mapper)
            : base(serviceUow, serviceRepository, new SubProductFromProductMapper(mapper))
        {
        }
        
      
    }
}