﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO.Identity;
using BLL.App.DTO.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace BLL.App.Services
{
    public class CompanyService
        : BaseEntityService<IAppUnitOfWork, ICompanyRepository, BLLAppDTO.Company, DALAppDTO.Company>, ICompanyService
    {
        public CompanyService(IAppUnitOfWork serviceUow, ICompanyRepository serviceRepository, IMapper mapper)
            : base(serviceUow, serviceRepository, new CompanyMapper(mapper))
        {
        }

        public async Task<BLLAppDTO.Company?> FirstOrDefaultAsyncBLL(int id, int userId = default, bool noTracking = true)
        {
            var dalCompany = await ServiceRepository.FirstOrDefaultAsync(id, userId, noTracking);
            if (dalCompany == null)
            {
                return null;
            }

            var bllCompany = Mapper.Map(dalCompany)!;
            if (dalCompany.CompanyUsers == null)
            {
                return bllCompany;
            }

            bllCompany.CompanyUsers = new Collection<AppUserCompany>();
            foreach (var dalUser in dalCompany.CompanyUsers)
            {
                var bllUser = new BLLAppDTO.Identity.AppUserCompany
                {
                    Id = dalUser.Id,
                    FullName = dalUser.FullName,
                    IsCompanyAdmin = await ServiceUow.AppUsers.IsUserCompanyAdmin(dalUser.Id),
                    WorkRestaurantIds = ServiceUow.AppUserWorkRestaurants.GetAppUserWorkRestaurantIds(dalUser.Id)
                };
                bllCompany.CompanyUsers.Add(bllUser);
            }
            
            return bllCompany;
        }



        public async Task<IEnumerable<BLL.App.DTO.CompanyOnlyName>> GetAllJustNameAsyncBLL(bool noTracking = true)
        {
            return await ServiceRepository.GetAllJustNameAsyncBLL();
        }

        public async Task<BLLAppDTO.Company?> FirstOrDefaultDeleteAsync(int id, bool noTracking = true)
        {
            return Mapper.Map(await ServiceRepository.FirstOrDefaultDeleteAsync(id, noTracking));
        }

        public async Task<IEnumerable<DAL.App.DTO.CompanyOnlyName>> GetAllJustNameAsync(bool noTracking = true)
        {
            
            return await ServiceRepository.GetAllJustNameAsync(noTracking);
        }
        
    }
}