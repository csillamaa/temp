﻿using System;
using AutoMapper;
using BLL.App.DTO.Mappers;
using BLL.App.DTO.Menu;
using BLL.App.Services;
using BLL.Base;
using BLL.Base.Services;
using Contracts.BLL.App;
using Contracts.BLL.App.Services;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App;
using Contracts.DAL.Base.Repositories;
using DAL.App.DTO.Order;
using MenuCategory = DAL.App.DTO.Menu.MenuCategory;


namespace BLL.App
{
    public class AppBLL : BaseBLL<IAppUnitOfWork>, IAppBLL
    {
        protected IMapper Mapper;

        public AppBLL(IAppUnitOfWork uow, IMapper mapper) : base(uow)
        {
            Mapper = mapper;
        }

        public IBaseEntityService<BLL.App.DTO.AppUserOrder, DAL.App.DTO.AppUserOrder> AppUserOrders =>
            GetService<IBaseEntityService<BLL.App.DTO.AppUserOrder, DAL.App.DTO.AppUserOrder>>(()
                => new BaseEntityService<IAppUnitOfWork,
                    IBaseRepository<DAL.App.DTO.AppUserOrder>, BLL.App.DTO.AppUserOrder, DAL.App.DTO.AppUserOrder>(Uow,
                    Uow.AppUserOrders,
                    new BaseMapper<BLL.App.DTO.AppUserOrder, DAL.App.DTO.AppUserOrder>(Mapper)));

        public IBaseEntityService<BLL.App.DTO.Archive.ArchivedAppUserOrder, DAL.App.DTO.Archive.ArchivedAppUserOrder>
            ArchivedAppUserOrders =>
            GetService<IBaseEntityService<BLL.App.DTO.Archive.ArchivedAppUserOrder,
                DAL.App.DTO.Archive.ArchivedAppUserOrder>>(()
                => new BaseEntityService<IAppUnitOfWork,
                    IBaseRepository<DAL.App.DTO.Archive.ArchivedAppUserOrder>, BLL.App.DTO.Archive.ArchivedAppUserOrder,
                    DAL.App.DTO.Archive.ArchivedAppUserOrder>(Uow, Uow.ArchivedAppUserOrders,
                    new BaseMapper<BLL.App.DTO.Archive.ArchivedAppUserOrder, DAL.App.DTO.Archive.ArchivedAppUserOrder>(
                        Mapper)));
        public IAppUserFavoriteRestaurantService AppUserFavoriteRestaurants =>
            GetService<IAppUserFavoriteRestaurantService>(() =>
                new AppUserFavoriteRestaurantService(Uow, Uow.AppUserFavoriteRestaurants, Mapper));

        public IAppUserWorkRestaurantService AppUserWorkRestaurants =>
            GetService<IAppUserWorkRestaurantService>(() =>
                new AppUserWorkRestaurantService(Uow, Uow.AppUserWorkRestaurants, Mapper));

        public IAppUserService AppUsers =>
            GetService<IAppUserService>(() =>
                new AppUserService(Uow, Uow.AppUsers, Mapper));

        public IBaseEntityService<BLL.App.DTO.BankCard, DAL.App.DTO.BankCard> BankCards =>
            GetService<IBaseEntityService<BLL.App.DTO.BankCard, DAL.App.DTO.BankCard>>(()
                => new BaseEntityService<IAppUnitOfWork,
                    IBaseRepository<DAL.App.DTO.BankCard>, BLL.App.DTO.BankCard, DAL.App.DTO.BankCard>(Uow,
                    Uow.BankCards,
                    new BaseMapper<BLL.App.DTO.BankCard, DAL.App.DTO.BankCard>(Mapper)));

        public IArchivedProductInOrderService ArchivedProductsInOrder =>
            GetService<IArchivedProductInOrderService>(() =>
                new ArchivedProductInOrderService(Uow, Uow.ArchivedProductsInOrder, Mapper));
        
        public IArchivedSubProductFromProductService ArchivedSubProductsFromProduct =>
            GetService<IArchivedSubProductFromProductService>(() =>
                new ArchivedSubProductFromProductService(Uow, Uow.ArchivedSubProductsFromProduct, Mapper));

        public IMenuService Menus =>
            GetService<IMenuService>(() => new MenuService(Uow, Uow.Menus, Mapper));
        
        public IMenuCategoryService MenuCategories =>
            GetService<IMenuCategoryService>(() => new MenuCategoryService(Uow, Uow.MenuCategories, Mapper));

        public IArchivedOrderService ArchivedOrders =>
            GetService<IArchivedOrderService>(() => new ArchivedOrderService(Uow, Uow.ArchivedOrders, Mapper));

        public ICompanyService Companies =>
            GetService<ICompanyService>(() => new CompanyService(Uow, Uow.Companies, Mapper));

        public IMenuInRestaurantService MenusInRestaurant =>
            GetService<IMenuInRestaurantService>(() => new MenuInRestaurantService(Uow, Uow.MenusInRestaurant, Mapper));

        public IProductService Products =>
            GetService<IProductService>(() => new ProductService(Uow, Uow.Products, Mapper));

        public IProductInOrderService ProductsInOrder =>
            GetService<IProductInOrderService>(() => new ProductInOrderService(Uow, Uow.ProductsInOrder, Mapper));

        public IProductsSubCategoryService ProductsSubCategories =>
            GetService<IProductsSubCategoryService>(() => new ProductsSubCategoryService(Uow, Uow.ProductsSubCategories, Mapper));

        public IOrderService Orders =>
            GetService<IOrderService>(() => new OrderService(Uow, Uow.Orders, Mapper));

        public IRestaurantService Restaurants =>
            GetService<IRestaurantService>(() => new RestaurantService(Uow, Uow.Restaurants, Mapper));

        public IProductsSubCategoryProductService ProductsSubCategoryProducts =>
            GetService<IProductsSubCategoryProductService>(() => new ProductsSubCategoryProductService(Uow, Uow.ProductsSubCategoryProducts, Mapper));

        public ISubProductFromProductService SubProductsFromProduct =>
            GetService<ISubProductFromProductService>(() => new SubProductFromProductService(Uow, Uow.SubProductsFromProduct, Mapper));

        public ITimeService Times =>
            GetService<ITimeService>(() => new TimeService(Uow, Uow.Times, Mapper));
    }
}