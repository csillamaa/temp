﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.Domain.Base;

namespace Contracts.DAL.Base.Repositories
{
    public interface IBaseRepository<TEntity> : IBaseRepository<TEntity, int>
        where TEntity : class, IDomainEntity
    {
    }

    public interface IBaseRepository<TEntity, TKey>
    where TEntity : class, IDomainEntity<TKey>
    where TKey: IEquatable<TKey>
    {
        Task<IEnumerable<TEntity>> GetAllAsync(TKey? userId = default, bool noTracking = true);

        Task<TEntity?> FirstOrDefaultAsync(TKey id, TKey? userId = default, bool noTracking = true);
        
        Task<bool> ExistsAsync(TKey id, TKey? userId = default);
        Task<TEntity> RemoveAsync(TKey id,TKey? userId);
        
        TEntity Add(TEntity entity);
        TEntity Update(TEntity entity);
        TEntity Remove(TEntity entity, TKey? userId = default);
        
        TEntity GetUpdatedEntityAfterSaveChanges(TEntity entity);
    }
}