﻿using System;

namespace BLL.App.DTO
{
    public class TimeCurrentDay
    {
        public DateTime StartingTime { get; set; }

        public DateTime EndingTime { get; set; }
    }
}