﻿using System;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Identity;
using Contracts.Domain.Base;
using Domain.Base;

namespace BLL.App.DTO
{
    public class BankCard: DomainEntity, IDomainAppUserId, IDomainAppUser<AppUser>
    {
        public string Number { get; set; } = default!;

        public DateTime ExpiryDate { get; set; }

        [StringLength(128,MinimumLength = 1)]
        public string CardOwnerName { get; set; } = default!;

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; } = default!;
    }
}