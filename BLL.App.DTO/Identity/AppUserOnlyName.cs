﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Identity
{
    public class AppUserOnlyName: DomainEntity
    {
        [MaxLength(140)]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Name")]
        public string FullName { get; set; } = default!;
    }
}