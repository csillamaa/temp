﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Restaurant;
using Domain.Base;

namespace BLL.App.DTO.Identity
{
    public class AppUserEdit : AppUserData
    {
        // [StringLength(65, MinimumLength = 1)]
        // public string FirstName { get; set; } = default!;
        //
        // [StringLength(75, MinimumLength = 1)]
        // public string LastName { get; set; } = default!;
        //
        // [MaxLength(140)]
        // public string Email { get; set; } = default!;
        //
        // [MaxLength(100)]
        // public string? Address { get; set; }
        // [MaxLength(20)]
        // public string PhoneNumber { get; set; } = default!;
        //
        // public int? CompanyId { get; set; }
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "CompanyName")]
        public string? CompanyName { get; set; }

        public DateTimeOffset? LockoutEnd { get; set; }
        
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.AppUser), Name = "WorkRestaurants")]
        public ICollection<RestaurantOnlyName>? WorkRestaurants { get; set; }
        public ICollection<RestaurantOnlyName>? AddableRestaurants { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.AppRole), Name = "Roles")]
        public IEnumerable<string>? Roles { get; set; }

        public IEnumerable<CompanyOnlyName>? AllCompanies { get; set; }
        
        // public ICollection<BankCard>? Cards { get; set; }

        // public ICollection<AppUserOrder>? Orders { get; set; }
        //
        // public ICollection<ArchivedAppUserOrder>? PastOrders { get; set; }

    }
}