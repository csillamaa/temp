﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Restaurant;
using Domain.Base;

namespace BLL.App.DTO.Identity
{
    public class AppUserIndex: DomainEntity
    {
       
        [MaxLength(140)]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Name")]
        public string FullName { get; set; } = default!;

        [MaxLength(140)]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Email")]
        public string Email { get; set; } = default!;

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "CompanyName")]
        public string? CompanyName { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.AppUser), Name = "WorkRestaurants")]
        public IEnumerable<RestaurantOnlyName>? WorkRestaurants { get; set; }
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.AppRole), Name = "Roles")]
        public IEnumerable<string>? Roles { get; set; }
    }
}