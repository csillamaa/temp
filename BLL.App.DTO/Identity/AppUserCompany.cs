﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Identity
{
    public class AppUserCompany: DomainEntity
    {
        [MinLength(1, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MinLength")]
        [MaxLength(140, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Name")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public string FullName { get; set; } = default!;
        
        public bool IsCompanyAdmin { get; set; }

        public int[]? WorkRestaurantIds { get; set; }
    }
}