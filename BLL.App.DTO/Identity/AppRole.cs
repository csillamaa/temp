﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Identity
{
    public class AppRole: DomainEntity
    {
        [MinLength(2, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MinLength")]
        [MaxLength(32, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Name")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
         public string Name { get; set; } = default!;

    }
}