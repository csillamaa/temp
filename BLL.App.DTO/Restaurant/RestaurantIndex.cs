﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Restaurant
{
    public class RestaurantIndex: DomainEntity
    {
        [MaxLength(60)]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Name")]
        public string Name { get; set; } = default!;
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Restaurant), Name = "isPublic")]
        public bool IsPublic { get; set; }
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "DateJoined")]
        public DateTime DateJoined { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "CompanyName")]
        public string CompanyName { get; set; } = default!;

        [MaxLength(60)]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Address")]
        public string Address { get; set; } = default!;

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Restaurant), Name = "WorkerCount")]
        public int WorkerCount { get; set; }
    }
}