﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Restaurant
{
    public class RestaurantData: DomainEntity
    {
        [MinLength(1, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MinLength")]
        [MaxLength(60, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Name")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public string Name { get; set; } = default!;
        
        [MaxLength(60)]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Picture")]
        public string? Picture { get; set; } 
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Restaurant), Name = "isPublic")]
        public bool IsPublic { get; set; }

        public DateTime DateJoined { get; set; }
        
        [MinLength(1, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MinLength")]
        [MaxLength(700, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Description")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public string Description { get; set; } = default!;

        public int CompanyId { get; set; }

        
        [MinLength(1, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MinLength")]
        [MaxLength(60, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Address")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public string Address { get; set; } = default!;

        [MinLength(5, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MinLength")]
        [MaxLength(20, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "PhoneNumber")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public string PhoneNumber { get; set; } = default!;

        [MinLength(2, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MinLength")]
        [MaxLength(60, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Email")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public string Email { get; set; } = default!;

        // public ICollection<Time>? OpeningTimes { get; set; }
    }
}