﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Menu;
using Domain.Base;
using Microsoft.AspNetCore.Mvc.Rendering;


namespace BLL.App.DTO.Restaurant
{
    public class RestaurantEdit: RestaurantData
    {
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "CompanyName")]
        public string CompanyName { get; set; } = default!;

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Restaurant), Name = "OpeningTimes")]
        public ICollection<Time>? OpeningTimes { get; set; }
        
        public string? ErrorMessage { get; set; }
        
        public ICollection<BLL.App.DTO.Identity.AppUserOnlyName>? Workers { get; set; }

        public SelectList? AllCompanies { get; set; }
        
        public ICollection<MenuOnlyName>? RestaurantMenus { get; set; }

        public List<MenuOnlyName>? CompanyMenus { get; set; }
    }
}