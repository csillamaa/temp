﻿using System.Collections.Generic;

namespace BLL.App.DTO.Restaurant
{
    public class RestaurantClientAllLists
    {
        public List<RestaurantClientAll> AllRestaurants { get; set; } = default!;
        
        public List<RestaurantClientAll>? UserFavoriteRestaurants { get; set; }
    }
}