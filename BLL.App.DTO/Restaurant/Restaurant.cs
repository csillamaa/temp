﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata.Ecma335;
using BLL.App.DTO.Archive;
using BLL.App.DTO.Menu;
using Domain.Base;

namespace BLL.App.DTO.Restaurant
{
    public class Restaurant : RestaurantData
    {
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "CompanyName")]
        
        public string? CompanyName { get; set; }

        public ICollection<Time>? OpeningTimes { get; set; }
        
        public ICollection<Menu.Menu>? Menus { get; set; }

        public ICollection<AppUserWorkRestaurant>? Workers { get; set; }
        // public ICollection<Order>? Orders { get; set; }
        //
        // public ICollection<ArchivedOrder>? CompletedOrders { get; set; }
    }
}