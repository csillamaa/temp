﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Restaurant
{
    public class RestaurantOnlyName: DomainEntity
    {
        [MaxLength(60)]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Name")]
        public string Name { get; set; } = default!;
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Restaurant), Name = "isPublic")]
        public bool IsPublic { get; set; }
    }
}