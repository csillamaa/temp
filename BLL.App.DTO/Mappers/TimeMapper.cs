﻿using AutoMapper;

namespace BLL.App.DTO.Mappers
{
    public class TimeMapper: BaseMapper<BLL.App.DTO.Time, DAL.App.DTO.Time>
    {
        public TimeMapper(IMapper mapper) : base(mapper)
        {
        }

        public static BLL.App.DTO.Time MapToBLL(DAL.App.DTO.Time time)
        {
            return new()
            {
                DayOfWeek = time.DayOfWeek,
                EndingTime = time.EndingTime,
                StartingTime = time.StartingTime
            };
        }
        public static DAL.App.DTO.Time MapToDal(Time time)
        {
            return new()
            {
                DayOfWeek = time.DayOfWeek,
                EndingTime = time.EndingTime,
                StartingTime = time.StartingTime
            };
        }
        public static TimeCurrentDay MapToBllCurrentDay(DAL.App.DTO.Time time)
        {
            return new()
            {
                EndingTime = time.EndingTime,
                StartingTime = time.StartingTime
            };
        }
    }
}