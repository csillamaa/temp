﻿using System.Linq;
using AutoMapper;
using BLLAppDTOProduct = BLL.App.DTO.Product;
using DALAppDTOProduct = DAL.App.DTO.Product;

namespace BLL.App.DTO.Mappers
{
    public class ProductsSubCategoryMapper: BaseMapper<BLLAppDTOProduct.ProductsSubCategory, DAL.App.DTO.Product.ProductsSubCategory>
    {
        public ProductsSubCategoryMapper(IMapper mapper) : base(mapper)
        {
        }

        public new static BLLAppDTOProduct.ProductsSubCategory Map(DALAppDTOProduct.ProductsSubCategory productsSubCategory)
        {
            return new()
            {
                Id = productsSubCategory.Id,
                Name = productsSubCategory.Name,
                Position = productsSubCategory.Position,
                MandatoryItemCount = productsSubCategory.MandatoryItemCount,
                MaxItemCount = productsSubCategory.MaxItemCount,
                SubCategoryProducts = productsSubCategory.SubCategoryProducts?
                    .Select(ProductsSubCategoryProductMapper.Map)
                    .OrderBy(p => p.Position)
                    .ToList()
            };
        }
        
        public static BLLAppDTOProduct.ProductsSubCategory MapToRegBLL(BLLAppDTOProduct.ProductsSubCategoryData productsSubCategoryData)
        {
            return new()
            {
                Id = productsSubCategoryData.Id,
                Name = productsSubCategoryData.Name,
                ProductId = productsSubCategoryData.ProductId,
                MandatoryItemCount = productsSubCategoryData.MandatoryItemCount,
                MaxItemCount = productsSubCategoryData.MaxItemCount,
                
            };
        }
        
        public static BLLAppDTOProduct.ProductsSubCategoryData? Map(DALAppDTOProduct.ProductsSubCategoryData? productsSubCategoryData)
        {
            if (productsSubCategoryData == null)
            {
                return null;
            }
            return new()
            {
                Id = productsSubCategoryData.Id,
                Name = productsSubCategoryData.Name,
                ProductId = productsSubCategoryData.ProductId,
                MandatoryItemCount = productsSubCategoryData.MandatoryItemCount,
                MaxItemCount = productsSubCategoryData.MaxItemCount,
            };
        }
        
        public static BLLAppDTOProduct.ProductsSubCategoryClient MapToBLLClient(DALAppDTOProduct.ProductsSubCategoryClient subCategory)
        {

            return new()
            {
                Id = subCategory.Id,
                Name = subCategory.Name,
                MandatoryItemCount = subCategory.MandatoryItemCount,
                MaxItemCount = subCategory.MaxItemCount,
                SubCategoryProducts = subCategory.SubCategoryProducts?.Select(ProductsSubCategoryProductMapper.MapToBLLClient).ToList()
            };
        }
    }
}