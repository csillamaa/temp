﻿using System.Linq;
using AutoMapper;
using DALAppDTOMenu = DAL.App.DTO.Menu;
using BLLAppDTOMenu = BLL.App.DTO.Menu;

namespace BLL.App.DTO.Mappers
{
    public class MenuMapper : BaseMapper<BLLAppDTOMenu.Menu, DALAppDTOMenu.Menu>
    {
        public MenuMapper(IMapper mapper) : base(mapper)
        {
        }

        public static BLLAppDTOMenu.MenuIndex Map(DALAppDTOMenu.MenuIndex menu)
        {
            return new()
            {
                
                Id = menu.Id,
                CompanyName = menu.CompanyName,
                Name = menu.Name,
                RestaurantsWithTheMenu = menu.RestaurantsWithTheMenu?.Select(RestaurantMapper.MapToBLL).ToList()
            };
        }
        
        public static BLLAppDTOMenu.MenuEdit Map(DALAppDTOMenu.MenuEdit menu)
        {
            return new()
            {
                Id = menu.Id,
                CompanyName = menu.CompanyName,
                Name = menu.Name,
                CompanyId = menu.CompanyId,
                MenuCategories = menu.MenuCategories?
                    .Select(MenuCategoryMapper.MapToOnlyName)
                    .OrderBy(c => c.Position)
                    .ToList(),
                CurrentCategory = MenuCategoryMapper.Map(menu.CurrentCategory)
            };
        }
        
        public static BLLAppDTOMenu.MenuData Map(DALAppDTOMenu.MenuData menu)
        {
            return new()
            {
                
                Id = menu.Id,
                Name = menu.Name,
                CompanyId = menu.CompanyId
            };
        }
        
        public static DALAppDTOMenu.MenuData Map(BLLAppDTOMenu.MenuData menu)
        {
            return new()
            {
                Id = menu.Id,
                Name = menu.Name,
                CompanyId = menu.CompanyId
            };
        }
        public static BLLAppDTOMenu.MenuOnlyName Map(DALAppDTOMenu.MenuOnlyName menu)
        {
            return new()
            {
                Id = menu.Id,
                Name = menu.Name,
            };
        }
        
        public static BLLAppDTOMenu.Menu MapToRegBLL(BLLAppDTOMenu.MenuData menu)
        {
            return new()
            {
                Id = menu.Id,
                Name = menu.Name,
                CompanyId = menu.CompanyId
            };
        }
        
        public static BLLAppDTOMenu.Menu MapToBLLMenu(DALAppDTOMenu.Menu menu)
        {
            return new()
            {
                Id = menu.Id,
                Name = menu.Name,
                CompanyId = menu.CompanyId,
                MenuCategories = menu.MenuCategories?
                    .Select(MenuCategoryMapper.MapToOnlyName)
                    .OrderBy(c => c.Position)
                    .ToList(),
                CurrentCategory = MenuCategoryMapper.Map(menu.CurrentCategory)
            };
        }
    }
}