﻿using AutoMapper;

namespace BLL.App.DTO.Mappers
{
    public class AppRoleMapper: BaseMapper<BLL.App.DTO.Identity.AppRole, Domain.App.Identity.AppRole>
    {
        public AppRoleMapper(IMapper mapper) : base(mapper)
        {
        }
    }
}