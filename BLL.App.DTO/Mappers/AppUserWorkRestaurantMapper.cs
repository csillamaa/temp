﻿using AutoMapper;

namespace BLL.App.DTO.Mappers
{
    public class AppUserWorkRestaurantMapper :  BaseMapper<BLL.App.DTO.AppUserWorkRestaurant, DAL.App.DTO.AppUserWorkRestaurant>
    {
        public AppUserWorkRestaurantMapper(IMapper mapper) : base(mapper)
        {
        }

        public static BLL.App.DTO.AppUserWorkRestaurant MapToBLL(
            DAL.App.DTO.AppUserWorkRestaurant appUserWorkRestaurant)
        {
            return new()
            {
                Id = appUserWorkRestaurant.Id,
                AppUserId = appUserWorkRestaurant.AppUserId,
                RestaurantId = appUserWorkRestaurant.RestaurantId
            };
        }

       
    }
}