﻿using System.Linq;
using AutoMapper;
using DALAppDTOMenu = DAL.App.DTO.Menu;
using BLLAppDTOMenu = BLL.App.DTO.Menu;

namespace BLL.App.DTO.Mappers
{
    public class MenuCategoryMapper: BaseMapper<BLLAppDTOMenu.MenuCategory, DALAppDTOMenu.MenuCategory>
    {
        public MenuCategoryMapper(IMapper mapper) : base(mapper)
        {
        }
        public new static BLLAppDTOMenu.MenuCategory? Map(DALAppDTOMenu.MenuCategory? category)
        {
            if (category == null)
            {
                return null;
            }
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                Position = category.Position,
                Products = category.Products?.Select(ProductMapper.MapToBLLData).ToList()
            };
        }
        
        public static BLLAppDTOMenu.MenuCategoryOnlyName MapToOnlyName(DALAppDTOMenu.MenuCategoryOnlyName category)
        {
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                Position = category.Position
            };
        }
        
        public static BLLAppDTOMenu.MenuCategoryData? Map(DALAppDTOMenu.MenuCategoryData? category)
        {
            if (category == null)
            {
                return null;
            }
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                MenuId = category.MenuId
            };
        }
        
        public static DALAppDTOMenu.MenuCategoryData Map(BLLAppDTOMenu.MenuCategoryData category)
        {
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                MenuId = category.MenuId
            };
        }
        
        public static BLLAppDTOMenu.MenuCategory MapToRegBLL(BLLAppDTOMenu.MenuCategoryData category)
        {
            return new()
            {
                Id = category.Id,
                Name = category.Name,
                MenuId = category.MenuId
            };
        }
    }
}