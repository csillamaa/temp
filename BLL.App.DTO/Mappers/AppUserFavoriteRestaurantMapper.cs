﻿using AutoMapper;

namespace BLL.App.DTO.Mappers
{
    public class AppUserFavoriteRestaurantMapper: BaseMapper<BLL.App.DTO.AppUserFavoriteRestaurant, DAL.App.DTO.AppUserFavoriteRestaurant>
    {
        public AppUserFavoriteRestaurantMapper(IMapper mapper) : base(mapper)
        {
        }
    }
}