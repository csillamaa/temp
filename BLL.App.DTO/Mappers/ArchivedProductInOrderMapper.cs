﻿using System.Linq;
using AutoMapper;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;

namespace BLL.App.DTO.Mappers
{
    public class ArchivedProductInOrderMapper : BaseMapper<BLLAppDTOOrderArchived.ArchivedProductInOrder, DALAppDTOOrderArchived.ArchivedProductInOrder>
    {
        public ArchivedProductInOrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static BLLAppDTOOrderArchived.ArchivedProductInOrderClient MapToBLLClient(DALAppDTOOrderArchived.ArchivedProductInOrderClient productInOrder)
        {
            return new()
            {
                Id = productInOrder.Id,
                ProductId = productInOrder.ProductId,
                Amount = productInOrder.Amount,
                OneProductPrice = productInOrder.OneProductPrice,
                Name = productInOrder.Name,
                SubProducts = productInOrder.SubProducts?.Select(ArchivedSubProductFromProductMapper.MapToBLLClient).ToList()
            };
        }
    }
}