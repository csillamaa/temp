﻿using AutoMapper;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;

namespace BLL.App.DTO.Mappers
{
    public class ArchivedSubProductFromProductMapper : BaseMapper<BLLAppDTOOrderArchived.ArchivedSubProductFromProduct, DALAppDTOOrderArchived.ArchivedSubProductFromProduct>
    {
        public ArchivedSubProductFromProductMapper(IMapper mapper) : base(mapper)
        {
        }

        public static BLLAppDTOOrderArchived.ArchivedSubProductFromProductClient MapToBLLClient(
            DALAppDTOOrderArchived.ArchivedSubProductFromProductClient subProduct)
        {
            return new()
            {
                Name = subProduct.Name,
                Price = subProduct.Price,
                SubCategoryName = subProduct.SubCategoryName
            };
        }
    }
}