﻿using System.Linq;
using AutoMapper;
using DALAppDTOOrder = DAL.App.DTO.Order;
using BLLAppDTOOrder = BLL.App.DTO.Order;

namespace BLL.App.DTO.Mappers
{
    public class OrderMapper : BaseMapper<BLLAppDTOOrder.Order, DALAppDTOOrder.Order>
    {
        public OrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static BLLAppDTOOrder.OrderClient? MapToBLLOrderClient(DALAppDTOOrder.OrderClient? order)
        {
            if (order == null)
            {
                return null;
            }

            return new()
            {
                Id = order.Id,
                SubmitTime = order.SubmitTime,
                CompletedBy = order.CompletedBy,
                RestaurantId = order.RestaurantId,
                RestaurantName = order.RestaurantName,
                CustomerComment = order.CustomerComment,
                IsSelfPickup = order.IsSelfPickup,
                IsReady = order.IsReady,
                AmountPaid = order.AmountPaid,
                FoodCost = order.FoodCost,
                ProductsInOrder = order.ProductsInOrder.Select(ProductInOrderMapper.MapToBLLClient).ToList()
            };
        }
        public static BLLAppDTOOrder.OrderWorker MapToBLLOrderWorker(DALAppDTOOrder.OrderWorker order)
        {
            return new()
            {
                Id = order.Id,
                SubmitTime = order.SubmitTime,
                CompletedBy = order.CompletedBy,
                InitialCompleteTimeEstimate = order.InitialCompleteTimeEstimate,
                CustomerComment = order.CustomerComment,
                AmountPaid = order.AmountPaid,
                FoodCost = order.FoodCost,
                // IsReady = order.IsReady,
                ProductsInOrder = order.ProductsInOrder.Select(ProductInOrderMapper.MapToBLLClient).ToList(),
                OrderAppUsers = order.OrderAppUsers.Select(AppUserMapper.MapToBLL).ToList()
            };
        }
    }
}