﻿using AutoMapper;
using BLLAppDTOProduct = BLL.App.DTO.Product;
using DALAppDTOProduct = DAL.App.DTO.Product;

namespace BLL.App.DTO.Mappers
{
    public class ProductsSubCategoryProductMapper: BaseMapper<BLLAppDTOProduct.ProductsSubCategoryProduct, DALAppDTOProduct.ProductsSubCategoryProduct>
    {
        public ProductsSubCategoryProductMapper(IMapper mapper) : base(mapper)
        {
        }

        public new static BLLAppDTOProduct.ProductsSubCategoryProduct Map(DALAppDTOProduct.ProductsSubCategoryProduct subProduct)
        {
            return new()
            {
                Id = subProduct.Id,
                Name = subProduct.Name,
                Price = subProduct.Price,
                ProductsSubCategoryId = subProduct.ProductsSubCategoryId,
                Position = subProduct.Position,
                Description = subProduct.Description,
                IsSelectedOnDefault = subProduct.IsSelectedOnDefault
            };
        }

        public static BLLAppDTOProduct.ProductsSubCategoryProduct MapToRegBLL(BLLAppDTOProduct.ProductsSubCategoryProductData subProduct)
        {
            return new()
            {
                Id = subProduct.Id,
                Name = subProduct.Name,
                Price = subProduct.Price,
                ProductsSubCategoryId = subProduct.ProductsSubCategoryId,
                Description = subProduct.Description,
                IsSelectedOnDefault = subProduct.IsSelectedOnDefault,
            };
        }
        
        public static BLLAppDTOProduct.ProductsSubCategoryProductData? Map(DALAppDTOProduct.ProductsSubCategoryProductData? subProduct)
        {
            if (subProduct == null)
            {
                return null;
            }
            return new()
            {
                Id = subProduct.Id,
                Name = subProduct.Name,
                Price = subProduct.Price,
                ProductsSubCategoryId = subProduct.ProductsSubCategoryId,
                Description = subProduct.Description,
                IsSelectedOnDefault = subProduct.IsSelectedOnDefault,
            };
        }
        
        public static BLLAppDTOProduct.ProductsSubCategoryProductClient MapToBLLClient(DALAppDTOProduct.ProductsSubCategoryProductClient subProduct)
        {
            return new()
            {
                Id = subProduct.Id,
                Price = subProduct.Price,
                Name = subProduct.Name,
                Description = subProduct.Description,
                IsSelectedOnDefault = subProduct.IsSelectedOnDefault,
            };
        }
    }
}