﻿using System.Linq;
using AutoMapper;
using DALAppDTOMenu = DAL.App.DTO.Menu;
using BLLAppDTOMenu = BLL.App.DTO.Menu;

namespace BLL.App.DTO.Mappers
{
    public class MenuInRestaurantMapper : BaseMapper<BLLAppDTOMenu.MenuInRestaurant, DALAppDTOMenu.MenuInRestaurant>
    {
        public MenuInRestaurantMapper(IMapper mapper) : base(mapper)
        {
        }
        
    }
}