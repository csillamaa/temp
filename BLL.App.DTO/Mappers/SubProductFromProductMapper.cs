﻿using AutoMapper;
using DALAppDTOOrder = DAL.App.DTO.Order;
using BLLAppDTOOrder = BLL.App.DTO.Order;

namespace BLL.App.DTO.Mappers
{
    public class SubProductFromProductMapper : BaseMapper<BLLAppDTOOrder.SubProductFromProduct, DALAppDTOOrder.SubProductFromProduct>
    {
        public SubProductFromProductMapper(IMapper mapper) : base(mapper)
        {
        }

        public static BLLAppDTOOrder.SubProductFromProductClient MapToBLLClient(
            DALAppDTOOrder.SubProductFromProductClient subProduct)
        {
            return new()
            {
                Name = subProduct.Name,
                Price = subProduct.Price,
                SubCategoryName = subProduct.SubCategoryName
            };
        }
    }
}