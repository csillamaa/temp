﻿using System.Linq;
using AutoMapper;
using BLLAppDTORestaurant = BLL.App.DTO.Restaurant;
using DALAppDTORestaurant = DAL.App.DTO.Restaurant;

namespace BLL.App.DTO.Mappers
{
    public class RestaurantMapper: BaseMapper<BLLAppDTORestaurant.Restaurant, DALAppDTORestaurant.Restaurant>
    {
        public RestaurantMapper(IMapper mapper) : base(mapper)
        {
        }
        
        public static BLLAppDTORestaurant.RestaurantEdit MapToBLL(DALAppDTORestaurant.RestaurantEdit restaurant)
        {
            return new ()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                CompanyName = restaurant.CompanyName,
                CompanyId = restaurant.CompanyId,
                Address = restaurant.Address,
                DateJoined = restaurant.DateJoined,
                PhoneNumber = restaurant.PhoneNumber,
                Picture = restaurant.Picture,
                Description = restaurant.Description,
                Email = restaurant.Email,
                IsPublic = restaurant.IsPublic,
                Workers = restaurant.Workers?.Select(AppUserMapper.MapToBLL).ToList(),
                OpeningTimes = restaurant.OpeningTimes?.Select(TimeMapper.MapToBLL).ToList(),
                RestaurantMenus = restaurant.RestaurantMenus?.Select(MenuMapper.Map).ToList()
            };
        }
        
        
        // TODO: Do i need this?
        public static DALAppDTORestaurant.RestaurantEdit MapToDal(BLLAppDTORestaurant.RestaurantEdit restaurant)
        {
            return new ()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                CompanyName = restaurant.CompanyName,
                CompanyId = restaurant.CompanyId,
                Address = restaurant.Address,
                DateJoined = restaurant.DateJoined,
                PhoneNumber = restaurant.PhoneNumber,
                Picture = restaurant.Picture,
                Description = restaurant.Description,
                Email = restaurant.Email,
                IsPublic = restaurant.IsPublic,
                OpeningTimes = restaurant.OpeningTimes?.Select(TimeMapper.MapToDal).ToList()
                
            };
        }
        
        public static BLLAppDTORestaurant.RestaurantIndex MapToDal(DALAppDTORestaurant.RestaurantIndex restaurant)
        {
            return new ()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                CompanyName = restaurant.CompanyName,
                Address = restaurant.Address,
                DateJoined = restaurant.DateJoined,
                IsPublic = restaurant.IsPublic,
                WorkerCount = restaurant.WorkerCount
            };
        }
        
        public static BLLAppDTORestaurant.RestaurantOnlyName MapToBLL(DALAppDTORestaurant.RestaurantOnlyName restaurant)
        {
            return new ()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                IsPublic = restaurant.IsPublic,
            };
        }
        
        public static BLLAppDTORestaurant.Restaurant MapToBLLRegularFromBLLData(BLLAppDTORestaurant.RestaurantData restaurant)
        {
            return new ()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                CompanyId = restaurant.CompanyId,
                Address = restaurant.Address,
                DateJoined = restaurant.DateJoined,
                PhoneNumber = restaurant.PhoneNumber,
                Picture = restaurant.Picture,
                Description = restaurant.Description,
                Email = restaurant.Email,
                IsPublic = restaurant.IsPublic,
            };
        }
        public static BLLAppDTORestaurant.RestaurantEdit MapToBLLEditFromBLLData(BLLAppDTORestaurant.RestaurantData restaurant)
        {
            return new ()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                CompanyId = restaurant.CompanyId,
                Address = restaurant.Address,
                DateJoined = restaurant.DateJoined,
                PhoneNumber = restaurant.PhoneNumber,
                Picture = restaurant.Picture,
                Description = restaurant.Description,
                Email = restaurant.Email,
                IsPublic = restaurant.IsPublic,
            };
        }
        
        // new---
        public static BLLAppDTORestaurant.RestaurantClientAll MapToBLLClientAll(DALAppDTORestaurant.RestaurantClientAll restaurant)
        {
            return new ()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                Picture = restaurant.Picture,
                OpeningTimes = TimeMapper.MapToBllCurrentDay(restaurant.OpeningTimes!)
            };
        }
        
        
        public static BLLAppDTORestaurant.RestaurantClient? MapToBLLClient(DALAppDTORestaurant.RestaurantClient? restaurant)
        {
            if (restaurant == null)
            {
                return null;
            }

            return new()
            {
                Id = restaurant.Id,
                Name = restaurant.Name,
                Address = restaurant.Address,
                PhoneNumber = restaurant.PhoneNumber,
                Picture = restaurant.Picture,
                Description = restaurant.Description,
                Email = restaurant.Email,
                OpeningTimes = restaurant.OpeningTimes?.Select(TimeMapper.MapToBLL).ToList(),
                Menus = restaurant.Menus?.Select(MenuMapper.MapToBLLMenu).ToList()
            };
        }
    }
}