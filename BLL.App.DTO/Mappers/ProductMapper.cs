﻿using System.Linq;
using AutoMapper;
using BLLAppDTOProduct = BLL.App.DTO.Product;
using DALAppDTOProduct = DAL.App.DTO.Product;

namespace BLL.App.DTO.Mappers
{
    public class ProductMapper : BaseMapper<BLLAppDTOProduct.Product, DAL.App.DTO.Product.Product>
    {
        public ProductMapper(IMapper mapper) : base(mapper)
        {
        }
        public new static BLLAppDTOProduct.Product Map(DALAppDTOProduct.Product product)
        {
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Price = product.Price,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                Position = product.Position
            };
        }
        
        public static BLLAppDTOProduct.Product MapToRegBLL(BLLAppDTOProduct.ProductData product)
        {
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Price = product.Price,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                Picture = product.Picture
            };
        }

        public static BLLAppDTOProduct.ProductCreate MapToCreate(BLLAppDTOProduct.ProductData product)
        {
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Price = product.Price,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                Picture = product.Picture
            };
        }
        
        public static BLLAppDTOProduct.ProductEdit? MapToEdit(DALAppDTOProduct.ProductEdit? product)
        {
            if (product == null)
            {
                return null;
            }
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Price = product.Price,
                Picture = product.Picture,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                ProductsSubCategories = product.ProductsSubCategories?
                    .Select(ProductsSubCategoryMapper.Map)
                    .OrderBy(c => c.Position)
                    .ToList()
            };
        }
        
        public static BLLAppDTOProduct.ProductEdit MapToEditBLL(BLLAppDTOProduct.ProductData product)
        {
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Price = product.Price,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                Picture = product.Picture
            };
        }
        
        public static BLLAppDTOProduct.ProductData MapToBLLData(DALAppDTOProduct.ProductData product)
        {
            return new()
            {
                Id = product.Id,
                MenuCategoryId = product.MenuCategoryId,
                Name = product.Name,
                Price = product.Price,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                Picture = product.Picture
            };
        }
        
        public static BLLAppDTOProduct.ProductClient? MapToBLLClient(DALAppDTOProduct.ProductClient? product)
        {
            if (product == null)
            {
                return null;
            }
            return new()
            {
                Id = product.Id,
                Name = product.Name,
                Price = product.Price,
                Picture = product.Picture,
                Description = product.Description,
                IsAlwaysPurchasable = product.IsAlwaysPurchasable,
                ProductsSubCategories = product.ProductsSubCategories?
                    .Select(ProductsSubCategoryMapper.MapToBLLClient)
                    .ToList()
            };
        }
    }
}