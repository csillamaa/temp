﻿using System.Linq;
using AutoMapper;
using DALAppDTOIdentity = DAL.App.DTO.Identity;
using BLLAppDTOIdentity = BLL.App.DTO.Identity;


namespace BLL.App.DTO.Mappers
{
    public class AppUserMapper:  BaseMapper<BLLAppDTOIdentity.AppUser, DALAppDTOIdentity.AppUser>
    {
        public AppUserMapper(IMapper mapper) : base(mapper)
        {
        }
        public static BLLAppDTOIdentity.AppUserIndex MapToBLL(DALAppDTOIdentity.AppUserIndex user)
        {
            return new()
            {
                FullName = user.FullName,
                Id = user.Id,
                Email = user.Email,
                CompanyName = user.CompanyName,
                WorkRestaurants = user.WorkRestaurants?.Select(RestaurantMapper.MapToBLL),
            };
        }
        public static BLLAppDTOIdentity.AppUserOnlyName MapToBLL(DALAppDTOIdentity.AppUserOnlyName user)
        {
            return new()
            {
                FullName = user.FullName,
                Id = user.Id,
            };
        }
        
        public static BLLAppDTOIdentity.AppUserEdit MapToBLL(DALAppDTOIdentity.AppUserEdit user)
        {
            return new()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                // Address = user.Address,
                CompanyName = user.CompanyName,
                CompanyId = user.CompanyId,
                LockoutEnd = user.LockoutEnd,
            };
        }
        
        public static BLLAppDTOIdentity.AppUserData MapToBLL(DALAppDTOIdentity.AppUserData user)
        {
            return new()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                // Address = user.Address,
                CompanyId = user.CompanyId,
            };
        }
        
        public static DALAppDTOIdentity.AppUserData MapToDal(BLLAppDTOIdentity.AppUserData user)
        {
            return new()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                // Address = user.Address,
                CompanyId = user.CompanyId,
            };
        }
    }
}