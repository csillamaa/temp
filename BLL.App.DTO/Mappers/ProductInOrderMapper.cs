﻿using System.Linq;
using AutoMapper;
using DALAppDTOOrder = DAL.App.DTO.Order;
using BLLAppDTOOrder = BLL.App.DTO.Order;

namespace BLL.App.DTO.Mappers
{
    public class ProductInOrderMapper : BaseMapper<BLLAppDTOOrder.ProductInOrder, DALAppDTOOrder.ProductInOrder>
    {
        public ProductInOrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static BLLAppDTOOrder.ProductInOrderClient MapToBLLClient(DALAppDTOOrder.ProductInOrderClient productInOrder)
        {
            return new()
            {
                Id = productInOrder.Id,
                ProductId = productInOrder.ProductId,
                Amount = productInOrder.Amount,
                OneProductPrice = productInOrder.OneProductPrice,
                Name = productInOrder.Name,
                SubProducts = productInOrder.SubProducts?.Select(SubProductFromProductMapper.MapToBLLClient).ToList()
            };
        }
    }
}