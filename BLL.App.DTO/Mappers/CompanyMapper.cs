﻿using System.Linq;
using AutoMapper;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace BLL.App.DTO.Mappers
{
    public class CompanyMapper: BaseMapper<BLL.App.DTO.Company, DAL.App.DTO.Company>
    {
        public CompanyMapper(IMapper mapper) : base(mapper)
        {
        }
        
        public static BLLAppDTO.CompanyOnlyName MapToBLL(DALAppDTO.CompanyOnlyName company)
        {
            return new()
            {
                Id = company.Id,
                Name = company.Name
            };
        }
        
        public override BLLAppDTO.Company Map(DALAppDTO.Company? company)
        {
            return new()
            {
                Id = company!.Id,
                Name = company.Name,
                DateJoined = company.DateJoined,
                Website = company.Website,
                PhoneNumber = company.PhoneNumber,
                Email = company.Email,
                Restaurants = company.Restaurants?.Select(RestaurantMapper.MapToBLL).ToList()
            };
        }
        
        public override DALAppDTO.Company Map(BLLAppDTO.Company? company)
        {
            return new()
            {
                Id = company!.Id,
                Name = company.Name,
                DateJoined = company.DateJoined,
                Website = company.Website,
                PhoneNumber = company.PhoneNumber,
                Email = company.Email,
                // Restaurants = company.Restaurants?.Select(RestaurantMapper.MapToDal).ToList()
            };
        }
    }
}