﻿using System.Linq;
using AutoMapper;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;

namespace BLL.App.DTO.Mappers
{
    public class ArchivedOrderMapper : BaseMapper<BLLAppDTOOrderArchived.ArchivedOrder, DALAppDTOOrderArchived.ArchivedOrder>
    {
        public ArchivedOrderMapper(IMapper mapper) : base(mapper)
        {
        }

        public static BLLAppDTOOrderArchived.ArchivedOrderClient? MapToBLLOrderClient(DALAppDTOOrderArchived.ArchivedOrderClient? order)
        {
            if (order == null)
            {
                return null;
            }

            return new()
            {
                Id = order.Id,
                SubmitTime = order.SubmitTime,
                CompletedBy = order.CompletedBy,
                RestaurantId = order.RestaurantId,
                RestaurantName = order.RestaurantName,
                CustomerComment = order.CustomerComment,
                OrderRating = order.OrderRating,
                CustomerOrderReviewComment = order.CustomerOrderReviewComment,
                IsSelfPickup = order.IsSelfPickup,
                AmountPaid = order.AmountPaid,
                FoodCost = order.FoodCost,
                ProductsInOrder = order.ProductsInOrder.Select(ArchivedProductInOrderMapper.MapToBLLClient).ToList()
            };
        }
        public static BLLAppDTOOrderArchived.ArchivedOrderWorker MapToBLLOrderWorker(DALAppDTOOrderArchived.ArchivedOrderWorker order)
        {
            return new()
            {
                Id = order.Id,
                SubmitTime = order.SubmitTime,
                ActualCompleteTime = order.ActualCompleteTime,
                EstimatedCompleteTime = order.EstimatedCompleteTime,
                OrderHandedOver = order.OrderHandedOver,
                CustomerComment = order.CustomerComment,
                Comment = order.Comment,
                OrderRating = order.OrderRating,
                CustomerOrderReviewComment = order.CustomerOrderReviewComment,
                AmountPaid = order.AmountPaid,
                FoodCost = order.FoodCost,
                ProductsInOrder = order.ProductsInOrder.Select(ArchivedProductInOrderMapper.MapToBLLClient).ToList(),
                OrderAppUsers = order.OrderAppUsers.Select(AppUserMapper.MapToBLL).ToList()
            };
        }
    }
}