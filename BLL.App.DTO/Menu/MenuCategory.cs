﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO.Menu
{
    public class MenuCategory : MenuCategoryOnlyName
    {
        public int MenuId { get; set; }
        
        // [Display(ResourceType = typeof(Resources.BLL.App.DTO.Menu), Name = "MenuString")]
        // public DTO.Menu.Menu Menu { get; set; } = default!;
        //

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Product), Name = "Products")]
        public List<Product.ProductData>? Products { get; set; }
    }
}