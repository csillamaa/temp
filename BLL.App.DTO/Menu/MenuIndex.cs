﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Restaurant;

namespace BLL.App.DTO.Menu
{
    public class MenuIndex : MenuOnlyName
    {
          [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "CompanyName")]
          public string CompanyName { get; set; } = default!;

          [Display(ResourceType = typeof(Resources.BLL.App.DTO.Menu), Name = "RestaurantsWithMenu")]
          public List<RestaurantOnlyName>? RestaurantsWithTheMenu { get; set; }
    }
}