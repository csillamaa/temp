﻿using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO.Menu
{
    public class MenuData : MenuOnlyName
    {
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "CompanyId")]
        public int CompanyId { get; set; }
    }
}