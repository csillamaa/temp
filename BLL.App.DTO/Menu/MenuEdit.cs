﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO.Menu
{
    public class MenuEdit : MenuData
    {
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "CompanyName")]
        public string CompanyName { get; set; } = default!;
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.MenuCategory), Name = "Categories")]
        public List<MenuCategoryOnlyName>? MenuCategories { get; set; }

        public MenuCategory? CurrentCategory { get; set; }

        public List<MenuOnlyName>? CompanyMenus { get; set; }
    }
}