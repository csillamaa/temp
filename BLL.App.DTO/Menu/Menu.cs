﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Menu
{
    public class Menu : MenuOnlyName
    {
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Menu), Name = "IsTemporary")]
        public bool IsTemporary { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Menu), Name = "TemporaryStart")]
        public DateTime? TemporaryMenuStart { get; set; }
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Menu), Name = "TemporaryEnd")]
        public DateTime? TemporaryMenuEnd { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "CompanyId")]
        public int CompanyId { get; set; }
        //
        // [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "CompanyName")]
        // public Company Company { get; set; } = default!;

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.MenuCategory), Name = "Categories")]
        public List<MenuCategoryOnlyName>? MenuCategories { get; set; }

        public MenuCategory? CurrentCategory { get; set; }

        // [Display(ResourceType = typeof(Resources.BLL.App.DTO.Menu), Name = "RestaurantsWithMenu")]
        // public ICollection<MenuInRestaurant>? RestaurantsWithTheMenu { get; set; }

        // public int? CurrentCategoryId { get; set; }
    }
}