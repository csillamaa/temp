﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Identity;
using BLL.App.DTO.Restaurant;
using Domain.Base;

namespace BLL.App.DTO
{
    public class Company : CompanyOnlyName
    {
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "DateJoined")]
        public DateTime DateJoined { get; set; }
        
        [MaxLength(60, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Company), Name = "WebSite")]
        public string? Website { get; set; }

        [MaxLength(20, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "PhoneNumber")]

        public string PhoneNumber { get; set; } = default!;

        [MaxLength(60, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [MinLength(3, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MinLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Email")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public string Email { get; set; } = default!;

        public ICollection<RestaurantOnlyName>? Restaurants { get; set; }
        public ICollection<AppUserCompany>? CompanyUsers { get; set; }
        // public ICollection<Menu>? Menus { get; set; }
        
    }
}