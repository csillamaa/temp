﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Product
{
    public class ProductsSubCategory : ProductsSubCategoryData
    {

        // [Display(ResourceType = typeof(Resources.BLL.App.DTO.Product), Name = "ProductString")]
        // public Product Product{ get; set; } = default!;
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Position")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public int Position { get; set; }

        public List<ProductsSubCategoryProduct>? SubCategoryProducts { get; set; }
    }
}