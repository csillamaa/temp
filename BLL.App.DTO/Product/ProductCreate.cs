using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Menu;

namespace BLL.App.DTO.Product
{
    public class ProductCreate : ProductData
    {
        // TODO: do i need it?
        public int MenuId { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Product), Name = "MenuCategory")]
        public List<MenuCategoryOnlyName> MenuCategories { get; set; } = default!;
    }
}