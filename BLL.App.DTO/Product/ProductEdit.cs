﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Menu;

namespace BLL.App.DTO.Product
{
    public class ProductEdit : ProductData
    {
        public int MenuId { get; set; }
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.MenuCategory), Name = "CategoryString")]
        public List<MenuCategoryOnlyName>? MenuCategories { get; set; }

        public List<ProductsSubCategory>? ProductsSubCategories { get; set; }

    }
}