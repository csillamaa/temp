﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Product
{
    public class ProductsSubCategoryData: DomainEntity
    {
        public int ProductId { get; set; }

        [MinLength(1, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MinLength")]
        [MaxLength(60, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Name")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public string Name { get; set; } = default!;
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ProductsSubCategory), Name = "MandatoryItemCount")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public int MandatoryItemCount { get; set; }
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ProductsSubCategory), Name = "MaxItemCount")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public int MaxItemCount { get; set; }

        // [Display(ResourceType = typeof(Resources.BLL.App.DTO.ProductsSubCategory), Name = "DefaultSubProductPosition")]
        // public int[]? DefaultSelectedSubProductsIds { get; set; }
        
    }
}