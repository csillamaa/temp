﻿using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Product
{
    public class ProductsSubCategoryProduct : DomainEntity
    {
        public int ProductsSubCategoryId { get; set; }
        
        // [Display(ResourceType = typeof(Resources.BLL.App.DTO.MenuCategory), Name = "CategoryString")]
        // [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
        //     ErrorMessageResourceName = "ErrorMessage_Required")]
        // public ProductsSubCategory ProductsSubCategory { get; set; } = default!;

        [MinLength(1, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MinLength")]
        [MaxLength(50, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Name")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public string Name { get; set; } = default!;

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Price")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public double Price { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Position")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public int Position { get; set; }
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ProductsSubCategoryProduct), Name = "IsSelectedOnDefault")]
        public bool IsSelectedOnDefault { get; set; }

        [MaxLength(250, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Description")]
        public string? Description { get; set; }
    }
}