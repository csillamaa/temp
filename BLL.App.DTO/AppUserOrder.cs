﻿using BLL.App.DTO.Identity;
using Contracts.Domain.Base;
using Domain.Base;

namespace BLL.App.DTO
{
    public class AppUserOrder: DomainEntity, IDomainAppUserId
    {
        public int OrderId { get; set; }
        // public Order.Order Order { get; set; } = default!;

        public int AppUserId { get; set; }
        // public AppUser AppUser { get; set; } = default!;
    }
}