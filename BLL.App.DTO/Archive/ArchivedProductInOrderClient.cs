﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Archive
{
    public class ArchivedProductInOrderClient: DomainEntity
    {
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Amount")]
        public int Amount { get; set; }

        public string Name { get; set; } = default!;
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Order), Name = "OneProductPrice")]
        public double OneProductPrice { get; set; }

        public int? ProductId { get; set; }
        // public Product.Product? Product { get; set; }

        // public int OrderId { get; set; }
        // public DTO.Order.Order Order { get; set; } = default!;
        public List<ArchivedSubProductFromProductClient>? SubProducts { get; set; }
    }
}