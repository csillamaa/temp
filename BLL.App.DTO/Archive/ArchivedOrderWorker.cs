﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Identity;
using Domain.Base;

namespace BLL.App.DTO.Archive
{
    public class ArchivedOrderWorker : DomainEntity
    {
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ArchivedOrder), Name = "SubmitTime")]
        public DateTime SubmitTime { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ArchivedOrder), Name = "EstimatedCompleteTime")]
        public DateTime EstimatedCompleteTime { get; set; }
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ArchivedOrder), Name = "ActualCompleteTime")]
        public DateTime ActualCompleteTime { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ArchivedOrder), Name = "OrderHandedOver")]
        public DateTime OrderHandedOver { get; set; }

        // public bool IsProcessed { get; set; }
        // public bool IsSelfPickup { get; set; }

        // public double DeliveryCost { get; set; }

        public double FoodCost { get; set; }
        
        // public bool IsReady { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Amount")]
        public double AmountPaid { get; set; }
        
        [MaxLength(500, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ArchivedOrder), Name = "CustomerComment")]
        public string? CustomerComment { get; set; }
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ArchivedOrder), Name = "OrderRating")]
        public int? OrderRating { get; set; }
        
        [MaxLength(350, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ArchivedOrder), Name = "CustomerOrderReviewComment")]
        public string? CustomerOrderReviewComment { get; set; } = default!;
        
        [MaxLength(250, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ArchivedOrder), Name = "Comment")]
        public string? Comment { get; set; } = default!;

        // public int RestaurantId { get; set; }
        // public Restaurant.Restaurant Restaurant { get; set; } = default!;

        public ICollection<ArchivedProductInOrderClient> ProductsInOrder { get; set; } = default!;
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Order), Name = "OrderAppUsers")]
        public ICollection<AppUserOnlyName> OrderAppUsers { get; set; } = default!;
    }
}