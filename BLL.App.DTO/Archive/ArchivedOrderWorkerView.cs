﻿using System.Collections.Generic;
using BLL.App.DTO.Order;

namespace BLL.App.DTO.Archive
{
    public class ArchivedOrderWorkerView
    {
        public int RestaurantId { get; set; }

        public string RestaurantName { get; set; } = default!;

        public ICollection<ArchivedOrderWorker>? Orders { get; set; }
    }
}