﻿using Contracts.Domain.Base;
using Domain.Base;

namespace BLL.App.DTO.Archive
{
    public class ArchivedAppUserOrder: DomainEntity, IDomainAppUserId
    {
        public int ArchivedOrderId { get; set; }
        // public ArchivedOrder ArchivedOrder { get; set; } = default!;

        public int AppUserId { get; set; }
        // public AppUser AppUser { get; set; } = default!;
    }
}