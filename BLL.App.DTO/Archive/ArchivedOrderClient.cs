﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Archive
{
    public class ArchivedOrderClient : DomainEntity
    {
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ArchivedOrder), Name = "SubmitTime")]
        public DateTime SubmitTime { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ArchivedOrder), Name = "SubmitTime")]
        public DateTime CompletedBy { get; set; }
        //
        // public DateTime OrderHandedOver { get; set; }
        //
        // public DateTime InitialCompleteTimeEstimate { get; set; }
        //
        // public bool IsProcessed { get; set; }
        public bool IsSelfPickup { get; set; }
        public bool IsReady { get; set; }


        // public double DeliveryCost { get; set; }

        public double FoodCost { get; set; }
        
        public double AmountPaid { get; set; }
        
        [MaxLength(500, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ArchivedOrder), Name = "CustomerComment")]
        public string? CustomerComment { get; set; }
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ArchivedOrder), Name = "OrderRating")]
        public int? OrderRating { get; set; }
        
        [MaxLength(350, ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_MaxLength")]
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ArchivedOrder), Name = "CustomerOrderReviewComment")]
        public string? CustomerOrderReviewComment { get; set; }
        public string RestaurantName { get; set; } = default!;
        public int RestaurantId { get; set; }
        
        // public Restaurant.Restaurant Restaurant { get; set; } = default!;

        public ICollection<ArchivedProductInOrderClient> ProductsInOrder { get; set; } = default!;
        
        // public ICollection<AppUserOrder> OrderAppUsers { get; set; } = default!;
    }
}