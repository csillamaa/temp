﻿using BLL.App.DTO.Product;
using Domain.Base;

namespace BLL.App.DTO.Archive
{
    public class ArchivedSubProductFromProduct : DomainEntity
    {
        public int ProductsSubCategoryProductId { get; set; }
        public ProductsSubCategoryProduct ProductsSubCategoryProduct { get; set; } = default!;

        public int ArchivedProductInOrderId { get; set; }
        public ArchivedProductInOrder ArchivedProductInOrder { get; set; } = default!;

        public int Amount { get; set; }
    }
}