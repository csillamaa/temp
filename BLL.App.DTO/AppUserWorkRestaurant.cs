﻿using BLL.App.DTO.Identity;
using Contracts.Domain.Base;
using Domain.Base;

namespace BLL.App.DTO
{
    public class AppUserWorkRestaurant: DomainEntity, IDomainAppUserId
    {
        public int RestaurantId { get; set; }

        public int AppUserId { get; set; }
    }
}