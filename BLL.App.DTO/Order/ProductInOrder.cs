﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Order
{
    public class ProductInOrder : DomainEntity
    {
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Amount")]
        public int Amount { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.ProductInOrder), Name = "OneProductPrice")]
        public double OneProductPrice { get; set; }

        public int? ProductId { get; set; }
        // public Product.Product? Product { get; set; }

        public int OrderId { get; set; }
        // public DTO.Order.Order Order { get; set; } = default!;
        public ICollection<SubProductFromProduct>? SubProducts { get; set; }
    }
}