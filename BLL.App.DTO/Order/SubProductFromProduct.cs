﻿using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Product;
using Domain.Base;

namespace BLL.App.DTO.Order
{
    public class SubProductFromProduct : DomainEntity
    {
        public int? ProductsSubCategoryProductId { get; set; }
        public ProductsSubCategoryProduct? ProductsSubCategoryProduct { get; set; } = default!;

        public int ProductInOrderId { get; set; }
        public ProductInOrder ProductInOrder { get; set; } = default!;

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Common), Name = "Amount")]
        [Required(ErrorMessageResourceType = typeof(com.akaver.Base.Resources.Common),
            ErrorMessageResourceName = "ErrorMessage_Required")]
        public int Amount { get; set; }
    }
}