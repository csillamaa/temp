﻿using System.Collections.Generic;

namespace BLL.App.DTO.Order
{
    public class OrderWorkerView
    {
        public int RestaurantId { get; set; }

        public string RestaurantName { get; set; } = default!;

        public ICollection<OrderWorker>? Orders { get; set; }
    }
}