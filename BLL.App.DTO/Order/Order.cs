﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace BLL.App.DTO.Order
{
    public class Order : DomainEntity
    {
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Order), Name = "SubmitTime")]
        public DateTime SubmitTime { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Order), Name = "CompletedBy")]
        public DateTime CompletedBy { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Order), Name = "OrderHandedOver")]
        public DateTime OrderHandedOver { get; set; }
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Order), Name = "InitialTimeEstimate")]
        public DateTime InitialCompleteTimeEstimate { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Order), Name = "IsProcessed")]
        public bool IsProcessed { get; set; }
        
        public bool IsReady { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Order), Name = "IsSelfPickUp")]
        public bool IsSelfPickup { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Order), Name = "DeliveryCost")]
        public double DeliveryCost { get; set; }

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Order), Name = "FoodCost")]
        public double FoodCost { get; set; }
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Order), Name = "AmountPaid")]
        public double AmountPaid { get; set; }
        
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Order), Name = "CustomerComment")]
        [MaxLength(500)]
        public string? CustomerComment { get; set; }

        public int RestaurantId { get; set; }
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Restaurant), Name = "RestaurantString")]
        public Restaurant.Restaurant Restaurant { get; set; } = default!;

        [Display(ResourceType = typeof(Resources.BLL.App.DTO.Product), Name = "Products")]
        public ICollection<ProductInOrder> ProductsInOrder { get; set; } = default!;
        [Display(ResourceType = typeof(Resources.BLL.App.DTO.AppUser), Name = "Users")]
        public ICollection<AppUserOrder> OrderAppUsers { get; set; } = default!;
    }
}