﻿using AutoMapper;
using BLL.App.DTO.Archive;
using BLL.App.DTO.Menu;
using BLL.App.DTO.Order;
using DAL.App.DTO.Product;
using Microsoft.AspNetCore.Authentication;
using ProductsSubCategory = BLL.App.DTO.Product.ProductsSubCategory;

namespace BLL.App.DTO.MappingProfiles
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<BLL.App.DTO.Archive.ArchivedOrder, ArchivedOrder>().ReverseMap();
            CreateMap<BLL.App.DTO.Archive.ArchivedAppUserOrder, ArchivedAppUserOrder>().ReverseMap();
            CreateMap<BLL.App.DTO.Archive.ArchivedProductInOrder, ArchivedProductInOrder>().ReverseMap();
            CreateMap<BLL.App.DTO.Archive.ArchivedSubProductFromProduct, ArchivedSubProductFromProduct>().ReverseMap();
            
            CreateMap<BLL.App.DTO.Identity.AppUser, DAL.App.DTO.Identity.AppUser>().ReverseMap();
            CreateMap<BLL.App.DTO.Identity.AppRole, Domain.App.Identity.AppRole>().ReverseMap();
            
            CreateMap<BLL.App.DTO.AppUserFavoriteRestaurant, DAL.App.DTO.AppUserFavoriteRestaurant>().ReverseMap();
            CreateMap<BLL.App.DTO.AppUserOrder, DAL.App.DTO.AppUserOrder>().ReverseMap();
            CreateMap<BLL.App.DTO.AppUserWorkRestaurant, DAL.App.DTO.AppUserWorkRestaurant>().ReverseMap();
            CreateMap<BLL.App.DTO.BankCard, DAL.App.DTO.BankCard>().ReverseMap();
            CreateMap<BLL.App.DTO.Company, DAL.App.DTO.Company>().ReverseMap();
            CreateMap<BLL.App.DTO.Menu.Menu, DAL.App.DTO.Menu.Menu>().ReverseMap();
            CreateMap<MenuCategory, DAL.App.DTO.Menu.MenuCategory>().ReverseMap();
            CreateMap<MenuInRestaurant, DAL.App.DTO.Menu.MenuInRestaurant>().ReverseMap();
            CreateMap<Order.Order, DAL.App.DTO.Order.Order>().ReverseMap();
            CreateMap<BLL.App.DTO.Product.Product, DAL.App.DTO.Product.Product>().ReverseMap();
            CreateMap<BLL.App.DTO.Order.ProductInOrder, DAL.App.DTO.Order.ProductInOrder>().ReverseMap();
            CreateMap<ProductsSubCategory, DAL.App.DTO.Product.ProductsSubCategory>().ReverseMap();
            CreateMap<Restaurant.Restaurant, DAL.App.DTO.Restaurant.Restaurant>().ReverseMap();
            CreateMap<BLL.App.DTO.Product.ProductsSubCategoryProduct, ProductsSubCategoryProduct>().ReverseMap();
            CreateMap<SubProductFromProduct, DAL.App.DTO.Order.SubProductFromProduct>().ReverseMap();
            CreateMap<BLL.App.DTO.Time, Time>().ReverseMap();
        }
    }
}