﻿using System;
using System.ComponentModel.DataAnnotations;
using Contracts.Domain.Base;
using Domain.Base;

namespace BLL.App.DTO
{
    public class Time: DomainEntity
    {
        // public int StartingDayOfWeek { get; set; }
        // public DateTime StartingTime { get; set; }
        //
        // public int EndingDayOfWeek { get; set; }
        // public DateTime EndingTime { get; set; }
        
       
        
        public int DayOfWeek { get; set; }
        
        [DataType(DataType.Time)]
        public DateTime StartingTime { get; set; }

        [DataType(DataType.Time)]
        public DateTime EndingTime { get; set; }
        //
        // public int? RestaurantId { get; set; }
        // public Domain.App.Restaurant? Restaurant { get; set; }
        //
        // public int? ProductId { get; set; }
        // public Product? Product { get; set; }
          
    }
}