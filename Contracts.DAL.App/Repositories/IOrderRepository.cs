﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTOOrder = DAL.App.DTO.Order;
using BLLAppDTOOrder = BLL.App.DTO.Order;
using DALAppDTOProduct = DAL.App.DTO.Product;

namespace Contracts.DAL.App.Repositories
{
    public interface IOrderRepository : IBaseRepository<DALAppDTOOrder.Order>, IOrderRepositoryCustom<DALAppDTOOrder.Order>
    {
      
    }

    public interface IOrderRepositoryCustom<TEntity>
    {
        public Task<ICollection<DALAppDTOOrder.OrderWorker>?> GetAllRestaurantActiveOrdersAsync(int restaurantId, bool isReady, bool noTracking = true);
        public Task<ICollection<BLLAppDTOOrder.OrderWorker>?> GetAllRestaurantActiveOrdersAsyncBLL(int restaurantId, bool isReady, bool noTracking = true);

        public Task<DALAppDTOOrder.OrderClient?> FirstOrDefaultClientAsync(int userId, bool noTracking = true);
        public Task<BLLAppDTOOrder.OrderClient?> FirstOrDefaultClientAsyncBLL(int userId, bool noTracking = true);

        Task<List<DALAppDTOOrder.OrderClient>?> GetAllActiveClientAsync(int userId, bool noTracking = true);

        Task<List<BLLAppDTOOrder.OrderClient>?> GetAllActiveClientAsyncBLL(int userId, bool noTracking = true);

        Task<bool> AddCustomerCommentToOrder(int userId, int orderId, string comment, bool noTracking = true);

        Task<bool> DoesOrderBelongToCustomer(int userId, int orderId, bool noTracking = true);

        Task<bool> SubmitOrder(int orderId, string? cardNumber, bool noTracking = true);

        Task<bool> DeleteIfOrderIsEmpty(int orderId, bool noTracking = true);

        Task<bool> ReCalculateOrderCost(int orderId, bool noTracking = true);
        
        Task<bool> ChangeOrderCompleteTime(int orderId, DateTime newCompleteTime, bool noTracking = true);

        Task<int?> GetRestaurantId(int orderId, bool noTracking = true);
        Task<bool> ChangeOrderToReady(int orderId, bool noTracking = true);
        Task<int?> GetCompanyId(int orderId, bool noTracking = true);
        
        public Task<bool> ArchiveOrder(int orderId, bool noTracking = true);

        Task<bool> IsUserBasketEmptyOrFromSameRestaurant(int userId, int restaurantId, bool noTracking = true);

        Task<bool> AddProductToOrder(int userBasketId, int userId, DALAppDTOProduct.ProductOnlyPrice product, int restaurantId, int amount,
            IEnumerable<DALAppDTOProduct.ProductsSubCategoryProductOnlyPrice> allSubProductsWithIdAndPrice, bool noTracking = true);

        Task<int> GetUserBasketId(int userId, bool noTracking = true);

        Task<bool> DeleteOrderIfNewProductFromDifferentRestaurant(int userBasketId, int restaurantId, bool noTracking = true);
    }
}