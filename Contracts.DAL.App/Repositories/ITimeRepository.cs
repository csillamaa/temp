﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTO = DAL.App.DTO;
using BLLAppDTO = BLL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface ITimeRepository: IBaseRepository<DALAppDTO.Time>,
        ITimeRepositoryCustom<DALAppDTO.Time>
    {
    }

    public interface ITimeRepositoryCustom<TEntity>
    {
        Task<List<TEntity>?> GetAllRestaurantOpenTimes(int restaurantId, bool noTracking = true);
        Task<bool> SaveNewTime(int restaurantId, TEntity time);

        Task<bool> IsRestaurantOpen(int restaurantId);
    }
}