﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DAL.App.DTO.Restaurant;
using DALAppDTO = DAL.App.DTO;
using BLLAppDTO = BLL.App.DTO;


namespace Contracts.DAL.App.Repositories
{
    public interface IRestaurantRepository : IBaseRepository<DALAppDTO.Restaurant.Restaurant>,
        IRestaurantRepositoryCustom<DALAppDTO.Restaurant.Restaurant>
    {

    }

    public interface IRestaurantRepositoryCustom<TEntity>
    {
        Task<IEnumerable<DALAppDTO.Restaurant.RestaurantIndex>> GetAllIndexAsync(bool noTracking = true);

        Task<IEnumerable<BLLAppDTO.Restaurant.RestaurantIndex>> GetAllIndexAsyncBLL(bool noTracking = true);

        // Task<IEnumerable<TEntity>> GetAllAppUserAvailableAsync(int userId = default, bool noTracking = true);

        Task<IList<DALAppDTO.Restaurant.RestaurantOnlyName>?> GetAllRestaurantsAppUserWorksInAsync(int userId, bool noTracking = true);
        
        Task<IList<BLLAppDTO.Restaurant.RestaurantOnlyName>?> GetAllRestaurantsAppUserWorksInAsyncBLL(int userId, bool noTracking = true);

        Task<ICollection<DALAppDTO.Restaurant.RestaurantOnlyName>?> GetAllRestaurantsAddableToUser(int companyId, int userId, bool noTracking = true);
        Task<ICollection<BLLAppDTO.Restaurant.RestaurantOnlyName>?> GetAllRestaurantsAddableToUserBLL(int companyId, int userId, bool noTracking = true);
        Task<IEnumerable<DALAppDTO.Restaurant.RestaurantIndex>?> GetAllCompanyRestaurants(int companyId, bool noTracking = true);
        Task<IEnumerable<BLLAppDTO.Restaurant.RestaurantIndex>?> GetAllCompanyRestaurantsBLL(int companyId, bool noTracking = true);

        Task<bool> IsRestaurantOwnedByCompany(int restaurantId, int companyId, bool noTracking = true);

        
        Task<DALAppDTO.Restaurant.RestaurantEdit?> FirstOrDefaultEditAsync(int id, bool noTracking = true);
        Task<BLLAppDTO.Restaurant.RestaurantEdit?> FirstOrDefaultEditAsyncBLL(int id, bool noTracking = true);


        Task<int> GetRestaurantCompanyId(int restaurantId, bool noTracking = true);

        bool IsRestaurantOpen(DALAppDTO.Time? restaurantOpenTime);

        Task<TEntity?> FirstOrDefaultDeleteAsync(int id, bool noTracking = true);

        Task<List<RestaurantClientAll>> GetAllClientAsync(bool noTracking = true);
        Task<List<BLLAppDTO.Restaurant.RestaurantClientAll>> GetAllClientAsyncBLL(bool noTracking = true);
        
        Task<List<RestaurantClientAll>?> GetAllAppUserFavouriteRestaurants(int[]? userFavoriteRestaurantIds, bool noTracking = true);
        Task<List<BLLAppDTO.Restaurant.RestaurantClientAll>?> GetAllAppUserFavouriteRestaurantsBLL(int[]? userFavoriteRestaurantIds, bool noTracking = true);

        Task<RestaurantClient?> FirstOrDefaultClientAsync(int id, int? currentCategoryPosition = 0,
            bool noTracking = true);
        
        Task<BLLAppDTO.Restaurant.RestaurantClient?> FirstOrDefaultClientAsyncBLL(int id, int? currentCategoryPosition = 0,
            bool noTracking = true);

        Task<string?> GetRestaurantName(int id, bool noTracking = true);

        Task<bool> IsRestaurantOpenById(int restaurantId, bool noTracking = true);

        Task<bool> IsProductInRestaurantMenu(int productId, int restaurantId, bool noTracking = true);
    }
}