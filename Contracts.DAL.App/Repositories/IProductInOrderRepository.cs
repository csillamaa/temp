﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTOOrder = DAL.App.DTO.Order;
using BLLAppDTOOrder = BLL.App.DTO.Order;

namespace Contracts.DAL.App.Repositories
{
    public interface IProductInOrderRepository : IBaseRepository<DALAppDTOOrder.ProductInOrder>, IProductInOrderRepositoryCustom<DALAppDTOOrder.ProductInOrder>
    {
      
    }

    public interface IProductInOrderRepositoryCustom<TEntity>
    {
        Task<bool> ChangeProductInOrderAmount(int orderId, int productInOrderId, int newAmount, bool noTracking = true);

        Task<int> GetOrderId(int id, bool noTracking = true);
        
        Task<bool> AddAmountToProductIfSameOneAlreadyInOrder(int userBasketId, int productId, int amount, int[] allSubProductIds, bool noTracking = true);

    }
}