﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTOMenu = DAL.App.DTO.Menu;
using BLLAppDTOMenu = BLL.App.DTO.Menu;

namespace Contracts.DAL.App.Repositories
{
    public interface IMenuRepository: IBaseRepository<DALAppDTOMenu.Menu>,
        IMenuRepositoryCustom<DALAppDTOMenu.Menu>
    {
    }

    public interface IMenuRepositoryCustom<TEntity>
    {
        Task<DALAppDTOMenu.MenuEdit?> FirstOrDefaultEditAsync(int menuId, int? categoryPicked, bool noTracking = true);
        Task<BLLAppDTOMenu.MenuEdit?> FirstOrDefaultEditAsyncBLL(int menuId, int? categoryPicked,bool noTracking = true);
        Task<IEnumerable<DALAppDTOMenu.MenuIndex>> GetAllIndexASync(int? companyId, bool noTracking = true);
        Task<IEnumerable<BLLAppDTOMenu.MenuIndex>> GetAllIndexASyncBLL(int? companyId, bool noTracking = true);

        Task<List<DALAppDTOMenu.MenuOnlyName>?> GetAllUnPickedCompanyMenus(int companyId, int restaurantId, bool noTracking = true);
        Task<List<BLLAppDTOMenu.MenuOnlyName>?> GetAllUnpickedCompanyMenusBLL(int companyId, int restaurantId, bool noTracking = true);

        Task<List<DALAppDTOMenu.MenuOnlyName>?> GetAllOtherCompanyMenus(int companyId, int menuId, bool noTracking = true);
        Task<List<BLLAppDTOMenu.MenuOnlyName>?> GetAllOtherCompanyMenusBLL(int companyId, int menuId, bool noTracking = true);
        
        Task<int> GetMenuCompanyId(int menuId, bool noTracking = true);

        Task<int> CopyMenuItemsToOtherMenu(int currentMenuId, int otherMenuId);
    }
}