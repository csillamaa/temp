﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DAL.App.DTO;
using DAL.App.DTO.Menu;
using BLLAppDTO = BLL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IMenuCategoryRepository : IBaseRepository<MenuCategory>, IMenuCategoryRepositoryCustom<MenuCategory>
    {
      
    }

    public interface IMenuCategoryRepositoryCustom<TEntity>
    {
        Task<List<TEntity>?> RemoveAllMenuCategories(int menuId);

        Task<MenuCategoryData?> FirstOrDefaultEditAsync(int id);
        Task<BLLAppDTO.Menu.MenuCategoryData?> FirstOrDefaultEditAsyncBLL(int id);

        // MenuCategory Add(MenuCategoryData category);
        // BLLAppDTO.Menu.MenuCategory Add(BLLAppDTO.Menu.MenuCategoryData category);

        int GetCategoryCountInMenu(int menuId);

        Task<bool> ChangeCategoryPosition(int id, bool moveUp);

        Task<int> GetCategoryCompanyId(int id);
        Task<int> GetCategoryMenuId(int id);

        Task<int> MoveCategoriesUpWhenDelete(int menuCategoryId);

        Task<List<MenuCategoryOnlyName>> GetAllMenuCategoriesOnlyName(int menuId, bool noTracking = true);
        Task<List<BLLAppDTO.Menu.MenuCategoryOnlyName>> GetAllMenuCategoriesOnlyNameBLL(int menuId, bool noTracking = true);
    }
}