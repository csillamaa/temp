﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTOMenu = DAL.App.DTO.Menu;
using BLLAppDTOMenu = BLL.App.DTO.Menu;

namespace Contracts.DAL.App.Repositories
{
    public interface IMenuInRestaurantRepository: IBaseRepository<DALAppDTOMenu.MenuInRestaurant>,
        IMenuInRestaurantRepositoryCustom<DALAppDTOMenu.MenuInRestaurant>
    {
    }

    public interface IMenuInRestaurantRepositoryCustom<TEntity>
    {
        Task<List<TEntity>> RemoveMenuFromAllRestaurants(int menuId);

        Task<TEntity?> RemoveMenuFromRestaurant(TEntity menuInRestaurant, bool noTracking = true);

        Task<bool> IsMenuAddableToRestaurant(TEntity menuInRestaurant, bool noTracking = true);

        Task<int> RemoveAllMenusFromRestaurant(int id, bool noTracking = true);

    }
}