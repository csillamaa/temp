﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;

namespace Contracts.DAL.App.Repositories
{
    public interface IArchivedProductInOrderRepository : IBaseRepository<DALAppDTOOrderArchived.ArchivedProductInOrder>,
        IArchivedProductInOrderRepositoryCustom<DALAppDTOOrderArchived.ArchivedProductInOrder>
    {
      
    }

    public interface IArchivedProductInOrderRepositoryCustom<TEntity>
    {
    }
}