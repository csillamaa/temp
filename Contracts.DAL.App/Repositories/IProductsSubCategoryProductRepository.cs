﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTOProduct = DAL.App.DTO.Product;
using BLLAppDTOProduct = BLL.App.DTO.Product;

namespace Contracts.DAL.App.Repositories
{
    public interface IProductsSubCategoryProductRepository: IBaseRepository<DALAppDTOProduct.ProductsSubCategoryProduct>,
        IProductsSubCategoryProductRepositoryCustom<DALAppDTOProduct.ProductsSubCategoryProduct>
    {
    }

    public interface IProductsSubCategoryProductRepositoryCustom<TEntity>
    {
        Task<int> MoveSubCategoryProductsUpWhenDelete(int subCategoryProductId);
        
        Task<int> GetProductsSubCategoryProductCompanyId(int id);
        
        int GetProductSubCategoryProductCountInSubCategory(int subCategoryId);

        Task<int> GetProductId(int id);

        Task<bool> ChangeSubCategoryProductPosition(int id, bool moveUp);

        Task<DALAppDTOProduct.ProductsSubCategoryProductData?> FirstOrDefaultEditAsync(int id, bool noTracking = true);
        Task<BLLAppDTOProduct.ProductsSubCategoryProductData?> FirstOrDefaultEditAsyncBLL(int id, bool noTracking = true);
        
        bool AreSubProductsFromTheProduct(int productId, int[] subCategoryProducts, bool noTracking = true);

        Task<IEnumerable<DALAppDTOProduct.ProductsSubCategoryProductOnlyPrice>> GetAllPricesById(int[] subProductIds, bool noTracking = true);

        Task<bool> IsThereTooManySubProductsFromACategory(int[] allSubProductIds, bool noTracking = true);
    }
}