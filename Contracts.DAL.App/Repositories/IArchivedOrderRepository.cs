﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DAL.App.DTO.Order;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;

namespace Contracts.DAL.App.Repositories
{
    public interface IArchivedOrderRepository : IBaseRepository<DALAppDTOOrderArchived.ArchivedOrder>, 
        IArchivedOrderRepositoryCustom<DALAppDTOOrderArchived.ArchivedOrder>
    {
      
    }

    public interface IArchivedOrderRepositoryCustom<TEntity>
    {
        // TODO what to do with the order type?
        
        public Task<ICollection<DALAppDTOOrderArchived.ArchivedOrderWorker>?> GetAllRestaurantCompletedOrdersAsync(int restaurantId, bool noTracking = true);
        public Task<ICollection<BLLAppDTOOrderArchived.ArchivedOrderWorker>?> GetAllRestaurantCompletedOrdersAsyncBLL(int restaurantId, bool noTracking = true);
        
        Task<List<DALAppDTOOrderArchived.ArchivedOrderClient>?> GetAllCompletedOrdersClientAsync(int userId, bool noTracking = true);

        Task<List<BLLAppDTOOrderArchived.ArchivedOrderClient>?> GetAllCompletedOrdersClientAsyncBLL(int userId, bool noTracking = true);
        
        Task<bool> AddCommentToOrder(int orderId, string comment, bool isCustomerComment, bool noTracking = true);
        Task<bool> AddRatingToOrder(int orderId, int rating, bool noTracking = true);

        Task<int?> GetCompanyId(int orderId, bool noTracking = true);

        Task<int?> GetRestaurantId(int orderId, bool noTracking = true);
    }
}
