﻿using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTO = DAL.App.DTO;
using BLLAppDTO = BLL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IAppUserFavoriteRestaurantsRepository: IBaseRepository<DALAppDTO.AppUserFavoriteRestaurant>,
        IAppUserFavoriteRestaurantsRepositoryCustom<DALAppDTO.AppUserFavoriteRestaurant>
    {
    }

    public interface IAppUserFavoriteRestaurantsRepositoryCustom<TEntity>
    {
        Task<int> RemoveAllUserFavoriteRestaurants(int userId, bool noTracking = true);

        Task<int[]?> GetAllAppUserFavoriteRestaurants(int? userId, bool noTracking = true);
    }
}