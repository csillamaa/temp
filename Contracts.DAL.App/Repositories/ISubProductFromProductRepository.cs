﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTOOrder = DAL.App.DTO.Order;
using BLLAppDTOOrder = BLL.App.DTO.Order;

namespace Contracts.DAL.App.Repositories
{
    public interface ISubProductFromProductRepository : IBaseRepository<DALAppDTOOrder.SubProductFromProduct>,
        ISubProductFromProductRepositoryCustom<DALAppDTOOrder.SubProductFromProduct>
    {
      
    }

    public interface ISubProductFromProductRepositoryCustom<TEntity>
    {
    }
}