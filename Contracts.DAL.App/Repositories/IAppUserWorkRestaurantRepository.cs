﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IAppUserWorkRestaurantRepository: IBaseRepository<AppUserWorkRestaurant>,
        IAppUserWorkRestaurantRepositoryCustom<AppUserWorkRestaurant>
    {
    }

    public interface IAppUserWorkRestaurantRepositoryCustom<TEntity>
    {
        public Task<IList<TEntity>?> RemoveFromAllRestaurants(int id, bool noTracking = true);
        
        int[]? GetAppUserWorkRestaurantIds(int userId,  bool noTracking = true);
    }
}