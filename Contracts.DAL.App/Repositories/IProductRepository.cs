﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTOProduct = DAL.App.DTO.Product;
using BLLAppDTOProduct = BLL.App.DTO.Product;

namespace Contracts.DAL.App.Repositories
{
    public interface IProductRepository: IBaseRepository<DALAppDTOProduct.Product>,
        IProductRepositoryCustom<DALAppDTOProduct.Product>
    {
    }

    public interface IProductRepositoryCustom<TEntity>
    {
        Task<int> GetProductCompanyId(int id);
        Task<int> GetProductMenuId(int id);
        int GetProductCountInCategory(int categoryId);

        Task<bool> ChangeProductPosition(int id, bool moveUp);

        Task<DALAppDTOProduct.ProductEdit?> FirstOrDefaultEditAsync(int id, bool noTracking = true);
        Task<BLLAppDTOProduct.ProductEdit?> FirstOrDefaultEditAsyncBLL(int id, bool noTracking = true);
        
        Task<DALAppDTOProduct.ProductClient?> FirstOrDefaultClientAsync(int id, bool noTracking = true);
        Task<BLLAppDTOProduct.ProductClient?> FirstOrDefaultClientAsyncBLL(int id, bool noTracking = true);
        Task<int> MoveProductsUpWhenDelete(int productId);

        Task<DALAppDTOProduct.ProductOnlyPrice> GetProductOnlyPriceById(int id, bool noTracking = true);
    }
}