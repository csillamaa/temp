﻿using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTOProduct = DAL.App.DTO.Product;
using BLLAppDTOProduct = BLL.App.DTO.Product;

namespace Contracts.DAL.App.Repositories
{
    public interface IProductsSubCategoryRepository: IBaseRepository<DALAppDTOProduct.ProductsSubCategory>,
        IProductsSubCategoryRepositoryCustom<DALAppDTOProduct.ProductsSubCategory>
    {
    }

    public interface IProductsSubCategoryRepositoryCustom<TEntity>
    {
        Task<DALAppDTOProduct.ProductsSubCategoryData?> FirstOrDefaultEditAsync(int id, bool noTracking = true);
        Task<BLLAppDTOProduct.ProductsSubCategoryData?> FirstOrDefaultEditAsyncBLL(int id, bool noTracking = true);
        int GetProductsSubCategoryCountInProduct(int productId);
        
        Task<int> MoveSubCategoriesUpWhenDelete(int productId);
        
        Task<int> GetSubCategoryCompanyId(int id);
        
        int GetSubCategoryCountInProduct(int productId);

        Task<int> GetProductId(int id);

        Task<bool> ChangeSubCategoryPosition(int id, bool moveUp);
    }
}