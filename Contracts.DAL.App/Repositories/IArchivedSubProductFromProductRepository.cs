﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTOOrderArchived = DAL.App.DTO.Archive;
using BLLAppDTOOrderArchived = BLL.App.DTO.Archive;

namespace Contracts.DAL.App.Repositories
{
    public interface IArchivedSubProductFromProductRepository : IBaseRepository<DALAppDTOOrderArchived.ArchivedSubProductFromProduct>,
        IArchivedSubProductFromProductRepositoryCustom<DALAppDTOOrderArchived.ArchivedSubProductFromProduct>
    {
      
    }

    public interface IArchivedSubProductFromProductRepositoryCustom<TEntity>
    {
    }
}