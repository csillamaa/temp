﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DALAppDTO = DAL.App.DTO;
using BLLAppDTO = BLL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IAppUserRepository:  IBaseRepository<DALAppDTO.Identity.AppUser>,
        IAppUserRepositoryCustom<DALAppDTO.Identity.AppUser>
    {
    }

    public interface IAppUserRepositoryCustom<TEntity>
    {
        Task<IEnumerable<string>> GetUserRoles(int userId);

        Task<IList<DALAppDTO.Identity.AppUserOnlyName>> GetAllUsers();
        Task<IEnumerable<BLLAppDTO.Identity.AppUserOnlyName>> GetAllUsersBLL();
        Task<bool> IsUserCompanyAdmin(int userId);
        
        Task<DALAppDTO.Identity.AppUserEdit?> FirstOrDefaultEditAsync(int userId, bool noTracking = true);
        Task<BLLAppDTO.Identity.AppUserEdit?> FirstOrDefaultEditAsyncBLL(int userId, bool noTracking = true);
        Task<int?> AddUser(DALAppDTO.Identity.AppUserData appUser);
        Task<int?> GetUserCompanyId(int userId);

        Task<int?> AddUserBLL(BLLAppDTO.Identity.AppUserData appUser);
        Task<List<DALAppDTO.Identity.AppUserIndex>> GetAllIndexAsync(int userId, bool noTracking = true);
        Task<ICollection<BLLAppDTO.Identity.AppUserIndex>> GetAllIndexAsyncBLL(int userId, bool noTracking = true);

        Task<bool> RemoveAllUserOrders(int id, bool noTracking = true);

        // Task<bool> RemoveAllCompanyUserRoles(int companyId);

        Task<int[]> GetAllCompanyUserIds(int companyId, bool noTracking = true);
        Task<int> RemoveAllCompanyUsers(int[] userIds, bool noTracking = true);

        Task<bool> AddUserFavoriteRestaurant(int restaurantId, int userId);
        Task<bool> RemoveUserFavoriteRestaurant(int restaurantId, int userId);
    }
}