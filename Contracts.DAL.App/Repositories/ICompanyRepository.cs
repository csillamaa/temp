﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;
using Microsoft.AspNetCore.Identity;

namespace Contracts.DAL.App.Repositories
{
    public interface ICompanyRepository: IBaseRepository<DALAppDTO.Company>, ICompanyRepositoryCustom<DALAppDTO.Company>
    {
    }

    public interface ICompanyRepositoryCustom<TEntity>
    {
         
        Task<IEnumerable<DALAppDTO.CompanyOnlyName>> GetAllJustNameAsync(bool noTracking = true);
        
        Task<IEnumerable<BLLAppDTO.CompanyOnlyName>> GetAllJustNameAsyncBLL(bool noTracking = true);

        Task<TEntity?> FirstOrDefaultDeleteAsync(int id, bool noTracking = true);

    }
}