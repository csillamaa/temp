﻿using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using Contracts.DAL.Base.Repositories;
using DAL.App.DTO;
using DAL.App.DTO.Archive;
using DAL.App.DTO.Menu;
using DAL.App.DTO.Order;

namespace Contracts.DAL.App
{
    public interface IAppUnitOfWork : IBaseUnitOfWork
    {
        
        IAppUserRepository AppUsers { get; }

        IBaseRepository<AppUserOrder> AppUserOrders { get; }
        IBaseRepository<ArchivedAppUserOrder> ArchivedAppUserOrders { get; }
        IAppUserFavoriteRestaurantsRepository AppUserFavoriteRestaurants { get; }
        IAppUserWorkRestaurantRepository AppUserWorkRestaurants { get; }
        IBaseRepository<BankCard> BankCards { get; }

        IArchivedProductInOrderRepository ArchivedProductsInOrder { get; }
        IArchivedSubProductFromProductRepository ArchivedSubProductsFromProduct { get; }
        IArchivedOrderRepository ArchivedOrders { get; }
        ICompanyRepository Companies { get; }
        IMenuRepository Menus { get; }
        IMenuCategoryRepository MenuCategories { get; }
        IMenuInRestaurantRepository MenusInRestaurant{ get; }
        IOrderRepository Orders { get; }
        IProductRepository Products { get; }
        IProductInOrderRepository ProductsInOrder { get; }
        IProductsSubCategoryRepository ProductsSubCategories { get; }
        IRestaurantRepository Restaurants { get; }
        IProductsSubCategoryProductRepository ProductsSubCategoryProducts{ get; }
        ISubProductFromProductRepository SubProductsFromProduct { get; }
        ITimeRepository Times { get; }
    }
}